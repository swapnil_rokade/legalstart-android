package com.adaptingsocial.lawyerapp.app;

import android.app.Application;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.adaptingsocial.lawyerapp.chat.model.model.QbConfigs;
import com.adaptingsocial.lawyerapp.chat.model.model.SampleConfigs;
import com.adaptingsocial.lawyerapp.chat.model.utill.ChatHelper;
import com.adaptingsocial.lawyerapp.chat.model.utill.ConfigUtils;
import com.adaptingsocial.lawyerapp.chat.model.utill.CoreConfigUtils;
import com.adaptingsocial.lawyerapp.chat.model.utill.SharedPrefsHelper;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.ServiceZone;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.messages.services.QBPushManager;
import com.quickblox.users.model.QBUser;

import java.io.IOException;

import static android.content.ContentValues.TAG;

/*************************************************************************************
 * Class Name: Lawyerapp
 * Created By: lbamarnani
 * Date:20-APRIL-17
 * Modified By:
 * Modified Date:
 * Purpose: This class will called
 * before any activity created in android application. We will use this to get
 * global instance of application resource
 *************************************************************************************/
public class Lawyerapp extends Application {
    private static Lawyerapp mInstance = null;
    public final String LOG_TAG = "Lawyerapp";
    private QbConfigs qbConfigs;
    private static SampleConfigs sampleConfigs;
    private static final String QB_CONFIG_DEFAULT_FILE_NAME = "qb_config.json";
    private QBChatDialog dialog;
    private String token;


    /**
     * Called when application start
     */
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        AppLog.showLogD(LOG_TAG, "Application Created");
        initQbConfigs();
        initCredentials();
        token = FirebaseInstanceId.getInstance().getToken();

        PreferenceUtils preferenceUtils = new PreferenceUtils(this);
        preferenceUtils.putString(preferenceUtils.KEY_TOKEN, token);


        if (preferenceUtils.getBoolean(preferenceUtils.KEY_USER_LOGIN)) {
            QBUser currentUser = SharedPrefsHelper.getInstance().getQbUser();
            login(currentUser);
        }
    }

    private void initQbConfigs() {
        Log.e(LOG_TAG, "QB CONFIG FILE NAME: " + getQbConfigFileName());
        qbConfigs = CoreConfigUtils.getCoreConfigsOrNull(getQbConfigFileName());
    }

    protected String getQbConfigFileName() {
        return QB_CONFIG_DEFAULT_FILE_NAME;
    }


    public void initCredentials() {
        if (qbConfigs != null) {
            QBSettings.getInstance().init(getApplicationContext(), qbConfigs.getAppId(), qbConfigs.getAuthKey(), qbConfigs.getAuthSecret());
            QBSettings.getInstance().setAccountKey(qbConfigs.getAccountKey());

            if (!TextUtils.isEmpty(qbConfigs.getApiDomain()) && !TextUtils.isEmpty(qbConfigs.getChatDomain())) {
                QBSettings.getInstance().setEndpoints(qbConfigs.getApiDomain(), qbConfigs
                        .getChatDomain(), ServiceZone.DEVELOPMENT);
                QBSettings.getInstance().setZone(ServiceZone.DEVELOPMENT);
            }
        }
    }

    public void setmInstance(Lawyerapp mInstance) {
        this.mInstance = mInstance;
    }

    public static Lawyerapp getmInstance() {
        return mInstance;
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        if (mInstance != null) {
            mInstance = null;
        }
    }

    public static synchronized Lawyerapp getInstance() {
        return mInstance;
    }

    private void initSampleConfigs() {
        try {
            sampleConfigs = ConfigUtils.getSampleConfigs(Constants.SAMPLE_CONFIG_FILE_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static SampleConfigs getSampleConfigs() {
        return sampleConfigs;
    }


    public void setdialog(QBChatDialog dialog) {
        this.dialog = dialog;
    }

    public QBChatDialog getDialog() {
        return dialog;
    }

    private void initPushManager() {
        QBPushManager.getInstance().addListener(new QBPushManager.QBSubscribeListener() {
            @Override
            public void onSubscriptionCreated() {
                Log.d(TAG, "SubscriptionCreated");
                Toast.makeText(Lawyerapp.this, "HELLLO   SUCESS", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSubscriptionError(Exception e, int resultCode) {
                Log.d(TAG, "SubscriptionError" + e.getLocalizedMessage());
                Toast.makeText(Lawyerapp.this, "HELLLO   ERROR", Toast.LENGTH_SHORT).show();
                if (resultCode >= 0) {
                    String error = GoogleApiAvailability.getInstance().getErrorString(resultCode);
                    Log.d(TAG, "SubscriptionError playServicesAbility: " + error);
                }
            }

            @Override
            public void onSubscriptionDeleted(boolean success) {

            }
        });
    }

    private void login(final QBUser user) {
        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Log.d("Result", "Log in sucess");
                loginToChat(user);

            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("Result", "Log in error");
                login(user);

            }


        });
    }

    private void loginToChat(final QBUser user) {
        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Log.e(TAG, "Chat login onSuccess()");

            }

            @Override
            public void onError(QBResponseException e) {
                Log.e(TAG, "Chat login onError(): " + e);
                loginToChat(user);
            }
        });
    }
}

