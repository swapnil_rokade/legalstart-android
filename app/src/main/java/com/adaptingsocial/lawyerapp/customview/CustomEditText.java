package com.adaptingsocial.lawyerapp.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import com.adaptingsocial.lawyerapp.R;

public class CustomEditText extends AppCompatEditText {
    private Typeface fontType;
    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode())
            return;
        final TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomEditTextView);
        String fontName = ta.getString(R.styleable.CustomEditTextView_font_name_edit);
        final Typeface font = Typeface.createFromAsset(context.getAssets(), "font/"+fontName );
        setTypeface(font);
        ta.recycle();
    }
}
