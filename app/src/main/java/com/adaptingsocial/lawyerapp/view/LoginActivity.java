package com.adaptingsocial.lawyerapp.view;


import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.VideoView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.app.Lawyerapp;
import com.adaptingsocial.lawyerapp.chat.model.utill.ChatHelper;
import com.adaptingsocial.lawyerapp.chat.model.utill.PREF;
import com.adaptingsocial.lawyerapp.chat.model.utill.SharedPrefsHelper;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WSLogin;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.yqritc.scalablevideoview.ScalableType;
import com.yqritc.scalablevideoview.ScalableVideoView;

import java.io.IOException;

public class LoginActivity extends BaseActivity {
    private EditText etEmail;
    private EditText etPassword;
    private TextView tvForgotPassword;
    private TextView tvSignIn;
    private TextView tvCreateNewAccount;
    private long mLastClickTime = 0;
    private AsyncLogin asyncGetLogin;
    private ProgressDialog progressDialog;
    private QBChatDialog qbChatDialog;
    private Lawyerapp lawyerapp;
    private ScalableVideoView videoView;
    final String TAG = this.getClass().getSimpleName();
    private int stopPosition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initializeComponents();
    }

    protected void initializeComponents() {
        lawyerapp = (Lawyerapp) this.getApplicationContext();
        etEmail = (EditText) findViewById(R.id.activity_login_edt_email);
        etPassword = (EditText) findViewById(R.id.activity_login_edt_password);
        tvCreateNewAccount = (TextView) findViewById(R.id.activity_login_tv_create_new_acc);
        tvForgotPassword = (TextView) findViewById(R.id.activity_login_tv_forgot_password);
        tvSignIn = (TextView) findViewById(R.id.activity_login_tv_signin);
        videoView = (ScalableVideoView) findViewById(R.id.activity_login_vv_splash);
        tvSignIn.setOnClickListener(LoginActivity.this);
        tvForgotPassword.setOnClickListener(LoginActivity.this);
        tvCreateNewAccount.setOnClickListener(LoginActivity.this);
        playVideo(0);
    }

    private void playVideo(int position) {
//        final Uri path4 = Uri.parse("android.resource://" + LoginActivity.this.getPackageName()
//                + "/" + R.raw.city);
//        videoView.setVideoURI(path4);
//        videoView.seekTo(position);
//        videoView.start();
//        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//
//            @Override
//            public void onCompletion(MediaPlayer mp) {
//                videoView.seekTo(0);
//                videoView.start();
//            }
//        });


        //test code

        try {
            videoView.setRawData(R.raw.city);
            videoView.setScalableType(ScalableType.CENTER_CROP);
            videoView.setVolume(0, 0);
            videoView.setLooping(true);
            videoView.prepare(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    videoView.start();
                }
            });
        } catch (IOException ioe) {
            //ignore
        }

    }
    @Override
    protected void onPause() {
        super.onPause();
        stopPosition = videoView.getCurrentPosition(); //stopPosition is an int
        videoView.pause();
        AppLog.showLogD(TAG, "ON PAUSE CALLED");
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.start();
        AppLog.showLogD(TAG, "ON RESUME CALLED");

    }



    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(LoginActivity.this, view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        final int id = view.getId();
        switch (id) {
            case R.id.activity_login_tv_signin:
                validation();
                break;
            case R.id.activity_login_tv_forgot_password:
                openForgotPasswordScreen();
                break;
            case R.id.activity_login_tv_create_new_acc:
                openRegistrationScreen();
                break;

        }

    }

    private void openHomeScreen() {
        SharedPrefsHelper.getInstance().save(PREF.PREF_USERNAME, etEmail.getText()
                .toString());
        final Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void openForgotPasswordScreen() {
        final Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    private void openRegistrationScreen() {
        final Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
        startActivity(intent);
    }

    private void validation() {
        final String email = etEmail.getText().toString().trim();
        final String password = etPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_enter_email));
            etEmail.requestFocus();
        } else if (!Utils.isValidEmail(email)) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_please_varify_your_email));
            etEmail.requestFocus();
        } else if (TextUtils.isEmpty(password)) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_enter_password));
            etPassword.requestFocus();
        } else if (password.length() < 6) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_password_length_less_6));
            etPassword.requestFocus();
        }

//        else if (!Utils.isValidPassword(password)) {
//            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_wrong_password));
//            etPassword.requestFocus();
//        }

        else {
            Bundle bundle = new Bundle();
            bundle.putString(WSConstants.WS_KEY_EMAIL, email);
            bundle.putString(WSConstants.WS_KEY_PASSWORD, password);
            callAsyncGetLogin(bundle);
        }
    }


    /**
     * Called for perform user Login
     *
     * @param bundle
     */
    private void callAsyncGetLogin(Bundle bundle) {
        if (Utils.isNetworkAvailable(this)) {
            if (asyncGetLogin != null && asyncGetLogin.getStatus() == AsyncTask.Status.PENDING) {
                asyncGetLogin.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (asyncGetLogin == null || asyncGetLogin.getStatus() == AsyncTask.Status.FINISHED) {
                asyncGetLogin = new AsyncLogin(bundle);
                asyncGetLogin.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.alert_no_internet));
        }
    }

    /**
     * call login web service
     */
    private class AsyncLogin extends AsyncTask<Void, Void, Void> {
        private Bundle bundle;
        private WSLogin wsGetLogin;


        public AsyncLogin(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wsGetLogin = new WSLogin(LoginActivity.this);
                wsGetLogin.executeService(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (isDestroyed()) {
                return;
            }


            if (!isCancelled()) {
                if (wsGetLogin.getCode() == WSConstants.STATUS_SUCCESS) {
                    // Initialise Chat service
                    final PreferenceUtils preferenceUtils = new PreferenceUtils(LoginActivity.this);
                    String qemail = preferenceUtils.getString(preferenceUtils.KEY_QUICK_BLOX_EMAIL);
                    String qpassword = preferenceUtils.getString(preferenceUtils
                            .KEY_QUICK_BLOX_PASSWORD);
                    final QBChatService chatService = QBChatService.getInstance();
                    final QBUser qbUser = new QBUser();
                    qbUser.setEmail(qemail);
                    qbUser.setPassword(qpassword);
                    login(qbUser);




                } else if (wsGetLogin.getCode() == WSConstants.STATUS_FAIL) {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    Utils.displayDialog(LoginActivity.this, getString(R.string.app_name), wsGetLogin.getMessage());

                }


                else {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    Utils.displayDialogWithPopBackStack(LoginActivity.this, getString(R.string
                                    .app_name),
                            getString(R.string.something_went_wrong_msg));

                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (asyncGetLogin != null && asyncGetLogin.getStatus() == AsyncTask.Status.RUNNING) {
            asyncGetLogin.cancel(true);
        }
    }

    private void login(final QBUser user) {
        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Log.d("Result", "Log in sucess");
                SharedPrefsHelper.getInstance().saveQbUser(user);
                SharedPrefsHelper.getInstance().save(PREF.PREF_USERNAME, etEmail.getText()
                        .toString());
                openHomeScreen();

//                SharedPrefsHelper.getInstance().save(PREF.PREF_QB_USER_PASSWORD, etPassword.getText().toString());
//                SharedPrefsHelper.getInstance().save(PREF.PREF_ISLOGIN, true);
                //old start

                //admin 27504635
                //zeel 27793832

//                final PreferenceUtils preferenceUtils = new PreferenceUtils(LoginActivity.this);
//                String blogId =preferenceUtils.getString(preferenceUtils.KEY_QUICK_BLOX_ID);
//                qbChatDialog = DialogUtils.buildPrivateDialog(Integer.parseInt(blogId));
//                QBRestChatService.createChatDialog(qbChatDialog).performAsync(new QBEntityCallback<QBChatDialog>() {
//                    @Override
//                    public void onSuccess(QBChatDialog result, Bundle params) {
//                        Log.v("RESULT IN CHAT", "dialog iopen sucess");
//                        final PreferenceUtils preferenceUtils = new PreferenceUtils(LoginActivity.this);
//                        lawyerapp.setdialog(result);
//                        preferenceUtils.putString(preferenceUtils.KEY_DIALOG_ID, result.getDialogId());
//                        if (progressDialog != null && progressDialog.isShowing()) {
//                            progressDialog.dismiss();
//                            openHomeScreen();
//                        }
//
//                    }
//
//                    @Override
//                    public void onError(QBResponseException responseException) {
//                        if (progressDialog != null && progressDialog.isShowing()) {
//                            progressDialog.dismiss();
//                        }
//                        Log.v("RESULT IN CHAT", "dialog iopen error");
//                    }
//                });
//old end
//                QBUsers.getUserByLogin(user.getLogin()).performAsync(new QBEntityCallback<QBUser>() {
//                    @Override
//                    public void onSuccess(QBUser qbUser, Bundle bundle) {
//
//
//
//
//
//
//                    }
//
//                    @Override
//                    public void onError(QBResponseException e) {
//                        progressDialog.dismiss();
//                        Utils.showSnackbar(findViewById(R.id.activity_login_layout_root), e.getMessage(), "OK",
//                                new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        finish();
//                                    }
//                                }, LoginActivity.this);
//                    }
//                });


            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("Result", "Log in error");
                progressDialog.dismiss();
                Utils.displayDialog(LoginActivity.this,getString(R.string.app_name),"Quick blox " +
                        "error.");

            }


        });
    }
}
