package com.adaptingsocial.lawyerapp.view;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.CameraAdapter;
import com.adaptingsocial.lawyerapp.customview.HorizontalListView;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@SuppressLint("NewApi")
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class CustomCameraActivity extends Activity implements SurfaceHolder.Callback, OnClickListener, MediaScannerConnectionClient, SensorEventListener {
    private static final String TAG = null;
    private Button btn_retake;
    private Button btn_use;
    private Button btn_turn;
    private ImageView img_capture;
    public static final String DEST =
            "/storage/emulated/0/PDF/";
    private boolean safeToTakePicture = false;

    private ImageView img_gallery;
    private ToggleButton togbtn_flash_on_off;

    Camera camera = null;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;
    boolean previewing = false, frontCamera = false;
    int which = 0;


    static String thePic, imgPath;

    private File sdRoot;
    private MediaScannerConnection conn;
    private String SCAN_PATH;
    private static final String FILE_TYPE = "image/*";
    private String dir;
    private String fileName;
    boolean hasFlash;
    boolean isFlashOn = false;
    private List<Size> mSupportedPreviewSizes;
    private List<Size> mSupportedPictureSize;
    private HorizontalListView horizontalListView;
    private ArrayList<File> imagepath;
    private ArrayList<String> imagepathstring;
    private long mLastClickTime = 0;

    // record the compass picture angle turned
    private float currentDegree = 0f;
    // device sensor manager
    private SensorManager mSensorManager;
    private CameraAdapter cameraAdapter;
    private ImageView btnPdf;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_camera);
        hasFlash = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        getWindow().setFormat(PixelFormat.UNKNOWN);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);


        initview();

        sdRoot = Environment.getExternalStorageDirectory();
        dir = "/Mydirectory/";
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);


        btn_retake.setOnClickListener(this);
        btn_use.setOnClickListener(this);
        btn_turn.setOnClickListener(this);
        img_capture.setOnClickListener(this);
        img_gallery.setOnClickListener(this);
        img_capture.setOnClickListener(this);
        togbtn_flash_on_off.setOnClickListener(this);

        //check number of camera avelable in device
        if (Camera.getNumberOfCameras() >= 2) {
            which = 0;

            btn_turn.setVisibility(View.GONE);
        } else {
            which = 1;

            btn_turn.setVisibility(View.GONE);
        }

        //check flash or not
        if (hasFlash) {
            togbtn_flash_on_off.setVisibility(View.VISIBLE);
            togbtn_flash_on_off.setChecked(true);
            togbtn_flash_on_off.setText(null);
            togbtn_flash_on_off.setText("");


        } else {
            togbtn_flash_on_off.setVisibility(View.GONE);
        }


    }


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();


        // for the system's orientation sensor registered listeners

        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);

    }


    @Override
    protected void onPause() {

        super.onPause();
        // to stop the listener and save battery
        mSensorManager.unregisterListener(this);

    }


    private void initview() {
        // TODO Auto-generated method stub
        btnPdf = (ImageView) findViewById(R.id.btn_pdf);
        horizontalListView = (HorizontalListView) findViewById(R.id.hlvSimpleList);
        imagepath = new ArrayList<File>();
        imagepathstring = new ArrayList<String>();
        img_capture = (ImageView) findViewById(R.id.img_capture);
        togbtn_flash_on_off = (ToggleButton) findViewById(R.id.togbtn_flash_on_off);
        btn_retake = (Button) findViewById(R.id.btn_retake);
        btn_use = (Button) findViewById(R.id.btn_use);
        btn_turn = (Button) findViewById(R.id.btn_turn);
        img_gallery = (ImageView) findViewById(R.id.img_gallery);
        surfaceView = (SurfaceView) findViewById(R.id.camerapreview);
        cameraAdapter = new CameraAdapter(CustomCameraActivity.this, imagepath, imagepathstring);
        horizontalListView.setAdapter(cameraAdapter);
        btnPdf.setOnClickListener(this);

    }

    public void btn_turn() {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD) {
            if (Camera.getNumberOfCameras() >= 2) {
                camera.stopPreview();
                camera.release();
                //"which" is just an integer flag
                switch (which) {
                    case 0:
                        camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                        which = 1;
                        frontCamera = true;
                        break;
                    case 1:
                        camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                        which = 0;
                        frontCamera = false;
                        break;
                }


                try {
                    camera.setPreviewDisplay(surfaceHolder);
                    Camera.Parameters parameters = camera.getParameters();

                    if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                        parameters.set("orientation", "portrait");
                        //parameters.setRotation(90);
                        camera.setDisplayOrientation(90);

                    }

                    //check flash or not
                    if (hasFlash) {
                        //check flash on or off
                        if (isFlashOn) {
                            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                        } else {
                            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        }
                    }
                    camera.setParameters(parameters);

                } catch (IOException exception) {
                    camera.release();
                }
                camera.startPreview();

            } else {
                AlertDialog.Builder ab = new AlertDialog.Builder(CustomCameraActivity.this);
                ab.setMessage("Device Having Only one Camera");
                ab.setCancelable(false);
                ab.setPositiveButton("ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub

                    }
                }).show();

            }
        } else {
            AlertDialog.Builder ab1 = new AlertDialog.Builder(CustomCameraActivity.this);
            ab1.setMessage("This Device Does Not Support Dual Camera Feature");
            ab1.setCancelable(false);
            ab1.setPositiveButton("ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub

                }
            }).show();
        }
    }


    public static void setCameraDisplayOrientation(Activity activity, int cameraId, android.hardware.Camera camera) {
        try {


            android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
            android.hardware.Camera.getCameraInfo(cameraId, info);
            int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
            int degrees = 0;
            switch (rotation) {
                case Surface.ROTATION_0:
                    degrees = 0;
                    break;
                case Surface.ROTATION_90:
                    degrees = 90;
                    break;
                case Surface.ROTATION_180:
                    degrees = 180;
                    break;
                case Surface.ROTATION_270:
                    degrees = 270;
                    break;
            }

            int result;
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (info.orientation + degrees) % 360;
                result = (360 - result) % 360; // compensate the mirror
            } else { // back-facing
                result = (info.orientation - degrees + 360) % 360;
            }
            camera.setDisplayOrientation(result);

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }


    }


    private PictureCallback mPicture = new PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {


            fileName = "Pink_shutter_" + Long.toString(System.currentTimeMillis()) + ".jpg";


            if (data != null) {
                int screenWidth = getResources().getDisplayMetrics().widthPixels;
                int screenHeight = getResources().getDisplayMetrics().heightPixels;
                Bitmap bm = BitmapFactory.decodeByteArray(data, 0, (data != null) ? data.length : 0);

                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

                    Bitmap scaled = Bitmap.createScaledBitmap(bm, screenHeight, screenWidth, true);
                    int w = scaled.getWidth();
                    int h = scaled.getHeight();

                    Log.e("width .........", "width =" + w);
                    Log.e("Hight .........", "Hight =" + h);


                    // Setting post rotate to 90
                    Matrix mtx = new Matrix();
                    System.out.println("frontCamera==" + frontCamera);

                    if (frontCamera) {
                        mtx.setRotate(270.0f);
                    } else {

                        ExifInterface exif = null;
                        try {
                            exif = new ExifInterface(data.toString());
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

//                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//
//
//                        Log.e("ExifInteface .........", "rotation =" + orientation);

                        mtx.postRotate(90);

                    }

                    // Rotating Bitmap

                    bm = Bitmap.createBitmap(scaled, 0, 0, w, h, mtx, true);
                    File mkDir = new File(sdRoot, dir);

                    if (!mkDir.exists()) {
                        mkDir.mkdirs();
                    }


                    File pictureFile = new File(sdRoot, dir + fileName);
                    imagepath.add(pictureFile);
                    imagepathstring.add(pictureFile.getAbsolutePath());
                    try {
                        FileOutputStream out = new FileOutputStream(pictureFile);
                        bm.compress(Bitmap.CompressFormat.JPEG, 30, out);
                        out.flush();
                        out.close();
                        img_gallery.setImageBitmap(bm);
                        thePic = "file://" + pictureFile.getAbsolutePath();
                        cameraAdapter.notifyDataSetChanged();

                        safeToTakePicture = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {


                    Bitmap scaled = Bitmap.createScaledBitmap(bm, screenWidth, screenHeight, true);
                    bm = scaled;

                    File mkDir = new File(sdRoot, dir);

                    if (!mkDir.exists()) {
                        mkDir.mkdirs();
                    }


                    File pictureFile = new File(sdRoot, dir + fileName);

                    try {
                        FileOutputStream out = new FileOutputStream(pictureFile);
                        bm.compress(Bitmap.CompressFormat.JPEG, 30, out);
                        out.flush();
                        out.close();


                        img_gallery.setImageBitmap(bm);
                        thePic = "file://" + pictureFile.getAbsolutePath();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            camera.startPreview();


//			btn_turn.setVisibility(View.GONE);
//			img_capture.setVisibility(View.GONE);
//			ibRetake.setVisibility(View.VISIBLE);
//			ibUse.setVisibility(View.VISIBLE);


//				if(imgNo!=SelectNumbersOfImages.num)
//           	 	{
//
//		           camera.startPreview();
//
//
//           	 	}
//				else
//				{
//					btn_turn.setVisibility(View.GONE);
//					img_capture.setVisibility(View.GONE);
//					ibRetake.setVisibility(View.VISIBLE);
//					ibUse.setVisibility(View.VISIBLE);
//				}


        }

    };


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        try {

            togbtn_flash_on_off.setText("");
            //check preview
            if (previewing) {
                camera.stopPreview();
                previewing = false;
            }

            //check which camera open(back and front camera)

            if (which == 1) {
                //Back camera
                Camera.open(which);
                //check orientation
                setCameraDisplayOrientation(CustomCameraActivity.this, CameraInfo.CAMERA_FACING_BACK, camera);
            } else {
                //front camera
                camera = Camera.open();
                //check orientation
                setCameraDisplayOrientation(CustomCameraActivity.this, CameraInfo.CAMERA_FACING_FRONT, camera);
            }


            camera.setPreviewDisplay(holder);
            Camera.Parameters parameters = camera.getParameters();

            if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                parameters.set("orientation", "portrait");
                //parameters.setRotation(90);
                camera.setDisplayOrientation(90);

            } else {
                //parameters.set("orientation", "landscape");
                //camera.setDisplayOrientation(180);


            }

            //check flash or not
            if (hasFlash) {
                //check flash on or off
                if (isFlashOn) {
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                } else {
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                }


            }

            //get best previewsize and picture size
            mSupportedPreviewSizes = parameters.getSupportedPreviewSizes();
            mSupportedPictureSize = parameters.getSupportedPictureSizes();
            Camera.Size Prviewsize = getBestPreviewSize(width, height, parameters);
            Camera.Size Picturesize = getBestPictureSize(camera, mSupportedPictureSize, true, width, height);


            if (mSupportedPreviewSizes != null) {
                parameters.setPreviewSize(Prviewsize.width, Prviewsize.height);
            }

            if (mSupportedPictureSize != null) {
                parameters.setPictureSize(Picturesize.width, Picturesize.height);
            }

            camera.setParameters(parameters);
            previewing = true;
        } catch (IOException exception) {
            exception.printStackTrace();
            camera.release();
        }
        camera.startPreview();
        safeToTakePicture = true;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            //Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //all code surfaceChanged
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        camera.stopPreview();
        camera.release();
        camera = null;
        previewing = false;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_retake:

                //retake picture
                File discardedPhoto = new File(sdRoot, dir + fileName);
                discardedPhoto.delete();

                btn_turn.setVisibility(View.VISIBLE);
                img_capture.setVisibility(View.VISIBLE);

                btn_retake.setVisibility(View.GONE);
                btn_use.setVisibility(View.GONE);
                camera.startPreview();

                break;

            case R.id.btn_use:

                break;


            case R.id.btn_turn:
                //camera turn(frontcamera and backcamear)
//                btn_turn();
//                try {
//                    createPdf(DEST);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (DocumentException e) {
//                    e.printStackTrace();
//                }
                break;

            case R.id.btn_pdf:
                try {
                    if (imagepathstring.size() > 0) {
                        createPdf(DEST);
                    } else {
                        Toast.makeText(this, "Please capture atleast one image.", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.img_capture:
                /**
                 * Logic to Prevent the Launch of the Fragment Twice if User makes
                 * the Tap(Click) very Fast.
                 */
                if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                //capture camera -
                if (imagepath.size() <= 9) {
                    if (hasFlash) {
                        Camera.Parameters parameters = camera.getParameters();

                        if (isFlashOn) {
                            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                        } else {
                            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        }
                        camera.setParameters(parameters);
                    }
                    if (safeToTakePicture) {
                        if(mPicture!=null) {
                            camera.takePicture(null, null, mPicture);
                            safeToTakePicture = false;
                        }
                    }

                } else {
                    Toast.makeText(CustomCameraActivity.this, "cant take more than 10 picture",
                            Toast.LENGTH_SHORT).show();
                }
                break;


            case R.id.img_gallery:

                try {

                    opengalary();
                } catch (ActivityNotFoundException e) {
                    Log.e(TAG, "No gallery: " + e);
                }

                break;

            case R.id.togbtn_flash_on_off:


                if (((ToggleButton) v).isChecked()) {
                    isFlashOn = false;
//                    togbtn_flash_on_off.setText("");
//                    togbtn_flash_on_off.setBackgroundResource(R.drawable.switch_on);
                    togbtn_flash_on_off.setText("");
                    togbtn_flash_on_off.setBackgroundResource(R.drawable.switch_off);

                } else {
                    isFlashOn = true;
                    togbtn_flash_on_off.setText("");
                    togbtn_flash_on_off.setBackgroundResource(R.drawable.switch_on);


                }


                break;


            default:
                break;
        }

    }

    private void opengalary() {

        SCAN_PATH = sdRoot + dir + fileName;

        if (conn != null) {
            conn.disconnect();
        }
        conn = new MediaScannerConnection(this, this);
        conn.connect();


    }

    @Override
    public void onMediaScannerConnected() {
        // TODO Auto-generated method stub

        Log.d("onMediaScannerConnected", "success" + conn);
        conn.scanFile(SCAN_PATH, FILE_TYPE);

    }

    @Override
    public void onScanCompleted(String path, Uri uri) {
        // TODO Auto-generated method stub

        try {
            Log.d("onScanCompleted", uri + "success" + conn);
            if (uri != null) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(uri);
                startActivity(intent);
            }
        } finally {
            conn.disconnect();
            conn = null;
        }
    }


    private Camera.Size getBestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea > resultArea) {
                        result = size;
                    }
                }
            }
        }

        return (result);
    }

    public static Camera.Size getBestPictureSize(Camera camera, List<Size> supportedSizes, boolean preview, int displayWidth, int displayHeight) {

        final int PREVIEW_SIZE_WIDTH_EMULATOR = 176;
        final int PREVIEW_SIZE_HEIGHT_EMULATOR = 144;
        final int PICTURE_SIZE_WIDTH_EMULATOR = 213;
        final int PICTURE_SIZE_HEIGHT_EMULATOR = 350;

        double temporalDiff = 0;
        double diff = Integer.MAX_VALUE;

        Camera.Size size = null;
        Camera.Size supportedSize = null;

        if (supportedSizes == null) {
            if (isAndroidEmulator(android.os.Build.MODEL)) {
                if (preview) {
                    size = camera.new Size(
                            PREVIEW_SIZE_WIDTH_EMULATOR,
                            PREVIEW_SIZE_HEIGHT_EMULATOR);
                } else {
                    size = camera.new Size(
                            PICTURE_SIZE_WIDTH_EMULATOR,
                            PICTURE_SIZE_HEIGHT_EMULATOR);
                }
            }
        } else {
            Iterator<Size> iterator = supportedSizes.iterator();
            while (iterator.hasNext()) {
                supportedSize = iterator.next();
                temporalDiff = Math.sqrt(
                        Math.pow(supportedSize.width - displayWidth, 2) +
                                Math.pow(supportedSize.height - displayHeight, 2));

                if (temporalDiff < diff) {
                    diff = temporalDiff;
                    size = supportedSize;
                }
            }

        }

        return size;
    }

    public static boolean isAndroidEmulator(String model) {

        return (model.compareToIgnoreCase("sdk") == 0);
    }

    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // TODO Auto-generated method stub


//			  // get the angle around the z-axis rotated
//	        float degree = Math.round(event.values[0]);
//
//	        // create a rotation animation (reverse turn degree degrees)
//	        RotateAnimation ra = new RotateAnimation(  currentDegree,   -degree, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,0.5f);
//
//	        // how long the animation will take place
//	        ra.setDuration(210);
//
//	        // set the animation after the end of the reservation status
//	        ra.setFillAfter(true);
//
//	        // Start the animation
//	        img_capture.startAnimation(ra);
//	        currentDegree = -degree;

    }

    public void createPdf(String dest) throws IOException, DocumentException {

        Document document = new Document();
        File docDirectory = new File(dest);
// have the object build the directory structure, if needed.
        if (!docDirectory.exists()) {
            docDirectory.mkdirs();
        }
// create a File object for the output file
        File outputFile = new File(docDirectory, "LegalStart-" + System.currentTimeMillis() + ".pdf");
// now attach the OutputStream to the file object, instead of a String representation
        FileOutputStream fos = new FileOutputStream(outputFile);
        PdfWriter.getInstance(document, fos);
        document.open();
        for (String image : imagepathstring) {
            Image img = Image.getInstance(image);
            document.setPageSize(img);
            document.newPage();
            img.setAbsolutePosition(0, 0);
            document.add(img);
        }
        document.close();
        Intent output = new Intent();
        output.putExtra("path", outputFile.getAbsoluteFile().getAbsolutePath());
        output.putExtra("name", "LegalStart-" + System.currentTimeMillis() + ".pdf");
        setResult(RESULT_OK, output);
        finish();
    }


}