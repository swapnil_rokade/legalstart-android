package com.adaptingsocial.lawyerapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;

/**
 * Created by indianic on 08/06/17.
 */

public class LauncherActivity extends BaseActivity {
    private String TAG = this.getClass().getSimpleName();
    private Handler handler;
    public final int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        setHandler();
    }

    private void setHandler() {
        handler = new Handler();
        handler.postDelayed(runnable, SPLASH_TIME_OUT);
    }


    final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            final PreferenceUtils preferenceUtils = new PreferenceUtils(LauncherActivity.this);
            if (preferenceUtils.getBoolean(preferenceUtils.KEY_USER_LOGIN)) {
                openHomeScreen();

            } else {
                openLoginActivity();


            }

        }
    };

    private void openLoginActivity() {
        final Intent intent = new Intent(LauncherActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();

    }

    private void openHomeScreen() {
        final Intent intent = new Intent(LauncherActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void callSplash() {
        final Intent intent = new Intent(LauncherActivity.this, SplashActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
    }
}
