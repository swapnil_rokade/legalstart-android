package com.adaptingsocial.lawyerapp.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.DrawerListAdapter;
import com.adaptingsocial.lawyerapp.app.Lawyerapp;
import com.adaptingsocial.lawyerapp.fragment.AppointmentFragment;
import com.adaptingsocial.lawyerapp.fragment.AppointmentListFragment;
import com.adaptingsocial.lawyerapp.fragment.BlogListFragment;
import com.adaptingsocial.lawyerapp.fragment.CaseDetailFragment;
import com.adaptingsocial.lawyerapp.fragment.CaseListFragment;
import com.adaptingsocial.lawyerapp.fragment.ChangePasswordFragment;
import com.adaptingsocial.lawyerapp.fragment.ChatFragment;
import com.adaptingsocial.lawyerapp.fragment.CompanyListFragment;
import com.adaptingsocial.lawyerapp.fragment.DashBoardFragment;
import com.adaptingsocial.lawyerapp.fragment.EditProfileFragment;
import com.adaptingsocial.lawyerapp.fragment.EmailFragment;
import com.adaptingsocial.lawyerapp.fragment.HomeFragment;
import com.adaptingsocial.lawyerapp.fragment.LetsChatFragment;
import com.adaptingsocial.lawyerapp.fragment.MyDocumentFragment;
import com.adaptingsocial.lawyerapp.fragment.QuestionCategoryFragment;
import com.adaptingsocial.lawyerapp.fragment.RequestDocumentFragment;
import com.adaptingsocial.lawyerapp.fragment.ServiceRequiredFragment;
import com.adaptingsocial.lawyerapp.fragment.SubscriptionFragment;
import com.adaptingsocial.lawyerapp.model.CaseListModel;
import com.adaptingsocial.lawyerapp.model.DrawerModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.ChatDeleteService;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsCheckPaymentStatus;
import com.adaptingsocial.lawyerapp.webservice.WsLogOut;
import com.google.android.gms.common.GoogleApiAvailability;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.messages.services.QBPushManager;
import com.quickblox.messages.services.SubscribeService;

import java.util.ArrayList;

/*************************************************************************************
 * Class Name: HomeActivity
 * Created By: lbamarnani
 * Date:24-APRIL-17
 * Modified By:
 * Modified Date:
 * Purpose: This class is used for  main  container.
 *************************************************************************************/
public class HomeActivity extends BaseActivity {
    private final String TAG = this.getClass().getSimpleName();
    public Toolbar toolbar;
    private TextView tvTitle;
    private DrawerLayout drawerLayout;
    private NavigationView mNavigationView;
    private ActionBarDrawerToggle drawerToggle;
    private ListView drawerList;
    private ArrayList<DrawerModel> drawerModelList;
    private Lawyerapp lawyerapp;
    private ImageView ivProfilePic;
    private TextView tvName;
    private PreferenceUtils preferenceUtils;
    private long mLastClickTime = 0;
    private LogOutAsyncTask logOutAsyncTask;
    private QBChatDialog qbChatDialog;
    private View viewShadow;
    private SubscriptionPaymentStatusAsyncTask subscriptionPaymentStatusAsyncTask;
    private ImageView ivTitle;
    private DrawerListAdapter dAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lawyerapp = (Lawyerapp) this.getApplicationContext();
        setContentView(R.layout.activity_home);
        initToolbar();
        setupToolbar(false, getString(R.string.legal_start), 0);
        initNavigationDrawer();
        if (getIntent().getExtras() != null) {
            if (getIntent().getStringExtra("from").equalsIgnoreCase("chatend")) {
//                startService(new Intent(getBaseContext(), ChatDeleteService.class));
                //dialog appear
                setUpMainFragment(new DashBoardFragment());

            }
            else if (getIntent().getStringExtra("from").equalsIgnoreCase("case")) {
                setUpMainFragment(new DashBoardFragment());
            }

            else if (getIntent().getStringExtra("from").equalsIgnoreCase("document")) {
                setUpMainFragment(new MyDocumentFragment());
            }
            else {

                setUpMainFragment(new ChatFragment());
            }


        } else {
            setUpMainFragment(new DashBoardFragment());



        }
//notification handling
//        BroadcastReceiver call_method = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                String action_name = intent.getAction();
//                if (action_name.equals("call_method")) {
//                    // call your method here and do what ever you want.
//                    Fragment mFragment = new MyDocumentFragment();
//                    setUpMainFragment(mFragment);
//                }
//            }
//
//            ;
//        };
//        registerReceiver(call_method, new IntentFilter("call_method"));


    }

    @Override
    protected void onResume() {
        super.onResume();
        AppLog.showLogD("Result", "call required method");
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mMessageReceiver,
                        new IntentFilter("my-integer"));
        QBSettings.getInstance().setEnablePushNotification(true);
        initPushManager();

        subscriptionStatusUsAsyncTask();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) toolbar.findViewById(R.id.toolbar_tv_title);
        ivTitle = (ImageView) toolbar.findViewById(R.id.toolbar_iv_title);

    }

    public void initNavigationDrawer() {

        preferenceUtils = new PreferenceUtils(HomeActivity.this);
        viewShadow = (View) findViewById(R.id.toolbar_shadow);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        drawerList = (ListView) findViewById(R.id.nav_list);
        drawerModelList = new ArrayList<>();
        drawerModelList.add(new DrawerModel(getString(R.string.home), R.drawable.ic_home));
        drawerModelList.add(new DrawerModel(getString(R.string.subscription_tag), R.drawable.ic_menu_subscribe));
        drawerModelList.add(new DrawerModel(getString(R.string.service_required), R.drawable.ic_service_required));
        drawerModelList.add(new DrawerModel(getString(R.string.change_password), R.drawable.ic_menu_password));
        drawerModelList.add(new DrawerModel(getString(R.string.support), R.drawable.ic_support));
        drawerModelList.add(new DrawerModel(getString(R.string.term_condition), R.drawable.ic_terms));
        drawerModelList.add(new DrawerModel(getString(R.string.privacy_policy), R.drawable.ic_privacy_policy));
        drawerModelList.add(new DrawerModel(getString(R.string.log_out), R.drawable.ic_logout));

        //Hidden Nav Items.
//        drawerModelList.add(new DrawerModel(getString(R.string.appointments), R.drawable.ic_appointmnts));
//        drawerModelList.add(new DrawerModel(getString(R.string.upload_document), R.drawable.ic_cases));
//        drawerModelList.add(new DrawerModel(getString(R.string.str_start_my_business), R.drawable.ic_questionaries));
//        drawerModelList.add(new DrawerModel(getString(R.string.startup_network), R.drawable.ic_company_information));
//        drawerModelList.add(new DrawerModel(getString(R.string.request_document), R.drawable.ic_request));
//        drawerModelList.add(new DrawerModel(getString(R.string.chat), R.drawable.ic_chat));
//        drawerModelList.add(new DrawerModel(getString(R.string.blog), R.drawable.ic_blog));
//        drawerModelList.add(new DrawerModel(getString(R.string.email_to_lawyer), R.drawable.ic_email_lawyer));
//        drawerModelList.add(new DrawerModel(getString(R.string.str_my_document), R.drawable.ic_my_docs_small));

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.nevigation_list_header, drawerList, false);
        ivProfilePic = (ImageView) header.findViewById(R.id.nevigation_list_header_iv_user);
        tvName = (TextView) header.findViewById(R.id.nevigation_list_header_tv_username);
        tvName.setText(preferenceUtils.getString(preferenceUtils.KEY_FIRST_NAME) + " " + preferenceUtils.getString(preferenceUtils.KEY_LAST_NAME));
        ivProfilePic.setOnClickListener(this);
        drawerList.addHeaderView(header, null, false);
        dAdapter = new DrawerListAdapter(this, drawerModelList);
        drawerList.setAdapter(dAdapter);
        drawerList.setOnItemClickListener(new DrawerItemClickListener());

        if (mNavigationView != null) {
            setupDrawerContent(mNavigationView);

        }

//        setUpMainFragment(new CaseListFragment());


    }

    private void subscriptionStatusUsAsyncTask() {
        if (Utils.isNetworkAvailable(HomeActivity.this)) {
            if (subscriptionPaymentStatusAsyncTask != null && subscriptionPaymentStatusAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                subscriptionPaymentStatusAsyncTask.execute();
            } else if (subscriptionPaymentStatusAsyncTask == null || subscriptionPaymentStatusAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                subscriptionPaymentStatusAsyncTask = new SubscriptionPaymentStatusAsyncTask();
                subscriptionPaymentStatusAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(HomeActivity.this, getString(R.string.app_name), getString(R.string
                    .check_internet_msg));
        }
    }

    public class SubscriptionPaymentStatusAsyncTask extends AsyncTask<Void, Void, Void> {
        private WsCheckPaymentStatus wsCheckPaymentStatus;
        private ProgressDialog progressDialog;

        public SubscriptionPaymentStatusAsyncTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            wsCheckPaymentStatus = new WsCheckPaymentStatus(HomeActivity.this);
            wsCheckPaymentStatus.executeService();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsCheckPaymentStatus.getCode() == WSConstants.STATUS_SUCCESS) {


                }

            } else if (wsCheckPaymentStatus.getCode() == WSConstants.STATUS_FAIL) {
                Utils.displayDialog(HomeActivity.this, getString(R.string.app_name),
                        wsCheckPaymentStatus
                                .getMessage());

            } else if (wsCheckPaymentStatus.getCode() == WSConstants.STATUS_LOGOUT) {
                Utils.displayLogoutDialog(HomeActivity.this, getString(R.string.app_name), wsCheckPaymentStatus
                        .getMessage());

            } else {
                Utils.displayDialogWithPopBackStack(HomeActivity.this, getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
            }
        }
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
            dAdapter.setSelectedItem(position > 0 ? position - 1 : position);
            dAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(HomeActivity.this, view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        final int id = view.getId();
        switch (id) {
            case R.id.nevigation_list_header_iv_user:
                setUpMainFragment(new EditProfileFragment());
                drawerLayout.closeDrawers();
                break;

        }
    }

    public void displayDialogcCreateUser(final Context context, final String title, final String
            message, final int from) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(R.string.gotit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (from == Constants.FROM_APPOINTMENT) {
                    final Fragment mFragment = new AppointmentListFragment();
                    setUpMainFragment(mFragment);
                } else {
                    final Fragment mFragment = new RequestDocumentFragment();
                    setUpMainFragment(mFragment);
                }
                drawerLayout.closeDrawers();

                dialog.dismiss();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }

    /**
     * Swaps fragments in the main content view
     */
    private void selectItem(int position) {
        final Fragment mFragment;
        final FragmentTransaction fragmentTransaction;
        preferenceUtils = new PreferenceUtils(HomeActivity.this);
        Boolean freeUser = preferenceUtils.getBoolean(preferenceUtils.KEY_APP_USER_TYPE);
        switch (position) {

            case 1:
                mFragment = new DashBoardFragment();
                setUpMainFragment(mFragment);
                drawerLayout.closeDrawers();
                break;

            case 2:
                mFragment = new SubscriptionFragment();
                setUpMainFragment(mFragment);
                drawerLayout.closeDrawers();
                break;
            case 3:
                mFragment = new ServiceRequiredFragment();
                setUpMainFragment(mFragment);
                drawerLayout.closeDrawers();
                break;

            case 4:
                mFragment = new ChangePasswordFragment();
                setUpMainFragment(mFragment);
                drawerLayout.closeDrawers();
                break;

            case 5:
                openMailClient();
                drawerLayout.closeDrawers();
                break;

            case 6:
                openStaticPage("termsconditions");
                drawerLayout.closeDrawers();

                break;
            case 7:
                openStaticPage("privacypolicy");
                drawerLayout.closeDrawers();
                break;
            case 8:
                if (Utils.isNetworkAvailable(HomeActivity.this)) {
                    displayDialog(HomeActivity.this, getString(R.string.app_name), getString(R.string.logout_msg),
                            getString
                                    (android.R.string.yes), getString
                                    (android.R.string.no), true, true);
                } else {
                    Utils.displayDialog(HomeActivity.this, getString(R.string.app_name),
                            getString(R.string.check_internet_msg));
                }
                break;

            case 9:
                mFragment = new EmailFragment();
                setUpMainFragment(mFragment);
                drawerLayout.closeDrawers();
                break;

            case 10:
                displayDialogcCreateUser(HomeActivity.this,
                        getString(R.string.app_name), getString(R.string.alert_appointments)
                        , Constants.FROM_APPOINTMENT);
                drawerLayout.closeDrawers();
                break;

            case 11:
                mFragment = new MyDocumentFragment();
                setUpMainFragment(mFragment);
                drawerLayout.closeDrawers();
                break;

            case 12:
                mFragment = new CaseListFragment();
                setUpMainFragment(mFragment);
                drawerLayout.closeDrawers();
                break;

            case 13:
                mFragment = new QuestionCategoryFragment();
                setUpMainFragment(mFragment);
                drawerLayout.closeDrawers();
                break;

            case 14:
                mFragment = new CompanyListFragment();
                setUpMainFragment(mFragment);
                drawerLayout.closeDrawers();
                break;

            case 15:
                displayDialogcCreateUser(HomeActivity.this,
                        getString(R.string.app_name),
                        getString(R.string.create_document_alert),
                        Constants.FROM_CREATE_DOCUMENT);
                break;

            case 16:
                if (preferenceUtils.getString(preferenceUtils.KEY_DIALOG_ID)
                        .equalsIgnoreCase("")) {
                    final LetsChatFragment chatFragment = new LetsChatFragment();
                    setUpMainFragment(chatFragment);
                    drawerLayout.closeDrawers();
                } else {
                    mFragment = new ChatFragment();
                    setUpMainFragment(mFragment);
                    drawerLayout.closeDrawers();
                }
                break;

            case 17:
                mFragment = new BlogListFragment();
                setUpMainFragment(mFragment);
                drawerLayout.closeDrawers();
                break;




            default:
                break;

        }
        drawerList.setItemChecked(position, true);

    }


    public void setSubscribeSelected(){
        dAdapter.setSelectedItem(9);
        dAdapter.notifyDataSetChanged();

    }

    private void openMailClient() {
        Log.i("Send email", "");
        String[] TO = {"info@legalstartapp.com"};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
//        emailIntent.setData(Uri.parse("info@legalstartapp.com"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
//        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
//        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));


        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(HomeActivity.this, R.string.alert_no_email_client,
                    Toast.LENGTH_SHORT)
                    .show();
        }

    }

    public  void displayPaidDialog(final Context context, final String title, final String
            message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(true);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(R.string.subscribe_now, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Fragment mFragment = new SubscriptionFragment();
                setUpMainFragment(mFragment);
                setSubscribeSelected();
                drawerLayout.closeDrawers();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
        posButton.setAllCaps(false);
    }



    private void openStaticPage(String pageCode) {
        Intent iStaticPage = new Intent(this, StaticPageActivity.class);
        iStaticPage.putExtra("pageCode", pageCode);
        startActivity(iStaticPage);

    }

    private void displayDialog(final Activity context, final String title, final String msg, final String strPositiveText, final String strNegativeText,
                               final boolean isNagativeBtn, final boolean isFinish) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(title);
        dialog.setCancelable(true);
        dialog.setMessage(msg);
        dialog.setPositiveButton(strPositiveText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (isFinish) {
                    callLogOutApi();


                }
            }
        });
        if (isNagativeBtn) {
            dialog.setNegativeButton(strNegativeText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }

    private void callLogOutApi() {
        if (Utils.isNetworkAvailable(HomeActivity.this)) {
            if (logOutAsyncTask != null && logOutAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                logOutAsyncTask.execute();
            } else if (logOutAsyncTask == null || logOutAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                logOutAsyncTask = new LogOutAsyncTask();
                logOutAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(HomeActivity.this, getString(R.string.app_name), getString(R.string
                    .check_internet_msg));
        }
    }


    public class LogOutAsyncTask extends AsyncTask<String, Void, Void> {
        private WsLogOut wsLogOut;
        private ProgressDialog progressDialog;

        public LogOutAsyncTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(HomeActivity.this);
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(String... params) {
            wsLogOut = new WsLogOut(HomeActivity.this);
            wsLogOut.executeService();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsLogOut.getCode() == WSConstants.STATUS_SUCCESS) {

                    SubscribeService.unSubscribeFromPushes(HomeActivity.this);
                    displayDialogFinishActivity(HomeActivity.this, getString(R.string.app_name),
                            wsLogOut.getMessage());

                } else if (wsLogOut.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(HomeActivity.this, getString(R.string.app_name), wsLogOut
                            .getMessage());
                } else {
                    Utils.displayDialogWithPopBackStack(HomeActivity.this, getString(R.string
                                    .app_name),
                            getString(R.string.something_went_wrong_msg));

                }


            }
        }
    }


    public void displayDialogFinishActivity(final Activity activity, final String title, final String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(activity.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                doLogOut();
                dialog.dismiss();
                activity.finish();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(activity, R.color.colorDarkGray));
    }

    private void doLogOut() {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(HomeActivity.this);
        preferenceUtils.putBoolean(preferenceUtils.KEY_USER_LOGIN, false);
        preferenceUtils.putString(preferenceUtils.KEY_USER_ID, "");
        preferenceUtils.putString(preferenceUtils.KEY_FIRST_NAME, "");
        preferenceUtils.putString(preferenceUtils.KEY_LAST_NAME, "");
        preferenceUtils.putString(preferenceUtils.KEY_ACESS_TOKEN, "");
        preferenceUtils.putString(preferenceUtils.KEY_TOKEN, "");
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    /**
     * sets up toolbar. This method sets up custom tool bar with title and back button where ever needed
     *
     * @param showBack - boolean to decide back or
     * @param title
     */
    public void setupToolbar(final boolean showBack, String title, int from) {
        setUpDrawerToggle(drawerLayout);
        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerToggle.setHomeAsUpIndicator(null);

        if (showBack) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(HomeActivity.this, R.drawable
                    .ic_back));
        } else {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(HomeActivity.this, R.drawable
                    .ic_menu));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyBoard(HomeActivity.this, v);
                if (showBack) {

                    setDrawerLock(true);
                    onBackPressed();


                } else {
                    if (mNavigationView != null) {
                        setupDrawerContent(mNavigationView);
                    }

                    drawerLayout.openDrawer(GravityCompat.START);
                    tvName.setText(preferenceUtils.getString(preferenceUtils.KEY_FIRST_NAME) + " " + preferenceUtils.getString(preferenceUtils.KEY_LAST_NAME));
                    if (preferenceUtils.getString(preferenceUtils.KEY_PROFILE_IMAGE) == null || preferenceUtils.getString(preferenceUtils.KEY_PROFILE_IMAGE)
                            .equalsIgnoreCase("")) {

                    } else {

                        Utils.getCircleImageView(HomeActivity.this, preferenceUtils.getString(preferenceUtils
                                .KEY_PROFILE_IMAGE), ivProfilePic);


                    }


                }
            }
        });


        if (from == 0) {
            tvTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            tvTitle.setCompoundDrawablePadding(5);
            if (TextUtils.isEmpty(title)) {
                ivTitle.setVisibility(View.VISIBLE);
                tvTitle.setVisibility(View.GONE);
            } else {
                ivTitle.setVisibility(View.GONE);
                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(title);
            }

        } else if (from == 1) {
//            Drawable img = getResources().getDrawable(R.drawable.online_green);
//            tvTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);
//            tvTitle.setCompoundDrawablePadding(5);
            tvTitle.setText(title);
        } else if (from == 2) {
//            Drawable img = getResources().getDrawable(R.drawable.ofline_gray);
//            tvTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);
//            tvTitle.setCompoundDrawablePadding(5);
            tvTitle.setText(title);
        } else {

        }

        drawerToggle.syncState();


    }

    protected void closeNavDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public void setDrawerLock(boolean isLock) {
        if (isLock) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
        }
//my code
        else if (getFragmentManager().getBackStackEntryCount() == 0) {
            Fragment myFragment = getFragmentManager()
                    .findFragmentById(R.id.activity_home_container_main);
            if (myFragment instanceof DashBoardFragment) {
                super.onBackPressed();
            }
            else{
                setUpMainFragment(new DashBoardFragment());
                     dAdapter.setSelectedItem(0);
            }        dAdapter.notifyDataSetChanged();
        }


        //prashant
        else if (getFragmentManager().getBackStackEntryCount() > 0) {
            Fragment myFragment = getFragmentManager()
                    .findFragmentById(R.id.activity_home_container_main);
            if (myFragment instanceof AppointmentFragment) {
                if (getFragmentManager().getBackStackEntryCount() > 2) {
                    FragmentManager fm = getFragmentManager();
                    fm.popBackStack(1, FragmentManager
                            .POP_BACK_STACK_INCLUSIVE);
                } else {
                    FragmentManager fm = getFragmentManager();
                    fm.popBackStack(0, FragmentManager
                            .POP_BACK_STACK_INCLUSIVE);
                }
            } else {
                getFragmentManager().popBackStack();
            }
        }

        else

        {
            super.onBackPressed();
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
//        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }


    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        if (mNavigationView != null) {
                            setupDrawerContent(mNavigationView);
                        }
                        setNavigation(menuItem.getItemId());
                        drawerLayout.closeDrawers();  // CLOSE DRAWER
                        return true;
                    }
                });
    }

    private void setUpDrawerToggle(DrawerLayout drawerLayout) {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,
                toolbar,  /* nav drawer icon to replace 'Up' caret */
                R.string.open_drawer,  /* "open drawer" description */
                R.string.close_drawer  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                drawerToggle.syncState();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                drawerToggle.syncState();
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                drawerToggle.syncState();
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);


    }

    private void setNavigation(final int menuItem) {
        final Fragment mFragment;
        final FragmentTransaction fragmentTransaction;

        if (menuItem == R.id.nav_menu_cases) {
            mFragment = new CaseListFragment();
            setUpMainFragment(mFragment);

        } else if (menuItem == R.id.nav_menu_appointments) {
            Utils.displayDialog(HomeActivity.this, getString(R.string.app_name),
                    "Will work in next mile stone.");
            mFragment = new AppointmentListFragment();
            setUpMainFragment(mFragment);

        } else if (menuItem == R.id.nav_menu_company_info) {

            mFragment = new CompanyListFragment();
            setUpMainFragment(mFragment);


        } else if (menuItem == R.id.nav_menu_chat) {
            mFragment = new ChatFragment();
            setUpMainFragment(mFragment);


        } else if (menuItem == R.id.nav_menu_blog) {
            mFragment = new BlogListFragment();
            setUpMainFragment(mFragment);


        } else if (menuItem == R.id.nav_menu_email) {
            Utils.displayDialog(HomeActivity.this, getString(R.string.app_name),
                    "Will work in next mile stone.");

        } else if (menuItem == R.id.nav_menu_my_document) {
            mFragment = new MyDocumentFragment();
            setUpMainFragment(mFragment);

        } else if (menuItem == R.id.nav_menu_log_out) {
            if (Utils.isNetworkAvailable(HomeActivity.this)) {
                displayDialog(HomeActivity.this, getString(R.string.app_name), getString(R.string.logout_msg),
                        getString
                                (android.R.string.ok), "", true, false);
            } else {
                Utils.displayDialog(HomeActivity.this, getString(R.string.app_name),
                        getString(R.string.check_internet_msg));
            }


        }


    }

    public void hideToolBar() {
        toolbar.setVisibility(View.GONE);
    }

    public void showToolBar() {
        toolbar.setVisibility(View.VISIBLE);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void hideShadow() {
        viewShadow.setVisibility(View.GONE);
    }

    public void showShadow() {
        viewShadow.setVisibility(View.VISIBLE);
    }


    protected boolean isNavDrawerOpen() {
        return drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START);
    }


    private void setUpMainFragment(Fragment nextFragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_home_container_main, nextFragment, nextFragment.getClass().getSimpleName()
        ).commit();
    }



    private void initPushManager() {
        QBPushManager.getInstance().addListener(new QBPushManager.QBSubscribeListener() {
            @Override
            public void onSubscriptionCreated() {
                Log.d(TAG, "SubscriptionCreated");
//                Toast.makeText(HomeActivity.this, "HELLLO   SUCESS", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSubscriptionError(Exception e, int resultCode) {
                Log.d(TAG, "SubscriptionError" + e.getLocalizedMessage());
                Toast.makeText(HomeActivity.this, "HELLLO   ERROR", Toast.LENGTH_SHORT).show();
                if (resultCode >= 0) {
                    String error = GoogleApiAvailability.getInstance().getErrorString(resultCode);
                    Log.d(TAG, "SubscriptionError playServicesAbility: " + error);
                }
            }

            @Override
            public void onSubscriptionDeleted(boolean success) {

            }
        });
    }

    // Handling the received Intents for the "my-integer" event
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            int yourInteger = intent.getIntExtra("message", -1/*default value*/);
            int count = getFragmentManager().getBackStackEntryCount();
            AppLog.showLogD("Result", "count" + count);
            startService(new Intent(getBaseContext(), ChatDeleteService.class));
            ChatFragment myFragment = (ChatFragment) getFragmentManager().findFragmentByTag
                    (ChatFragment.class.getSimpleName());
            if (myFragment != null && myFragment.isVisible()) {
                // add your code here
               // Check if no view has focus:
                View view = HomeActivity.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                displayDialog(HomeActivity.this, getString(R.string.app_name), "Admin has ended " +
                        "your chat.");
            }


        }


    };

    public void displayDialog(final Context context, final String title, final String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                setUpMainFragment(new DashBoardFragment());

            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mMessageReceiver);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getExtras() != null) {
            if (intent.getStringExtra("from").equalsIgnoreCase("chatend")) {
//                startService(new Intent(getBaseContext(), ChatDeleteService.class));
                //dialog appear
                setUpMainFragment(new DashBoardFragment());

            }
            else if (intent.getStringExtra("from").equalsIgnoreCase("case")) {
                setUpMainFragment(new DashBoardFragment());
            }

            else if (intent.getStringExtra("from").equalsIgnoreCase("document")) {
                setUpMainFragment(new MyDocumentFragment());
            }
            else {
                AppLog.showLogD("hey", "notify");
                setUpMainFragment(new ChatFragment());
            }


        } else {
            setUpMainFragment(new DashBoardFragment());
        }
    }
}
