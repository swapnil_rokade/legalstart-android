package com.adaptingsocial.lawyerapp.view;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WSLogin;
import com.adaptingsocial.lawyerapp.webservice.WSRegistration;

public class RegistrationActivity extends BaseActivity {
    private TextView tvCreateNewAccount;
    private TextView tvTermAndCondition;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etRepeatPassword;
    private EditText etFirstName;
    private EditText etLastName;
    private ImageView ivClose;
    private long mLastClickTime = 0;
    private ProgressDialog progressDialog;
    private AsyncRegistration asyncRegistration;
    private TextView tvPrivacyPolicy;
    private TextView tvTermCondition;
//    private CheckBox cbTermncondition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        initializeComponents();
    }

    protected void initializeComponents() {
//        cbTermncondition= (CheckBox) findViewById(R.id.activity_registration_cb_privacy_policy);
        tvPrivacyPolicy= (TextView) findViewById(R.id.activity_registration_tv_privacy_policy);
        etEmail = (EditText) findViewById(R.id.activity_registration_et_email);
        etPassword = (EditText) findViewById(R.id.activity_registration_et_password);
        etRepeatPassword = (EditText) findViewById(R.id.activity_registration_repeat_et_password);
        etFirstName = (EditText) findViewById(R.id.activity_registration_et_first_name);
        etLastName = (EditText) findViewById(R.id.activity_registration_et_last_name);
        tvCreateNewAccount = (TextView) findViewById(R.id.activity_registration_tv_create_new_account);
        tvTermAndCondition = (TextView) findViewById(R.id.activity_registration_tv_terms_condition);
        ivClose = (ImageView) findViewById(R.id.activity_registration_iv_close);
        ivClose.setOnClickListener(this);
        tvCreateNewAccount.setOnClickListener(this);
        tvTermAndCondition.setOnClickListener(this);
        tvPrivacyPolicy.setOnClickListener(this);
    }


    private void validation() {
        final String firstName = etFirstName.getText().toString().trim();
        final String lastName = etLastName.getText().toString().trim();
        final String email = etEmail.getText().toString().trim();
        final String password = etPassword.getText().toString().trim();
        final String repeatPassword = etRepeatPassword.getText().toString().trim();

        if (TextUtils.isEmpty(firstName)) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_enter_first_name));
            etFirstName.requestFocus();
        } else if (TextUtils.isEmpty(lastName)) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_enter_last_name));
            etLastName.requestFocus();
        } else if (TextUtils.isEmpty(email)) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_enter_email));
            etEmail.requestFocus();
        } else if (!Utils.isValidEmail(email)) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_please_varify_your_email));
            etEmail.requestFocus();
        } else if (TextUtils.isEmpty(password)) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_enter_password));
            etPassword.requestFocus();
        } else if (password.length() < 6) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_password_length_less_6));
            etPassword.requestFocus();
        } else if (!Utils.isValidPassword(password)) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_wrong_password));
            etPassword.requestFocus();
        } else if (TextUtils.isEmpty(repeatPassword)) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_enter_confirm_password));
            etRepeatPassword.requestFocus();
        } else if (!password.equals(repeatPassword)) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_confirm_password));
            etRepeatPassword.requestFocus();
        }
//        else if (!cbTermncondition.isChecked()) {
//            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.val_condition));
//        }


        else {

           //open pop up  for sequrity permit from user

            displayDialog(this, getString(R.string.app_name), getString(R.string
                    .privacy_msg));

        }
    }

    public  void displayDialog(final Context context, final String title, final String message) {
        final String firstName = etFirstName.getText().toString().trim();
        final String lastName = etLastName.getText().toString().trim();
        final String email = etEmail.getText().toString().trim();
        final String password = etPassword.getText().toString().trim();
        final String repeatPassword = etRepeatPassword.getText().toString().trim();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Bundle bundle = new Bundle();
                bundle.putString(WSConstants.WS_KEY_FIRSTNAME, firstName);
                bundle.putString(WSConstants.WS_KEY_LASTNAME, lastName);
                bundle.putString(WSConstants.WS_KEY_EMAIL, email);
                bundle.putString(WSConstants.WS_KEY_PASSWORD, password);
                callAsyncRegistration(bundle);
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(RegistrationActivity.this, view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        final int id = view.getId();
        switch (id) {
            case R.id.activity_registration_tv_create_new_account:
                validation();
                break;
            case R.id.activity_registration_tv_terms_condition:
                openStaticPage("termsconditions");

                break;
            case R.id.activity_registration_tv_privacy_policy:
                openStaticPage("privacypolicy");
                break;
            case R.id.activity_registration_iv_close:
                finish();
                break;

        }
    }
    private void openStaticPage(String pageCode){
        Intent iStaticPage=new Intent(RegistrationActivity.this,StaticPageActivity.class);
        iStaticPage.putExtra("pageCode",pageCode);
        startActivity(iStaticPage);

    }

    /**
     * Called for perform user Login
     *
     * @param bundle
     */
    private void callAsyncRegistration(Bundle bundle) {
        if (Utils.isNetworkAvailable(this)) {
            if (asyncRegistration != null && asyncRegistration.getStatus() == AsyncTask.Status.PENDING) {
                asyncRegistration.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (asyncRegistration == null || asyncRegistration.getStatus() == AsyncTask.Status.FINISHED) {
                asyncRegistration = new AsyncRegistration(bundle);
                asyncRegistration.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.alert_no_internet));
        }
    }

    /**
     * call login web service
     */
    private class AsyncRegistration extends AsyncTask<Void, Void, Void> {

        private Bundle bundle;
        private ProgressDialog progressDialog;
        private WSRegistration wsRegistration;

        public AsyncRegistration(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(RegistrationActivity.this);
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wsRegistration = new WSRegistration(RegistrationActivity.this);
                wsRegistration.executeService(bundle);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            super.onPostExecute(aVoid);
            if (isDestroyed()) {
                return;
            }
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (!isCancelled()) {

                if (wsRegistration.getCode() == WSConstants.STATUS_SUCCESS) {
                    Utils.displayDialogFinishActivity(RegistrationActivity.this, getString(R.string.app_name),
                            wsRegistration.getMessage());
//                    finish();
                }
                else if (wsRegistration.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(RegistrationActivity.this, getString(R.string.app_name), wsRegistration.getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(RegistrationActivity.this, getString(R.string
                                    .app_name),
                            getString(R.string.something_went_wrong_msg));

                }
            }
        }
    }
}
