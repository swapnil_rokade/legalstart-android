package com.adaptingsocial.lawyerapp.view;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsStaticPages;

/**
 * Created by indianic on 24/07/17.
 */

public class StaticPageActivity extends BaseActivity {
    private StaticPageAsyncTask staticPageAsyncTask;
    private WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_page);
        initializeComponents();
    }
    protected void initializeComponents() {
        webView= (WebView) findViewById(R.id.activity_static_page_wv_content);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String pageCode = extras.getString("pageCode");
            callAsyncStaticPage(pageCode);
        }

    }

    private void callAsyncStaticPage(String pageCode) {
        if (Utils.isNetworkAvailable(this)) {
            if (staticPageAsyncTask != null && staticPageAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                staticPageAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (staticPageAsyncTask == null || staticPageAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                staticPageAsyncTask = new StaticPageAsyncTask(pageCode);
                staticPageAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.alert_no_internet));
        }
    }

    public class StaticPageAsyncTask extends AsyncTask<Void, Void, Void> {
        private WsStaticPages wsStaticPages;
        private ProgressDialog progressDialog;
        private String pageCode;

        public StaticPageAsyncTask(String pageCode) {
            this.pageCode = pageCode;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(StaticPageActivity.this);
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            wsStaticPages = new WsStaticPages(StaticPageActivity.this);
            wsStaticPages.executeService(pageCode);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (wsStaticPages.getCode() == WSConstants.STATUS_SUCCESS) {
                webView.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        view.loadUrl(url);
                        return true;
                    }
                });
                webView.loadData(wsStaticPages.getPageContent(), "text/html; charset=UTF-8", null);

            }
            else if (wsStaticPages.getCode() == WSConstants.STATUS_FAIL) {
                Utils.displayDialog(StaticPageActivity.this, getString(R.string.app_name),
                        wsStaticPages.getMessage());

            }
            else {
                Utils.displayDialogWithPopBackStack(StaticPageActivity.this, getString(R.string
                        .app_name), getString(R.string.something_went_wrong_msg));
            }

        }
    }

}
