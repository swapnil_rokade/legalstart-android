package com.adaptingsocial.lawyerapp.view;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WSForgotPassword;

public class ForgotPasswordActivity extends BaseActivity {

    private EditText etEmail;
    private TextView tvForgotPassword;
    private TextView tvBacktoSignIn;
    private ImageView ivClose;
    private long mLastClickTime = 0;
    private AsyncForgotPassword asyncGetForgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initializeComponents();
    }

    protected void initializeComponents() {

        etEmail = (EditText) findViewById(R.id.activity_forgot_password_et_email);
        tvBacktoSignIn = (TextView) findViewById(R.id.activity_forgot_password_tv_back_to_sign_in);
        tvForgotPassword = (TextView) findViewById(R.id.activity_forgot_password_tv_forgot_password);
        ivClose = (ImageView) findViewById(R.id.activity_forgot_password_iv_close);
        ivClose.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        tvBacktoSignIn.setOnClickListener(this);
    }


    private void validation() {
        final String email = etEmail.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_enter_email));
            etEmail.requestFocus();
        } else if (!Utils.isValidEmail(email)) {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.err_msg_please_varify_your_email));
            etEmail.requestFocus();
        } else {
            Bundle bundle = new Bundle();
            bundle.putString(WSConstants.WS_KEY_EMAIL, email);
            callAsyncGetForgotPassword(bundle);
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(ForgotPasswordActivity.this, view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        final int id = view.getId();
        switch (id) {
            case R.id.activity_forgot_password_iv_close:
                finish();
                break;
            case R.id.activity_forgot_password_tv_forgot_password:
                validation();
                break;
            case R.id.activity_forgot_password_tv_back_to_sign_in:
                finish();
                break;
        }
    }

    /**
     * Called for perform user Login
     *
     * @param bundle
     */
    private void callAsyncGetForgotPassword(Bundle bundle) {
        if (Utils.isNetworkAvailable(this)) {
            if (asyncGetForgotPassword != null && asyncGetForgotPassword.getStatus() == AsyncTask.Status.PENDING) {
                asyncGetForgotPassword.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (asyncGetForgotPassword == null || asyncGetForgotPassword.getStatus() == AsyncTask.Status.FINISHED) {
                asyncGetForgotPassword = new AsyncForgotPassword(bundle);
                asyncGetForgotPassword.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(this, getString(R.string.app_name), getString(R.string.alert_no_internet));
        }
    }

    /**
     * call login web service
     */
    private class AsyncForgotPassword extends AsyncTask<Void, Void, Void> {

        private Bundle bundle;
        private WSForgotPassword wsForgotPassword;
        private ProgressDialog progressDialog;

        public AsyncForgotPassword(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ForgotPasswordActivity.this);
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wsForgotPassword = new WSForgotPassword(ForgotPasswordActivity.this);
                wsForgotPassword.executeService(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (isDestroyed()) {
                return;
            }
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (!isCancelled()) {
                if (wsForgotPassword.getCode() == WSConstants.STATUS_SUCCESS) {
                    Utils.displayDialogFinishActivity(ForgotPasswordActivity.this, getString(R.string.app_name), wsForgotPassword.getMessage());
                } else if (wsForgotPassword.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(ForgotPasswordActivity.this, getString(R.string.app_name), wsForgotPassword.getMessage());
                } else {
                    Utils.displayDialog(ForgotPasswordActivity.this, getString(R.string.app_name), wsForgotPassword.getMessage());
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (asyncGetForgotPassword != null && asyncGetForgotPassword.getStatus() == AsyncTask.Status.RUNNING) {
            asyncGetForgotPassword.cancel(true);
        }
    }

}
