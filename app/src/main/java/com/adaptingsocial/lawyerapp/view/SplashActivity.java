package com.adaptingsocial.lawyerapp.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;
import android.widget.VideoView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;

/*************************************************************************************
 * Class Name: SplashActivity
 * Created By: lbamarnani
 * Date:20-APRIL-17
 * Modified By:
 * Modified Date:
 * Purpose: This class is used for showing splash screen which stay
 * here approx 3 second.
 *************************************************************************************/
public class SplashActivity extends BaseActivity {
    final String TAG = this.getClass().getSimpleName();
    private VideoView videoView;
    private int stopPosition;
    private TextView tvSkip;
    private long mLastClickTime = 0;
    private ProgressDialog progressDialog;
    private PreferenceUtils preferenceUtils;
    private Snackbar snackbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        preferenceUtils = new PreferenceUtils(SplashActivity.this);
        //Text Scroll for company_detail scroll on
//        ((TextView)findViewById(R.id.tv_info_about_comp)).setMovementMethod(new ScrollingMovementMethod());
        playVideo(0);
//        openHomeScreen();

    }

    private void playVideo(int position) {
        tvSkip = (TextView) findViewById(R.id.activity_splash_tv_skip);
        tvSkip.setOnClickListener(SplashActivity.this);
        videoView = (VideoView) findViewById(R.id.activity_splash_vv_splash);
        final Uri path4 = Uri.parse("android.resource://" + SplashActivity.this.getPackageName()
                + "/" + R.raw.city);
        videoView.setVideoURI(path4);
        videoView.seekTo(position);
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                final PreferenceUtils preferenceUtils = new PreferenceUtils(SplashActivity.this);
                if (preferenceUtils.getBoolean(preferenceUtils.KEY_USER_LOGIN)) {
                    openHomeScreen();

                } else {
                    openLoginActivity();


                }

            }
        });

    }

    private void openLoginActivity() {
        final Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();

    }

    private void openHomeScreen() {
        final Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopPosition = videoView.getCurrentPosition(); //stopPosition is an int
        videoView.pause();
        AppLog.showLogD(TAG, "ON PAUSE CALLED");
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.start();
        AppLog.showLogD(TAG, "ON PAUSE CALLED");

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(SplashActivity.this, view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        if (view == tvSkip) {
            preferenceUtils = new PreferenceUtils(SplashActivity.this);
            if (preferenceUtils.getBoolean(preferenceUtils.KEY_USER_LOGIN)) {
                openHomeScreen();

            } else {
                openLoginActivity();


            }

        }
    }

}
