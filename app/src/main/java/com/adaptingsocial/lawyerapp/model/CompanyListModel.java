package com.adaptingsocial.lawyerapp.model;

import android.os.Parcel;
import android.os.Parcelable;


public class CompanyListModel implements Parcelable {
    private String companyId;
    private String companyTitle;
    private String companyAddress;
    private String companyContact;
    private String companyEmail;
    private String latitude;
    private String longitude;
    private String companyImage;
    private String companyBackgroundImage;
    private String companyDescreption;
    private String companyField;

    public String getCompanyField() {
        return companyField;
    }

    public void setCompanyField(String companyField) {
        this.companyField = companyField;
    }


    public CompanyListModel() {
    }


    protected CompanyListModel(Parcel in) {
        companyId = in.readString();
        companyTitle = in.readString();
        companyAddress = in.readString();
        companyContact = in.readString();
        companyEmail = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        companyImage = in.readString();
        companyDescreption = in.readString();
        companyBackgroundImage = in.readString();
        companyField = in.readString();
    }

    public static final Creator<CompanyListModel> CREATOR = new Creator<CompanyListModel>() {
        @Override
        public CompanyListModel createFromParcel(Parcel in) {
            return new CompanyListModel(in);
        }

        @Override
        public CompanyListModel[] newArray(int size) {
            return new CompanyListModel[size];
        }
    };

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getCompanyImage() {
        return companyImage;
    }

    public void setCompanyImage(String companyImage) {
        this.companyImage = companyImage;
    }

    public String getCompanyDescreption() {
        return companyDescreption;
    }

    public void setCompanyDescreption(String companyDescreption) {
        this.companyDescreption = companyDescreption;
    }

    public String getCompanyBackgroundImage() {
        return companyBackgroundImage;
    }

    public void setCompanyBackgroundImage(String companyBackgroundImage) {
        this.companyBackgroundImage = companyBackgroundImage;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyTitle;
    }

    public void setCompanyTitle(String companyTitle) {
        this.companyTitle = companyTitle;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyContact() {
        return companyContact;
    }

    public void setCompanyContact(String companyContact) {
        this.companyContact = companyContact;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(companyId);
        dest.writeString(companyTitle);
        dest.writeString(companyAddress);
        dest.writeString(companyContact);
        dest.writeString(companyEmail);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(companyImage);
        dest.writeString(companyDescreption);
        dest.writeString(companyBackgroundImage);
        dest.writeString(companyField);
    }
}
