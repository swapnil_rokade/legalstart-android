package com.adaptingsocial.lawyerapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.jivesoftware.smackx.carbons.packet.CarbonExtension;

/**
 * Created by indianic on 03/07/17.
 */

public class QuestionModel implements Parcelable {
    public static final Creator<QuestionModel> CREATOR = new Creator<QuestionModel>() {
        @Override
        public QuestionModel createFromParcel(Parcel in) {
            return new QuestionModel(in);
        }

        @Override
        public QuestionModel[] newArray(int size) {
            return new QuestionModel[size];
        }
    };
    private String id;
    private String documentId;
    private String qtype;
    private String question;
    private String qoption;
    private String status;
    private String qtypename;
    private String qtypecode;
    private String answer = "";
    private String flow_finish;
    private String[] answers;
    private String textfield_open;

    public String getMultipleotherFieldanswer() {
        return multipleotherFieldanswer;
    }

    public void setMultipleotherFieldanswer(String multipleotherFieldanswer) {
        this.multipleotherFieldanswer = multipleotherFieldanswer;
    }

    public String multipleotherFieldanswer;
    public String getTextfield_open() {
        return textfield_open;
    }

    public String getMultipleotherField() {
        return multipleotherField;
    }

    public void setMultipleotherField(String multipleotherField) {
        this.multipleotherField = multipleotherField;
    }

    public String multipleotherField;

    public void setTextfield_open(String textfield_open) {
        this.textfield_open = textfield_open;
    }



    public String getAdd_another() {
        return add_another;
    }

    public void setAdd_another(String add_another) {
        this.add_another = add_another;
    }

    private  String add_another;

    public String getFlow_finish() {
        return flow_finish;
    }

    public void setFlow_finish(String flow_finish) {
        this.flow_finish = flow_finish;
    }


    public QuestionModel(Parcel in) {
        id = in.readString();
        documentId = in.readString();
        qtype = in.readString();
        question = in.readString();
        qoption = in.readString();
        status = in.readString();
        qtypename = in.readString();
        qtypecode = in.readString();
        answer = in.readString();
        answers = in.createStringArray();
        flow_finish = in.readString();
        add_another=in.readString();
        multipleotherField=in.readString();
    }


    public QuestionModel() {

    }

    public String[] getAnswers() {
        return answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(documentId);
        dest.writeString(qtype);
        dest.writeString(question);
        dest.writeString(qoption);
        dest.writeString(status);
        dest.writeString(qtypename);
        dest.writeString(qtypecode);
        dest.writeString(answer);
        dest.writeStringArray(answers);
        dest.writeString(flow_finish);
        dest.writeString(add_another);
        dest.writeString(multipleotherField);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getQtype() {
        return qtype;
    }

    public void setQtype(String qtype) {
        this.qtype = qtype;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQoption() {
        return qoption;
    }

    public void setQoption(String qoption) {
        this.qoption = qoption;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQtypename() {
        return qtypename;
    }

    public void setQtypename(String qtypename) {
        this.qtypename = qtypename;
    }

    public String getQtypecode() {
        return qtypecode;
    }

    public void setQtypecode(String qtypecode) {
        this.qtypecode = qtypecode;
    }


}
