package com.adaptingsocial.lawyerapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * CompanyCategoryListModel class created.
 */

public class CompanyCategoryListModel implements Parcelable{
    private String companyCategoryId;
    private String companyCategoryName;
    private boolean isSelected;

    public CompanyCategoryListModel() {
    }

    protected CompanyCategoryListModel(Parcel in) {
        companyCategoryId = in.readString();
        companyCategoryName = in.readString();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<CompanyCategoryListModel> CREATOR = new Creator<CompanyCategoryListModel>() {
        @Override
        public CompanyCategoryListModel createFromParcel(Parcel in) {
            return new CompanyCategoryListModel(in);
        }

        @Override
        public CompanyCategoryListModel[] newArray(int size) {
            return new CompanyCategoryListModel[size];
        }
    };

    public String getCompanyCategoryId() {
        return companyCategoryId;
    }

    public void setCompanyCategoryId(String companyCategoryId) {
        this.companyCategoryId = companyCategoryId;
    }

    public String getCompanyCategoryName() {
        return companyCategoryName;
    }

    public void setCompanyCategoryName(String companyCategoryName) {
        this.companyCategoryName = companyCategoryName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(companyCategoryId);
        dest.writeString(companyCategoryName);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }
}
