package com.adaptingsocial.lawyerapp.model;

import java.util.ArrayList;

public class DocumentQuestionModel {
    private String id;
    private String qtype;
    private String documentId;
    private String question;
    private String qoption;
    private String status;
    private String qtypename;
    private String qtypecode;
    private String answer = "";
    private String[] answers;
    private ArrayList<QuestionOptionModel> optionList = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQtype() {
        return qtype;
    }

    public void setQtype(String qtype) {
        this.qtype = qtype;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQoption() {
        return qoption;
    }

    public void setQoption(String qoption) {
        this.qoption = qoption;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQtypename() {
        return qtypename;
    }

    public void setQtypename(String qtypename) {
        this.qtypename = qtypename;
    }

    public String getQtypecode() {
        return qtypecode;
    }

    public void setQtypecode(String qtypecode) {
        this.qtypecode = qtypecode;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String[] getAnswers() {
        return answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }

    public ArrayList<QuestionOptionModel> getOptionList() {
        return optionList;
    }

    public void setOptionList(ArrayList<QuestionOptionModel> optionList) {
        this.optionList = optionList;
    }
}
