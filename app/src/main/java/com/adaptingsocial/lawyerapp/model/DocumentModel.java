package com.adaptingsocial.lawyerapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by indianic on 25/04/17.
 */

public class DocumentModel implements  Parcelable{
    private String documentName;
    private String date;
    private String uploadedBy;
    private String imageExt;
    private String filePath;
    private String documentId;
    private String userid;
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }



    protected DocumentModel(Parcel in) {
        documentName = in.readString();
        date = in.readString();
        uploadedBy = in.readString();
        imageExt = in.readString();
        filePath = in.readString();
        documentId = in.readString();
        userid = in.readString();
    }

    public static final Creator<DocumentModel> CREATOR = new Creator<DocumentModel>() {
        @Override
        public DocumentModel createFromParcel(Parcel in) {
            return new DocumentModel(in);
        }

        @Override
        public DocumentModel[] newArray(int size) {
            return new DocumentModel[size];
        }
    };

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


    public String getImageExt() {
        return imageExt;
    }

    public void setImageExt(String imageExt) {
        this.imageExt = imageExt;
    }


    public DocumentModel() {

    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(String uploadedBy) {
        this.uploadedBy = uploadedBy;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(documentName);
        dest.writeString(date);
        dest.writeString(uploadedBy);
        dest.writeString(imageExt);
        dest.writeString(filePath);
        dest.writeString(documentId);
        dest.writeString(userid);
    }
}
