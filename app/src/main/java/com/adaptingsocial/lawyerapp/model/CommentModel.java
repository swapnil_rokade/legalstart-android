package com.adaptingsocial.lawyerapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by indianic on 01/05/17.
 */

public class CommentModel implements Parcelable {
    private String userId;
    private String name;
    private String likeCounts;
    private String blogLike;
    private String blog_comment_id;

    public String getBlog_comment_id() {
        return blog_comment_id;
    }

    public void setBlog_comment_id(String blog_comment_id) {
        this.blog_comment_id = blog_comment_id;
    }


    public String getBlogLike() {
        return blogLike;
    }

    public void setBlogLike(String blogLike) {
        this.blogLike = blogLike;
    }


    public CommentModel() {

    }

    protected CommentModel(Parcel in) {
        userId = in.readString();
        name = in.readString();
        likeCounts = in.readString();
        profileImage = in.readString();
        comment = in.readString();
        date = in.readString();
        blogLike = in.readString();
        blog_comment_id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(name);
        dest.writeString(likeCounts);
        dest.writeString(profileImage);
        dest.writeString(comment);
        dest.writeString(date);
        dest.writeString(blogLike);
        dest.writeString(blog_comment_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CommentModel> CREATOR = new Creator<CommentModel>() {
        @Override
        public CommentModel createFromParcel(Parcel in) {
            return new CommentModel(in);
        }

        @Override
        public CommentModel[] newArray(int size) {
            return new CommentModel[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLikeCounts() {
        return likeCounts;
    }

    public void setLikeCounts(String likeCounts) {
        this.likeCounts = likeCounts;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String profileImage;
    private String comment;
    private String date;


}

