package com.adaptingsocial.lawyerapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class QuestionOptionModel implements Parcelable {
    public static final Creator<QuestionOptionModel> CREATOR = new Creator<QuestionOptionModel>() {
        @Override
        public QuestionOptionModel createFromParcel(Parcel in) {
            return new QuestionOptionModel(in);
        }

        @Override
        public QuestionOptionModel[] newArray(int size) {
            return new QuestionOptionModel[size];
        }
    };
    private String optionId;
    private String option;

    public QuestionOptionModel() {

    }

    protected QuestionOptionModel(Parcel in) {
        optionId = in.readString();
        option = in.readString();
    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(optionId);
        dest.writeString(option);
    }
}
