package com.adaptingsocial.lawyerapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by indianic on 03/07/17.
 */

public class QuesionCatagoryModel implements Parcelable {
    private String id;
    private String name;
    private String image;
    private String ammount;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void setAmmount(String ammount) {
        this.ammount = ammount;
    }


    public String getAmmount() {
        return ammount;
    }


    public QuesionCatagoryModel() {

    }

    protected QuesionCatagoryModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        image = in.readString();
        ammount = in.readString();
        description = in.readString();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeString(ammount);
        dest.writeString(description);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuesionCatagoryModel> CREATOR = new Creator<QuesionCatagoryModel>() {
        @Override
        public QuesionCatagoryModel createFromParcel(Parcel in) {
            return new QuesionCatagoryModel(in);
        }

        @Override
        public QuesionCatagoryModel[] newArray(int size) {
            return new QuesionCatagoryModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


}
