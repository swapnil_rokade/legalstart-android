package com.adaptingsocial.lawyerapp.model;


public class DrawerModel {
    private String title;
    private int image;

    public DrawerModel(String stitle, int image) {
        this.title = stitle;
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
