package com.adaptingsocial.lawyerapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by indianic on 23/06/17.
 */

public class SubscriptionModel implements Parcelable {
    private String id;
    private String title;
    private String ammount;
    private String description;

    public SubscriptionModel() {

    }

    protected SubscriptionModel(Parcel in) {
        id = in.readString();
        title = in.readString();
        ammount = in.readString();
        description = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(ammount);
        dest.writeString(description);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SubscriptionModel> CREATOR = new Creator<SubscriptionModel>() {
        @Override
        public SubscriptionModel createFromParcel(Parcel in) {
            return new SubscriptionModel(in);
        }

        @Override
        public SubscriptionModel[] newArray(int size) {
            return new SubscriptionModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAmmount() {
        return ammount;
    }

    public void setAmmount(String ammount) {
        this.ammount = ammount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
