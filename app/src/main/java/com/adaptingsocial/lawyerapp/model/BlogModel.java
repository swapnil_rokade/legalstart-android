package com.adaptingsocial.lawyerapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by indianic on 01/05/17.
 */

public class BlogModel implements Parcelable{
    private String blogId;
    private String blogName;
    private String blogDescription;
    private String blogImage;
    private String bolgDate;
    private ArrayList<CommentModel>commentModelArrayList;

    public ArrayList<CommentModel> getCommentModelArrayList() {
        return commentModelArrayList;
    }

    public void setCommentModelArrayList(ArrayList<CommentModel> commentModelArrayList) {
        this.commentModelArrayList = commentModelArrayList;
    }


    public BlogModel() {

    }

    protected BlogModel(Parcel in) {
        blogId = in.readString();
        blogName = in.readString();
        blogDescription = in.readString();
        blogImage = in.readString();
        bolgDate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(blogId);
        dest.writeString(blogName);
        dest.writeString(blogDescription);
        dest.writeString(blogImage);
        dest.writeString(bolgDate);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BlogModel> CREATOR = new Creator<BlogModel>() {
        @Override
        public BlogModel createFromParcel(Parcel in) {
            return new BlogModel(in);
        }

        @Override
        public BlogModel[] newArray(int size) {
            return new BlogModel[size];
        }
    };

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public String getBlogName() {
        return blogName;
    }

    public void setBlogName(String blogName) {
        this.blogName = blogName;
    }

    public String getBlogDescription() {
        return blogDescription;
    }

    public void setBlogDescription(String blogDescription) {
        this.blogDescription = blogDescription;
    }

    public String getBlogImage() {
        return blogImage;
    }

    public void setBlogImage(String blogImage) {
        this.blogImage = blogImage;
    }

    public String getBolgDate() {
        return bolgDate;
    }

    public void setBolgDate(String bolgDate) {
        this.bolgDate = bolgDate;
    }


}
