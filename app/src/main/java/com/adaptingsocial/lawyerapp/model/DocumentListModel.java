package com.adaptingsocial.lawyerapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * DocumentListModel class created.
 */

public class DocumentListModel implements Parcelable {
    private String documentId;
    private String documentName;
    private String documentDoc;
    private String documentType;
    private String documentAmt;
    private String userPaymentStaus;
    private Boolean isView =false;
    private String localFilePath="";
    private String users_document_status="";

    public String getUsers_document_status() {
        return users_document_status;
    }

    public void setUsers_document_status(String users_document_status) {
        this.users_document_status = users_document_status;
    }

    public String getLocalFilePath() {
        return localFilePath;
    }

    public void setLocalFilePath(String localFilePath) {
        this.localFilePath = localFilePath;
    }



    public Boolean getView() {
        return isView;
    }

    public void setView(Boolean view) {
        isView = view;
    }


    public String getDocumentDoc() {
        return documentDoc;
    }

    public void setDocumentDoc(String documentDoc) {
        this.documentDoc = documentDoc;
    }


    public String getUserPaymentStaus() {
        return userPaymentStaus;
    }

    public void setUserPaymentStaus(String userPaymentStaus) {
        this.userPaymentStaus = userPaymentStaus;
    }



    public DocumentListModel() {

    }

    protected DocumentListModel(Parcel in) {
        documentId = in.readString();
        documentName = in.readString();
        documentDoc = in.readString();
        documentType = in.readString();
        documentAmt = in.readString();
        userPaymentStaus = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(documentId);
        dest.writeString(documentName);
        dest.writeString(documentDoc);
        dest.writeString(documentType);
        dest.writeString(documentAmt);
        dest.writeString(userPaymentStaus);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DocumentListModel> CREATOR = new Creator<DocumentListModel>() {
        @Override
        public DocumentListModel createFromParcel(Parcel in) {
            return new DocumentListModel(in);
        }

        @Override
        public DocumentListModel[] newArray(int size) {
            return new DocumentListModel[size];
        }
    };

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentUrl() {
        return documentDoc;
    }

    public void setDocumentUrl(String documentDoc) {
        this.documentDoc = documentDoc;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentAmt() {
        return documentAmt;
    }

    public void setDocumentAmt(String documentAmt) {
        this.documentAmt = documentAmt;
    }
}
