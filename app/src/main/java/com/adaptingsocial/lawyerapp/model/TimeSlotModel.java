package com.adaptingsocial.lawyerapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * TimeSlotModel class created.
 */

public class TimeSlotModel implements Parcelable{

    private String name;
    private String id;
    private boolean isBooked;
    private String startTime;
    private String date;
    private String endTime;
    private boolean isSelected;


    public TimeSlotModel() {
    }

    protected TimeSlotModel(Parcel in) {
        name = in.readString();
        id = in.readString();
        isBooked = in.readByte() != 0;
        startTime = in.readString();
        date = in.readString();
        endTime = in.readString();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<TimeSlotModel> CREATOR = new Creator<TimeSlotModel>() {
        @Override
        public TimeSlotModel createFromParcel(Parcel in) {
            return new TimeSlotModel(in);
        }

        @Override
        public TimeSlotModel[] newArray(int size) {
            return new TimeSlotModel[size];
        }
    };

    public String getDate() {
        return date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isBooked() {
        return isBooked;
    }

    public void setBooked(boolean booked) {
        isBooked = booked;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(id);
        dest.writeByte((byte) (isBooked ? 1 : 0));
        dest.writeString(startTime);
        dest.writeString(date);
        dest.writeString(endTime);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }
}
