package com.adaptingsocial.lawyerapp.model;

public class MyDocumentModel {
    private String documentId;
    private String documentName;
    private String documentFile;
    private String localFilePath="";
    private Boolean isView =false;
    public Boolean getView() {
        return isView;
    }

    public void setView(Boolean view) {
        isView = view;
    }
    public String getLocalFilePath() {
        return localFilePath;
    }

    public void setLocalFilePath(String localFilePath) {
        this.localFilePath = localFilePath;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentFile() {
        return documentFile;
    }

    public void setDocumentFile(String documentFile) {
        this.documentFile = documentFile;
    }
}
