package com.adaptingsocial.lawyerapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by indianic on 10/08/17.
 */

public class QuestionDescriptionDetailModal implements Parcelable {
    private String question_state_category_id;
    private String state_name;
    private String ammount;

    public String getAmmount() {
        return ammount;
    }

    public void setAmmount(String ammount) {
        this.ammount = ammount;
    }


    private String other_information;
    private String additionaCharge="";
    public String getAdditionaCharge() {
        return additionaCharge;
    }

    public void setAdditionaCharge(String additionaCharge) {
        this.additionaCharge = additionaCharge;
    }



    public QuestionDescriptionDetailModal() {

    }

    protected QuestionDescriptionDetailModal(Parcel in) {
        question_state_category_id = in.readString();
        state_name = in.readString();
        ammount = in.readString();
        other_information = in.readString();
        additionaCharge = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(question_state_category_id);
        dest.writeString(state_name);
        dest.writeString(other_information);
        dest.writeString(additionaCharge);
        dest.writeString(ammount);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuestionDescriptionDetailModal> CREATOR = new Creator<QuestionDescriptionDetailModal>() {
        @Override
        public QuestionDescriptionDetailModal createFromParcel(Parcel in) {
            return new QuestionDescriptionDetailModal(in);
        }

        @Override
        public QuestionDescriptionDetailModal[] newArray(int size) {
            return new QuestionDescriptionDetailModal[size];
        }
    };

    public String getQuestion_state_category_id() {
        return question_state_category_id;
    }

    public void setQuestion_state_category_id(String question_state_category_id) {
        this.question_state_category_id = question_state_category_id;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }




    public String getOther_information() {
        return other_information;
    }

    public void setOther_information(String other_information) {
        this.other_information = other_information;
    }


}
