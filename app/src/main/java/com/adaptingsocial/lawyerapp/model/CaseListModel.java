package com.adaptingsocial.lawyerapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by indianic on 25/04/17.
 */

public class CaseListModel implements Parcelable {
    private String caseId;
    private String caseTitle;
    private String courtName;
    private String caseNo;
    private String caseBehalfOf;
    private String userName;
    private String concerns;
    private String documentType;
    private String agreement;




    private ArrayList<DocumentModel> documentModelArrayList;

    public CaseListModel() {

    }

    protected CaseListModel(Parcel in) {
        caseId = in.readString();
        caseTitle = in.readString();
        courtName = in.readString();
        caseNo = in.readString();
        caseBehalfOf = in.readString();
        userName = in.readString();
        concerns = in.readString();
        documentType = in.readString();
        agreement = in.readString();
        documentModelArrayList = in.createTypedArrayList(DocumentModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(caseId);
        dest.writeString(caseTitle);
        dest.writeString(courtName);
        dest.writeString(caseNo);
        dest.writeString(caseBehalfOf);
        dest.writeString(userName);
        dest.writeString(concerns);
        dest.writeString(documentType);
        dest.writeString(agreement);
        dest.writeTypedList(documentModelArrayList);
    }


    public String getAgreement() {
        return agreement;
    }

    public void setAgreement(String agreement) {
        this.agreement = agreement;
    }

    public ArrayList<DocumentModel> getDocumentModelArrayList() {
        return documentModelArrayList;
    }

    public void setDocumentModelArrayList(ArrayList<DocumentModel> documentModelArrayList) {
        this.documentModelArrayList = documentModelArrayList;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getConcerns() {
        return concerns;
    }

    public void setConcerns(String concerns) {
        this.concerns = concerns;
    }

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getCaseTitle() {
        return caseTitle;
    }

    public void setCaseTitle(String caseTitle) {
        this.caseTitle = caseTitle;
    }

    public String getCourtName() {
        return courtName;
    }

    public void setCourtName(String courtName) {
        this.courtName = courtName;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getCaseBehalfOf() {
        return caseBehalfOf;
    }

    public void setCaseBehalfOf(String caseBehalfOf) {
        this.caseBehalfOf = caseBehalfOf;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CaseListModel> CREATOR = new Creator<CaseListModel>() {
        @Override
        public CaseListModel createFromParcel(Parcel in) {
            return new CaseListModel(in);
        }

        @Override
        public CaseListModel[] newArray(int size) {
            return new CaseListModel[size];
        }
    };
}
