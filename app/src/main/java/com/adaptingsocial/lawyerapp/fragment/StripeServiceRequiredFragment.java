package com.adaptingsocial.lawyerapp.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsStripServiceRequiredPayment;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

public class StripeServiceRequiredFragment extends BaseFragment {
    private Button btnPay;
    private CardInputWidget mCardInputWidget;
    private long mLastClickTime = 0;
    private ProgressDialog pd;
    private AsyncPayment asyncPayment;
    private Bundle mBundle;
    private TextView tvAmount;
    private String amount = "";
    private String itemName = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_stripe, container, false);

    }

    @Override
    protected void initializeComponent(View view) {
        btnPay = (Button) view.findViewById(R.id.fragment_stripe_btn_pay);
        mCardInputWidget = (CardInputWidget) view.findViewById(R.id
                .fragment_stripe_card_input_widget);
        tvAmount = (TextView) view.findViewById(R.id.fragment_stripe_tv_amount);
        btnPay.setOnClickListener(this);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        mBundle = getArguments();
        if (mBundle != null) {
            amount = mBundle.getString("ammount");
            itemName = mBundle.getString("itemName");
            tvAmount.setText("$ " + this.amount);
        }
        final Card cardToSave = mCardInputWidget.getCard();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(getActivity(), view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        final int id = view.getId();
        switch (id) {
            case R.id.fragment_stripe_btn_pay:
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.loading));
                pd.show();
                final Card cardToSave = mCardInputWidget.getCard();
                if (cardToSave == null) {
                    pd.dismiss();
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.card_invalid));


                } else {
                    boolean validation = cardToSave.validateCard();
                    Log.d("Result", "validation is" + validation);
                    Stripe stripe = new Stripe(getActivity(), Constants.PUBLISHABLE_KEY);
                    stripe.createToken(
                            cardToSave,
                            new TokenCallback() {
                                public void onSuccess(Token token) {
                                    // Send token to your server
                                    Log.d("Result", "validation true token is" + token.getId());
                                    callAsyncPayment(token.getId());
                                }

                                public void onError(Exception error) {
                                    Log.d("Result", "token null");
                                    pd.dismiss();
                                    Utils.displayDialog(getActivity(), getString(R.string
                                            .app_name), getString(R.string.token_error));
                                }
                            });


                }

                break;

        }
    }

    private void callAsyncPayment(String token) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncPayment != null && asyncPayment.getStatus() == AsyncTask.Status.PENDING) {
                asyncPayment.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (asyncPayment == null || asyncPayment.getStatus() == AsyncTask.Status.FINISHED) {
                asyncPayment = new AsyncPayment(token);
                asyncPayment.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
                    .alert_no_internet));
        }
    }


    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, getString(R.string.payment_process), 0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }

    private class AsyncPayment extends AsyncTask<Void, Void, Void> {
        private String token;
        String msg = "";
        private WsStripServiceRequiredPayment wsStripServiceRequiredPayment;
        private String chargeId = "";

        public AsyncPayment(String token) {
            this.token = token;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Void doInBackground(Void... params) {
            wsStripServiceRequiredPayment = new WsStripServiceRequiredPayment(getActivity());
            wsStripServiceRequiredPayment.executeService(token, amount);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (wsStripServiceRequiredPayment.getCode() == WSConstants.STATUS_SUCCESS) {
                pd.dismiss();
                displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name),
                        wsStripServiceRequiredPayment.getMessage());
            } else if (wsStripServiceRequiredPayment.getCode() == WSConstants.STATUS_FAIL) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), wsStripServiceRequiredPayment.getMessage());

            } else {
                Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string
                                .app_name),
                        getString(R.string.something_went_wrong_msg));
            }


        }
    }

    public void displayDialogWithPopBackStack(final Context context, final String title, final String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);

        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getFragmentManager().popBackStack();
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity
                        .RESULT_OK, null);
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }
}
