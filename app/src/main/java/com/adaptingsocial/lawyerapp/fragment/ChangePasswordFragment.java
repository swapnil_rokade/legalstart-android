package com.adaptingsocial.lawyerapp.fragment;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSChangePassword;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;

public class ChangePasswordFragment extends BaseFragment {

    private EditText etOldPassword;
    private EditText etNewPassword;
    private EditText etRepeatPassword;
    private TextView tvChangePassword;
    private ImageView ivClose;
    private long mLastClickTime = 0;
    private AsyncChangePassword asyncGetChangePassword;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_change_password, container, false);
    }

    @Override
    protected void initializeComponent(View view) {
        etOldPassword = (EditText) view.findViewById(R.id.fragment_change_password_et_old_password);
        etNewPassword = (EditText) view.findViewById(R.id.fragment_change_password_et_new_password);
        etRepeatPassword = (EditText) view.findViewById(R.id.fragment_change_password_et_repeat_password);
        ivClose = (ImageView) view.findViewById(R.id.fragment_change_password_iv_close);
        tvChangePassword = (TextView) view.findViewById(R.id.fragment_change_password_tv_change_password);
        tvChangePassword.setOnClickListener(this);
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    private void validation() {

        final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
        final String oldPassword = etOldPassword.getText().toString().trim();
        final String newPassword = etNewPassword.getText().toString().trim();
        final String repeatPassword = etRepeatPassword.getText().toString().trim();

        if (TextUtils.isEmpty(oldPassword)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_enter_password));
            etOldPassword.requestFocus();
        } else if (oldPassword.length() < 6) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_password_length_less_6));
            etOldPassword.requestFocus();
        } else if (!Utils.isValidPassword(oldPassword)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_wrong_password));
            etOldPassword.requestFocus();
        } else if (TextUtils.isEmpty(newPassword)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_enter_password));
            etNewPassword.requestFocus();
        } else if (newPassword.length() < 6) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_password_length_less_6));
            etNewPassword.requestFocus();
        } else if (!Utils.isValidPassword(newPassword)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_wrong_password));
            etNewPassword.requestFocus();
        } else if (TextUtils.isEmpty(repeatPassword)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_enter_confirm_password));
            etRepeatPassword.requestFocus();
        } else if (!newPassword.equals(repeatPassword)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_confirm_password));
            etRepeatPassword.requestFocus();
        } else {
            Bundle bundle = new Bundle();
            bundle.putString(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
            bundle.putString(WSConstants.WS_KEY_OLD_PASSWORD, oldPassword);
            bundle.putString(WSConstants.WS_KEY_NEW_PASSWORD, newPassword);
            callAsyncGetChangePassword(bundle);
        }
    }
    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.str_change_password),0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }
    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(getActivity(), view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        final int id = view.getId();
        switch (id) {
            case R.id.fragment_change_password_iv_close:
                getActivity().finish();
                break;
            case R.id.fragment_change_password_tv_change_password:
                validation();
                break;
        }
    }

    /**
     * Called for perform user change password
     *
     * @param bundle
     */
    private void callAsyncGetChangePassword(Bundle bundle) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncGetChangePassword != null && asyncGetChangePassword.getStatus() == AsyncTask.Status.PENDING) {
                asyncGetChangePassword.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (asyncGetChangePassword == null || asyncGetChangePassword.getStatus() == AsyncTask.Status.FINISHED) {
                asyncGetChangePassword = new AsyncChangePassword(bundle);
                asyncGetChangePassword.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.alert_no_internet));
        }
    }

    /**
     * call change password web service
     */
    private class AsyncChangePassword extends AsyncTask<Void, Void, Void> {

        private Bundle bundle;
        private WSChangePassword wsChangePassword;
        private ProgressDialog progressDialog;

        public AsyncChangePassword(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wsChangePassword = new WSChangePassword(getActivity());
                wsChangePassword.executeService(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (getActivity().isDestroyed()) {
                return;
            }
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (!isCancelled()) {
                if (wsChangePassword.getCode() == WSConstants.STATUS_SUCCESS) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsChangePassword.getMessage());
                } else if (wsChangePassword.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsChangePassword.getMessage());
                } else {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsChangePassword.getMessage());
                }
            }
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (asyncGetChangePassword != null && asyncGetChangePassword.getStatus() == AsyncTask.Status.RUNNING) {
            asyncGetChangePassword.cancel(true);
        }
    }

}
