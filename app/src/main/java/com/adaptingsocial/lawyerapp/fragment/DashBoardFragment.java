package com.adaptingsocial.lawyerapp.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;


public class DashBoardFragment extends BaseFragment {
    private LinearLayout llAppointment;
    private LinearLayout llUploadDocument;
    private LinearLayout llStartupPros;
    private LinearLayout llCreateDocument;
    private LinearLayout llStartMyBusiness;
    private LinearLayout llChat;
    private LinearLayout llTheResource;
    private LinearLayout llEmailLawyer;
    private LinearLayout llMyDocument;
    private LinearLayout llRetainer;
    private long mLastClickTime = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();

    }

    @Override
    protected void initializeComponent(View view) {
        llAppointment = (LinearLayout) view.findViewById(R.id.fragment_dashboard_llAppointment);
        llUploadDocument = (LinearLayout) view.findViewById(R.id.fragment_dashboard_llUploadDocument);
        llStartupPros = (LinearLayout) view.findViewById(R.id.fragment_dashboard_llStartupPros);
        llCreateDocument = (LinearLayout) view.findViewById(R.id.fragment_dashboard_llCreateDocument);
        llStartMyBusiness = (LinearLayout) view.findViewById(R.id.fragment_dashboard_llStartMyBusiness);
        llChat = (LinearLayout) view.findViewById(R.id.fragment_dashboard_llChat);
        llTheResource = (LinearLayout) view.findViewById(R.id.fragment_dashboard_llResource);
        llEmailLawyer = (LinearLayout) view.findViewById(R.id.fragment_dashboard_llEmailLawyer);
        llMyDocument = (LinearLayout) view.findViewById(R.id.fragment_dashboard_llMyDocument);
        llRetainer = (LinearLayout) view.findViewById(R.id.fragment_dashboard_llRetainer);
        llAppointment.setOnClickListener(this);
        llUploadDocument.setOnClickListener(this);
        llStartupPros.setOnClickListener(this);
        llCreateDocument.setOnClickListener(this);
        llStartMyBusiness.setOnClickListener(this);
        llChat.setOnClickListener(this);
        llTheResource.setOnClickListener(this);
        llEmailLawyer.setOnClickListener(this);
        llMyDocument.setOnClickListener(this);
        llRetainer.setOnClickListener(this);


        //        DashBoaredAdapter adapter = new DashBoaredAdapter(getActivity());
//        grid = (GridView) view.findViewById(R.id.gridview);
//        grid.setAdapter(adapter);
//        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//
//                PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
//                Boolean freeUser = preferenceUtils.getBoolean(preferenceUtils.KEY_APP_USER_TYPE);
//                if (position == 0) {
//                    if (freeUser) {
//                        Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
//                                .app_paid_msg));
//                    } else {
//
//                        displayDialogcCreateUser(getActivity(),
//                                getString(R.string.app_name),
//                                getString(R.string.alert_appointments), Constants.FROM_APPOINTMENT);
//
//                    }
//
//                } else if (position == 1) {
//                    if (freeUser) {
//                        Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
//                                .app_paid_msg));
//                    } else {
//                        final CaseListFragment caseListFragment = new CaseListFragment();
//                        setUpMainFragment(caseListFragment);
//                    }
//
//                } else if (position == 2) {
//                    if (freeUser) {
//                        Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
//                                .app_paid_msg));
//                    } else {
//                        final QuestionCategoryFragment questionCategoryFragment = new
// QuestionCategoryFragment();
//                        setUpMainFragment(questionCategoryFragment);
//                    }
//
//
//                } else if (position == 3) {
//                    final CompanyListFragment companyListFragment = new CompanyListFragment();
//                    setUpMainFragment(companyListFragment);
//
//
//                } else if (position == 4) {
//                    if (freeUser) {
//                        Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
//                                .app_paid_msg));
//                    } else {
//
//                        displayDialogcCreateUser(getActivity(),
//                                getString(R.string.app_name),
//                                getString(R.string.create_document_alert), Constants.FROM_CREATE_DOCUMENT);
//
//                    }
//
//
//                } else if (position == 5) {
//                    if (freeUser) {
//                        Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
//                                .app_paid_msg));
//                    } else {
//                        if (preferenceUtils.getString(preferenceUtils.KEY_DIALOG_ID)
//                                .equalsIgnoreCase("")) {
//                            final LetsChatFragment chatFragment = new LetsChatFragment();
//                            setUpMainFragment(chatFragment);
//                        } else {
//                            final ChatFragment chatFragment = new ChatFragment();
//                            setUpMainFragment(chatFragment);
//                        }
//
//
//                    }
//
//                } else if (position == 6) {
//                    final BlogListFragment blogsListFragment = new BlogListFragment();
//                    setUpMainFragment(blogsListFragment);
//
//                } else if (position == 7) {
//                    if (freeUser) {
//                        Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
//                                .app_paid_msg));
//                    } else {
//                        final EmailFragment emailFragment = new EmailFragment();
//                        setUpMainFragment(emailFragment);
//                    }
//
//
//                } else if (position == 8) {
//                    final SubscriptionFragment subscriptionFragment = new SubscriptionFragment();
//                    setUpMainFragment(subscriptionFragment);
//
//                } else if (position == 9) {
//                    final MyDocumentFragment myDocumentFragment = new MyDocumentFragment();
//                    setUpMainFragment(myDocumentFragment);
//
//                }
//
//            }
//        });

    }

    public void displayDialogCreateUser(final Context context, final String title, final String
            message, final int from) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(R.string.gotit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (from == Constants.FROM_APPOINTMENT) {
                    final AppointmentListFragment appointmentFragment = new AppointmentListFragment();
                    setUpMainFragment(appointmentFragment);
                } else {
                    final RequestDocumentFragment requestDocumentFragment = new RequestDocumentFragment();
                    setUpMainFragment(requestDocumentFragment);
                }


                dialog.dismiss();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }

    private void setUpMainFragment(Fragment nextFragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_home_container_main, nextFragment, nextFragment.getClass().getSimpleName()
        ).commit();
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false, "", 0);
            ((HomeActivity) getActivity()).showShadow();
            Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
            toolBar.getMenu().clear();

        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Utils.hideSoftKeyBoard(getActivity(), v);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        final int id = v.getId();
        PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
        Boolean freeUser = preferenceUtils.getBoolean(preferenceUtils.KEY_APP_USER_TYPE);
        switch (id) {
            case R.id.fragment_dashboard_llAppointment:
//                if (freeUser) {
//                    displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
//                            .app_paid_msg));
//                } else {
                    displayDialogCreateUser(getActivity(),
                            getString(R.string.app_name),
                            getString(R.string.alert_appointments), Constants.FROM_APPOINTMENT);

                //}
                break;
            case R.id.fragment_dashboard_llUploadDocument:
//                if (freeUser) {
//                    displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
//                            .app_paid_msg));
//                } else {
                    final CaseListFragment caseListFragment = new CaseListFragment();
                    setUpMainFragment(caseListFragment);
                //}

                break;
            case R.id.fragment_dashboard_llStartupPros:
                final CompanyListFragment companyListFragment = new CompanyListFragment();
                setUpMainFragment(companyListFragment);
                break;
            case R.id.fragment_dashboard_llCreateDocument:
//                if (freeUser) {
//                    displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
//                            .app_paid_msg));
//                } else {
                    displayDialogCreateUser(getActivity(),
                            getString(R.string.app_name),
                            getString(R.string.create_document_alert), Constants.FROM_CREATE_DOCUMENT);

                //}
                break;
            case R.id.fragment_dashboard_llStartMyBusiness:
//                if (freeUser) {
//                    displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
//                            .app_paid_msg));
//                } else {
                    final QuestionCategoryFragment questionCategoryFragment = new
                            QuestionCategoryFragment();
                    setUpMainFragment(questionCategoryFragment);
              //  }
                break;
            case R.id.fragment_dashboard_llChat:
//                if (freeUser) {
//                    displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
//                            .app_paid_msg));
//                } else {
                    if (preferenceUtils.getString(preferenceUtils.KEY_DIALOG_ID)
                            .equalsIgnoreCase("")) {
                        final LetsChatFragment chatFragment = new LetsChatFragment();
                        setUpMainFragment(chatFragment);
                    } else {
                        final ChatFragment chatFragment = new ChatFragment();
                        setUpMainFragment(chatFragment);
                    }


               // }
                break;
            case R.id.fragment_dashboard_llResource:
                final BlogListFragment blogListFragment = new BlogListFragment();
                setUpMainFragment(blogListFragment);
                break;
            case R.id.fragment_dashboard_llEmailLawyer:
//                if (freeUser) {
//                   displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
//                            .app_paid_msg));
//                } else {
                    final EmailFragment emailFragment = new EmailFragment();
                    setUpMainFragment(emailFragment);
               // }

                break;
            case R.id.fragment_dashboard_llMyDocument:
                final MyDocumentFragment myDocumentFragment = new MyDocumentFragment();
                setUpMainFragment(myDocumentFragment);

                break;
            case R.id.fragment_dashboard_llRetainer:
                final SubscriptionFragment subscriptionFragment = new SubscriptionFragment();
                setUpMainFragment(subscriptionFragment);
                break;
        }
    }


    public void displayDialog(final Context context, final String title, final String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(true);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(R.string.subscribe_now, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                final SubscriptionFragment subscriptionFragment = new SubscriptionFragment();
                setUpMainFragment(subscriptionFragment);
                ((HomeActivity)getActivity()).setSubscribeSelected();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
        posButton.setAllCaps(false);
    }
}
