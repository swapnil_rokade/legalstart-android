package com.adaptingsocial.lawyerapp.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.AddCaseAdapter;
import com.adaptingsocial.lawyerapp.model.DocumentModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.GetFilePathInternalStoarage;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.CustomCameraActivity;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsAddCase;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.adaptingsocial.lawyerapp.utill.Utils.displayDialog;

/**
 * Created by indianic on 24/04/17.
 */

public class AddCaseFragment extends BaseFragment {


    private ListView lvUploadedDocument;
    private ArrayList<DocumentModel> documentModelArrayList;
    private AddCaseAdapter addCaseAdapter;
    private static final int PICKFILE_REQUEST_CODE = 101;
    private LinearLayout llAttachDocument;
    private long mLastClickTime = 0;
    private EditText etCaseTitle;
    private EditText etPartiesInvoleved;
    private EditText etDocumentType;
    private EditText etCourtName;
    private EditText etConcerns;
    private TextView tvAddCase;
    private AddCaseAsyncTask addCaseAsyncTask;
    private Dialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_add_case, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();


    }

    @Override
    protected void initializeComponent(View view) {
        etCaseTitle = (EditText) view.findViewById(R.id.fragment_add_et_document_title);
        etDocumentType = (EditText) view.findViewById(R.id.fragment_add_et_document_type);
        etPartiesInvoleved = (EditText) view.findViewById(R.id.fragment_add_et_parties_involved);
        etConcerns = (EditText) view.findViewById(R.id.fragment_add_et_concerns);


        tvAddCase = (TextView) view.findViewById(R.id.fragment_add_tv_add_case);
        llAttachDocument = (LinearLayout) view.findViewById(R.id.fragment_add_ll_attach_document);
        lvUploadedDocument = (ListView) view.findViewById(R.id.fragment_add_lv_documents);
        documentModelArrayList = new ArrayList<>();
        addCaseAdapter = new AddCaseAdapter(getActivity(), documentModelArrayList);
        lvUploadedDocument.setAdapter(addCaseAdapter);
        llAttachDocument.setOnClickListener(this);
        tvAddCase.setOnClickListener(this);

    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, getString(R.string.str_new_document), 0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }

    public void openLocalStorage() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("return-data", true);
        startActivityForResult(

                Intent.createChooser(intent, "Complete action using"),
                PICKFILE_REQUEST_CODE);


    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(getActivity(), view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        final int id = view.getId();
        switch (id) {
            case R.id.fragment_add_ll_attach_document:
                openDialog();
//                if (documentModelArrayList.size() > Constants.ATTACH_DOCUMENT_LIMIT) {
//                    Utils.displayDialog(getActivity(), getString(R.string.app_name),
//                            getString(R.string.cant_attach_more_thn_10));
//                } else {
//                    if (checkSdCardPermissions()) {
//                        showFileChooser();
//                    } else {
//                        requestSdCardPermissions(Constants.WRITE_EXTERNAL_STORAGE_PERMISSION, false);
//                    }
//
//                }

                break;


            case R.id.fragment_add_tv_add_case:
                PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
                Boolean freeUser = preferenceUtils.getBoolean(preferenceUtils.KEY_APP_USER_TYPE);
                if (freeUser) {
                    displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.app_paid_msg));
                } else {
                    validateFields();
                    break;


                }
        }
    }

    public boolean checkSdCardPermissions() {
        boolean hasPermission = (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            return false;
        } else {
            return true;
        }

    }

    public void requestSdCardPermissions(int type, boolean isActivity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission
                .WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission
                    .WRITE_EXTERNAL_STORAGE}, type);
        } else {
            // Requesting permission clearly i.e for the first time.
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission
                    .WRITE_EXTERNAL_STORAGE}, type);
        }

    }


    private void validateFields() {
        String caseTitle = etCaseTitle.getText().toString().trim();
        String documentType = etDocumentType.getText().toString().trim();
        String partiesInvolved = etPartiesInvoleved.getText().toString().trim();
        String concerns = etConcerns.getText().toString().trim();


        if (caseTitle.equals("")) {
            displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.enter_case_title_msg));
            etCaseTitle.requestFocus();
        } else {
            if (Utils.isNetworkAvailable(getActivity())) {
                Bundle bundle = new Bundle();
                bundle.putString(WSConstants.WS_KEY_CASE_TITLE, caseTitle);
                bundle.putString(WSConstants.WS_KEY_DOCUMENT_TEXT_TYPE, documentType);
                bundle.putString(WSConstants.WS_KEY_AGREEMENT, partiesInvolved);
                bundle.putString(WSConstants.WS_KEY_CONCERN, concerns);
                addCaseUsAsyncTask(bundle);
            } else {
                displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
            }
        }
    }

    private void addCaseUsAsyncTask(Bundle bundle) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (addCaseAsyncTask != null && addCaseAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                addCaseAsyncTask.execute();
            } else if (addCaseAsyncTask == null || addCaseAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                addCaseAsyncTask = new AddCaseAsyncTask(bundle);
                addCaseAsyncTask.execute();
            }
        } else {
            displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    public class AddCaseAsyncTask extends AsyncTask<String, Void, Void> {
        private WsAddCase wsAddCase;
        private ProgressDialog progressDialog;
        private Bundle bundle;

        public AddCaseAsyncTask(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(String... params) {
            wsAddCase = new WsAddCase(getActivity());
            wsAddCase.executeService(bundle, documentModelArrayList);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsAddCase.getCode() == WSConstants.STATUS_SUCCESS) {
                    displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name),
                            getString(R.string.alert_add_document));

                } else if (wsAddCase.getCode() == WSConstants.STATUS_FAIL) {
                    displayDialog(getActivity(), getString(R.string.app_name), wsAddCase.getMessage());

                } else if (wsAddCase.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsAddCase
                            .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

                }


            }
        }
    }

    public void displayDialogWithPopBackStack(final Context context, final String title, final String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(R.string.o, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                dialog.dismiss();
                getFragmentManager().popBackStack();
                //old code
//                dialog.dismiss();
////                getFragmentManager().popBackStack();
//
//                openAddAppointmentScreen();
            }
        });
//        alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
////                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
////                dialog.dismiss();
////                getFragmentManager().popBackStack();
//
//            }
//        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }

    private void openAddAppointmentScreen() {
        final AppointmentFragment appointmentFragment = new AppointmentFragment();
//        final FragmentManager fragmentManager = getFragmentManager();
//        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.add(R.id.activity_home_container_main, appointmentFragment,
//                AddCaseFragment.class
//                        .getSimpleName());
//        appointmentFragment.setTargetFragment(this, 1);
//        fragmentTransaction.addToBackStack(AppointmentFragment.class.getSimpleName());
//        fragmentTransaction.hide(this);
//        fragmentTransaction.commit();


        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        fragmentTransaction.remove(AddCaseFragment.this);
        fragmentTransaction.commit();
        getFragmentManager().popBackStack();
        FragmentTransaction fragmentTransaction2 = getFragmentManager().beginTransaction();
        fragmentTransaction2.replace(R.id.activity_home_container_main, appointmentFragment,
                appointmentFragment.getClass().getSimpleName());
        fragmentTransaction2.addToBackStack(null);

        fragmentTransaction2.commit();


    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(
                    Intent.createChooser(intent, getString(R.string.select_file_to_upload)),
                    PICKFILE_REQUEST_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), R.string.please_install_file_manager,
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.WRITE_EXTERNAL_STORAGE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            showFileChooser();
        } else if (requestCode == Constants.PERMISSION_REQUEST_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Intent iCamera = new Intent(getActivity(), CustomCameraActivity.class);
                startActivityForResult(iCamera, 4);
                dialog.dismiss();
            } else {
                displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_permission_camera));
            }
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICKFILE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    AppLog.showLogD("Result", "File Uri: " + uri.toString());
                    // Get the path
                    String path = null;
                    path = GetFilePathInternalStoarage.getPath(getActivity(), uri);
                    AppLog.showLogD("Result", "File Path: " + path);
                    if (path != null && !path.equalsIgnoreCase("null")) {
                        String filename = path.substring(path.lastIndexOf("/") + 1);
                        String extension = filename.substring(filename.lastIndexOf("."));
                        String fileNameWithOutExt = filename.replaceFirst("[.][^.]+$", "");
                        AppLog.showLogD("Result", "File name: " + fileNameWithOutExt);
                        AppLog.showLogD("Result", "File ext: " + extension);
                        if ((extension.equalsIgnoreCase(".png")) || (extension.equalsIgnoreCase("" +
                                ".jpeg")) || (extension.equalsIgnoreCase(".xls")) || (extension
                                .equalsIgnoreCase(".doc")) || (extension.equalsIgnoreCase(".txt"))
                                || (extension
                                .equalsIgnoreCase(".pdf")) || (extension.equalsIgnoreCase("" +
                                ".docx")) || (extension.equalsIgnoreCase("" +
                                ".jpg"))) {

                            DocumentModel documentModel = new DocumentModel();
                            documentModel.setFilePath(path);
                            documentModel.setDocumentName(fileNameWithOutExt);
                            documentModel.setImageExt(extension);
                            documentModel.setUploadedBy(getString(R.string.uploaded_by_me));
                            documentModel.setDate(Utils.getCurrentDate());
                            documentModelArrayList.add(documentModel);
                            addCaseAdapter.notifyDataSetChanged();
                        } else {
                            displayDialog(getActivity(), getString(R.string.app_name),
                                    getString(R.string.unsupported_files));
                        }

                    } else {
                        displayDialog(getActivity(), getString(R.string.app_name),
                                getString(R.string.unsupported_files));
                    }


                }
                break;
            case 4:
                if (data != null) {
                    String path = data.getStringExtra("path");
                    String fileName = data.getStringExtra("name");
                    DocumentModel documentModel = new DocumentModel();
                    documentModel.setFilePath(path);
                    documentModel.setDocumentName(fileName);
                    documentModel.setImageExt(".pdf");
                    documentModel.setUploadedBy(getString(R.string.uploaded_by_me));
                    documentModel.setDate(Utils.getCurrentDate());
                    documentModelArrayList.add(documentModel);
                    addCaseAdapter.notifyDataSetChanged();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (addCaseAsyncTask != null && addCaseAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            addCaseAsyncTask.cancel(true);
        }
        if (addCaseAsyncTask != null && addCaseAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            addCaseAsyncTask.cancel(true);
        }


    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }

    private void openDialog() {
        dialog = new Dialog(getActivity(), R.style.picture_dialog_style);
        dialog.setContentView(R.layout.dialog_upload);
        final WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(wlmp);
        dialog.show();
        final TextView tvInternalStorage = (TextView) dialog.findViewById(R.id
                .dialog_upload_tv_storage);
        final TextView tvCamera = (TextView) dialog.findViewById(R.id.dialog_upload_tv_camera);
        final TextView tvCancel = (TextView) dialog.findViewById(R.id.dialog_upload_tv_cancel);
        tvCamera.setText("Take a picture");
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String CAMERA_PERMISSION = Manifest.permission.CAMERA;
                final String WRITE_STORAGE_PERMISSION = Manifest.permission.WRITE_EXTERNAL_STORAGE;
                if (Utils.checkForPermission(getActivity(), CAMERA_PERMISSION) && Utils.checkForPermission(getActivity(), WRITE_STORAGE_PERMISSION)) {
                    Intent iCamera = new Intent(getActivity(), CustomCameraActivity.class);
                    startActivityForResult(iCamera, 4);
                    dialog.dismiss();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{CAMERA_PERMISSION, WRITE_STORAGE_PERMISSION}, Constants.PERMISSION_REQUEST_CAMERA);
                    }
                }


            }
        });
        tvInternalStorage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (documentModelArrayList.size() > Constants.ATTACH_DOCUMENT_LIMIT) {
                    displayDialog(getActivity(), getString(R.string.app_name),
                            getString(R.string.cant_attach_more_thn_10));
                } else {
                    if (checkSdCardPermissions()) {
                        showFileChooser();
                    } else {
                        requestSdCardPermissions(Constants.WRITE_EXTERNAL_STORAGE_PERMISSION, false);
                    }

                }
                dialog.dismiss();

            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


}







