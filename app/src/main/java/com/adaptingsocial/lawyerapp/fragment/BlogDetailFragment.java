package com.adaptingsocial.lawyerapp.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.CommentAdapter;
import com.adaptingsocial.lawyerapp.model.BlogModel;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsAddComment;

/**
 * Created by indianic on 15/06/17.
 */

public class BlogDetailFragment extends BaseFragment {
    private BlogModel blogModel;
    private ImageView ivBg;
    private TextView tvBlogTitle;
    private TextView tvBlogDate;
    private TextView tvBlogDescription;
    private ListView lvComment;
    private TextView tvAddComment;
    private CommentAdapter commentAdapter;
    private AddComentAsyncTask addComentAsyncTask;
    private WsAddComment wsAddComment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_blog_detail, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        final Bundle bundle = getArguments();
        if (bundle != null) {
            blogModel = bundle.getParcelable(Constants.INTENT_KEY_BLOG_MODEL);
            if (blogModel != null) {
                tvBlogTitle.setText("" + blogModel.getBlogName());
                tvBlogDate.setText("" + blogModel.getBolgDate());
                tvBlogDescription.setText("" + blogModel.getBlogDescription());
                Utils.setImageView(getActivity(), blogModel.getBlogImage(), ivBg);
                commentAdapter = new CommentAdapter(getActivity(), blogModel.getCommentModelArrayList(), "");
                lvComment.setAdapter(commentAdapter);
            }
        }

    }

    @Override
    protected void initializeComponent(View view) {
        ivBg = (ImageView) view.findViewById(R.id.row_blog_iv_background);
        tvBlogTitle = (TextView) view.findViewById(R.id.row_blog_tv_title);
        tvBlogDate = (TextView) view.findViewById(R.id.row_blog_tv_date);
        tvBlogDescription = (TextView) view.findViewById(R.id.row_blog_tv_description);
        lvComment = (ListView) view.findViewById(R.id.row_blog_lv_comments);
        tvAddComment = (TextView) view.findViewById(R.id.row_blog_tv_add_comment);
        tvAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(blogModel.getBlogId());
            }
        });


    }

    public void openDialog(final String id) {
        final Dialog dialog = new Dialog(getActivity(), R.style.picture_dialog_style);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_post_comment);
        final WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.FILL_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(wlmp);
        dialog.show();
        final EditText etComment = (EditText) dialog.findViewById(R.id.dialog_post_comment_et_add_comment);
        TextView tvPostComment = (TextView) dialog.findViewById(R.id
                .dialog_post_comment_tv_post_comment);
        tvPostComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etComment.getText().toString().trim().equalsIgnoreCase("")) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.add_comment_msg));
                } else {
                    callPostCommentApi(id, etComment.getText().toString().trim());
                    dialog.dismiss();
                }


            }
        });

    }

    private void callPostCommentApi(String id, String comment) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (addComentAsyncTask != null && addComentAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                addComentAsyncTask.execute();
            } else if (addComentAsyncTask == null || addComentAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                addComentAsyncTask = new AddComentAsyncTask(id, comment);
                addComentAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name),
                    getString(R.string
                            .check_internet_msg));
        }

    }

    public class AddComentAsyncTask extends AsyncTask<String, Void, Void> {
        private ProgressDialog progressDialog;
        private String id;
        private String comment;
        private int position;


        public AddComentAsyncTask(String id, String comment) {
            this.id = id;
            this.comment = comment;


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(String... params) {
            wsAddComment = new WsAddComment(getActivity());
            wsAddComment.executeService(id, comment);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsAddComment.getCode() == WSConstants.STATUS_SUCCESS) {
                    displayDialog(getActivity(), getString(R.string.app_name), wsAddComment
                            .getMessage());


                } else if (wsAddComment.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsAddComment
                            .getMessage());

                } else if (wsAddComment.getCode() == WSConstants.STATUS_NO_DATA_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsAddComment
                            .getMessage());


                } else if (wsAddComment.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name),
                            wsAddComment
                                    .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string
                                    .app_name),
                            getString(R.string.something_went_wrong_msg));

                }


            }

        }

    }

    public void displayDialog(final Context context, final String title, final String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                blogModel.setCommentModelArrayList(wsAddComment.getCommentListModelArrayList());
                commentAdapter = new CommentAdapter(getActivity(), blogModel.getCommentModelArrayList(), "");
                lvComment.setAdapter(commentAdapter);


            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, getString(R.string.blog), 0);
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }

}
