package com.adaptingsocial.lawyerapp.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.CompanyCategoryListAdapter;
import com.adaptingsocial.lawyerapp.adapter.CompanyListAdapter;
import com.adaptingsocial.lawyerapp.model.CompanyCategoryListModel;
import com.adaptingsocial.lawyerapp.model.CompanyListModel;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSCompanyCategoryList;
import com.adaptingsocial.lawyerapp.webservice.WSCompanyList;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;

import java.util.ArrayList;


public class CompanyListFragment extends BaseFragment implements AdapterView.OnItemClickListener, AbsListView.OnScrollListener {
    private ListView lvCompanyList;
    private CompanyListAdapter companyListAdapter;
    private ArrayList<CompanyListModel> companyListModelArrayList;
    private ArrayList<CompanyCategoryListModel> companyCategoryListModelArrayList;
    private CompanyListAsyncTask companyListAsyncTask;
    private CompanyCategoryListAsyncTask companyCategoryListAsyncTask;
    private CompanyCategoryListAdapter companyCategoryListAdapter;
    private int pageIndex = 1;
    private boolean isLoadMore = false;
    private ArrayList<CompanyListModel> tempCompanyList;
    private String categoryId = "0";
    private int count;
    private int nextPage;
    private Bundle bundle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_company_list, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        companyCategoryListUsAsyncTask();
    }

    @Override
    protected void initializeComponent(View view) {
        lvCompanyList = (ListView) view.findViewById(R.id.fragment_company_list_lv_case);
        companyListModelArrayList = new ArrayList<>();
        lvCompanyList.setOnItemClickListener(this);
        lvCompanyList.setOnScrollListener(this);
        tempCompanyList = new ArrayList<>();
        companyListAdapter = new CompanyListAdapter(getActivity(), companyListModelArrayList, CompanyListFragment.this);
        lvCompanyList.setAdapter(companyListAdapter);
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    private void companyListUsAsyncTask(final int pageIndex, final String categoryId) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (companyListAsyncTask != null && companyListAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                companyListAsyncTask.execute();
            } else if (companyListAsyncTask == null || companyListAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                companyListAsyncTask = new CompanyListAsyncTask(pageIndex, categoryId);
                companyListAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    private void companyCategoryListUsAsyncTask() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (companyCategoryListAsyncTask != null && companyCategoryListAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                companyCategoryListAsyncTask.execute();
            } else if (companyCategoryListAsyncTask == null || companyCategoryListAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                companyCategoryListAsyncTask = new CompanyCategoryListAsyncTask();
                companyCategoryListAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    public void nextCallForMoreData() {
//        pageIndex++;
//        companyListUsAsyncTask(pageIndex, "0");
    }


    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.startup_network), 0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
        toolBar.inflateMenu(R.menu.menu_company_listing_filter);
        toolBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_company_listing_iv_filter:
                        openDialog();
                        return true;
                }
                return false;
            }


        });
    }

    private void openAddCompanyDetailScreen(Bundle bundle) {
        final CompanyDetailFragment companyDetailFragment = new CompanyDetailFragment();
        companyDetailFragment.setArguments(bundle);
        final FragmentManager fragmentManager = getFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, companyDetailFragment,
                CompanyDetailFragment.class
                        .getSimpleName());
        fragmentTransaction.addToBackStack(CompanyListFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
        //Boolean freeUser = preferenceUtils.getBoolean(preferenceUtils.KEY_APP_USER_TYPE);
//        if (freeUser) {
//          displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.app_paid_msg));
//        } else {
            final Bundle bundle = new Bundle();
            bundle.putParcelable(getString(R.string.str_company_detail), companyListModelArrayList.get(position));
            openAddCompanyDetailScreen(bundle);
        //}
    }

    public void displayDialog(final Context context, final String title, final String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(R.string.subscribe_now, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                final SubscriptionFragment subscriptionFragment = new SubscriptionFragment();
                ((HomeActivity)getActivity()).setSubscribeSelected();
                setUpMainFragment(subscriptionFragment);

            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
        posButton.setAllCaps(false);
    }

    private void setUpMainFragment(Fragment nextFragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_home_container_main, nextFragment, nextFragment.getClass().getSimpleName()
        ).commit();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == 0 && view.getLastVisiblePosition() == (view.getAdapter().getCount() - 1)) {
            if (isLoadMore) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    // startRetriveNightClubListTask(pageindex, projectId);
                    if (nextPage == 1) {
                        if (companyListModelArrayList.size() < count) {
                            pageIndex++;
                            companyListUsAsyncTask(pageIndex, categoryId);
                        }
                    }

                }
            }
        }
    }

    public void openEmailEditor(final String emailAdd) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{emailAdd});
        i.putExtra(Intent.EXTRA_SUBJECT, "");
        i.putExtra(Intent.EXTRA_TEXT, "");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public void openDialUp(final String contactNo) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + contactNo));
        startActivity(callIntent);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        isLoadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
    }

    public class CompanyCategoryListAsyncTask extends AsyncTask<Void, Void, Void> {
        private WSCompanyCategoryList wsCompanyCategoryList;
        private ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            wsCompanyCategoryList = new WSCompanyCategoryList(getActivity());
            wsCompanyCategoryList.executeService();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (wsCompanyCategoryList.getCode() == WSConstants.STATUS_SUCCESS) {
                companyListUsAsyncTask(pageIndex, categoryId);

                if (wsCompanyCategoryList.getCompanyCategoryListModelArrayList() != null && wsCompanyCategoryList.getCompanyCategoryListModelArrayList().size() > 0) {
                    companyCategoryListModelArrayList = wsCompanyCategoryList.getCompanyCategoryListModelArrayList();
                    final CompanyCategoryListModel companyCategoryListModel = new CompanyCategoryListModel();
                    companyCategoryListModel.setCompanyCategoryName("All");
                    companyCategoryListModel.setSelected(true);
                    companyCategoryListModel.setCompanyCategoryId("0");
                    companyCategoryListModelArrayList.add(0, companyCategoryListModel);

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsCompanyCategoryList.getMessage());
                }


            }
            else if (wsCompanyCategoryList.getCode() == WSConstants.STATUS_FAIL) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), wsCompanyCategoryList.getMessage());

            }
            else if (wsCompanyCategoryList.getCode() == 100) {
                Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsCompanyCategoryList
                        .getMessage());

            }

            else {
                Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
            }

        }
    }

    private void openDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_company_list_filter);
        final WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.TOP | Gravity.RIGHT;
        dialog.getWindow().setAttributes(wlmp);
        final ListView listView = (ListView) dialog.findViewById(R.id.dialog_company_list_lv_filter);
        companyCategoryListAdapter = new CompanyCategoryListAdapter(getActivity(), companyCategoryListModelArrayList);
        listView.setAdapter(companyCategoryListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();
                pageIndex = 1;
                companyListModelArrayList.clear();
                listView.setAdapter(null);
                for (int i = 0; i < companyCategoryListModelArrayList.size(); i++) {
                    companyCategoryListModelArrayList.get(i).setSelected(false);
                }
                companyCategoryListModelArrayList.get(position).setSelected(true);
                categoryId = companyCategoryListModelArrayList.get(position).getCompanyCategoryId();
                companyListUsAsyncTask(pageIndex, categoryId);
            }
        });
        dialog.show();

    }


    public class CompanyListAsyncTask extends AsyncTask<Void, Void, Void> {
        private final String companyId;
        private WSCompanyList wsCompanyList;
        private ProgressDialog progressDialog;
        private int pageIndex;

        public CompanyListAsyncTask(final int pageIndex, final String companyId) {
            this.pageIndex = pageIndex;
            this.companyId = companyId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            wsCompanyList = new WSCompanyList(getActivity());
            wsCompanyList.executeService(pageIndex, companyId);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

                if (wsCompanyList.getCode() == WSConstants.STATUS_SUCCESS) {
                    if (wsCompanyList.getCompanyListModelArrayList() != null && wsCompanyList.getCompanyListModelArrayList().size() > 0) {
                        tempCompanyList = wsCompanyList.getCompanyListModelArrayList();
                        nextPage = Integer.parseInt(wsCompanyList.getNextPage());
                        count = Integer.parseInt(wsCompanyList.getCount());
                        companyListModelArrayList.addAll(wsCompanyList.getCompanyListModelArrayList());
                        companyListAdapter.notifyDataSetChanged();
                    }

                }
                else if (wsCompanyList.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsCompanyList.getMessage());

                }
                else if (wsCompanyList.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsCompanyList
                            .getMessage());

                }
                else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
                }

        }
    }

    public void openDrawMapFragment(Bundle bundle) {
        this.bundle = bundle;
        final String ACCESS_COARSE_PERMISSION = Manifest.permission.ACCESS_COARSE_LOCATION;
        if (Utils.checkForPermission(getActivity(), ACCESS_COARSE_PERMISSION)) {
            final DrawMapFragment drawMapFragment = new DrawMapFragment();
            drawMapFragment.setArguments(bundle);
            final FragmentManager fragmentManager = getFragmentManager();
            final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.activity_home_container_main, drawMapFragment,
                    DrawMapFragment.class
                            .getSimpleName());
            fragmentTransaction.addToBackStack(DrawMapFragment.class.getSimpleName());
            fragmentTransaction.hide(this);
            fragmentTransaction.commit();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{ACCESS_COARSE_PERMISSION}, Constants.PERMISSION_REQUEST_CODE);
            }
            //requestForPermissions(READ_PHONE_PERMISSION, PERMISSION_REQUEST_CODE);
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openDrawMapFragment(bundle);
            } else {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_permission_location));
            }
        }
    }

    //    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == Constants.PERMISSION_REQUEST_CODE) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                openDrawMapFragment(bundle);
//            } else {
//                Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_permission_location));
//            }
//        }
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (companyListAsyncTask != null && companyListAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            companyListAsyncTask.cancel(true);
        }

    }

}
