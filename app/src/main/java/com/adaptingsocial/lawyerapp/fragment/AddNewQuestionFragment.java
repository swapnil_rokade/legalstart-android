package com.adaptingsocial.lawyerapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.MyTextWatcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by indianic on 04/07/17.
 */

public class AddNewQuestionFragment extends BaseFragment {
    private String answers;
    //    private List<String> optionlist;
    private ArrayList<LinkedHashMap<String, String>> listHashMap;
    private String[] selectAnswer = null;
    private ArrayList<String> answersList;
    private EditText etFirst;
    private EditText etSecond;
    //private String answer = "";
    private boolean first = true;
    private View rootView;
    private Button btnAddNew;
    private RelativeLayout rlAddNew;
    private String addMore = "";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle mBundle = getArguments();
        if (mBundle != null) {
            answers = mBundle.getString("answers");
            //optionlist = Arrays.asList(answers.split("\\s*,\\s*"));
            selectAnswer = mBundle.getStringArray("answersselect");
            addMore = mBundle.getString("addmore");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_new_question, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        if (addMore!=null && !addMore.equalsIgnoreCase("")) {
            rlAddNew.setVisibility(View.GONE);
        } else {
            rlAddNew.setVisibility(View.VISIBLE);
        }
        updateView();
    }

    private void updateView() {
        LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.llmain);
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        linearLayout.removeAllViews();
        answersList = new ArrayList<>();
        if (selectAnswer == null) {
            answersList.add(answers);

        } else {

            for (String obj : selectAnswer) {
                answersList.add(obj);
            }
        }

        listHashMap = new ArrayList<>();
        int i = 0;
        for (String answer1 : answersList) {

            List<String> optionlist = Arrays.asList(answer1.split("\\s*,\\s*"));
            LinkedHashMap<String, String> stringHashMap = new LinkedHashMap<>();
            for (int k = 0; k < optionlist.size(); k = k + 2) {
                AppLog.showLogD("Result", "index is " + k);
                View viewrow = inflater.inflate(R.layout.row_add_new, linearLayout, false);
                TextView tvFirst = (TextView) viewrow.findViewById(R.id
                        .row_add_new_tv_title_one);
                TextView tvSecond = (TextView) viewrow.findViewById(R.id
                        .row_add_new_tv_title_two);
                etFirst = (EditText) viewrow.findViewById(R.id.row_add_new_et_title_one);
                etSecond = (EditText) viewrow.findViewById(R.id.row_add_new_et_title_two);
                ImageView ivRemove = (ImageView) viewrow.findViewById(R.id.row_add_new_iv_remove);
                ivRemove.setTag(i);
                if (i == 0) {
                    ivRemove.setVisibility(View.GONE);

                } else {
                    ivRemove.setVisibility(View.VISIBLE);
                    ivRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectAnswer = getAnswer();
                            if (selectAnswer != null) {
                                int size = selectAnswer.length;
                                if (size > 1) {
                                    String[] newAnswers = new String[size - 1];
                                    int tag = (int) v.getTag();
                                    int m = 0;
                                    for (int index = 0; index < size; index++) {
                                        if (index != tag) {
                                            newAnswers[m] = selectAnswer[index];
                                            m++;
                                        }

                                    }
                                    selectAnswer = newAnswers;

                                }

                            }

                            updateView();
                        }
                    });
                }
                String option = optionlist.get(k);
                String[] ops = option.split(":");
                String left = option.replaceAll(":", "");
                String right = "";
                if (ops != null && ops.length > 1) {
                    left = ops[0];
                    right = ops[1];
                }
                tvFirst.setText(left);
                etFirst.setTag(left);
                etFirst.setText(right);
                stringHashMap.put(left, right);

                if (k <= optionlist.size() - 2) {
                    tvSecond.setVisibility(View.VISIBLE);

                    String option1 = optionlist.get(k + 1);
                    String[] ops1 = option1.split(":");
                    String left1 = option1.replaceAll(":", "");
                    String right1 = "";
                    if (ops1 != null && ops1.length > 1) {
                        left1 = ops1[0];
                        right1 = ops1[1];
                    }

                    tvSecond.setText(left1);
                    etSecond.setTag(left1);
                    etSecond.setText(right1);
                    stringHashMap.put(left1, right1);
                } else {
                    tvSecond.setVisibility(View.GONE);
                    etSecond.setVisibility(View.GONE);
                }
                etFirst.addTextChangedListener(new MyTextWatcher(etFirst, stringHashMap,
                        AddNewQuestionFragment.this));
                etSecond.addTextChangedListener(new MyTextWatcher(etSecond, stringHashMap, AddNewQuestionFragment.this));
                linearLayout.addView(viewrow);
            }
            listHashMap.add(stringHashMap);
            i++;
        }

    }


    @Override
    protected void initializeComponent(View view) {
        btnAddNew = (Button) view.findViewById(R.id.btnAdd);
        rlAddNew = (RelativeLayout) view.findViewById(R.id.rladd);
        btnAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddNewClicked();

            }
        });

    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }


    public String[] getAnswer() {
        answersList = new ArrayList<String>();
        for (LinkedHashMap<String, String> stringHashMap : listHashMap) {
            String answer = null;
            for (HashMap.Entry<String, String> entry : stringHashMap.entrySet()) {
                System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
                if (answer == null) {
                    answer = entry.getKey() + ":" + entry.getValue();
                    //first = false;
                } else {
                    answer += "," + entry.getKey() + ":" + entry.getValue();
                }
            }
            answersList.add(answer);
        }
        String[] array = new String[answersList.size()];
        answersList.toArray(array);
        return array;
    }

    public void setStringHashMap(HashMap<String, String> stringHashMap) {
        //this.stringHashMap = stringHashMap;
    }

    public void onAddNewClicked() {
        String newAnswer = answers;
        selectAnswer = getAnswer();
        if (selectAnswer == null) {
            selectAnswer = new String[2];
            selectAnswer[0] = answers;
            selectAnswer[1] = newAnswer;
        } else {
            int size = selectAnswer.length;
            String[] newAnswers = new String[size + 1];
            System.arraycopy(selectAnswer, 0, newAnswers, 0, size);
            newAnswers[size] = answers;
            selectAnswer = newAnswers;
        }


        updateView();
    }

    //REMOVE
    public void onRemoveClicked() {
        selectAnswer = getAnswer();
        if (selectAnswer != null) {
            int size = selectAnswer.length;
            if (size > 1) {
                String[] newAnswers = new String[size - 1];
                System.arraycopy(selectAnswer, 0, newAnswers, 0, size - 1);
                selectAnswer = newAnswers;

            }

        }

        updateView();
    }
}
