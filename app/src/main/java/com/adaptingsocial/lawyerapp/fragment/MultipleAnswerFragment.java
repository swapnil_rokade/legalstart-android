package com.adaptingsocial.lawyerapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;

import java.util.Arrays;
import java.util.List;

/**
 * Created by indianic on 04/07/17.
 */

public class MultipleAnswerFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {
    List<String> temp;
    private String answers;
    private String selectAnswer = "";
    private List<String> optionlist;
    private String answer = "";
    private boolean first = true;
    private SparseBooleanArray sparseBooleanArray;
    private EditText etOther;
    private String otheranswer = "";
    private String otheranswerlbl = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle mBundle = getArguments();
        if (mBundle != null) {
            answers = mBundle.getString("answers");
            selectAnswer = mBundle.getString("answersselect");
            optionlist = Arrays.asList(answers.split("\\s*,\\s*"));
            otheranswer = mBundle.getString("otheranswer");
            otheranswerlbl = mBundle.getString("otheranswerlbl");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_multiple_answer, container, false);

    }

    // "question_options": "Accommodations,Construction,Consulting,Entertainment,Finance,Insurance\u00a0"
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etOther = (EditText) getView().findViewById(R.id.fragment_multiple_choice_answer_et_other);
        LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.llmain);
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        for (int j = 0; j < optionlist.size(); j++) {
            if (selectAnswer != null) {
                if (selectAnswer.contains(optionlist.get(j))) {
                    if(optionlist.get(j).equalsIgnoreCase("other")){
                        etOther.setVisibility(View.VISIBLE);
                        etOther.setText("" + otheranswerlbl);
                    }
                    else{
//                        etOther.setVisibility(View.GONE);
                    }
                    sparseBooleanArray.put(j, true);
                }
                else {
                    if (otheranswer != null)
                        if (otheranswer.equalsIgnoreCase(String.valueOf(j))) {
                            etOther.setVisibility(View.VISIBLE);
                            etOther.setText("" + otheranswerlbl);

                        }
                        else {
//                            etOther.setVisibility(View.GONE);
//
                        }
                    sparseBooleanArray.put(j, false);
                }
            } else {
                sparseBooleanArray.put(j, false);
            }


        }
//temp




        /*for (int k = 0; k < optionlist.size(); k = k + 2) {
            AppLog.showLogD("Result", "index is " + k);

            View viewrow = inflater.inflate(R.layout.row_multiple_choice_answers, linearLayout, false);
            CheckBox cbFirst = (CheckBox) viewrow.findViewById(R.id.fragment_multiple_answer_cb_option);
            CheckBox cbSecond = (CheckBox) viewrow.findViewById(R.id
                    .fragment_multiple_answer_cb_answer_second);
            cbFirst.setButtonDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.check_box_button_selector));
            cbSecond.setButtonDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.check_box_button_selector));


            cbFirst.setChecked(sparseBooleanArray.get(k));
            cbFirst.setOnCheckedChangeListener(this);
            cbSecond.setOnCheckedChangeListener(this);

            cbFirst.setText(optionlist.get(k));
            cbFirst.setTag(k);

            if (k <= optionlist.size() - 2) {
                cbSecond.setVisibility(View.VISIBLE);
                cbSecond.setText(optionlist.get(k + 1));
                cbSecond.setTag(k + 1);
                cbSecond.setChecked(sparseBooleanArray.get(k + 1));
            } else {
                cbSecond.setVisibility(View.GONE);
            }

            linearLayout.addView(viewrow);
        }*/

        for (int k = 0; k < optionlist.size(); k++) {
            AppLog.showLogD("Result", "index is " + k);

            final View viewrow = inflater.inflate(R.layout.row_multiple_choice_answers, linearLayout, false);
            final CheckBox cbFirst = (CheckBox) viewrow.findViewById(R.id.fragment_multiple_answer_cb_option);
            /*final CheckBox cbSecond = (CheckBox) viewrow.findViewById(R.id
                    .fragment_multiple_answer_cb_answer_second);*/
            cbFirst.setButtonDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.check_box_button_selector));
            //cbSecond.setButtonDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.check_box_button_selector));


            cbFirst.setChecked(sparseBooleanArray.get(k));
            cbFirst.setOnCheckedChangeListener(this);
            //cbSecond.setOnCheckedChangeListener(this);

            cbFirst.setText(optionlist.get(k));
            cbFirst.setTag(k);

            /*if (k <= optionlist.size() - 2) {
                cbSecond.setVisibility(View.VISIBLE);
                cbSecond.setText(optionlist.get(k + 1));
                cbSecond.setTag(k + 1);
                cbSecond.setChecked(sparseBooleanArray.get(k + 1));
            } else {
                cbSecond.setVisibility(View.GONE);
            }*/

            linearLayout.addView(viewrow);
        }
    }

    @Override
    protected void initializeComponent(View view) {
        sparseBooleanArray = new SparseBooleanArray();


    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public String getAnswer() {
        for (int u = 0; u < sparseBooleanArray.size(); u++) {
            if (sparseBooleanArray.get(u)) {
                if (first) {
                    if (optionlist.get(u).equalsIgnoreCase("other")) {
                        otheranswer = String.valueOf(u);
                        otheranswerlbl = etOther.getText().toString().trim();
                        answer += optionlist.get(u);
                    } else {

                        answer += optionlist.get(u);
                    }
                    first = false;
                } else {
                    if (optionlist.get(u).equalsIgnoreCase("other")) {
                        otheranswer = String.valueOf(u);
                        otheranswerlbl = etOther.getText().toString().trim();
                        answer += "," + optionlist.get(u);
                    } else {
                        answer += "," + optionlist.get(u);
                    }
                }

            }

        }
        return answer;
    }

    public String getotherAnswer() {
        return otheranswer;
    }

    public String getotherAnswerlbl() {
        return otheranswerlbl;
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int getPosition = (Integer) buttonView.getTag();
        Log.i("comVisa", "getPos ==" + getPosition);
        if (isChecked) {
            buttonView.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_sea_green));
            sparseBooleanArray.put(getPosition, true);
            if (buttonView.getText().toString().trim().equalsIgnoreCase("other")) {
                otheranswer = String.valueOf(getPosition);
                etOther.setVisibility(View.VISIBLE);

            }
        } else {
            buttonView.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorDarkGray));
            sparseBooleanArray.put(getPosition, false);
            if (buttonView.getText().toString().trim().equalsIgnoreCase("other")) {
                otheranswer = "";
                etOther.setVisibility(View.GONE);

            }

        }


    }
}
