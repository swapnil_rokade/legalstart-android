package com.adaptingsocial.lawyerapp.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsChatnotify;



public class LetsChatFragment extends BaseFragment {
    private EditText etName;
    private EditText etEmail;
    private EditText etPurpose;
    private TextView tvLetsChat;
    private long mLastClickTime = 0;
    private AsyncNotifyChat asyncChatNotify;
    private PreferenceUtils preferenceUtils;
    private Boolean freeUser;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_lets_chat, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();


    }

    @Override
    protected void initializeComponent(View view) {
        etName = (EditText) view.findViewById(R.id.fragment_lets_chat_et_name);
        etEmail = (EditText) view.findViewById(R.id.fragment_lets_chat_et_email);
        etPurpose = (EditText) view.findViewById(R.id.fragment_lets_chat_et_purpose);
        tvLetsChat = (TextView) view.findViewById(R.id.fragment_lets_chat_tv_lets_chat);
        preferenceUtils = new PreferenceUtils(getActivity());
        freeUser = preferenceUtils.getBoolean(preferenceUtils.KEY_APP_USER_TYPE);

        etEmail.setText(preferenceUtils.getString(preferenceUtils.KEY_EMAIL));
        tvLetsChat.setOnClickListener(this);
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false, "Chat", 0);
            ((HomeActivity) getActivity()).showShadow();

        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Utils.hideSoftKeyBoard(getActivity(), v);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        final int id = v.getId();
        switch (id) {
            case R.id.fragment_lets_chat_tv_lets_chat:
                if (freeUser) {
                    displayPaidDialog(getActivity(), getString(R.string.app_name), getString(R.string.app_paid_msg));
                } else {
                validation();
                break;
                }
        }
    }

    private void validation() {
        final String name = etName.getText().toString().trim();
        final String email = etEmail.getText().toString().trim();
        final String purpose = etPurpose.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.enter_name));
            etName.requestFocus();
        } else if (TextUtils.isEmpty(email)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
                    .err_msg_enter_email));
            etEmail.requestFocus();
        } else if (!Utils.isValidEmail(email)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
                    .err_msg_please_varify_your_email));
            etEmail.requestFocus();
        } else if (TextUtils.isEmpty(purpose)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.enter_purpose));
            etPurpose.requestFocus();
        } else {
            Bundle bundle = new Bundle();
            bundle.putString(WSConstants.WS_KEY_NAME, name);
            bundle.putString(WSConstants.WS_KEY_PURPOSE, purpose);
            bundle.putString(WSConstants.WS_KEY_EMAIL, email);
            callAsyncChatNotify(bundle);
        }

    }
    public  void displayPaidDialog(final Context context, final String title, final String
            message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(true);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(R.string.subscribe_now, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Fragment mFragment = new SubscriptionFragment();
                setUpMainFragment(mFragment);

            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
        posButton.setAllCaps(false);
    }

    private void callAsyncChatNotify(Bundle bundle) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncChatNotify != null && asyncChatNotify.getStatus() == AsyncTask.Status.PENDING) {
                asyncChatNotify.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (asyncChatNotify == null || asyncChatNotify.getStatus() == AsyncTask.Status.FINISHED) {
                asyncChatNotify = new AsyncNotifyChat(bundle);
                asyncChatNotify.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
                    .alert_no_internet));
        }

    }

    /**
     * call login web service
     */
    private class AsyncNotifyChat extends AsyncTask<Void, Void, Void> {
        private Bundle bundle;
        private WsChatnotify wsChatnotify;
        private ProgressDialog progressDialog;

        public AsyncNotifyChat(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wsChatnotify = new WsChatnotify(getActivity());
                wsChatnotify.executeService(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (!isCancelled()) {
                if (wsChatnotify.getCode() == WSConstants.STATUS_SUCCESS) {
                    displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.caht_sucess_info));
                } else if (wsChatnotify.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsChatnotify.getMessage
                            ());

                } else if (wsChatnotify.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsChatnotify
                            .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string
                                    .app_name),
                            getString(R.string.something_went_wrong_msg));

                }
            }
        }
    }

    public void displayDialogWithPopBackStack(final Context context, final String title, final String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                dialog.dismiss();
//                ((Activity) context).getFragmentManager().popBackStack();


                ChatFragment chatFragment=new ChatFragment();
                setUpMainFragment(chatFragment);


            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }
    private void setUpMainFragment(Fragment nextFragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_home_container_main, nextFragment, nextFragment.getClass().getSimpleName()
        ).commit();
    }
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }
}
