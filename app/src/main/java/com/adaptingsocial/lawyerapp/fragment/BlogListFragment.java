package com.adaptingsocial.lawyerapp.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.BlogListAdapter;
import com.adaptingsocial.lawyerapp.model.BlogModel;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSBlogList;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;

import java.util.ArrayList;

/**
 * Created by indianic on 15/06/17.
 */

public class BlogListFragment extends BaseFragment implements AdapterView.OnItemClickListener, AbsListView.OnScrollListener {
    private ListView lvBlogList;
    private ArrayList<BlogModel> blogModelArrayList;
    private BlogListAdapter blogListAdapter;
    private BlogListAsyncTask blogListAsyncTask;
    private int nextPage;
    private int page = 1;
    private boolean isLoadMore = false;
    private int pageIndex = 1;
    private int count;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_blog_list, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        BlogListUsAsyncTask("1", 0, false);

    }

    @Override
    protected void initializeComponent(View view) {
        lvBlogList = (ListView) view.findViewById(R.id.fragment_blog_list_lv_case);
        blogModelArrayList = new ArrayList<>();
        blogListAdapter = new BlogListAdapter(getActivity(), blogModelArrayList, BlogListFragment.this);
        lvBlogList.setAdapter(blogListAdapter);
        lvBlogList.setOnItemClickListener(this);
        lvBlogList.setOnScrollListener(this);
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.blog), 0);
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }

    public void BlogListUsAsyncTask(String pageIndex, int position, boolean flag) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (blogListAsyncTask != null && blogListAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                blogListAsyncTask.execute();
            } else if (blogListAsyncTask == null || blogListAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                blogListAsyncTask = new BlogListAsyncTask(pageIndex, position, flag);
                blogListAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
        //Boolean freeUser = preferenceUtils.getBoolean(preferenceUtils.KEY_APP_USER_TYPE);
//        if (freeUser) {
//           displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.app_paid_msg));
//        } else {
            final Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.INTENT_KEY_BLOG_MODEL, blogModelArrayList.get(position));
            openBlogDetailScreen(bundle);
        //}

    }


    public void displayDialog(final Context context, final String title, final String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(R.string.subscribe_now, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                final SubscriptionFragment subscriptionFragment = new SubscriptionFragment();
                ((HomeActivity)getActivity()).setSubscribeSelected();
                setUpMainFragment(subscriptionFragment);

            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
        posButton.setAllCaps(false);
    }


    private void setUpMainFragment(Fragment nextFragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_home_container_main, nextFragment, nextFragment.getClass().getSimpleName()
        ).commit();
    }

    private void openBlogDetailScreen(Bundle bundle) {
        final BlogDetailFragment blogDetailFragment = new BlogDetailFragment();
        blogDetailFragment.setArguments(bundle);
        final FragmentManager fragmentManager = getFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, blogDetailFragment,
                BlogDetailFragment.class
                        .getSimpleName());
        fragmentTransaction.addToBackStack(BlogListFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == 0 && view.getLastVisiblePosition() == (view.getAdapter().getCount() - 1)) {
            if (isLoadMore) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    // startRetriveNightClubListTask(pageindex, projectId);
                    if (nextPage == 1) {
                        if (blogModelArrayList.size() < count) {
                            pageIndex++;
                            BlogListUsAsyncTask(String.valueOf(pageIndex), 0, false);
                        }
                    }

                }
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        isLoadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
    }


    public class BlogListAsyncTask extends AsyncTask<String, Void, Void> {
        private WSBlogList wsBlogList;
        private ProgressDialog progressDialog;
        private String pageIndex;
        private int position;
        boolean flag;

        public BlogListAsyncTask(String pageIndex, int position, boolean flag) {
            this.pageIndex = pageIndex;
            this.position = position;
            this.flag = flag;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(String... params) {
            wsBlogList = new WSBlogList(getActivity());
            wsBlogList.executeService(pageIndex);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsBlogList.getCode() == WSConstants.STATUS_SUCCESS) {
                    if (wsBlogList.getBlogListModelArrayList() != null && wsBlogList.getBlogListModelArrayList().size() > 0) {
                        count = Integer.parseInt(wsBlogList.getCount());
                        nextPage = Integer.parseInt(wsBlogList.getNextPage());
                        blogModelArrayList.addAll(wsBlogList.getBlogListModelArrayList());
                        blogListAdapter.notifyDataSetChanged();
                        Log.e("Paging called", "page index==>" + pageIndex);

                    } else {
                        Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsBlogList.getMessage());

                    }


                }
                else if (wsBlogList.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsBlogList.getMessage());

                }
                else if (wsBlogList.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsBlogList
                            .getMessage());

                }

                else if (wsBlogList.getCode() == WSConstants.STATUS_NO_DATA_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsBlogList.getMessage());


                }
                else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

                }


            }

        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (blogListAsyncTask != null && blogListAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            blogListAsyncTask.cancel(true);
        }
        if (blogListAsyncTask != null && blogListAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            blogListAsyncTask.cancel(true);
        }

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }
}
