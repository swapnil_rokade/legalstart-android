package com.adaptingsocial.lawyerapp.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CardDetailModel;
import com.adaptingsocial.lawyerapp.model.CreditCardObject;
import com.adaptingsocial.lawyerapp.model.SubscriptionModel;
import com.adaptingsocial.lawyerapp.service.AnetIntentService;
import com.adaptingsocial.lawyerapp.utill.AnetResultReceiver;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.FormattingTextWatcher;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsAuthorizeSubscriptionPayment;
import com.adaptingsocial.lawyerapp.webservice.WsSubscriptionPayment;

import net.authorize.aim.Result;
import net.authorize.util.Luhn;

import tgio.rncryptor.RNCryptorNative;

/**
 * Created by indianic on 22/08/17.
 */

public class AuthorizeSubscriptionFragment extends BaseFragment implements
        FormattingTextWatcher.UpdateUICallBack, AnetResultReceiver.ReceiverCallback {
    private SubscriptionModel subscriptionModel;
    private Bundle mBundle;
    private EditText edtCardNumber;
    private EditText edtExpiryDate;
    private EditText edtCVV;
    private LinearLayout llParent;
    private long mLastClickTime = 0;
    private TextView tvCharge;
    private ProgressDialog progressDialog;
    private AnetResultReceiver resultReceiver;
    private String amount = "";
    private String itemName = "";
    private String paymentID;
    private AsyncSetPayment asyncSetPayment;
    private TextView tvAmount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_authorie_payment, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        mBundle = getArguments();
        subscriptionModel = mBundle.getParcelable(Constants.INTENT_KEY_Document_MODEL);
        amount = subscriptionModel.getAmmount();
        itemName = subscriptionModel.getTitle();
        tvAmount.setText("$ " + this.amount);

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, getString(R.string.payment_process), 0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();

    }

    @Override
    protected void initializeComponent(View view) {
        tvAmount = (TextView) view.findViewById(R.id.fragment_authorie_tv_amount);
        final FormattingTextWatcher cardNumberTextWatcher = new FormattingTextWatcher(FormattingTextWatcher.FieldType.CARD_NUMBER);
        final FormattingTextWatcher cardExpDateTextWatcher = new FormattingTextWatcher(FormattingTextWatcher.FieldType.EXP_DATE);
        llParent = (LinearLayout) view.findViewById(R.id.fragment_authorie_llParent);
        edtCardNumber = (EditText) view.findViewById(R.id.fragment_authorie_edtCardNumber);
        edtExpiryDate = (EditText) view.findViewById(R.id.fragment_authorie_edtExpiryDate);
        edtCVV = (EditText) view.findViewById(R.id.fragment_authorie_edtCVV);
        tvCharge = (TextView) view.findViewById(R.id.fragment_authorie_btn_pay);
        tvCharge.setOnClickListener(this);
        cardNumberTextWatcher.setFormattingTextWatcher(this);
        cardExpDateTextWatcher.setFormattingTextWatcher(this);

        edtCardNumber.addTextChangedListener(cardNumberTextWatcher);
        edtExpiryDate.addTextChangedListener(cardExpDateTextWatcher);
        edtExpiryDate.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //Detects delete key from keyboard and restrict it to delete from last position
                return keyCode == KeyEvent.KEYCODE_DEL
                        && edtExpiryDate.getSelectionStart() < edtExpiryDate.getText().length();
            }
        });
        resultReceiver = new AnetResultReceiver(new Handler());
        resultReceiver.setReceiverCallback(this);
        tvCharge.setOnClickListener(this);
        startServiceAuthenticateUser();
    }

    /**
     * Authenticate merchant with admin side
     *
     * @return boolean
     */
    private boolean startServiceAuthenticateUser() {
        try {
            progressDialog = Utils.showProgressDialog(getActivity(), getString(R.string.msg_loading), false);
            final Intent intent = new Intent(getActivity(), AnetIntentService.class);
            intent.setAction(AnetIntentService.ACTION_AUTHENTICATE_USER);
            intent.putExtra(AnetResultReceiver.RESULT_RECEIVER_TAG, resultReceiver);
            getActivity().startService(intent);
        } catch (Exception e) {
            Utils.dismissProgressDialog(progressDialog);
            return false;
        }
        return true;
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if (resultCode == AnetIntentService.AUTHENTICATE_USER_RESULT_CODE) {
            final net.authorize.mobile.Result result = (net.authorize.mobile.Result) resultData.getSerializable(AnetIntentService.AUTHENTICATE_USER_STATUS);
            Utils.dismissProgressDialog(progressDialog);
            if (result != null && !result.isOk()) {
                showPaymentFailedDialog(getString(R.string.msg_payment_auhtentiaction_failed));
            }
        } else if (resultCode == AnetIntentService.TRANSACTION_RESULT_CODE) {
            final Result transactionResult = (Result) resultData.getSerializable(AnetIntentService.TRANSACTION_STATUS);
            if (transactionResult != null && !transactionResult.isApproved()) {
                if (transactionResult.getTransactionResponseErrors().size() != 0) {
                    Utils.dismissProgressDialog(progressDialog);
                    final String errorMessage = transactionResult.getTransactionResponseErrors().get(0).getReasonText();
                    showPaymentFailedDialog(errorMessage);
                } else {
                    Utils.dismissProgressDialog(progressDialog);
                    showPaymentFailedDialog(getString(R.string.msg_payment_failed_unknown_reason));
                }
            } else {
                if (transactionResult != null) {
                    updatePaymentInfo(transactionResult.getTransId());
                }
            }
        } else if (resultCode == AnetIntentService.SESSION_EXPIRED_CODE) {
            Utils.dismissProgressDialog(progressDialog);
            showPaymentFailedDialog(getString(R.string.msg_payment_session_expired));
        }
    }

    private void showPaymentFailedDialog(final String message) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        alertDialog.setMessage(message);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                if (TextUtils.isEmpty(edtCardNumber.getText().toString())) {
                    getFragmentManager().popBackStack();
                }
            }
        });
        alertDialog.show();
    }


    @Override
    public void updateText(FormattingTextWatcher.FieldType fieldType, String text, FormattingTextWatcher textWatcher) {
        EditText editText = null;
        switch (fieldType) {
            case CARD_NUMBER:
                editText = edtCardNumber;
                break;
            case EXP_DATE:
                editText = edtExpiryDate;
                break;
        }
        if (editText != null) {
            editText.removeTextChangedListener(textWatcher);
            editText.setText(text);
            editText.addTextChangedListener(textWatcher);
            editText.setSelection(editText.getText().length());
        }
    }

    private void updatePaymentInfo(final String paymentID) {
        this.paymentID = paymentID;
        Utils.dismissProgressDialog(progressDialog);
        if (Utils.isNetworkAvailable(getActivity())) {
//            asyncSetPaymentAsyncTask(amount);
        } else {
            Utils.dismissProgressDialog(progressDialog);
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setCancelable(false);
            alertDialog.setTitle(getString(R.string.app_name));
            alertDialog.setMessage(getString(R.string.check_internet_msg));
            alertDialog.setPositiveButton(R.string.ok, new DialogInterface
                    .OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    progressDialog = Utils.showProgressDialog(getActivity(), getString(R.string.msg_loading), false);
                    updatePaymentInfo(paymentID);
                }
            });
            alertDialog.show();
        }
    }

    private void asyncSetPaymentAsyncTask(CardDetailModel cardDetailModel) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncSetPayment != null && asyncSetPayment.getStatus() == AsyncTask.Status.PENDING) {
                asyncSetPayment.execute();
            } else if (asyncSetPayment == null || asyncSetPayment.getStatus() == AsyncTask.Status.FINISHED) {
                asyncSetPayment = new AsyncSetPayment(cardDetailModel);
                asyncSetPayment.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }


    private class AsyncSetPayment extends AsyncTask<String, Void, Void> {
        private ProgressDialog progressDialog;
        private WsSubscriptionPayment  wsSubscriptionPayment;
        private CardDetailModel cardDetailModel;


        public AsyncSetPayment(CardDetailModel cardDetailModel) {
            this.cardDetailModel = cardDetailModel;
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(String... params) {
            wsSubscriptionPayment = new WsSubscriptionPayment(getActivity());
            wsSubscriptionPayment.executeService(cardDetailModel);
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (!isCancelled()) {
                Utils.dismissProgressDialog(progressDialog);
                if (wsSubscriptionPayment.getCode() == WSConstants.STATUS_SUCCESS) {
                    final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
                    preferenceUtils.putBoolean(preferenceUtils.KEY_APP_USER_TYPE, false);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name),
                            wsSubscriptionPayment.getMessage());


                } else if (wsSubscriptionPayment.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsSubscriptionPayment.getMessage());

                } else if (wsSubscriptionPayment.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsSubscriptionPayment
                            .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

                }


            }

        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Utils.hideSoftKeyBoard(getActivity(), v);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.fragment_authorie_btn_pay:
                if (validation()) {
                    if (Utils.isNetworkAvailable(getActivity())) {
//                        makeTransaction();
                        prepareData();


                    } else {
                        Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
                    }
                }
                break;
        }
    }

    private void prepareData() {

//        String enCryptKey = "vjkd#g&m36@k8?j3m2dvm0ke5f";
//        RNCryptorNative.encryptAsync(cardNumber, enCryptKey, new RNCryptorNative
//                .RNCryptorNativeCallback() {
//            @Override
//            public void done(String encrypted, Exception e) {
//                System.out.println("encrypted async: " + encrypted);
//                CardDetailModel cardDetailModel = new CardDetailModel();
//                cardDetailModel.setCardNumber(encrypted);
//                cardDetailModel.setAmount(amount);
//                cardDetailModel.setOrderNumber(subscriptionModel.getId());
//
//                final String cardExpirationDate = edtExpiryDate.getText().toString().replace("/", "");
//                final String cardExpMonth = cardExpirationDate.substring(0, 2);
//                final String cardExpYear = cardExpirationDate.substring(2, 4);
//                final String cardExpDate = cardExpYear + "-" + cardExpMonth;
//                cardDetailModel.setExpDate(cardExpDate);
//                final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
//                String fName = preferenceUtils.getString(preferenceUtils.KEY_FIRST_NAME);
//                String lName = preferenceUtils.getString(preferenceUtils.KEY_LAST_NAME);
//                cardDetailModel.setFirstName(fName);
//                cardDetailModel.setLastName(lName);
//                asyncSetPaymentAsyncTask(cardDetailModel);
//
//            }
//        });
        final String cardNumber = edtCardNumber.getText().toString().replaceAll(" ", "");
        CardDetailModel cardDetailModel = new CardDetailModel();
        final byte[] encryptedTextInByte = Utils.encryption(cardNumber);
        final String encyptedText = Utils.convertEncryptedByteToString(encryptedTextInByte);
        cardDetailModel.setCardNumber(encyptedText);
        cardDetailModel.setAmount(amount);
        cardDetailModel.setOrderNumber(subscriptionModel.getId());
        final String cardExpirationDate = edtExpiryDate.getText().toString().replace("/", "");
        final String cardExpMonth = cardExpirationDate.substring(0, 2);
        final String cardExpYear = cardExpirationDate.substring(2, 4);
        final String cardExpDate = "20"+cardExpYear + "-" + cardExpMonth;
        cardDetailModel.setExpDate(cardExpDate);
        final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
        String fName = preferenceUtils.getString(preferenceUtils.KEY_FIRST_NAME);
        String lName = preferenceUtils.getString(preferenceUtils.KEY_LAST_NAME);
        cardDetailModel.setFirstName(fName);
        cardDetailModel.setLastName(lName);
        asyncSetPaymentAsyncTask(cardDetailModel);
    }

    private boolean validation() {
        boolean flag = true;
        final String cardNumber = edtCardNumber.getText().toString().replaceAll(" ", "");
        if (TextUtils.isEmpty(edtCardNumber.getText().toString())) {
            edtCardNumber.requestFocus();
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
                    .msg_please_enter_card_number));
            flag = false;
        } else if (!Luhn.isCardValid(cardNumber)) {
            edtCardNumber.requestFocus();
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.msg_please_enter_valid_card_number)
            );
            flag = false;
        } else if (TextUtils.isEmpty(edtExpiryDate.getText().toString())) {
            edtExpiryDate.requestFocus();
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
                    .msg_please_enter_expiry_date));
            flag = false;
        } else if (edtExpiryDate.getText().toString().length() < 5) {
            edtExpiryDate.requestFocus();
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_please_enter_valid_exp_date));
            flag = false;
        } else if (!Utils.isValidExpDate(edtExpiryDate.getText().toString().substring(0, 2), edtExpiryDate.getText().toString().substring(3, 5))) {
            edtExpiryDate.requestFocus();
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_please_enter_valid_exp_date));
            flag = false;
        } else if (TextUtils.isEmpty(edtCVV.getText())) {
            edtCVV.requestFocus();
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.msg_please_enter_cvv_number));
            flag = false;
        } else if (edtCVV.getText().toString().length() < 3) {
            edtCVV.requestFocus();
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
                    .msg_please_enter_valid_cvv_number));
            flag = false;
        }
        return flag;
    }

    public void makeTransaction() {
        progressDialog = Utils.showProgressDialog(getActivity(), getString(R.string.msg_loading), false);
        final String cardNumber = edtCardNumber.getText().toString().replace(" ", "");
        final String cardExpirationDate = edtExpiryDate.getText().toString().replace("/", "");
        final String cardCVV = edtCVV.getText().toString();
        final String cardExpMonth, cardExpYear;
        final String totalAmount = amount;
        cardExpMonth = cardExpirationDate.substring(0, 2);
        cardExpYear = cardExpirationDate.substring(2, 4);
        final CreditCardObject creditCardObject = new CreditCardObject(cardNumber, cardCVV,
                cardExpMonth, cardExpYear);
        final Intent intent = new Intent(getActivity(), AnetIntentService.class);
        intent.setAction(AnetIntentService.ACTION_MAKE_TRANSACTION);
        intent.putExtra(Constants.CREDIT_CARD_TAG, creditCardObject);
        intent.putExtra(Constants.ZIPCODE_TAG, "");
        intent.putExtra(Constants.AMOUNT_TAG, totalAmount);
        intent.putExtra(Constants.ITEM_NAME, itemName);
        final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
        intent.putExtra(Constants.USER_NAME, preferenceUtils.getString(preferenceUtils.KEY_FIRST_NAME));
        intent.putExtra(Constants.USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        intent.putExtra(AnetResultReceiver.RESULT_RECEIVER_TAG, resultReceiver);
        getActivity().startService(intent);
    }
}
