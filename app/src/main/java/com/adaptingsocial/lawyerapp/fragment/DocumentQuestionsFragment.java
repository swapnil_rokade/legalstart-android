package com.adaptingsocial.lawyerapp.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.DocumentListModel;
import com.adaptingsocial.lawyerapp.model.QuestionModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WSDocumentQuestionList;
import com.adaptingsocial.lawyerapp.webservice.WSDocumentSubmitAnswer;
import com.adaptingsocial.lawyerapp.webservice.WsFreeDocument;

import java.util.ArrayList;

/**
 * Fragment for show questions for create new document
 */
public class DocumentQuestionsFragment extends BaseFragment implements View.OnClickListener,
        SingleChoiceFragment.OnFlowItemClickListener {

    /**
     * Request code for call paypal payment gateway
     */
    private static int REQUEST_CODE_PAYPAL = 101;
    /**
     * Request code for call stripe payment gateway
     */
    private static int REQUEST_CODE_STRIPE = 102;

    private RelativeLayout rlmain;
    private TextView tvSubmit;
    private TextView tvNext;
    private TextView tvQuestion;
    private TextView tvPrivious;
    private TextView tvQuestionNo;
    private TextView tvComplete;
    private TextView tvUnComplete;
    /**
     * Model class of selected document which is passed from root fragment
     */
    private DocumentListModel documentListModel;
    /**
     * Id of selected document which is passed from root fragment
     */
    private String documentId;
    /**
     * AsyncTask for get question list form webservice
     */
    private AsyncGetQuestionList asyncGetQuestionList;

    /**
     * TODO
     */
    private Dialog dialog;
    /**
     * TODO
     */
    private ArrayList<QuestionModel> questionList;
    /**
     * TODO
     */
    private int nextValue = 0;
    private int questionNo = 1;
    private ProgressDialog progressDialog;
    private AsyncSubmitAnswer asyncSubmitAnswer;
    private AsyncFreeDocumentStatus asyncFreeDocumentStatus;
    private String paymentId;
    private String inserId = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_document_questions, container, false);
    }

    @Override
    protected void initializeComponent(View view) {
        final Bundle bundle = getArguments();
        if (bundle != null) {
            documentListModel = bundle.getParcelable("document_model");
        }

        initToolbar();
        tvComplete = (TextView) view.findViewById(R.id.fragment_question_tv_question_complete);
        tvUnComplete = (TextView) view.findViewById(R.id.fragment_question_tv_question_uncomplete);
        tvSubmit = (TextView) view.findViewById(R.id.fragment_document_questions_tv_submit);
        tvNext = (TextView) view.findViewById(R.id.fragment_document_questions_tv_next);
        tvPrivious = (TextView) view.findViewById(R.id.fragment_document_questions_tv_privious);
        rlmain = (RelativeLayout) view.findViewById(R.id.rlmain);
        tvQuestion = (TextView) view.findViewById(R.id.fragment_document_questions_tv_question);
        tvQuestionNo = (TextView) view.findViewById(R.id.fragment_document_questions_tv_question_no);

        tvNext.setOnClickListener(this);
        tvPrivious.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        callAsyncGetQuestionList();
    }

    /**
     * Initialize toolbar
     */
    private void initToolbar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, documentListModel.getDocumentName(), 0);
            ((HomeActivity) getActivity()).showShadow();
            final Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
            toolBar.getMenu().clear();
        }
    }

    /**
     * Call AsyncTask for get question list
     */
    private void callAsyncGetQuestionList() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncGetQuestionList != null && asyncGetQuestionList.getStatus() == AsyncTask.Status.PENDING) {
                asyncGetQuestionList.execute();
            } else if (asyncGetQuestionList == null || asyncGetQuestionList.getStatus() == AsyncTask.Status.FINISHED) {
                asyncGetQuestionList = new AsyncGetQuestionList();
                asyncGetQuestionList.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    private void setUpMainFragment(Fragment nextFragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_document_questions_container_answers, nextFragment, nextFragment.getClass().getSimpleName()
        ).commit();
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.fragment_document_questions_tv_next:

                if (saveAnswers()) {
                    nextValue++;
                    questionNo++;
                    tvPrivious.setVisibility(View.VISIBLE);
                    if (nextValue < questionList.size()) {

                        if (nextValue == questionList.size() - 1) {
                            tvSubmit.setVisibility(View.VISIBLE);
                            tvNext.setVisibility(View.GONE);
                        } else {
                            tvSubmit.setVisibility(View.GONE);
                            tvNext.setVisibility(View.VISIBLE);
                        }
                        tvComplete.setText("" + questionNo);
                        tvQuestion.setText(questionList.get(nextValue).getQuestion());
//                        if (questionList.get(nextValue).getFlow_finish().equalsIgnoreCase
//                                ("yes")) {
//                            tvSubmit.setVisibility(View.VISIBLE);
//                            tvNext.setVisibility(View.GONE);
//                        }

                        tvQuestionNo.setText(String.valueOf(questionNo));

                        if (questionList.get(nextValue).getQtypecode().equalsIgnoreCase("AddNew")) {
                            AddNewQuestionFragment addNewQuestionFragment = new
                                    AddNewQuestionFragment();
                            Bundle bundle = new Bundle();

                            bundle.putString("addmore", questionList.get(nextValue).getAdd_another());
                            bundle.putString("answers", questionList.get(nextValue).getQoption());
                            bundle.putStringArray("answersselect", questionList.get(nextValue)
                                    .getAnswers());
                            addNewQuestionFragment.setArguments(bundle);
                            setUpMainFragment(addNewQuestionFragment);

                        } else if (questionList.get(nextValue).getQtypecode().equalsIgnoreCase
                                ("ShortAnswerQuestion")) {

                            ShortTypeQuestionFragment shortTypeQuestionFragment = new
                                    ShortTypeQuestionFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("answersselect", questionList.get(nextValue)
                                    .getAnswer());
                            bundle.putString("answers", questionList.get(nextValue).getQoption());
                            shortTypeQuestionFragment.setArguments(bundle);
                            setUpMainFragment(shortTypeQuestionFragment);

                        } else if (questionList.get(nextValue).getQtypecode().equalsIgnoreCase
                                ("LongAnswerQuestion")) {
                            LongTypeQuestionFragment longTypeQuestionFragment = new
                                    LongTypeQuestionFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("answers", questionList.get(nextValue).getQoption());
                            bundle.putString("answersselect", questionList.get(nextValue)
                                    .getAnswer());
                            longTypeQuestionFragment.setArguments(bundle);
                            setUpMainFragment(longTypeQuestionFragment);

                        } else if (questionList.get(nextValue).getQtypecode().equalsIgnoreCase
                                ("SingleChoice")) {
                            SingleChoiceFragment singleChoiceFragment = new
                                    SingleChoiceFragment();
                            singleChoiceFragment.setOnItemClickListener(this);
                            Bundle bundle = new Bundle();
                            bundle.putString("answers", questionList.get(nextValue).getQoption());
                            bundle.putString("answersselect", questionList.get(nextValue)
                                    .getAnswer());
                            bundle.putString("textfield_open", questionList.get(nextValue)
                                    .getTextfield_open());
                            singleChoiceFragment.setArguments(bundle);
                            setUpMainFragment(singleChoiceFragment);
                        } else if (questionList.get(nextValue).getQtypecode().equalsIgnoreCase
                                ("Multiselect")) {
                            MultipleAnswerFragment multipleAnswerFragment = new
                                    MultipleAnswerFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("answers", questionList.get(nextValue).getQoption());
                            bundle.putString("answersselect", questionList.get(nextValue)
                                    .getAnswer());
                            multipleAnswerFragment.setArguments(bundle);
                            setUpMainFragment(multipleAnswerFragment);
                        }
                    }


                }


                break;

            case R.id.fragment_document_questions_tv_privious:
                /*if (nextValue == questionList.size() - 1) {
                    if (questionList.get(nextValue).getAnswer() != null) {
                        saveAnswers();
                    }
                }*/

                nextValue = nextValue - 1;
                questionNo--;

                if (nextValue != -1) {
                    if (nextValue == 0) {
                        tvPrivious.setVisibility(View.GONE);
                    }
                    tvQuestionNo.setText("" + questionNo);
                    tvQuestion.setText(questionList.get(nextValue).getQuestion());
                    tvComplete.setText("" + questionNo);
                    if (nextValue == questionList.size()) {
                        tvSubmit.setVisibility(View.VISIBLE);
                        tvNext.setVisibility(View.GONE);
                    } else {
                        tvSubmit.setVisibility(View.GONE);
                        tvNext.setVisibility(View.VISIBLE);
                    }


                    if (questionList.get(nextValue).getQtypecode().equalsIgnoreCase("AddNew")) {
                        AddNewQuestionFragment addNewQuestionFragment = new
                                AddNewQuestionFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("answers", questionList.get(nextValue).getQoption());
                        bundle.putStringArray("answersselect", questionList.get(nextValue)
                                .getAnswers());
                        bundle.putString("addmore", questionList.get(nextValue).getAdd_another());
                        addNewQuestionFragment.setArguments(bundle);
                        setUpMainFragment(addNewQuestionFragment);

                    } else if (questionList.get(nextValue).getQtypecode().equalsIgnoreCase
                            ("ShortAnswerQuestion")) {
                        ShortTypeQuestionFragment shortTypeQuestionFragment = new
                                ShortTypeQuestionFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("answers", questionList.get(nextValue).getQoption());
                        bundle.putString("answersselect", questionList.get(nextValue)
                                .getAnswer());
                        shortTypeQuestionFragment.setArguments(bundle);
                        setUpMainFragment(shortTypeQuestionFragment);

                    } else if (questionList.get(nextValue).getQtypecode().equalsIgnoreCase
                            ("LongAnswerQuestion")) {
                        LongTypeQuestionFragment longTypeQuestionFragment = new
                                LongTypeQuestionFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("answers", questionList.get(nextValue).getQoption());
                        bundle.putString("answersselect", questionList.get(nextValue)
                                .getAnswer());
                        longTypeQuestionFragment.setArguments(bundle);
                        setUpMainFragment(longTypeQuestionFragment);

                    } else if (questionList.get(nextValue).getQtypecode().equalsIgnoreCase
                            ("SingleChoice")) {
                        SingleChoiceFragment singleChoiceFragment = new
                                SingleChoiceFragment();
                        singleChoiceFragment.setOnItemClickListener(this);
                        Bundle bundle = new Bundle();
                        bundle.putString("answers", questionList.get(nextValue).getQoption());
                        bundle.putString("answersselect", questionList.get(nextValue)
                                .getAnswer());
                        bundle.putString("textfield_open", questionList.get(nextValue)
                                .getTextfield_open());
                        singleChoiceFragment.setArguments(bundle);
                        setUpMainFragment(singleChoiceFragment);
                    } else if (questionList.get(nextValue).getQtypecode().equalsIgnoreCase
                            ("Multiselect")) {
                        MultipleAnswerFragment multipleAnswerFragment = new
                                MultipleAnswerFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("answers", questionList.get(nextValue).getQoption());
                        bundle.putString("answersselect", questionList.get(nextValue)
                                .getAnswer());
                        bundle.putString("otheranswer", questionList.get(nextValue)
                                .getMultipleotherField());
                        bundle.putString("otheranswerlbl", questionList.get(nextValue)
                                .getMultipleotherFieldanswer());
                        multipleAnswerFragment.setArguments(bundle);
                        setUpMainFragment(multipleAnswerFragment);
                    }


                } else {
                    nextValue = 0;
                }
                break;
            case R.id.fragment_document_questions_tv_submit:
                if (questionList.get(nextValue).getAnswer() != null) {
                    if (saveAnswers()) {
                        if (documentListModel.getUsers_document_status().equalsIgnoreCase("0")) {
                            asyncFreeDocumentStatus();
                        }
                        else {
//                            openDialog();
                            openStripeScreen();
                        }


                    }
                }
        }
    }

    private void asyncSubmitAnswer(boolean free) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncSubmitAnswer != null && asyncSubmitAnswer.getStatus() == AsyncTask.Status.PENDING) {
                asyncSubmitAnswer.execute();
            } else if (asyncSubmitAnswer == null || asyncSubmitAnswer.getStatus() == AsyncTask.Status.FINISHED) {
                asyncSubmitAnswer = new AsyncSubmitAnswer(free);
                asyncSubmitAnswer.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolbar();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (asyncGetQuestionList != null && asyncGetQuestionList.getStatus() == AsyncTask.Status.RUNNING) {
            asyncGetQuestionList.cancel(true);
        }
    }

    private boolean saveAnswers() {
        boolean flag = false;
        final Fragment f = getActivity().getFragmentManager().
                findFragmentById(R.id.fragment_document_questions_container_answers);
        if (f instanceof ShortTypeQuestionFragment) {
            String answer = ((ShortTypeQuestionFragment) f).getAnswer();
            if (answer.equalsIgnoreCase("")) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please write " +
                        "answer.");
                flag = false;
            } else {
                questionList.get(nextValue).setAnswer(answer);
                flag = true;

            }
        } else if (f instanceof LongTypeQuestionFragment) {
            String answer = ((LongTypeQuestionFragment) f).getAnswer();
            if (answer.equalsIgnoreCase("")) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please write " +
                        "answer.");


                flag = false;
            } else {
                questionList.get(nextValue).setAnswer(answer);
                flag = true;
            }
        } else if (f instanceof MultipleAnswerFragment) {
            String answer = ((MultipleAnswerFragment) f).getAnswer();
            if (answer.equalsIgnoreCase("")) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please select " +
                        "answer.");
                flag = false;
            }
            else if(((MultipleAnswerFragment) f).getotherAnswerlbl()!=null &&((MultipleAnswerFragment) f).getotherAnswerlbl().equalsIgnoreCase("")){
                Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please write " +
                        "answer.");
                flag = false;
            }
            else {
                questionList.get(nextValue).setAnswer(answer);
                questionList.get(nextValue).setMultipleotherField(((MultipleAnswerFragment) f).getotherAnswer());

                questionList.get(nextValue).setMultipleotherFieldanswer(((MultipleAnswerFragment) f).getotherAnswerlbl());
                flag = true;
            }
        } else if (f instanceof SingleChoiceFragment) {
            String answer = ((SingleChoiceFragment) f).getAnswer();
            if (answer.equalsIgnoreCase("")) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please select " +
                        "answer.");
                flag = false;
            } else {
                questionList.get(nextValue).setAnswer(answer);
                flag = true;
            }
        } else if (f instanceof AddNewQuestionFragment) {
            String[] answers = ((AddNewQuestionFragment) f).getAnswer();
            flag = true;
            for (String answer : answers) {

                String[] options = answer.split(",");
                for (String option : options) {
                    String[] ops1 = option.split(":");

                    if (ops1 == null || ops1.length <= 1) {
                        Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please write " +
                                "answer.");
                        flag = false;
                        break;
                    }
                }
                if (!flag) {
                    break;
                }
            }
            if (flag) {
                questionList.get(nextValue).setAnswers(answers);
            }
        }
        return flag;
    }

    private void openDialog() {
        dialog = new Dialog(getActivity(), R.style.picture_dialog_style);
        dialog.setContentView(R.layout.dialog_payment);
        final WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(wlmp);
        dialog.show();
        final TextView tvPaypal = (TextView) dialog.findViewById(R.id
                .dialog_payment_paypal);
        final TextView tvStripe = (TextView) dialog.findViewById(R.id.dialog_payment_stripe);
        final TextView tvCancel = (TextView) dialog.findViewById(R.id.dialog_payment_cancel);
        tvStripe.setText(R.string.authorize);
        tvPaypal.setVisibility(View.GONE);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvPaypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPaypalScreen();
                dialog.dismiss();

            }
        });
        tvStripe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                openStripeScreen();
                openAuthorizePayment();
                dialog.dismiss();
            }
        });
    }

    private void openStripeScreen() {
        final StripeFragment stripePaymentFragment = new StripeFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_Document_MODEL, documentListModel);
        stripePaymentFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, stripePaymentFragment,
                CaseDetailFragment.class
                        .getSimpleName());
        stripePaymentFragment.setTargetFragment(DocumentQuestionsFragment.this, REQUEST_CODE_STRIPE);
        fragmentTransaction.addToBackStack(this.getClass().getSimpleName());
        fragmentTransaction.hide(DocumentQuestionsFragment.this);
        fragmentTransaction.commit();
    }

    private void openAuthorizePayment() {
        final AuthorizeDocumentPaymentFragment authorizeDocumentPaymentFragment = new AuthorizeDocumentPaymentFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_Document_MODEL, documentListModel);
        authorizeDocumentPaymentFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, authorizeDocumentPaymentFragment,
                AuthorizeDocumentPaymentFragment.class
                        .getSimpleName());
        fragmentTransaction.addToBackStack(AuthorizeDocumentPaymentFragment.class.getSimpleName());
        authorizeDocumentPaymentFragment.setTargetFragment(DocumentQuestionsFragment.this, REQUEST_CODE_STRIPE);
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    private void openPaypalScreen() {
        final PaypalPaymentFragment paypalPaymentFragment = new PaypalPaymentFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_Document_MODEL, documentListModel);
        paypalPaymentFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, paypalPaymentFragment,
                CaseDetailFragment.class
                        .getSimpleName());
        paypalPaymentFragment.setTargetFragment(DocumentQuestionsFragment.this, REQUEST_CODE_PAYPAL);
        fragmentTransaction.addToBackStack(DocumentQuestionsFragment.this.getClass().getSimpleName());
        fragmentTransaction.hide(DocumentQuestionsFragment.this);
        fragmentTransaction.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_CANCELED) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please try again.");
            return;
        }
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_PAYPAL) {
            AppLog.showLogD("Result", "call on activity fragment");
            /*displayDialog(getActivity(), getString(R.string.app_name), "Thanks your " +
                    "payment has been succes.");*/
            asyncSubmitAnswer(false);

        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_STRIPE) {
            paymentId = data.getExtras().getString("payment_id");
            asyncSubmitAnswer(false);
            //Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please try again.");
        }
    }

    @Override
    public void onItemClick(String answer) {
        flowFinish(answer);
    }


    /**
     * AsyncTask for get question list from webservice
     */
    private class AsyncGetQuestionList extends AsyncTask<Void, Void, Void> {
        private WSDocumentQuestionList wsDocumentQuestionList;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }
            progressDialog.setCancelable(false);

            wsDocumentQuestionList = new WSDocumentQuestionList(getActivity());
        }

        @Override
        protected Void doInBackground(Void... params) {
            wsDocumentQuestionList.executeService(documentListModel.getDocumentId());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (!isAdded()) {
                return;
            }

            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (wsDocumentQuestionList.getCode() == WSConstants.STATUS_SUCCESS) {

                questionList = wsDocumentQuestionList.getQuestionModelArrayList();

                if (wsDocumentQuestionList.getQuestionModelArrayList() != null &&
                        wsDocumentQuestionList.getQuestionModelArrayList()
                                .size() > 0) {
                    tvSubmit.setVisibility(View.VISIBLE);
                    tvNext.setVisibility(View.VISIBLE);
                    if (wsDocumentQuestionList.getQuestionModelArrayList()
                            .size() == 1) {
                        tvSubmit.setVisibility(View.VISIBLE);
                        tvNext.setVisibility(View.GONE);
                    } else {
                        tvSubmit.setVisibility(View.GONE);
                        tvNext.setVisibility(View.VISIBLE);
                    }

                    tvPrivious.setVisibility(View.GONE);
                    rlmain.setVisibility(View.VISIBLE);
                    tvQuestion.setText(questionList.get(0).getQuestion());
//                    if (questionList.get(0).getFlow_finish().equalsIgnoreCase("yes")) {
//                        tvSubmit.setVisibility(View.VISIBLE);
//                        tvNext.setVisibility(View.GONE);
//                    } else {
//                        tvSubmit.setVisibility(View.GONE);
//                        tvNext.setVisibility(View.VISIBLE);
//                    }
                    tvUnComplete.setText("" + questionList.size());
                    if (questionList.get(0).getQtypecode().equalsIgnoreCase("AddNew")) {

                        AddNewQuestionFragment addNewQuestionFragment = new
                                AddNewQuestionFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("answers", questionList.get(0).getQoption());
                        bundle.putString("addmore", questionList.get(0).getAdd_another());
                        addNewQuestionFragment.setArguments(bundle);
                        setUpMainFragment(addNewQuestionFragment);

                    } else if (questionList.get(0).getQtypecode().equalsIgnoreCase
                            ("ShortAnswerQuestion")) {

                        ShortTypeQuestionFragment shortTypeQuestionFragment = new
                                ShortTypeQuestionFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("answers", questionList.get(0).getQoption());
                        shortTypeQuestionFragment.setArguments(bundle);
                        setUpMainFragment(shortTypeQuestionFragment);

                    } else if (questionList.get(0).getQtypecode().equalsIgnoreCase
                            ("LongAnswerQuestion")) {

                        LongTypeQuestionFragment longTypeQuestionFragment = new
                                LongTypeQuestionFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("answers", questionList.get(0).getQoption());
                        longTypeQuestionFragment.setArguments(bundle);
                        setUpMainFragment(longTypeQuestionFragment);

                    } else if (questionList.get(0).getQtypecode().equalsIgnoreCase
                            ("SingleChoice")) {

                        SingleChoiceFragment singleChoiceFragment = new
                                SingleChoiceFragment();
                        singleChoiceFragment.setOnItemClickListener(DocumentQuestionsFragment.this);
                        Bundle bundle = new Bundle();

                        bundle.putString("answers", questionList.get(0).getQoption());
                        singleChoiceFragment.setArguments(bundle);
                        setUpMainFragment(singleChoiceFragment);
                    } else if (questionList.get(0).getQtypecode().equalsIgnoreCase
                            ("Multiselect")) {

                        final MultipleAnswerFragment multipleAnswerFragment = new
                                MultipleAnswerFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("answers", questionList.get(0).getQoption());
                        multipleAnswerFragment.setArguments(bundle);
                        setUpMainFragment(multipleAnswerFragment);
                    }

                } else {
                    rlmain.setVisibility(View.GONE);
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsDocumentQuestionList.getMessage());

                }
            } else if (wsDocumentQuestionList.getCode() == WSConstants.STATUS_FAIL) {
                rlmain.setVisibility(View.GONE);
                Utils.displayDialog(getActivity(), getString(R.string.app_name), wsDocumentQuestionList.getMessage());

            } else if (wsDocumentQuestionList.getCode() == WSConstants.STATUS_LOGOUT) {
                rlmain.setVisibility(View.GONE);
                Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsDocumentQuestionList
                        .getMessage());

            } else if (wsDocumentQuestionList.getCode() == WSConstants.STATUS_NO_DATA_FAIL) {


            } else {

                Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

            }

        }
    }

    private class AsyncSubmitAnswer extends AsyncTask<Void, Void, Void> {
        private boolean free;

        public AsyncSubmitAnswer(boolean free) {
            this.free = free;

        }

        private WSDocumentSubmitAnswer wsSubmitAnswer;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!free) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage(getString(R.string.msg_loading));
                progressDialog.show();
                progressDialog.setCancelable(false);
            }

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (documentListModel.getUsers_document_status().equalsIgnoreCase("0")) {
                    wsSubmitAnswer = new WSDocumentSubmitAnswer(getActivity(), questionList,
                            documentListModel.getDocumentId(), inserId);
                    wsSubmitAnswer.executeService();
                } else {
                    wsSubmitAnswer = new WSDocumentSubmitAnswer(getActivity(), questionList, documentListModel.getDocumentId(), paymentId);
                    wsSubmitAnswer.executeService();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (!isCancelled()) {

                if (wsSubmitAnswer.getCode() == WSConstants.STATUS_SUCCESS) {
                    /*Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name),
                            wsSubmitAnswer.getMessage());*/
                    if (!free) {
                        final Fragment fragment = getTargetFragment();
                        if (fragment != null) {
                            fragment.onActivityResult(1, Activity.RESULT_OK, null);
                            getFragmentManager().popBackStack();
                        }
                    } else {
                        final Fragment fragment = getTargetFragment();
                        if (fragment != null) {
                            fragment.onActivityResult(2, Activity.RESULT_OK, null);
                            getFragmentManager().popBackStack();
                        }
                    }


                } else if (wsSubmitAnswer.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsSubmitAnswer.getMessage());

                } else if (wsSubmitAnswer.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsSubmitAnswer
                            .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
                }
            }
        }
    }

    private void asyncFreeDocumentStatus() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncFreeDocumentStatus != null &&
                    asyncFreeDocumentStatus.getStatus() == AsyncTask.Status.PENDING) {
                asyncFreeDocumentStatus.execute();
            } else if (asyncFreeDocumentStatus == null ||
                    asyncFreeDocumentStatus.getStatus() == AsyncTask.Status.FINISHED) {
                asyncFreeDocumentStatus = new AsyncFreeDocumentStatus();
                asyncFreeDocumentStatus.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    //freee  document status update class
    private class AsyncFreeDocumentStatus extends AsyncTask<Void, Void, Void> {
        private WsFreeDocument wsFreeDocument;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wsFreeDocument = new WsFreeDocument(getActivity());
                wsFreeDocument.executeService(documentListModel.getDocumentId());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (!isCancelled()) {
                if (wsFreeDocument.getCode() == WSConstants.STATUS_SUCCESS) {
                    inserId = wsFreeDocument.getInsertId();
                    asyncSubmitAnswer(true);
                } else if (wsFreeDocument.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsFreeDocument.getMessage());

                } else if (wsFreeDocument.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsFreeDocument.getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
                }
            }
        }
    }

    public void flowFinish(String answer) {
        if (nextValue != questionList.size() - 1) {
            if (answer.equalsIgnoreCase(questionList.get(nextValue).getFlow_finish())) {
                tvSubmit.setVisibility(View.VISIBLE);
                tvNext.setVisibility(View.GONE);
            } else {
                tvSubmit.setVisibility(View.GONE);
                tvNext.setVisibility(View.VISIBLE);
            }


        }
    }


}
