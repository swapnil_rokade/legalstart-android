package com.adaptingsocial.lawyerapp.fragment;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;

import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.DateSelector;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatePickerFragment extends DialogFragment {
    private boolean isFromDate;
    private DateSelector dateSelector;
    private Calendar mCalendar;
    private boolean isFutureDateAllowed = false;
    private boolean isPastDateAllowed=false;
    private int requestDate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            requestDate = getArguments().getInt(Constants.REQUEST_FOR_DATE);
            isFutureDateAllowed = getArguments().getBoolean(Constants.INTENT_KEY_IS_FUTURE_DATE_ALLOWED);
            isPastDateAllowed=getArguments().getBoolean(Constants.INTENT_KEY_IS_PAST_DATE_ALLOWED);

        }
    }

    public void setFilterData(final boolean isFromDate, final DateSelector dateSelector, final String previousSelectedDate) {
        this.isFromDate = isFromDate;
        this.dateSelector = dateSelector;
        mCalendar = Calendar.getInstance();
        try {
            if (!TextUtils.isEmpty(previousSelectedDate)) {
                final DateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMATE_DD_MMM_YY);
                final Date previousDate = formatter.parse(previousSelectedDate);

                mCalendar.setTime(previousDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        if (mCalendar == null) {
            mCalendar = Calendar.getInstance();
        }
        final DatePickerDialog picker = new DatePickerDialog(getActivity(), null, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH)) {
        };
        picker.setCancelable(true);
        picker.setCanceledOnTouchOutside(true);
        picker.setButton(DialogInterface.BUTTON_POSITIVE, getActivity().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dateSelector != null) {
                    if (isFromDate) {
                        dateSelector.onFromDateSelect(picker.getDatePicker(),requestDate);
                    } else {
                        dateSelector.onToDateSelect(picker.getDatePicker());
                    }
                }
            }
        });
        picker.setButton(DialogInterface.BUTTON_NEGATIVE, getActivity().getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                Log.e("Picker", "Cancel!");
            }
        });



        if (isPastDateAllowed) {
            picker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        }

        /**
         * Restrict the Future Date Selection on Conditionally basis
         * (Restrict it for the Birth Date Selection : MyProfileFragment)
         */
        if (isFutureDateAllowed) {

            picker.getDatePicker().setMaxDate(new Date().getTime());
        }
        picker.show();
        return picker;
    }
}
