package com.adaptingsocial.lawyerapp.fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.QuestionStateAdapter;
import com.adaptingsocial.lawyerapp.model.QuesionCatagoryModel;
import com.adaptingsocial.lawyerapp.model.QuestionDescriptionDetailModal;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsQuestionDescriptionDetail;

import java.util.ArrayList;

/**
 * Created by indianic on 11/08/17.
 */

public class QuestionCatListFragment extends BaseFragment {
    private ListView lvQuestionCatList;
    private ArrayList<QuestionDescriptionDetailModal> questionDescriptionList;
    private StateCategoryListAsyncTask stateCategoryListAsyncTask;
    private QuesionCatagoryModel questionCatagoryModel;
    private QuestionStateAdapter questionStateAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_question_cat_list, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Bundle mBundle = getArguments();
        if (mBundle != null) {
            questionCatagoryModel = mBundle.getParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL);
        }
        initToolBar();
        questionCategoryListUsAsyncTask();
    }

    @Override
    protected void initializeComponent(View view) {
        lvQuestionCatList = (ListView) view.findViewById(R.id
                .fragment_question_list_cat_list_lv_state);
//        lvQuestionCatList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                openQuestionFragment(questionCatagoryModel);
//            }
//        });


    }

    private void questionCategoryListUsAsyncTask() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (stateCategoryListAsyncTask != null && stateCategoryListAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                stateCategoryListAsyncTask.execute();
            } else if (stateCategoryListAsyncTask == null || stateCategoryListAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                stateCategoryListAsyncTask = new StateCategoryListAsyncTask();
                stateCategoryListAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    public class StateCategoryListAsyncTask extends AsyncTask<Void, Void, Void> {
        private WsQuestionDescriptionDetail wsQuestionDescriptionDetail;
        private ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            wsQuestionDescriptionDetail = new WsQuestionDescriptionDetail(getActivity(),questionCatagoryModel);
            wsQuestionDescriptionDetail.executeService(questionCatagoryModel);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (wsQuestionDescriptionDetail.getCode() == WSConstants.STATUS_SUCCESS) {
                if (wsQuestionDescriptionDetail.getQuestionDescriptionList() != null && wsQuestionDescriptionDetail
                        .getQuestionDescriptionList().size() > 0) {
                    questionDescriptionList = wsQuestionDescriptionDetail.getQuestionDescriptionList();
                    questionStateAdapter = new QuestionStateAdapter(getActivity(),
                            questionDescriptionList,
                            QuestionCatListFragment.this,questionCatagoryModel,questionCatagoryModel);
                    lvQuestionCatList.setAdapter(questionStateAdapter);

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsQuestionDescriptionDetail.getMessage());
                }


            } else if (wsQuestionDescriptionDetail.getCode() == WSConstants.STATUS_FAIL) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), wsQuestionDescriptionDetail.getMessage());

            } else if (wsQuestionDescriptionDetail.getCode() == 100) {
                Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsQuestionDescriptionDetail
                        .getMessage());

            } else {
                Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
            }

        }
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, questionCatagoryModel.getName(),
                    0);
            ((HomeActivity) getActivity()).showShadow();
            Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
            toolBar.getMenu().clear();

        }
    }
    private void openQuestionFragment(QuesionCatagoryModel quesionCatagoryModel) {
        final QuestionFragment questionFragment = new QuestionFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL, quesionCatagoryModel);
        questionFragment.setArguments(mBundle);

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, questionFragment,
                QuestionCatListFragment.class
                        .getSimpleName());
        fragmentTransaction.addToBackStack(QuestionCatListFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }
}
