package com.adaptingsocial.lawyerapp.fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adaptingsocial.lawyerapp.R;

/**
 * Created by indianic on 01/05/17.
 */

public class CoomentFragment extends DialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_post_comment, container,
                false);
        return rootView;
    }
}
