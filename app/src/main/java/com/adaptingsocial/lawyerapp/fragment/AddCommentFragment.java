package com.adaptingsocial.lawyerapp.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsAddComment;

/**
 * Created by indianic on 02/05/17.
 */

public class AddCommentFragment extends BaseFragment {
    private AddComentAsyncTask addComentAsyncTask;
    private String id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_post_comment, container, false);


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void initializeComponent(View view) {
        final EditText etComment = (EditText) view.findViewById(R.id.dialog_post_comment_et_add_comment);
        TextView tvPostComment = (TextView) view.findViewById(R.id
                .dialog_post_comment_tv_post_comment);

        final Bundle mBundle = getArguments();
        if (mBundle != null) {
            id = mBundle.getString("id");
        }

        tvPostComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etComment.getText().toString().trim().equalsIgnoreCase("")) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name),
                            getString(R.string.add_comment_msg));
                } else {
                    callPostCommentApi(id, etComment.getText().toString().trim());

                }


            }
        });

    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }


    private void callPostCommentApi(String id, String comment) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (addComentAsyncTask != null && addComentAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                addComentAsyncTask.execute();
            } else if (addComentAsyncTask == null || addComentAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                addComentAsyncTask = new AddComentAsyncTask(id, comment);
                addComentAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name),
                    getString(R.string
                            .check_internet_msg));
        }

    }

    public class AddComentAsyncTask extends AsyncTask<String, Void, Void> {
        private WsAddComment wsAddComment;
        private ProgressDialog progressDialog;
        private String id;
        private String comment;


        public AddComentAsyncTask(String id, String comment) {
            this.id = id;
            this.comment = comment;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(String... params) {
            wsAddComment = new WsAddComment(getActivity());
            wsAddComment.executeService(id, comment);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsAddComment.getCode() == WSConstants.STATUS_SUCCESS) {

                    displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsAddComment.getMessage());


                } else if (wsAddComment.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsAddComment
                            .getMessage());

                } else if (wsAddComment.getCode() == WSConstants.STATUS_NO_DATA_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsAddComment
                            .getMessage());


                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string
                                    .app_name),
                            getString(R.string.something_went_wrong_msg));

                }


            }

        }

    }
    public void displayDialogWithPopBackStack(final Context context, final String title, final String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                dialog.dismiss();
                ((Activity) context).getFragmentManager().popBackStack();

            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }

}
