package com.adaptingsocial.lawyerapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.adaptingsocial.lawyerapp.R;

/**
 * Created by indianic on 04/07/17.
 */

public class LongTypeQuestionFragment extends  BaseFragment {
    private EditText etAnswer;
    private String answers="";
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle mBundle = getArguments();
        if (mBundle != null) {
            answers = mBundle.getString("answersselect");


        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_long_type_question, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etAnswer.setText(answers);
    }

    @Override
    protected void initializeComponent(View view) {
        etAnswer= (EditText) view.findViewById(R.id.fragment_long_type_question_et_answer);

    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }
    public String getAnswer() {
        return etAnswer.getText().toString().trim();

    }
}
