package com.adaptingsocial.lawyerapp.fragment;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.CaseDetailAdapter;
import com.adaptingsocial.lawyerapp.model.CaseListModel;
import com.adaptingsocial.lawyerapp.model.DocumentModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsCaseDetail;

import java.util.ArrayList;


public class CaseDetailFragment extends BaseFragment {
    private ListView lvCaseDetailDocumentList;
    private CaseDetailAdapter caseDetailAdapter;
    private CaseListModel caseModel;
    private TextView tvCaseTitle;
    private TextView tvConcerns;
    private TextView tvDocType;
    private TextView tvPartiesAgreement;
    private String caseName;
    private CaseDetailAsyncTask caseDetailAsyncTask;
    private TextView tvNodata;
    private ArrayList<DocumentModel> DOClIST;

    protected void initializeComponent(View view) {
        final Bundle mBundle = getArguments();
        if (mBundle != null) {
            caseModel = mBundle.getParcelable(Constants.INTENT_KEY_CASE_MODEL);
        }
        tvNodata = (TextView) view.findViewById(R.id.fragment_case_detail_tv_no_data);
        tvCaseTitle = (TextView) view.findViewById(R.id.fragment_case_detail_tv_case_name);
        tvConcerns = (TextView) view.findViewById(R.id.fragment_case_detail_tv_concerns);
        tvDocType = (TextView) view.findViewById(R.id.fragment_case_detail_tv_doc_type);
        tvPartiesAgreement = (TextView) view.findViewById(R.id.fragment_case_detail_tv_parties_agreement);
        lvCaseDetailDocumentList = (ListView) view.findViewById(R.id.fragment_case_detail_lv_document);
        if (caseModel != null) {
            tvCaseTitle.setText("" + caseModel.getCaseTitle());
            tvConcerns.setText("" + caseModel.getConcerns());
            tvDocType.setText("" + caseModel.getDocumentType());
            tvPartiesAgreement.setText("" + caseModel.getAgreement());
            caseName = caseModel.getCaseTitle();
            if (caseModel.getDocumentModelArrayList() != null) {
                if (caseModel.getDocumentModelArrayList().size() > 0 && caseModel
                        .getDocumentModelArrayList() != null) {
                    DOClIST = caseModel.getDocumentModelArrayList();
                    caseDetailAdapter = new CaseDetailAdapter(getActivity(), DOClIST);
                    lvCaseDetailDocumentList.setAdapter(caseDetailAdapter);
                    tvNodata.setVisibility(View.INVISIBLE);
                    lvCaseDetailDocumentList.setVisibility(View.VISIBLE);
                } else {
                    tvNodata.setText(getString(R.string.no_document_uploaded));
                    tvNodata.setVisibility(View.VISIBLE);
                    lvCaseDetailDocumentList.setVisibility(View.INVISIBLE);
                }
            } else {
                tvNodata.setText(getString(R.string.no_document_uploaded));
                tvNodata.setVisibility(View.VISIBLE);
                lvCaseDetailDocumentList.setVisibility(View.GONE);
            }
        }


    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_case_detail, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();

    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, caseName, 0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
        toolBar.inflateMenu(R.menu.menu_update_case);
        toolBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_case_update:
                        openUpdateCaseFragment(caseModel);
                        return true;
                }
                return false;
            }


        });
    }

    private void openUpdateCaseFragment(CaseListModel caseListModel) {
        final UpdateCaseFragment updateCaseFragment = new UpdateCaseFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_CASE_MODEL, caseListModel);
        updateCaseFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, updateCaseFragment,
                UpdateCaseFragment.class
                        .getSimpleName());
        updateCaseFragment.setTargetFragment(this, 3);
        fragmentTransaction.addToBackStack(UpdateCaseFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }

    private void CaseDetailUsAsyncTask() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (caseDetailAsyncTask != null && caseDetailAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                caseDetailAsyncTask.execute();
            } else if (caseDetailAsyncTask == null || caseDetailAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                caseDetailAsyncTask = new CaseDetailAsyncTask();
                caseDetailAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    public class CaseDetailAsyncTask extends AsyncTask<String, Void, Void> {
        private WsCaseDetail wsCaseDetail;
        private ProgressDialog progressDialog;


        public CaseDetailAsyncTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(String... params) {
            wsCaseDetail = new WsCaseDetail(getActivity());
            wsCaseDetail.executeService(caseModel.getCaseId());
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsCaseDetail.getCode() == WSConstants.STATUS_SUCCESS) {
                    if (DOClIST != null && DOClIST.size() > 0) {
                        DOClIST.clear();
                    }
                    if (wsCaseDetail.getCaseListModelArrayList() != null && wsCaseDetail.getCaseListModelArrayList().size() > 0) {
                        ArrayList<CaseListModel> caseListModelArrayList = wsCaseDetail.getCaseListModelArrayList();
                        tvCaseTitle.setText("" + caseListModelArrayList.get(0).getCaseTitle());
                        tvConcerns.setText("" + caseListModelArrayList.get(0).getCourtName());
                        tvDocType.setText("" + caseListModelArrayList.get(0).getCaseNo());
                        tvPartiesAgreement.setText("" + caseListModelArrayList.get(0).getCaseBehalfOf());
                        DOClIST = caseListModelArrayList.get(0).getDocumentModelArrayList();

                        caseModel.setCaseTitle(caseListModelArrayList.get(0).getCaseTitle());
                        caseModel.setCourtName(caseListModelArrayList.get(0).getCourtName());
                        caseModel.setCaseNo(caseListModelArrayList.get(0).getCaseNo());
                        caseModel.setCaseBehalfOf(caseListModelArrayList.get(0).getCaseBehalfOf());
                        caseModel.setDocumentModelArrayList(DOClIST);

                        if (DOClIST != null) {
                            if (DOClIST.size() > 0 && DOClIST != null) {
                                DOClIST = caseListModelArrayList.get(0).getDocumentModelArrayList();
                                caseDetailAdapter = new CaseDetailAdapter(getActivity(), DOClIST);
                                lvCaseDetailDocumentList.setAdapter(caseDetailAdapter);
                                tvNodata.setVisibility(View.GONE);
                                lvCaseDetailDocumentList.setVisibility(View.VISIBLE);
                            } else {
                                tvNodata.setText(getString(R.string.no_document_uploaded));
                                tvNodata.setVisibility(View.VISIBLE);
                                lvCaseDetailDocumentList.setVisibility(View.GONE);
                            }
                        } else {
                            tvNodata.setText(getString(R.string.no_document_uploaded));
                            tvNodata.setVisibility(View.VISIBLE);
                            lvCaseDetailDocumentList.setVisibility(View.GONE);
                        }
                    }

                } else if (wsCaseDetail.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsCaseDetail.getMessage());

                } else if (wsCaseDetail.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsCaseDetail
                            .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

                }


            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == 3) {
            AppLog.showLogD("Result", "call on activity fragment");
            CaseDetailUsAsyncTask();
        }
    }
}
