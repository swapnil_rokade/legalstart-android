package com.adaptingsocial.lawyerapp.fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CompanyListModel;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsEmail;


public class CompanyDetailFragment extends BaseFragment {
    private ImageView ivCompanyBackground;
    private ImageView ivCompanyPic;
    private TextView tvComopanyDetail;
    private TextView tvComopanyName;
    private TextView tvComopanyAddress;
    private TextView tvComopanyEmail;
    private CompanyListModel companyListModel;
    private long mLastClickTime = 0;
    private String contactNo = "";
    private String emailAdd = "";
    private String name = "";
    private String address = "";
    private String descreption = "";
    private SendEmailAsyncTask sendEmailAsyncTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_company_detail, container, false);

    }

    @Override
    protected void initializeComponent(View view) {
        ivCompanyBackground = (ImageView) view.findViewById(R.id.fragment_company_detail_iv_background_pic);
        ivCompanyPic = (ImageView) view.findViewById(R.id.fragment_company_detail_iv_company_pic);
        tvComopanyAddress = (TextView) view.findViewById(R.id.fragment_company_detail_tv_company_address);
        tvComopanyName = (TextView) view.findViewById(R.id.fragment_company_detail_tv_company_name);
        tvComopanyDetail = (TextView) view.findViewById(R.id.fragment_company_detail_tv_company_detail);
        tvComopanyEmail = (TextView) view.findViewById(R.id.fragment_company_detail_tv_company_email);
        tvComopanyEmail.setOnClickListener(this);
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        final Bundle bundle = getArguments();
        if (bundle != null) {
            companyListModel = bundle.getParcelable(getString(R.string.str_company_detail));
        }
        if (companyListModel != null) {
            Utils.setImageView(getActivity(), companyListModel.getCompanyImage(), ivCompanyPic);
            Utils.setImageView(getActivity(), companyListModel.getCompanyBackgroundImage(), ivCompanyBackground);
            name = companyListModel.getCompanyName();
            address = companyListModel.getCompanyAddress();
            descreption = companyListModel.getCompanyDescreption();
            tvComopanyName.setText(name);
            tvComopanyAddress.setText(address);
            emailAdd = companyListModel.getCompanyEmail();
            contactNo = companyListModel.getCompanyContact();
        }
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, getString(R.string.str_company_detail), 0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(getActivity(), view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        final int id = view.getId();
        switch (id) {
            case R.id.fragment_company_detail_tv_company_email:
//                openEmailEditor(emailAdd);
                if (Utils.isNetworkAvailable(getActivity())) {
                    Bundle bundle = new Bundle();
                    final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
                    bundle.putString("to", emailAdd);
                    bundle.putString("from", preferenceUtils.getString(preferenceUtils.KEY_EMAIL));
                    bundle.putString("message", "message");
                    bundle.putString("subject", "subject");
                    bundle.putString("type", "Company");
                    bundle.putString("companyname", tvComopanyName.getText().toString().trim());
                    sendEmailUsAsyncTask(bundle);

                } else {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
                }
                break;
        }
    }



    private void sendEmailUsAsyncTask(Bundle bundle) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (sendEmailAsyncTask != null && sendEmailAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                sendEmailAsyncTask.execute();
            } else if (sendEmailAsyncTask == null || sendEmailAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                sendEmailAsyncTask = new SendEmailAsyncTask(bundle);
                sendEmailAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    public class SendEmailAsyncTask extends AsyncTask<String, Void, Void> {
        private WsEmail wsEmail;
        private ProgressDialog progressDialog;
        private Bundle bundle;
        public SendEmailAsyncTask(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(String... params) {
            wsEmail = new WsEmail(getActivity());
            wsEmail.executeService(bundle);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsEmail.getCode() == WSConstants.STATUS_SUCCESS) {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsEmail.getMessage());

                } else if (wsEmail.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsEmail.getMessage());

                } else if (wsEmail.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsEmail
                            .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

                }


            }
        }
    }
    public void openEmailEditor(final String emailAdd) {
        final EmailFragment emailFragment = new EmailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("email", emailAdd);
        bundle.putString("companyname",tvComopanyName.getText().toString().trim());
        emailFragment.setArguments(bundle);
        final FragmentManager fragmentManager = getFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, emailFragment,
                EmailFragment.class
                        .getSimpleName());
        fragmentTransaction.addToBackStack(CompanyDetailFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();


    }

    public void openDialUp(final String contactNo) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + contactNo));
        startActivity(callIntent);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }
}
