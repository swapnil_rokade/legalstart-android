package com.adaptingsocial.lawyerapp.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.QuesionCatagoryModel;
import com.adaptingsocial.lawyerapp.model.QuestionDescriptionDetailModal;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;

/**
 * Created by indianic on 06/07/17.
 */

public class PaypalQuestionFragment extends BaseFragment {
    private WebView webView;
    private Bundle mBundle;
    private QuesionCatagoryModel questionCatagoryModel;
    private String paymentUrl;
    private QuestionDescriptionDetailModal questionDescriptionDetailModal;

    @Override
    protected void initializeComponent(View view) {
        webView = (WebView) view.findViewById(R.id.fragment_paypal_wv_payment);
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_paypal, container, false);


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        mBundle = getArguments();
        if (mBundle != null) {
            questionCatagoryModel = mBundle.getParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL);
            questionDescriptionDetailModal = mBundle.getParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_LIST);
            final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
            if (questionCatagoryModel.getName().equalsIgnoreCase("llc")) {
                if (questionDescriptionDetailModal.getAdditionaCharge().equalsIgnoreCase("")) {
                    double ammount = Double.parseDouble(questionCatagoryModel.getAmmount())
                            + Double.parseDouble(questionDescriptionDetailModal
                            .getAmmount());
                    paymentUrl = "http://php54.indianic.com/lawyerapp/WS/questionnaire_paypal_payment?&" + "question_category_id="
                            + questionCatagoryModel.getId() + "&amount=" + String.valueOf(ammount) + "& users_id=" +
                            preferenceUtils.getString(preferenceUtils.KEY_USER_ID) +
                            "&access_token=" + preferenceUtils.getString(preferenceUtils.KEY_ACESS_TOKEN) + "&state_name=" + questionDescriptionDetailModal.getState_name()
                            + "&additional=" + questionDescriptionDetailModal.getAdditionaCharge();
                } else {

                    double ammount = (Double.parseDouble(questionCatagoryModel.getAmmount()))
                            + (Double.parseDouble(questionDescriptionDetailModal
                            .getAmmount())) + (Double.parseDouble(questionDescriptionDetailModal
                            .getAdditionaCharge()));
                    paymentUrl = "http://php54.indianic.com/lawyerapp/WS/questionnaire_paypal_payment?&" + "question_category_id="
                            + questionCatagoryModel.getId() + "&amount=" + String.valueOf(ammount) + "& users_id=" +
                            preferenceUtils.getString(preferenceUtils.KEY_USER_ID) +
                            "&access_token=" + preferenceUtils.getString(preferenceUtils.KEY_ACESS_TOKEN) + "&state_name=" + questionDescriptionDetailModal.getState_name()
                            + "&additional=" + questionDescriptionDetailModal.getAdditionaCharge();
                }


            } else {
                if (questionDescriptionDetailModal.getAdditionaCharge().equalsIgnoreCase("")) {
                    double ammount = Double.parseDouble(questionCatagoryModel.getAmmount())
                            + Double.parseDouble(questionDescriptionDetailModal
                            .getAmmount());
                    paymentUrl = "http://php54.indianic.com/lawyerapp/WS/questionnaire_paypal_payment?&" + "question_category_id="
                            + questionCatagoryModel.getId() + "&amount=" + String.valueOf(ammount) + "& users_id=" +
                            preferenceUtils.getString(preferenceUtils.KEY_USER_ID) +
                            "&access_token=" + preferenceUtils.getString(preferenceUtils.KEY_ACESS_TOKEN) + "&state_name=" + questionDescriptionDetailModal.getState_name()
                            + "&additional=" + questionDescriptionDetailModal.getAdditionaCharge();
                } else {
                    double ammount = Double.parseDouble(questionCatagoryModel.getAmmount())
                            + Double.parseDouble(questionDescriptionDetailModal
                            .getAmmount() + Double.parseDouble(questionDescriptionDetailModal
                            .getAdditionaCharge()));
                    paymentUrl = "http://php54.indianic.com/lawyerapp/WS/questionnaire_paypal_payment?&" + "question_category_id="
                            + questionCatagoryModel.getId() + "&amount=" + String.valueOf(ammount) + "& users_id=" +
                            preferenceUtils.getString(preferenceUtils.KEY_USER_ID) +
                            "&access_token=" + preferenceUtils.getString(preferenceUtils
                            .KEY_ACESS_TOKEN) + "&state_name=" + questionDescriptionDetailModal.getState_name()
                            + "&additional=" + questionDescriptionDetailModal.getAdditionaCharge();
                }
            }


            webView.getSettings().setJavaScriptEnabled(true);
            if (Build.VERSION.SDK_INT >= 21) {
                webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            }
            startWebView(paymentUrl);
        }
    }

    //    http://php54.indianic.com/lawyerapp/WS/questionnaire_paypal_payment?
    // users_id=1&question_category_id=1&access_token=DYtcGpL26O&amount=2
    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, getString(R.string.payment_process), 0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }

    private void startWebView(String url) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i("result", "Processing webview url click..." + view.getUrl());
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                Log.i("result", "Finished loading URL: " + url);
                boolean status = url.contains("fsucces" +
                        "");
                Log.i("result", "payment status" + status);
                if (status) {
                    Log.i("result", "payment value" + url.substring(url.length() - 1));
                    //payment sucess
                    if (url.substring(url.length() - 1).equalsIgnoreCase("1")) {



                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                        getFragmentManager().popBackStack();

                    }
                    //payment failed
                    else {
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, null);
                        getFragmentManager().popBackStack();
                    }

                }


            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e("result", "Error: " + description);
                Toast.makeText(getActivity(), "Oh no! " + description, Toast.LENGTH_SHORT).show();

            }
        });
        webView.loadUrl(paymentUrl);
    }



}
