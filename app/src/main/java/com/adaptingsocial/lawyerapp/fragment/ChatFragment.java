package com.adaptingsocial.lawyerapp.fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.app.Lawyerapp;
import com.adaptingsocial.lawyerapp.chat.model.adapter.ChatAdapter;
import com.adaptingsocial.lawyerapp.chat.model.callback.PaginationHistoryListener;
import com.adaptingsocial.lawyerapp.chat.model.callback.QbChatDialogMessageListenerImp;
import com.adaptingsocial.lawyerapp.chat.model.callback.VerboseQbChatConnectionListener;
import com.adaptingsocial.lawyerapp.chat.model.utill.ChatHelper;
import com.adaptingsocial.lawyerapp.chat.model.utill.QbDialogHolder;
import com.adaptingsocial.lawyerapp.chat.model.utill.QbDialogUtils;
import com.adaptingsocial.lawyerapp.chat.model.utill.Toaster;
import com.adaptingsocial.lawyerapp.utill.ChatDeleteService;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.QBRoster;
import com.quickblox.chat.listeners.QBRosterListener;
import com.quickblox.chat.listeners.QBSubscriptionListener;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.chat.model.QBPresence;
import com.quickblox.chat.utils.DialogUtils;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.SmackException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by indianic on 24/04/17.
 */

public class ChatFragment extends BaseFragment {
    private QBChatDialog qbChatDialog;
    private ChatMessageListener chatMessageListener;
    private StickyListHeadersListView messagesListView;
    private EditText messageEditText;
    private LinearLayout attachmentPreviewContainerLayout;
    private ProgressBar progressBar;
    private int skipPagination = 0;
    private ChatAdapter chatAdapter;
    private ArrayList<QBChatMessage> unShownMessages;
    private Snackbar snackbar;
    private String dialogId;
    private Lawyerapp lawyerapp;
    private ImageButton ibSend;
    private VerboseQbChatConnectionListener chatConnectionListener;
    private static final String PROPERTY_SAVE_TO_HISTORY = "save_to_history";
    private QBRoster chatRoster;
    private QBRosterListener qbRosterListener;


    @Override
    protected void initializeComponent(final View view) {
        lawyerapp = (Lawyerapp) getActivity().getApplicationContext();
        final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
        String blogId = preferenceUtils.getString(preferenceUtils.KEY_QUICK_BLOX_ID);
        qbChatDialog = DialogUtils.buildPrivateDialog(Integer.parseInt(blogId));
        QBRestChatService.createChatDialog(qbChatDialog).performAsync(new QBEntityCallback<QBChatDialog>() {
            @Override
            public void onSuccess(QBChatDialog result, Bundle params) {
                Log.v("RESULT IN CHAT", "dialog iopen sucess" + result.getUpdatedAt());
//                Long tsLong = System.currentTimeMillis()/1000;
//                String ts = tsLong.toString();
//
//                Calendar c = Calendar.getInstance();
//                SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
//                String currentdate = sdf.format(c.getTime());
//                SimpleDateFormat curFormater = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
//                try {
//                    Date datecurrent = curFormater.parse(currentdate);
//                    Date date = result.getUpdatedAt();
//                    long timeDiff = Math.abs(datecurrent.getTime() - date.getTime());
//                    System.out.println("difference:" + timeDiff); //differencs in ms
//                    // if user didn't do anything last 5 minutes (5*60 seconds)
//                    if((timeDiff) > 5*60){
//                        // user is offline now
//
//                        Log.v("RESULT IN CHAT", "offline");
//                    }
//                    else{
//                        Log.v("RESULT IN CHAT", "online");
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }

                preferenceUtils.putString(preferenceUtils.KEY_DIALOG_ID, result.getDialogId());
                qbChatDialog.setDialogId(result.getDialogId());
                qbChatDialog.initForChat(QBChatService.getInstance());
                chatMessageListener = new ChatMessageListener();
                qbChatDialog.addMessageListener(chatMessageListener);
                initChatConnectionListener();
                initViews(view);
                initChat();

            }

            @Override
            public void onError(QBResponseException responseException) {
                Log.v("RESULT IN CHAT", "dialog iopen error");
                preferenceUtils.putString(preferenceUtils.KEY_DIALOG_ID, "");
            }
        });


    }

    @Override
    public void onPause() {
        super.onPause();
        if (chatConnectionListener != null)
            chatConnectionListener.connectionClosed();
    }

    private void initChatConnectionListener() {

        chatConnectionListener = new VerboseQbChatConnectionListener(getSnackbarAnchorView()) {
            @Override
            public void reconnectionSuccessful() {
                super.reconnectionSuccessful();
                skipPagination = 0;
                switch (qbChatDialog.getType()) {
                    case GROUP:
                        chatAdapter = null;
                        // Join active room if we're in Group Chat
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                joinGroupChat();
                            }
                        });
                        break;
                }
            }
        };
    }


    private void joinGroupChat() {
        progressBar.setVisibility(View.VISIBLE);
        ChatHelper.getInstance().join(qbChatDialog, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle b) {
                if (snackbar != null) {
                    snackbar.dismiss();
                }
                loadDialogUsers();
            }

            @Override
            public void onError(QBResponseException e) {
                progressBar.setVisibility(View.GONE);
                snackbar = showErrorSnackbar(R.string.check_internet_msg, e, null);
            }
        });
    }

    @Override
    protected View getSnackbarAnchorView() {
        if (getView() != null) {
            return getView().findViewById(R.id.list_chat_messages);
        } else {
            return null;
        }
    }

    private void initViews(View view) {

        messagesListView = (StickyListHeadersListView) view.findViewById(R.id.list_chat_messages);
        messageEditText = (EditText) view.findViewById(R.id.edit_chat_message);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_chat);

        ibSend = (ImageButton) view.findViewById(R.id.button_chat_send);
        ibSend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onSendChatClick();
            }
        });


    }
    //temp
    // Supposing that your value is an integer declared somewhere as: int myInteger;
    private void sendMessage() {
        // The string "my-integer" will be used to filer the intent
        Intent intent = new Intent("my-integer");
        // Adding some data
        intent.putExtra("message", 1);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


    private void initChat() {
        switch (qbChatDialog.getType()) {
            case GROUP:
            case PUBLIC_GROUP:
                break;

            case PRIVATE:
                loadDialogUsers();
                break;

            default:
                Toaster.shortToast(String.format("%s %s", getString(R.string.chat_unsupported_type), qbChatDialog.getType().name()));
                getActivity().finish();
                break;
        }
    }


    private void loadDialogUsers() {
        ChatHelper.getInstance().getUsersFromDialog(qbChatDialog, new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> users, Bundle bundle) {
                setChatNameToActionBar();
//                ChatHelper.getInstance().addtoRoaster(QbDialogUtils.getOpponentIdForPrivateDialog(qbChatDialog));
//                isUserAvailable(QbDialogUtils.getOpponentIdForPrivateDialog(qbChatDialog));
//                roasterList();


                loadChatHistory();
            }

            @Override
            public void onError(QBResponseException e) {
                showErrorSnackbar(R.string.chat_load_users_error, e,
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadDialogUsers();
                            }
                        });
            }
        });
    }


    private void userpresencechange(final QBPresence presence) {
        Log.e("PresenceChange", "Yes");
        if (presence.getType() == QBPresence.Type.online) {
            Log.e("PresenceChange", "online");
            ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.admin), 1);
            Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
            toolBar.getMenu().clear();

        } else {
            Log.e("PresenceChange", "offline");
            if (getActivity() != null)
                ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.admin), 2);
            Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
            toolBar.getMenu().clear();
//            if (toolBar != null)
//                toolBar.inflateMenu(R.menu.meny_notifychat);
//            toolBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
//                @Override
//                public boolean onMenuItemClick(MenuItem item) {
//                    switch (item.getItemId()) {
//                        case R.id.menu_chat_notify:
//                            openLetsChat();
//                            return true;
//                    }
//                    return false;
//                }
//
//
//            });


        }

    }


    private void loadChatHistory() {
        ChatHelper.getInstance().loadChatHistory(qbChatDialog, skipPagination, new QBEntityCallback<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatMessage> messages, Bundle args) {
                // The newest messages should be in the end of list,
                // so we need to reverse list to show messages in the right order
                Collections.reverse(messages);

//                // Do this after success Chat login
//                chatRoster = QBChatService.getInstance().getRoster(QBRoster.SubscriptionMode.mutual, subscriptionListener);
//                chatRoster.addRosterListener(qbRosterListener);
                if (getActivity() != null)
                    if (chatAdapter == null) {

                        chatAdapter = new ChatAdapter(getActivity(), qbChatDialog, messages);

                        chatAdapter.setPaginationHistoryListener(new PaginationHistoryListener() {
                            @Override
                            public void downloadMore() {
                                loadChatHistory();
                            }
                        });
                        chatAdapter.setOnItemInfoExpandedListener(new ChatAdapter.OnItemInfoExpandedListener() {
                            @Override
                            public void onItemInfoExpanded(final int position) {
                                if (isLastItem(position)) {
                                    // HACK need to allow info textview visibility change so posting it via handler

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            messagesListView.setSelection(position);
                                        }
                                    });
                                } else {
                                    messagesListView.smoothScrollToPosition(position);
                                }
                            }

                            private boolean isLastItem(int position) {
                                return position == chatAdapter.getCount() - 1;
                            }
                        });
                        if (unShownMessages != null && !unShownMessages.isEmpty()) {
                            List<QBChatMessage> chatList = chatAdapter.getList();
                            for (QBChatMessage message : unShownMessages) {
                                if (!chatList.contains(message)) {
                                    chatAdapter.add(message);
                                }
                            }
                        }
                        messagesListView.setAdapter(chatAdapter);
                        messagesListView.setAreHeadersSticky(false);
                        messagesListView.setDivider(null);
                    } else {

                        chatAdapter.addList(messages);
                        messagesListView.setSelection(messages.size());
                    }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(QBResponseException e) {
                progressBar.setVisibility(View.GONE);
                skipPagination -= ChatHelper.CHAT_HISTORY_ITEMS_PER_PAGE;
                snackbar = showErrorSnackbar(R.string.check_internet_msg, e, null);
            }
        });
        skipPagination += ChatHelper.CHAT_HISTORY_ITEMS_PER_PAGE;
    }

    QBSubscriptionListener subscriptionListener = new QBSubscriptionListener() {
        @Override
        public void subscriptionRequested(int userId) {
            // addtoRoaster(userId);
            Log.e("RoasterChatService", "subbbbbbbb roaster=====" + userId);
        }
    };

    private void setChatNameToActionBar() {
        String chatName = QbDialogUtils.getDialogName(qbChatDialog);
        //call toolbar
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_chat, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();


    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.admin), 2);
            ((HomeActivity) getActivity()).showShadow();
            Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
            toolBar.getMenu().clear();
//            toolBar.inflateMenu(R.menu.meny_notifychat);
//            toolBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
//                @Override
//                public boolean onMenuItemClick(MenuItem item) {
//                    switch (item.getItemId()) {
//                        case R.id.menu_chat_notify:
//                            openLetsChat();
//                            return true;
//                    }
//                    return false;
//                }
//
//
//            });
        }
    }

    private void openLetsChat() {
        final LetsChatFragment letsChatFragment = new LetsChatFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, letsChatFragment,
                LetsChatFragment.class
                        .getSimpleName());
        letsChatFragment.setTargetFragment(this, 1);
        fragmentTransaction.addToBackStack(AddCaseFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
            isUserAvailable(QbDialogUtils.getOpponentIdForPrivateDialog(qbChatDialog));
        }
    }

    public class ChatMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {
            showMessage(qbChatMessage);
        }
    }


    private void scrollMessageListDown() {
        messagesListView.setSelection(messagesListView.getCount() - 1);

    }


    public void onSendChatClick() {
        String text = messageEditText.getText().toString().trim();
        if (!TextUtils.isEmpty(text)) {
            sendChatMessage(text, null);
        }
    }


    private void sendChatMessage(String text, QBAttachment attachment) {
        QBChatMessage chatMessage = new QBChatMessage();
        if (attachment != null) {
            chatMessage.addAttachment(attachment);
        } else {
            chatMessage.setBody(text);
        }
        chatMessage.setProperty(PROPERTY_SAVE_TO_HISTORY, "1");
        chatMessage.setDateSent(System.currentTimeMillis() / 1000);
        chatMessage.setMarkable(true);

        if (!QBDialogType.PRIVATE.equals(qbChatDialog.getType()) && !qbChatDialog.isJoined()) {
            Toaster.shortToast("You're still joining a group chat, please wait a bit");
            return;
        }

        try {
            qbChatDialog.sendMessage(chatMessage);

            if (QBDialogType.PRIVATE.equals(qbChatDialog.getType())) {
                showMessage(chatMessage);
            }

            if (attachment != null) {
//                attachmentPreviewAdapter.remove(attachment);
            } else {
                messageEditText.setText("");
            }
        } catch (SmackException.NotConnectedException e) {
            Log.w("Result", e);
            Toaster.shortToast("Can't send a message, You are not connected to chat");
        }
    }


    public void showMessage(QBChatMessage message) {
        if (chatAdapter != null) {
            chatAdapter.add(message);
            scrollMessageListDown();

        } else {
            if (unShownMessages == null) {
                unShownMessages = new ArrayList<>();
            }
            unShownMessages.add(message);


        }
    }


    //online offline users
    public void roasterList() {
        qbRosterListener = new QBRosterListener() {
            @Override
            public void entriesDeleted(Collection<Integer> userIds) {
                Log.e("Roaster", "Delete roaster");
            }

            @Override
            public void entriesAdded(Collection<Integer> userIds) {
                Log.e("Roaster", "Addedto roaster 11111");
            }

            @Override
            public void entriesUpdated(Collection<Integer> userIds) {
                Log.e("Roaster", "Update roaster");
            }

            @Override
            public void presenceChanged(QBPresence presence) {
                Log.e("Roaster", "Presence change roaster Chat screennnnnnnn" + presence.getUserId() + "==" + presence.getType());
                if (presence.getUserId().toString().equalsIgnoreCase(QbDialogUtils.getOpponentIdForPrivateDialog(qbChatDialog).toString())) {
                    userpresencechange(presence);
                }
                //isUserAvailable(presence.getUserId());


            }
        };


        QBSubscriptionListener subscriptionListener = new QBSubscriptionListener() {
            @Override
            public void subscriptionRequested(int userId) {
                // addtoRoaster(userId);
                Log.e("RoasterChatService", "subbbbbbbb roaster=====" + userId);
            }
        };


        // Do this after success Chat login
        chatRoster = QBChatService.getInstance().getRoster(QBRoster.SubscriptionMode.mutual, subscriptionListener);
        chatRoster.addRosterListener(qbRosterListener);
    }

    public void isUserAvailable(int opponentID) {
        QBPresence presence = ChatHelper.getInstance().getChatRoster().getPresence(opponentID);
        if (presence == null) {
            return;
        }
        userpresencechange(presence);
    }


}
