package com.adaptingsocial.lawyerapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.adaptingsocial.lawyerapp.R;

import java.util.Arrays;
import java.util.List;

/**
 * Created by indianic on 04/07/17.
 */

public class SingleChoiceFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {
    private String answers;
    private String selectAnswers;
    private List<String> optionlist;

    private String answer = "";
    private RadioGroup rbAnswers;
    private EditText etOther;
    private String txtFieldOpen;

    private OnFlowItemClickListener onItemClickListener;
    // constructor


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle mBundle = getArguments();
        if (mBundle != null) {
            answers = mBundle.getString("answers");
            selectAnswers = mBundle.getString("answersselect");
            if (mBundle.getString("textfield_open") != null) {
                txtFieldOpen = mBundle.getString("textfield_open");

            }
            optionlist = Arrays.asList(answers.split("\\s*,\\s*"));

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_single_choice_answer, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rbAnswers = (RadioGroup) getView().findViewById(R.id.fragment_single_choice_answer_rb_answer);
        etOther = (EditText) getView().findViewById(R.id.fragment_single_choice_answer_et_other);
        RadioGroup.LayoutParams rprms;
        boolean foundAnswer = false;
        RadioButton rbOther = null;
        for (int k = 0; k < optionlist.size(); k++) {
            final RadioButton radioButton = new RadioButton(getActivity());
            radioButton.setText(optionlist.get(k));
            if (optionlist.get(k).equalsIgnoreCase("other")) {
                rbOther = radioButton;
            }
            if (optionlist.get(k).equalsIgnoreCase(txtFieldOpen)) {
                rbOther = radioButton;
            }
            radioButton.setButtonDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.radio_button_selector));
            radioButton.setId(k);
            radioButton.setPadding(10, 10, 10, 10);
            if (selectAnswers != null) {
                if (optionlist.get(k).equalsIgnoreCase(selectAnswers)) {
                    radioButton.setChecked(true);
                    answer = optionlist.get(k);
                    foundAnswer = true;
                }
            }

            rprms = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            rbAnswers.addView(radioButton, rprms);
            radioButton.setOnCheckedChangeListener(this);
        }

        if (selectAnswers != null && !selectAnswers.isEmpty() && !foundAnswer && rbOther != null) {
            rbOther.setChecked(true);
            etOther.setVisibility(View.VISIBLE);
            etOther.setText(selectAnswers);

        }





    }

    @Override
    protected void initializeComponent(View view) {

    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public String getAnswer() {
        if (etOther.getVisibility() == View.VISIBLE) {
            answer = etOther.getText().toString().trim();
        }
        return answer;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            if (buttonView.getText().toString().trim().equalsIgnoreCase("other")) {
                etOther.setVisibility(View.VISIBLE);

            } else if (buttonView.getText().toString().trim().equalsIgnoreCase(txtFieldOpen)) {
                answer = etOther.getText().toString().trim();
                etOther.setVisibility(View.VISIBLE);
            } else {
                answer = buttonView.getText().toString().trim();
                etOther.setVisibility(View.GONE);
            }
            buttonView.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_sea_green));

            onItemClickListener.onItemClick(buttonView.getText().toString().trim());
        } else {
            buttonView.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorDarkGray));
        }
    }

    public void setOnItemClickListener(OnFlowItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnFlowItemClickListener {
        void onItemClick(String answer);
    }

}
