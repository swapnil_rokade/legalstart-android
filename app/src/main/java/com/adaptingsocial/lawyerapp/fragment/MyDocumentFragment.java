package com.adaptingsocial.lawyerapp.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.MyDocumentAdapter;
import com.adaptingsocial.lawyerapp.model.MyDocumentModel;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WSMyDocument;

import java.util.ArrayList;

public class MyDocumentFragment extends BaseFragment {

    private RecyclerView rvMyDocument;
    private ArrayList<MyDocumentModel> myDocumentModelArrayList;
    private MyDocumentListAsyncTask myDocumentListAsyncTask;
    private MyDocumentAdapter myDocumentAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_document, container, false);
    }

    @Override
    protected void initializeComponent(View view) {
        initToolBar();
        rvMyDocument = (RecyclerView) view.findViewById(R.id.fragment_my_document_rv_document);
        rvMyDocument.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMyDocument.setHasFixedSize(true);
        myDocumentModelArrayList = new ArrayList<>();
        myDocumentListListUsAsyncTask();
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.str_my_document), 0);
            ((HomeActivity) getActivity()).hideShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }

    private void myDocumentListListUsAsyncTask() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (myDocumentListAsyncTask != null && myDocumentListAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                myDocumentListAsyncTask.execute();
            } else if (myDocumentListAsyncTask == null || myDocumentListAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                myDocumentListAsyncTask = new MyDocumentListAsyncTask();
                myDocumentListAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    public class MyDocumentListAsyncTask extends AsyncTask<Void, Void, Void> {
        private WSMyDocument wsMyDocument;
        private ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            wsMyDocument = new WSMyDocument(getActivity());
            wsMyDocument.executeService();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (wsMyDocument.getCode() == WSConstants.STATUS_SUCCESS) {

                if (wsMyDocument.getDocumentList() != null && wsMyDocument.getDocumentList().size() > 0) {
                    myDocumentModelArrayList.addAll(wsMyDocument.getDocumentList());
                    myDocumentAdapter = new MyDocumentAdapter(getActivity(), myDocumentModelArrayList);
                    rvMyDocument.setAdapter(myDocumentAdapter);

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsMyDocument.getMessage());
                }


            } else if (wsMyDocument.getCode() == WSConstants.STATUS_FAIL) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), wsMyDocument.getMessage());

            } else if (wsMyDocument.getCode() == 100) {
                Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsMyDocument
                        .getMessage());

            } else {
                Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
            }

        }
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }
}
