package com.adaptingsocial.lawyerapp.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.QuesionCatagoryModel;
import com.adaptingsocial.lawyerapp.model.QuestionDescriptionDetailModal;
import com.adaptingsocial.lawyerapp.model.SubscriptionModel;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsQuestionStripePayment;
import com.adaptingsocial.lawyerapp.webservice.WsStripeSubscriptionPayment;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;



public class StripeQuestionFragment extends BaseFragment {
//    private static final String PUBLISHABLE_KEY = "pk_test_tg9SgFWR8SqAQbksgLQtkiTq";
    private Button btnpay;
    private CardInputWidget mCardInputWidget;
    private long mLastClickTime = 0;
    private ProgressDialog pd;
    private Bundle mBundle;
    private TextView tvAmmount;
    private QuesionCatagoryModel questionCatagoryModel;
    private AsyncPayment asyncPayment;
    private QuestionDescriptionDetailModal questionDescriptionDetailModal;
    private String amount="";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initializeComponent(View view) {
        btnpay = (Button) view.findViewById(R.id.fragment_stripe_btn_pay);
        mCardInputWidget = (CardInputWidget) view.findViewById(R.id
                .fragment_stripe_card_input_widget);
        tvAmmount = (TextView) view.findViewById(R.id.fragment_stripe_tv_amount);
        btnpay.setOnClickListener(this);

    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_stripe, container, false);

    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        mBundle = getArguments();
        if(mBundle!=null) {
            questionCatagoryModel = mBundle.getParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL);
            questionDescriptionDetailModal = mBundle.getParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_LIST);
        }



        if(!questionDescriptionDetailModal.getAdditionaCharge().equalsIgnoreCase("")){
            double amount = Double.parseDouble(questionDescriptionDetailModal.getAmmount())
                    + Double.parseDouble(questionDescriptionDetailModal
                    .getAdditionaCharge());
            this.amount = String.valueOf(amount);
        }
        else{
            this.amount = questionDescriptionDetailModal.getAmmount();
        }


        tvAmmount.setText("$" + "" + amount);
        final Card cardToSave = mCardInputWidget.getCard();

    }
    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, getString(R.string.payment_process), 0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }
    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(getActivity(), view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        final int id = view.getId();
        switch (id) {
            case R.id.fragment_stripe_btn_pay:
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.loading));
                pd.show();
                final Card cardToSave = mCardInputWidget.getCard();
                if (cardToSave == null) {
                    pd.dismiss();
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.card_invalid));


                }
                else {
                    boolean validation = cardToSave.validateCard();
                    Log.d("Result", "validation is" + validation);
                    Stripe stripe = new Stripe(getActivity(), Constants.PUBLISHABLE_KEY);
                    stripe.createToken(
                            cardToSave,
                            new TokenCallback() {
                                public void onSuccess(Token token) {
                                    // Send token to your server
                                    Log.d("Result", "validation true token is" + token.getId());
                                    callAsyncPayment(token.getId());

                                }
                                public void onError(Exception error) {
                                    Log.d("Result", "token null");
                                    pd.dismiss();
                                    Utils.displayDialog(getActivity(), getString(R.string
                                            .app_name), getString(R.string.token_error));
                                }
                            });


                }

                break;

        }
    }

    private void callAsyncPayment(String token) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncPayment != null && asyncPayment.getStatus() == AsyncTask.Status.PENDING) {
                asyncPayment.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (asyncPayment == null || asyncPayment.getStatus() == AsyncTask.Status.FINISHED) {
                asyncPayment = new AsyncPayment(token);
                asyncPayment.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
                    .alert_no_internet));
        }
    }
    private class AsyncPayment extends AsyncTask<Void, Void, Void> {
        private String token;
        private WsQuestionStripePayment wsStripeSubscriptionPayment;
        public AsyncPayment(String token) {
            this.token = token;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Void doInBackground(Void... params) {
            wsStripeSubscriptionPayment = new WsQuestionStripePayment(getActivity());
            wsStripeSubscriptionPayment.executeService(token, amount,
                    questionCatagoryModel.getId());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            if (wsStripeSubscriptionPayment.getCode() == WSConstants.STATUS_SUCCESS) {
                final String paymentId = wsStripeSubscriptionPayment.getPaymentId();
                final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
                preferenceUtils.putBoolean(preferenceUtils.KEY_APP_USER_TYPE, false);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,  new Intent().putExtra("payment_id",paymentId));
                getFragmentManager().popBackStack();

// Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name),
//                        wsStripeSubscriptionPayment.getMessage());

                Toast.makeText(getActivity(),wsStripeSubscriptionPayment.getMessage(),Toast.LENGTH_LONG).show();
            }
            else if (wsStripeSubscriptionPayment.getCode() == WSConstants.STATUS_FAIL) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), wsStripeSubscriptionPayment.getMessage());

            }
            else {
                Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string
                                .app_name),
                        getString(R.string.something_went_wrong_msg));
            }

        }
    }

}
