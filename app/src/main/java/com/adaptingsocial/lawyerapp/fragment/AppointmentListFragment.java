package com.adaptingsocial.lawyerapp.fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.AppointmentListAdapter;
import com.adaptingsocial.lawyerapp.customview.CustomViewPager;
import com.adaptingsocial.lawyerapp.model.TimeSlotModel;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSAppointmentBookedList;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;

import java.util.ArrayList;

/**
 * Created by indianic on 09/05/17.
 */

public class AppointmentListFragment extends BaseFragment {
    private AsyncAppointmentBookedList asyncAppointmentBookedList;
    private AppointmentListAdapter adapter;
    private ArrayList<TimeSlotModel> pastAppointmentArray;
    private ArrayList<TimeSlotModel> upAppointmentArray;
    private CustomViewPager viewPager;
    private TabLayout tabLayout;
    private Bundle bundle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_appointmentlist, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
    }

    @Override
    protected void initializeComponent(View view) {
        tabLayout = (TabLayout) view.findViewById(R.id.fragment_appointment_list_tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Upcoming Appointment"));
        tabLayout.addTab(tabLayout.newTab().setText("Past Appointment"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabTextColors(
                ContextCompat.getColor(getActivity(), R.color.colorLightGray),
                ContextCompat.getColor(getActivity(), R.color.colorBlue)
        );


        pastAppointmentArray = new ArrayList<>();
        upAppointmentArray = new ArrayList<>();

        viewPager = (CustomViewPager) view.findViewById(R.id.fragment_appointment_list_pager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setPagingEnabled(false);
        bundle = new Bundle();
        final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
        bundle.putString(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        callAsyncGetAppointmentList(bundle);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.tab_appointment_list),0);
            ((HomeActivity) getActivity()).hideShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
        toolBar.inflateMenu(R.menu.menu_case);
        toolBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_case_add:
                        openAddAppointmentScreen();
                        return true;
                }
                return false;
            }


        });
    }

    private void openAddAppointmentScreen() {
        final AppointmentFragment appointmentFragment = new AppointmentFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, appointmentFragment,
                AppointmentFragment.class
                        .getSimpleName());
        appointmentFragment.setTargetFragment(this, 1);
        fragmentTransaction.addToBackStack(AppointmentFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
            if (pastAppointmentArray.size() > 0) {
                pastAppointmentArray.clear();
            }
            callAsyncGetAppointmentList(bundle);
        }
    }


    /**
     * Called for perform user Login
     *
     * @param bundle
     */
    private void callAsyncGetAppointmentList(Bundle bundle) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncAppointmentBookedList != null && asyncAppointmentBookedList.getStatus() == AsyncTask.Status.PENDING) {
                asyncAppointmentBookedList.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (asyncAppointmentBookedList == null || asyncAppointmentBookedList.getStatus() == AsyncTask.Status.FINISHED) {
                asyncAppointmentBookedList = new AsyncAppointmentBookedList(bundle);
                asyncAppointmentBookedList.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.alert_no_internet));
        }
    }

    /**
     * call login web service
     */
    private class AsyncAppointmentBookedList extends AsyncTask<Void, Void, Void> {
        private Bundle bundle;
        private WSAppointmentBookedList wsAppointmentBookedList;
        private ProgressDialog progressDialog;

        public AsyncAppointmentBookedList(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wsAppointmentBookedList = new WSAppointmentBookedList(getActivity());
                wsAppointmentBookedList.executeService(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }


                if (wsAppointmentBookedList.getCode() == WSConstants.STATUS_SUCCESS) {
                    if (wsAppointmentBookedList.getPastAppointmentArray() != null && wsAppointmentBookedList.getPastAppointmentArray().size() > 0) {
                        if (pastAppointmentArray.size() > 0) {
                            pastAppointmentArray.clear();
                        }
                        pastAppointmentArray.addAll(wsAppointmentBookedList.getPastAppointmentArray());
                    }
                    if (wsAppointmentBookedList.getUpAppointmentArray() != null && wsAppointmentBookedList.getUpAppointmentArray().size() > 0) {
                        if (upAppointmentArray.size() > 0) {
                            upAppointmentArray.clear();
                        }
                        upAppointmentArray.addAll(wsAppointmentBookedList.getUpAppointmentArray());
                    }
                    adapter = new AppointmentListAdapter(getFragmentManager(),
                            pastAppointmentArray, upAppointmentArray, tabLayout.getTabCount());
                    viewPager.setAdapter(adapter);
//                    adapter.notifyDataSetChanged();
                }

            else if (wsAppointmentBookedList.getCode() == WSConstants.STATUS_FAIL) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), wsAppointmentBookedList.getMessage());

            }

            else if (wsAppointmentBookedList.getCode() == WSConstants.STATUS_LOGOUT) {
                Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsAppointmentBookedList
                        .getMessage());

            }


            else {
                Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
            }
        }
    }
}



