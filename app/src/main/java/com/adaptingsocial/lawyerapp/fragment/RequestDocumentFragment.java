package com.adaptingsocial.lawyerapp.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.RequestDocumentAdapter;
import com.adaptingsocial.lawyerapp.model.DocumentListModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WSDocumentList;

import java.util.ArrayList;

/**
 * Created by indianic on 20/05/17.
 */

public class RequestDocumentFragment extends BaseFragment implements AdapterView.OnItemClickListener {
    private ListView lvRequestDocument;
    private RequestDocumentAdapter requestDocumentAdapter;
    private DocumentListAsyncTask documentListAsyncTask;
    private ArrayList<DocumentListModel> documentListModelArrayList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_request_document, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();

    }

    @Override
    protected void initializeComponent(View view) {
        documentListModelArrayList = new ArrayList<>();
        lvRequestDocument = (ListView) view.findViewById(R.id.fragment_request_document_lv_document);
        requestDocumentAdapter = new RequestDocumentAdapter(getActivity(),
                documentListModelArrayList, this);
        lvRequestDocument.setAdapter(requestDocumentAdapter);
        lvRequestDocument.setOnItemClickListener(this);
        documentListUsAsyncTask();
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.request_document), 0);
            ((HomeActivity) getActivity()).showShadow();
            Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
            toolBar.getMenu().clear();

        }
    }


    private void documentListUsAsyncTask() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (documentListAsyncTask != null && documentListAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                documentListAsyncTask.execute();
            } else if (documentListAsyncTask == null || documentListAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                documentListAsyncTask = new DocumentListAsyncTask();
                documentListAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }


    public class DocumentListAsyncTask extends AsyncTask<Void, Void, Void> {
        private WSDocumentList wsDocumentList;
        private ProgressDialog progressDialog;
        private int pageIndex;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            wsDocumentList = new WSDocumentList(getActivity());
            wsDocumentList.executeService();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsDocumentList.getCode() == WSConstants.STATUS_SUCCESS) {
                    if (wsDocumentList.getDocumentListModelArrayList() != null && wsDocumentList.getDocumentListModelArrayList().size() > 0) {
                        documentListModelArrayList.clear();
                        documentListModelArrayList.addAll(wsDocumentList
                                .getDocumentListModelArrayList());
                        requestDocumentAdapter.notifyDataSetChanged();
                    }

                }

                else if (wsDocumentList.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsDocumentList
                            .getMessage());

                }

                else if (wsDocumentList.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsDocumentList.getMessage());

                }

                else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
                }
            }
        }
    }

    public void showDownload(View view) {
        Intent i = new Intent();
        i.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
        startActivity(i);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (documentListAsyncTask != null && documentListAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            documentListAsyncTask.cancel(true);
        }

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            AppLog.showLogD("Result", "call on activity fragment");
            displayDialog(getActivity(), getString(R.string.app_name), "Thanks your " +
                    "payment has been succes.");

        }
        else if(resultCode == Activity.RESULT_OK && requestCode == 2){
            documentListUsAsyncTask();
        }


        else if (resultCode == Activity.RESULT_CANCELED && requestCode == 1) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please try again.");
        }
    }

    public  void displayDialog(final Context context, final String title, final String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                documentListUsAsyncTask();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }

    @Override
    public void onResume() {
        super.onResume();
        requestDocumentAdapter.notifyDataSetChanged();
    }
}
