package com.adaptingsocial.lawyerapp.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.chat.model.utill.PREF;
import com.adaptingsocial.lawyerapp.chat.model.utill.SharedPrefsHelper;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsEmail;

import static com.adaptingsocial.lawyerapp.utill.Utils.displayDialog;

/**
 * Created by indianic on 15/06/17.
 */

public class EmailFragment extends BaseFragment {
    private EditText etFrom;
    private EditText etTo;
    private EditText etSubject;
    private EditText etMessage;
    private TextView tvSend;
    private long mLastClickTime = 0;
    private SendEmailAsyncTask sendEmailAsyncTask;
    private Bundle bundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_email, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bundle = getArguments();
        if (bundle != null) {
            initToolBar(getString(R.string.email_company), true);
            String email = bundle.getString("email");
            etTo.setText(email);
            etTo.setVisibility(View.VISIBLE);
        } else {
            initToolBar(getString(R.string.email_to_lawyer), false);
            etTo.setVisibility(View.GONE);
            etFrom.setText(""+ SharedPrefsHelper.getInstance().get(PREF.PREF_USERNAME));
        }


    }

    @Override
    protected void initializeComponent(View view) {
        etFrom = (EditText) view.findViewById(R.id.fragment_email_et_from);
        etTo = (EditText) view.findViewById(R.id.fragment_email_et_to);
        etSubject = (EditText) view.findViewById(R.id.fragment_email_et_subject);
        etMessage = (EditText) view.findViewById(R.id.fragment_email_et_text);
        tvSend = (TextView) view.findViewById(R.id.fragment_email_tv_send);
        tvSend.setOnClickListener(this);

    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public void initToolBar(String title, boolean value) {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(value, title, 0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(getActivity(), view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        final int id = view.getId();
        switch (id) {
            case R.id.fragment_email_tv_send:
                PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
                Boolean freeUser = preferenceUtils.getBoolean(preferenceUtils.KEY_APP_USER_TYPE);
                if (freeUser) {
                    displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.app_paid_msg));
                } else {
                    validateFields();
                    break;
                }
        }
    }

    private void validateFields() {
        String to = etTo.getText().toString().trim();
        String from = etFrom.getText().toString().trim();
        String subject = etSubject.getText().toString().trim();
        String message = etMessage.getText().toString().trim();
        if (bundle == null) {

            if (from.equals("")) {
                displayDialog(getActivity(), getString(R.string.app_name), "Please enter sender" +
                        " address.");
                etFrom.requestFocus();
            } else if (!Utils.isValidEmail(from)) {
                displayDialog(getActivity(), getString(R.string.app_name), "Please enter valid " +
                        "sender address.");
                etFrom.requestFocus();
            } else if (subject.equals("")) {
                displayDialog(getActivity(), getString(R.string.app_name), "Please enter " +
                        "subject"

                );
                etSubject.requestFocus();

            } else if (message.equals("")) {
                displayDialog(getActivity(), getString(R.string.app_name), "Please enter " +
                        "message." +
                        ".");
                etMessage.requestFocus();
            }


            else {
                if (Utils.isNetworkAvailable(getActivity())) {
                    Bundle bundle = new Bundle();
                    bundle.putString("to", to);
                    bundle.putString("from", from);
                    bundle.putString("message", message);
                    bundle.putString("subject", subject);
                    bundle.putString("type", "Lawyer");
                    addCaseUsAsyncTask(bundle);

                } else {
                    displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
                }
            }
        } else {
            if (from.equals("")) {
                displayDialog(getActivity(), getString(R.string.app_name), "Please enter sender" +
                        " address.");
                etFrom.requestFocus();
            } else if (!Utils.isValidEmail(from)) {
                displayDialog(getActivity(), getString(R.string.app_name), "Please enter valid " +
                        "sender address.");
                etFrom.requestFocus();
            } else if (to.equals("")) {
                displayDialog(getActivity(), getString(R.string.app_name), "Please enter receiver" +
                        " address.");
                etTo.requestFocus();

            } else if (!Utils.isValidEmail(to)) {
                displayDialog(getActivity(), getString(R.string.app_name), "Please enter valid " +
                        "receiver address.");
                etTo.requestFocus();
            } else if (subject.equals("")) {
                displayDialog(getActivity(), getString(R.string.app_name), "Please enter " +
                        "subject"

                );
                etSubject.requestFocus();

            } else if (message.equals("")) {
                displayDialog(getActivity(), getString(R.string.app_name), "Please enter " +
                        "message." +
                        ".");
                etMessage.requestFocus();
            } else {
                if (Utils.isNetworkAvailable(getActivity())) {
                    Bundle bundle = new Bundle();
                    bundle.putString("to", to);
                    bundle.putString("from", from);
                    bundle.putString("message", message);
                    bundle.putString("subject", subject);
                    bundle.putString("type", "Company");
                    addCaseUsAsyncTask(bundle);

                } else {
                    displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
                }
            }
        }

    }

    private void addCaseUsAsyncTask(Bundle bundle) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (sendEmailAsyncTask != null && sendEmailAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                sendEmailAsyncTask.execute();
            } else if (sendEmailAsyncTask == null || sendEmailAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                sendEmailAsyncTask = new SendEmailAsyncTask(bundle);
                sendEmailAsyncTask.execute();
            }
        } else {
            displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    public class SendEmailAsyncTask extends AsyncTask<String, Void, Void> {
        private WsEmail wsEmail;
        private ProgressDialog progressDialog;
        private Bundle bundle;

        public SendEmailAsyncTask(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(String... params) {
            wsEmail = new WsEmail(getActivity());
            wsEmail.executeService(bundle);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsEmail.getCode() == WSConstants.STATUS_SUCCESS) {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsEmail.getMessage());

                } else if (wsEmail.getCode() == WSConstants.STATUS_FAIL) {
                    displayDialog(getActivity(), getString(R.string.app_name), wsEmail.getMessage());

                } else if (wsEmail.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsEmail
                            .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

                }


            }
        }
    }
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar(getString(R.string.share_your_concern), true);
        }
    }
}
