//package com.adaptingsocial.lawyerapp.fragment;
//
//import android.app.ProgressDialog;
//import android.os.AsyncTask;
//import android.os.Bundle;
//
//import android.support.v4.view.ViewPager;
//import android.support.v7.widget.Toolbar;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.adaptingsocial.lawyerapp.R;
//import com.adaptingsocial.lawyerapp.adapter.BlogsAdapter;
//
//import com.adaptingsocial.lawyerapp.model.BlogModel;
//import com.adaptingsocial.lawyerapp.utill.AppLog;
//import com.adaptingsocial.lawyerapp.utill.Utils;
//import com.adaptingsocial.lawyerapp.view.HomeActivity;
//import com.adaptingsocial.lawyerapp.webservice.WSBlogList;
//import com.adaptingsocial.lawyerapp.webservice.WSConstants;
//
//import java.util.ArrayList;
//
//
//public class BlogFragment extends BaseFragment implements BlogsAdapter.addCommentListner {
//    private BlogsAdapter blogsAdapter;
//    private ViewPager vpBlogs;
//    private ArrayList<BlogModel> blogModelArrayList;
//    private ArrayList<BlogModel> blogModelFinalArrayList;
//    private BlogListAsyncTask blogListAsyncTask;
//    int i = 0;
//    int currentposition;
//    private String nextPage;
//    private ArrayList<BlogModel> blogClone;
//    private int filePosition = 0;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.fragment_blogs, container, false);
//
//    }
//
//    @Override
//    protected void initializeComponent(View view) {
//        vpBlogs = (ViewPager) view.findViewById(R.id.fragment_blogs_vp);
//        blogModelFinalArrayList = new ArrayList<>();
//
//
//
//
//    }
//
//    @Override
//    public void onViewCreated(View view, Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        initToolBar();
//        blogModelArrayList = new ArrayList<>();
//        CaseListUsAsyncTask("", 0, false);
//        vpBlogs.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//
//
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                AppLog.showLogD("Result", "on pagesscroll" + position);
//
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                AppLog.showLogD("Result", "on page change" + position);
//                currentposition = position;
//
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//                AppLog.showLogD("Result", "on pagesscroll stae chane" + state);
//                if (state == ViewPager.SCROLL_STATE_DRAGGING) {
//                    if (currentposition == vpBlogs.getAdapter().getCount() - 1) {
////                        currentposition = 0;
//                        AppLog.showLogD("Result", "on page change last page");
////                        if (nextPage.equalsIgnoreCase("1")) {
////                            if (i == 0) {
////                                CaseListUsAsyncTask("", 0, false);
////                                i++;
////                            } else {
////                                CaseListUsAsyncTask(String.valueOf(i), 0, false);
////                                i++;
////                            }
////
////                        } else {
////                            Utils.displayDialog(getActivity(), getString(R.string.app_name), "No " +
////                                    "more blog.");
////                        }
//                    }
//                }
//
//            }
//
//
//        });
//
//
//    }
//
//
//    public void CaseListUsAsyncTask(String pageIndex, int position, boolean flag) {
//        if (Utils.isNetworkAvailable(getActivity())) {
//            if (blogListAsyncTask != null && blogListAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
//                blogListAsyncTask.execute();
//            } else if (blogListAsyncTask == null || blogListAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
//                blogListAsyncTask = new BlogListAsyncTask(pageIndex, position, flag);
//                blogListAsyncTask.execute();
//            }
//        } else {
//            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
//        }
//    }
//
//    public void initToolBar() {
//        if (getActivity() instanceof HomeActivity) {
//            ((HomeActivity) getActivity()).showToolBar();
//            ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.blogs));
//        }
//        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
//        toolBar.getMenu().clear();
//    }
//
//    @Override
//    public void onCommentAdd() {
//        blogsAdapter.openDialog(blogModelFinalArrayList.get(vpBlogs.getCurrentItem()-1).getBlogId
//                (), vpBlogs.getCurrentItem()-1);
//    }
//
//    public class BlogListAsyncTask extends AsyncTask<String, Void, Void> {
//        private WSBlogList wsBlogList;
//        private ProgressDialog progressDialog;
//        private String pageIndex;
//        private int position;
//        boolean flag;
//
//        public BlogListAsyncTask(String pageIndex, int position, boolean flag) {
//            this.pageIndex = pageIndex;
//            this.position = position;
//            this.flag = flag;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage(getString(R.string.msg_loading));
//            progressDialog.show();
//            progressDialog.setCancelable(false);
//        }
//
//        @Override
//        protected Void doInBackground(String... params) {
//            wsBlogList = new WSBlogList(getActivity());
//            wsBlogList.executeService(pageIndex);
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            if (progressDialog != null && progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
//            if (!isCancelled()) {
//                if (wsBlogList.getCode() == WSConstants.STATUS_SUCCESS) {
//                    nextPage = wsBlogList.getNextPage();
//                    if (wsBlogList.getBlogListModelArrayList() != null && wsBlogList.getBlogListModelArrayList().size() >
//                            0) {
//                        blogsAdapter = new BlogsAdapter(getActivity(), BlogFragment.this,
//                                wsBlogList.getBlogListModelArrayList(),vpBlogs);
//                        vpBlogs.setAdapter(blogsAdapter);
//                        vpBlogs.setCurrentItem(vpBlogs.getCurrentItem());
//
//
//
//
//
////                            blogModelFinalArrayList.get(vpBlogs.getCurrentItem())
////                                    .setCommentModelArrayList(wsBlogList.getBlogListModelArrayList()
////                                            .get(vpBlogs.getCurrentItem()).getCommentModelArrayList());
////
//
////                        if (flag) {
//////                            blogModelFinalArrayList.get(vpBlogs.getCurrentItem())
//////                                    .setCommentModelArrayList(blogsAdapter.getCommentList());
////                        } else {
////                            blogModelFinalArrayList.addAll(wsBlogList.getBlogListModelArrayList());
////                            View view = (View) vpBlogs.findViewWithTag("myview" + vpBlogs.getCurrentItem());
////                        }
//
//
////                        blogModelFinalArrayList.addAll(wsBlogList.getBlogListModelArrayList());
////                        blogsAdapter.notifyDataSetChanged();
////                        vpBlogs.setCurrentItem(vpBlogs.getCurrentItem() + 1);
////
////                        blogsAdapter = new BlogsAdapter(getActivity(), BlogFragment.this,
////                                blogModelFinalArrayList);
//
////                        vpBlogs.setAdapter(blogsAdapter);
////                        vpBlogs.setCurrentItem(0);
////                        if (view != null) {
////                            if ((ListView) view.findViewById(R.id
////                                    .row_blog_lv_comments) != null) {
////                                CommentAdapter commentAdapter = (CommentAdapter) ((ListView) view.findViewById(R.id
////                                        .row_blog_lv_comments))
////                                        .getAdapter();
////                                if (commentAdapter == null) {
////                                    commentAdapter = new CommentAdapter(getActivity(), blogModelFinalArrayList.get(vpBlogs
////                                            .getCurrentItem()).getCommentModelArrayList(), blogModelFinalArrayList.get(vpBlogs
////                                            .getCurrentItem()).getBlogId());
////                                    ((ListView) view.findViewById(R.id
////                                            .row_blog_lv_comments)).setAdapter(commentAdapter);
////                                } else {
////                                    commentAdapter.setData(blogModelFinalArrayList.get(vpBlogs
////                                            .getCurrentItem()).getCommentModelArrayList());
////                                }
////
////
////                            }
////                        } else {
////                            blogsAdapter.notifyDataSetChanged();
////                        }
//
////                        vpBlogs.setCurrentItem(position);
//
//                    } else {
//                        Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsBlogList.getMessage());
//
//                    }
//
//
//                } else if (wsBlogList.getCode() == WSConstants.STATUS_FAIL) {
//                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsBlogList.getMessage());
//
//                } else if (wsBlogList.getCode() == WSConstants.STATUS_NO_DATA_FAIL) {
//                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsBlogList.getMessage());
//
//
//                } else {
//                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
//
//                }
//
//
//            }
//
//        }
//
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        /**
//         * Cancel the AsyncTask instance if the
//         * Fragment gets destroyed in any case.
//         */
//        if (blogListAsyncTask != null && blogListAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
//            blogListAsyncTask.cancel(true);
//        }
//        if (blogListAsyncTask != null && blogListAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
//            blogListAsyncTask.cancel(true);
//        }
//
//    }
//
//
//}
