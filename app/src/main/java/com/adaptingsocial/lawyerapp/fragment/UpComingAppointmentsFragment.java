package com.adaptingsocial.lawyerapp.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.UpAppointmentAdapter;
import com.adaptingsocial.lawyerapp.model.TimeSlotModel;
import com.adaptingsocial.lawyerapp.swipemenuview.SwipeMenu;
import com.adaptingsocial.lawyerapp.swipemenuview.SwipeMenuCreator;
import com.adaptingsocial.lawyerapp.swipemenuview.SwipeMenuItem;
import com.adaptingsocial.lawyerapp.swipemenuview.SwipeMenuListView;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WSDeleteAppointment;

import java.util.ArrayList;

/**
 * Created by indianic on 09/05/17.
 */

public class UpComingAppointmentsFragment extends BaseFragment implements AdapterView.OnItemClickListener {
    private UpAppointmentAdapter appointmentListAdapter;
    private ArrayList<TimeSlotModel> appointmentModelArrayList;
    private SwipeMenuListView lvUpComingAppointments;
    private TextView tvNodata;
    private DeleteAppointmentAsyncTask deleteAppointmentAsyncTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_upcoming_appointment, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    protected void initializeComponent(View view) {
        lvUpComingAppointments = (SwipeMenuListView) view.findViewById(R.id.fragment_upcoming_appointment_lv_appointment);
        tvNodata = (TextView) view.findViewById(R.id.fragment_upcoming_appointment_tv_no_data);
        lvUpComingAppointments.setOnItemClickListener(this);
        setSwipeMenu();
        if (getArguments() != null) {
            appointmentModelArrayList = getArguments().getParcelableArrayList(Constants.UP_PARCELABLE_ARRAY);
        }
        appointmentListAdapter = new UpAppointmentAdapter(getActivity(), appointmentModelArrayList);
        lvUpComingAppointments.setAdapter(appointmentListAdapter);

        if (appointmentModelArrayList.size() == 0) {
            tvNodata.setText(R.string.str_no_up_comming_appointment_is_available);
            tvNodata.setVisibility(View.VISIBLE);
            lvUpComingAppointments.setVisibility(View.GONE);
        } else {
            tvNodata.setVisibility(View.GONE);
            lvUpComingAppointments.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    private void setSwipeMenu() {
        final SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {

                final SwipeMenuItem swipeMenuItemDelete = new SwipeMenuItem(getActivity());
                swipeMenuItemDelete.setBackground(new ColorDrawable(ContextCompat
                        .getColor(getActivity(), R.color.colorOrange)));
                swipeMenuItemDelete.setWidth(Utils.dpToPx(getActivity(), 70));
                swipeMenuItemDelete.setTitle(R.string.basic_action_1);
                swipeMenuItemDelete.setTitleColor(Color.WHITE);
//                swipeMenuItemDelete.setIcon(R.drawable.ic_remove);
                swipeMenuItemDelete.setTitleSize((int) 15);
                menu.addMenuItem(swipeMenuItemDelete);

            }
        };

        lvUpComingAppointments.setMenuCreator(creator);
        lvUpComingAppointments.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {

                    case 0:
                        displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), "Are you sure want to delete appointment?", position);
                        break;
                }
                return false;
            }
        });


        lvUpComingAppointments.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override
            public void onSwipeStart(int position) {
            }

            @Override
            public void onSwipeEnd(int position) {
            }
        });
    }

    public void displayDialogWithPopBackStack(final Context context, final String title, final String message, final int position) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
                final Bundle bundle = new Bundle();
                bundle.putString(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
                bundle.putString(WSConstants.WS_KEY_APPOINTMENT_ID, appointmentModelArrayList.get(position).getId());
                appointmentModelArrayList.remove(position);
                deleteCaseUsAsyncTask(bundle);
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    private void deleteCaseUsAsyncTask(final Bundle bundle) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (deleteAppointmentAsyncTask != null && deleteAppointmentAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                deleteAppointmentAsyncTask.execute();
            } else if (deleteAppointmentAsyncTask == null || deleteAppointmentAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                deleteAppointmentAsyncTask = new DeleteAppointmentAsyncTask(bundle);
                deleteAppointmentAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    public class DeleteAppointmentAsyncTask extends AsyncTask<String, Void, Void> {
        private final Bundle bundle;
        private WSDeleteAppointment wsDeleteAppointment;
        private ProgressDialog progressDialog;

        public DeleteAppointmentAsyncTask(final Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(String... params) {
            wsDeleteAppointment = new WSDeleteAppointment(getActivity());
            wsDeleteAppointment.executeService(bundle);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsDeleteAppointment.getCode() == WSConstants.STATUS_SUCCESS) {
                    appointmentListAdapter.notifyDataSetChanged();
                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsDeleteAppointment.getMessage());

                }


            } else if (wsDeleteAppointment.getCode() == WSConstants.STATUS_FAIL) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), wsDeleteAppointment.getMessage());

            } else if (appointmentModelArrayList.size() == 0) {
                tvNodata.setText("No upcoming Appointment is available.");
                tvNodata.setVisibility(View.VISIBLE);
                lvUpComingAppointments.setVisibility(View.GONE);
            }

            else if (wsDeleteAppointment.getCode() == WSConstants.STATUS_LOGOUT) {
                Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsDeleteAppointment
                        .getMessage());

            }

            else {
                Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
            }
        }
    }
}


