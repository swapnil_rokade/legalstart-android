package com.adaptingsocial.lawyerapp.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.AddCaseAdapter;
import com.adaptingsocial.lawyerapp.model.DocumentModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.GetFilePathInternalStoarage;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.CustomCameraActivity;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsServiceUploadDocument;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


public class ServiceRequiredFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener {
    private RadioGroup radioGroup;
    private RadioButton rbYes;
    private RadioButton rbNo;
    private RadioGroup rgPayment;
    private Dialog dialog;
    private static final int PICKFILE_REQUEST_CODE = 101;
    private TextView tvUploadDocument;
    private ArrayList<DocumentModel> documentModelArrayList;
    private ListView lvUploadedDocument;
    private AddCaseAdapter addCaseAdapter;
    private LinearLayout llAttachDocument;
    private TextView tvPay;
    private long mLastClickTime = 0;
    private String ammount = "";
    private RadioButton rbPublication;
    private RadioButton rbCertificationNewjersy;
    private RadioButton rbCertificationNewYork;
    private UploadDocumentAsyncTask uploadDocumentAsyncTask;
    private String itemName = "";
    private String from = "";
    private boolean IsBack;
    private TextView tvAttatchLabel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_service_required, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Bundle mBundle = getArguments();
        if (mBundle != null) {
            from = mBundle.getString(Constants.INTENT_KEY_QUESTION_FROM_CATAGORY);
            if (from != null && from.equalsIgnoreCase(Constants.INTENT_KEY_QUESTION_FROM_CATAGORY)) {
                IsBack = true;
                initToolBar(IsBack);
            } else {
                IsBack = false;
                initToolBar(IsBack);
            }

        }


    }

    @Override
    protected void initializeComponent(View view) {
        tvAttatchLabel = (TextView) view.findViewById(R.id.fragment_add_tv_attach_document_text);
        radioGroup = (RadioGroup) view.findViewById(R.id.fragment_service_required_rg_answers);
        rbPublication = (RadioButton) view.findViewById(R.id.fragment_service_required_rb_publication);
        rbCertificationNewYork = (RadioButton) view.findViewById(R.id.fragment_service_required_rb_newyork_certification);
        rbCertificationNewjersy = (RadioButton) view.findViewById(R.id.fragment_service_required_rb_newjersey_certification);
        lvUploadedDocument = (ListView) view.findViewById(R.id.fragment_service_required_lv_upload_document);
        rgPayment = (RadioGroup) view.findViewById(R.id.fragment_service_required_rb_pay);


        rbNo = (RadioButton) view.findViewById(R.id.fragment_service_required_rb_no);
        rbYes = (RadioButton) view.findViewById(R.id.fragment_service_required_rb_yes);
        tvUploadDocument = (TextView) view.findViewById(R.id.fragment_service_required_tv_upload_document);
        llAttachDocument = (LinearLayout) view.findViewById(R.id.fragment_add_ll_attach_document);
        tvPay = (TextView) view.findViewById(R.id.fragment_service_required_tv_pay);
        rgPayment.setOnCheckedChangeListener(this);
//        radioGroup.setOnCheckedChangeListener(this);
        llAttachDocument.setOnClickListener(this);
        tvUploadDocument.setOnClickListener(this);
        tvPay.setOnClickListener(this);
        documentModelArrayList = new ArrayList<>();
        addCaseAdapter = new AddCaseAdapter(getActivity(), documentModelArrayList);
        lvUploadedDocument.setAdapter(addCaseAdapter);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.fragment_service_required_rb_yes) {
                    rgPayment.setVisibility(View.VISIBLE);
                    tvUploadDocument.setVisibility(View.GONE);
                    lvUploadedDocument.setVisibility(View.GONE);
                    llAttachDocument.setVisibility(View.GONE);
                    tvAttatchLabel.setVisibility(View.GONE);
                    tvPay.setVisibility(View.VISIBLE);
                    ammount = "";
                    rgPayment.clearCheck();
                    documentModelArrayList.clear();
                    addCaseAdapter.notifyDataSetChanged();


                } else if (checkedId == R.id.fragment_service_required_rb_no) {


                    rbCertificationNewjersy.setChecked(false);
                    rbCertificationNewYork.setChecked(false);
                    rbPublication.setChecked(false);

                    rgPayment.setVisibility(View.VISIBLE);
                    tvUploadDocument.setVisibility(View.GONE);
                    lvUploadedDocument.setVisibility(View.VISIBLE);
                    llAttachDocument.setVisibility(View.VISIBLE);
                    tvAttatchLabel.setVisibility(View.VISIBLE);
                    tvPay.setVisibility(View.VISIBLE);
                    ammount = "";
                    documentModelArrayList.clear();
                    addCaseAdapter.notifyDataSetChanged();
                    rgPayment.clearCheck();


                }
            }
        });


    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public void initToolBar(boolean back) {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(back, getString(R.string.service_required),
                    0);
            ((HomeActivity) getActivity()).hideShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }

//    @Override
//    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//            if (buttonView.getId() == R.id.fragment_service_required_rb_yes) {
//                if (isChecked) {
//                    rgPayment.setVisibility(View.VISIBLE);
//                    tvUploadDocument.setVisibility(View.GONE);
//                    lvUploadedDocument.setVisibility(View.GONE);
//                    llAttachDocument.setVisibility(View.GONE);
//                    tvPay.setVisibility(View.VISIBLE);
//                    ammount = "";
//                    rbCertificationNewjersy.setChecked(false);
//                    rbCertificationNewYork.setChecked(false);
//                    rbPublication.setChecked(false);
//                }
//            }
//            else if (buttonView.getId() == R.id.fragment_service_required_rb_no) {
//                if (isChecked) {
//                    rgPayment.setVisibility(View.GONE);
//                    tvUploadDocument.setVisibility(View.VISIBLE);
//                    lvUploadedDocument.setVisibility(View.VISIBLE);
//                    llAttachDocument.setVisibility(View.VISIBLE);
//                    tvPay.setVisibility(View.GONE);
//                    ammount = "";
//                }
//
//            }
//            else if (buttonView.getId() == R.id.fragment_service_required_rb_publication) {
//                ammount = Constants.PUBLICATION_PRICE;
//
//            }
//            else if (buttonView.getId() == R.id.fragment_service_required_rb_newyork_certification) {
//                ammount = Constants.NEW_YORK_CERTIFICATION;
//            }
//            else if (buttonView.getId() == R.id.fragment_service_required_rb_newjersey_certification) {
//                ammount = Constants.NEW_JERSY_CERTIFICATION;
//            }
//
//        }


    private void openDialog() {
        dialog = new Dialog(getActivity(), R.style.picture_dialog_style);
        dialog.setContentView(R.layout.dialog_upload);
        final WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(wlmp);
        dialog.show();
        final TextView tvInternalStorage = (TextView) dialog.findViewById(R.id
                .dialog_upload_tv_storage);
        final TextView tvCamera = (TextView) dialog.findViewById(R.id.dialog_upload_tv_camera);
        final TextView tvCancel = (TextView) dialog.findViewById(R.id.dialog_upload_tv_cancel);
        tvCamera.setText("Take a picture");
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvInternalStorage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkSdCardPermissions()) {
                    showFileChooser();
                } else {
                    requestSdCardPermissions(Constants.WRITE_EXTERNAL_STORAGE_PERMISSION, false);
                }
                dialog.dismiss();
            }
        });

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String CAMERA_PERMISSION = Manifest.permission.CAMERA;
                final String WRITE_STORAGE_PERMISSION = Manifest.permission.WRITE_EXTERNAL_STORAGE;
                if (Utils.checkForPermission(getActivity(), CAMERA_PERMISSION) && Utils.checkForPermission(getActivity(), WRITE_STORAGE_PERMISSION)) {
                    Intent iCamera = new Intent(getActivity(), CustomCameraActivity.class);
                    startActivityForResult(iCamera, 4);
                    dialog.dismiss();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{CAMERA_PERMISSION, WRITE_STORAGE_PERMISSION}, Constants.PERMISSION_REQUEST_CAMERA);
                    }
                }


            }
        });
    }

    public void requestSdCardPermissions(int type, boolean isActivity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission
                .WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission
                    .WRITE_EXTERNAL_STORAGE}, type);
        } else {
            // Requesting permission clearly i.e for the first time.
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission
                    .WRITE_EXTERNAL_STORAGE}, type);
        }

    }

    public boolean checkSdCardPermissions() {
        boolean hasPermission = (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            return false;
        } else {
            return true;
        }

    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(
                    Intent.createChooser(intent, getString(R.string.select_file_to_upload)),
                    PICKFILE_REQUEST_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), R.string.please_install_file_manager,
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.WRITE_EXTERNAL_STORAGE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            showFileChooser();
        } else if (requestCode == Constants.PERMISSION_REQUEST_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Intent iCamera = new Intent(getActivity(), CustomCameraActivity.class);
                startActivityForResult(iCamera, 4);
                dialog.dismiss();
            } else {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_permission_camera));
            }
        }

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar(IsBack);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICKFILE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    AppLog.showLogD("Result", "File Uri: " + uri.toString());
                    // Get the path
                    String path = null;
                    path = GetFilePathInternalStoarage.getPath(getActivity(), uri);
                    AppLog.showLogD("Result", "File Path: " + path);
                    if (path != null && !path.equalsIgnoreCase("null")) {
                        String filename = path.substring(path.lastIndexOf("/") + 1);
                        String extension = filename.substring(filename.lastIndexOf("."));
                        String fileNameWithOutExt = filename.replaceFirst("[.][^.]+$", "");
                        AppLog.showLogD("Result", "File name: " + fileNameWithOutExt);
                        AppLog.showLogD("Result", "File ext: " + extension);

                        if ((extension.equalsIgnoreCase(".png")) || (extension.equalsIgnoreCase("" +
                                ".jpeg")) || (extension.equalsIgnoreCase(".xls")) || (extension
                                .equalsIgnoreCase(".doc")) || (extension.equalsIgnoreCase(".txt"))
                                || (extension
                                .equalsIgnoreCase(".pdf")) || (extension.equalsIgnoreCase("" +
                                ".docx")) || (extension.equalsIgnoreCase("" +
                                ".jpg"))) {

                            DocumentModel documentModel = new DocumentModel();
                            documentModel.setFilePath(path);
                            documentModel.setDocumentName(fileNameWithOutExt);
                            documentModel.setImageExt(extension);
                            documentModel.setUploadedBy(getString(R.string.uploaded_by_me));
                            documentModel.setDate(Utils.getCurrentDate());
                            documentModelArrayList.add(documentModel);
                            addCaseAdapter.notifyDataSetChanged();
                        } else {
                            Utils.displayDialog(getActivity(), getString(R.string.app_name),
                                    getString(R.string.unsupported_files));
                        }

                    } else {
                        Utils.displayDialog(getActivity(), getString(R.string.app_name),
                                getString(R.string.unsupported_files));
                    }


                }
                break;
            case 4:
                if (data != null) {
                    String path = data.getStringExtra("path");
                    String fileName = data.getStringExtra("name");
                    DocumentModel documentModel = new DocumentModel();
                    documentModel.setFilePath(path);
                    documentModel.setDocumentName(fileName);
                    documentModel.setImageExt(".pdf");
                    documentModel.setUploadedBy(getString(R.string.uploaded_by_me));
                    documentModel.setDate(Utils.getCurrentDate());
                    documentModelArrayList.add(documentModel);
                    addCaseAdapter.notifyDataSetChanged();

                }
                break;
            case 1:
                if (resultCode == Activity.RESULT_OK && requestCode == 1) {
                    if (documentModelArrayList.size() > 0) {
                        uploadDocumentAsyncTask();
                    }

                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Utils.hideSoftKeyBoard(getActivity(), v);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        final int id = v.getId();
        switch (id) {
            case R.id.fragment_service_required_tv_pay:

//                ================
                if (rbPublication.isChecked() || rbCertificationNewjersy.isChecked() ||
                        rbCertificationNewYork.isChecked()) {
                    Log.d("Result", "checked");
                    if (llAttachDocument.getVisibility() == View.VISIBLE) {
                        if (documentModelArrayList.size() == 0) {
                            Utils.displayDialog(getActivity(), getString(R.string.app_name),
                                    getString(R.string.alert_attach_document));
                        } else {
                            openStripeServiceRequiredFragment();
                        }
                    } else {
                        openStripeServiceRequiredFragment();
                    }


                } else {
                    Log.d("Result", "unchecked");
                    Utils.displayDialog(getActivity(), getString(R.string.app_name),
                            getString(R.string.msg_package_select));

                }
//                =============

                /////old code start
//                if (!ammount.equalsIgnoreCase("") && documentModelArrayList.size() > 0) {
//                    openStripeServiceRequiredFragment();
////                    openPaymentDialog();
//                } else {
//                    if (llAttachDocument.getVisibility() == View.VISIBLE) {
//                        if (documentModelArrayList.size() == 0) {
//                            Utils.displayDialog(getActivity(), getString(R.string.app_name),
//                                    getString(R.string.alert_attach_document));
//                        } else if (!ammount.equalsIgnoreCase("")) {
////                            openPaymentDialog();
//                            openStripeServiceRequiredFragment();
//                        } else {
//                            Utils.displayDialog(getActivity(), getString(R.string.app_name),
//                                    getString(R.string.msg_package_select));
//                        }
//
//                    } else {
//                        if (llAttachDocument.getVisibility() == View.GONE) {
//                            if (!ammount.equalsIgnoreCase("")) {
////                                openPaymentDialog();
//                                openStripeServiceRequiredFragment();
//                            } else {
//                                Utils.displayDialog(getActivity(), getString(R.string.app_name),
//                                        getString(R.string.msg_package_select));
//                            }
//
//                        } else {
//                            Utils.displayDialog(getActivity(), getString(R.string.app_name),
//                                    getString(R.string.msg_package_select));
//                        }
//
//                    }
//
//                }
//old code end
                break;
            case R.id.fragment_service_required_tv_upload_document:
                if (documentModelArrayList.size() > 0) {
                    uploadDocumentAsyncTask();
                } else {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.alert_attach_document));
                }

                break;
            case R.id.fragment_add_ll_attach_document:
                openDialog();
                break;


        }
    }

    private void openPaymentDialog() {
        final Dialog dialog = new Dialog(getActivity(), R.style.picture_dialog_style);
        dialog.setContentView(R.layout.dialog_payment);
        final WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(wlmp);
        dialog.show();
        final TextView tvPaypal = (TextView) dialog.findViewById(R.id
                .dialog_payment_paypal);
        final TextView tvAuthorize = (TextView) dialog.findViewById(R.id.dialog_payment_stripe);
        final TextView tvCancel = (TextView) dialog.findViewById(R.id.dialog_payment_cancel);
        tvAuthorize.setText(R.string.authorize);
        tvPaypal.setVisibility(View.GONE);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvPaypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPaypalScreen();
                dialog.dismiss();

            }
        });
        tvAuthorize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAuthorizePayment();
                dialog.dismiss();

            }
        });

    }

    private void openAuthorizePayment() {
        final AuthorizeServicePaymentFragment authorizeServicePaymentFragment = new AuthorizeServicePaymentFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putString("ammount", ammount);
        mBundle.putString("itemName", itemName);
        authorizeServicePaymentFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, authorizeServicePaymentFragment,
                StripeQuestionFragment.class
                        .getSimpleName());
        authorizeServicePaymentFragment.setTargetFragment(ServiceRequiredFragment.this, 1);
        fragmentTransaction.addToBackStack(PaypalQuestionFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    private void openStripeServiceRequiredFragment() {
        final StripeServiceRequiredFragment stripeServiceRequiredFragment = new StripeServiceRequiredFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putString("ammount", ammount);
        mBundle.putString("itemName", itemName);
        stripeServiceRequiredFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, stripeServiceRequiredFragment,
                StripeQuestionFragment.class
                        .getSimpleName());
        stripeServiceRequiredFragment.setTargetFragment(ServiceRequiredFragment.this, 1);
        fragmentTransaction.addToBackStack(PaypalQuestionFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }


    private void openPaypalScreen() {
        final PaypalServiceRequiredFragment paypalServiceRequiredFragment = new PaypalServiceRequiredFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putString("ammount", ammount);
        paypalServiceRequiredFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, paypalServiceRequiredFragment,
                StripeQuestionFragment.class
                        .getSimpleName());
        paypalServiceRequiredFragment.setTargetFragment(ServiceRequiredFragment.this, 1);
        fragmentTransaction.addToBackStack(PaypalQuestionFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
//        if (checkedId == R.id.fragment_service_required_rb_yes) {
//            rgPayment.setVisibility(View.VISIBLE);
//            tvUploadDocument.setVisibility(View.GONE);
//            lvUploadedDocument.setVisibility(View.GONE);
//            llAttachDocument.setVisibility(View.GONE);
//            tvAttatchLabel.setVisibility(View.GONE);
//            tvPay.setVisibility(View.VISIBLE);
//            ammount = "";
//            if(rbCertificationNewjersy.isChecked()){
//                rbCertificationNewjersy.setChecked(false);
//            }
//            else if(rbCertificationNewYork.isChecked()){
//                rbCertificationNewYork.setChecked(false);
//
//            }
//            else if(rbPublication.isChecked()){
//                rbPublication.setChecked(false);
//            }
////            rbCertificationNewjersy.setChecked(false);
////            rbCertificationNewYork.setChecked(false);
////            rbPublication.setChecked(false);
//
//
////            rgPayment.clearCheck();
//            documentModelArrayList.clear();
//            addCaseAdapter.notifyDataSetChanged();
//
//
//        } else if (checkedId == R.id.fragment_service_required_rb_no) {
////            rbPublication.setSelected(false);
////            rbCertificationNewYork.setSelected(false);
////            rbCertificationNewjersy.setSelected(false);
//
//
////            rbCertificationNewjersy.setChecked(false);
////            rbCertificationNewYork.setChecked(false);
////            rbPublication.setChecked(false);
//
//
//            rgPayment.setVisibility(View.VISIBLE);
//            tvUploadDocument.setVisibility(View.GONE);
//            lvUploadedDocument.setVisibility(View.VISIBLE);
//            llAttachDocument.setVisibility(View.VISIBLE);
//            tvAttatchLabel.setVisibility(View.VISIBLE);
//            tvPay.setVisibility(View.VISIBLE);
//            ammount = "";
//            documentModelArrayList.clear();
//            addCaseAdapter.notifyDataSetChanged();
////            rgPayment.clearCheck();
//
//            if(rbCertificationNewjersy.isChecked()){
//                rbCertificationNewjersy.setChecked(false);
//            }
//            else if(rbCertificationNewYork.isChecked()){
//                rbCertificationNewYork.setChecked(false);
//
//            }
//            else if(rbPublication.isChecked()){
//                rbPublication.setChecked(false);
//            }
//
//
//        }
        if (checkedId == R.id.fragment_service_required_rb_publication) {

            itemName = getString(R.string.NewYorkPublication);
            ammount = Constants.PUBLICATION_PRICE;


        } else if (checkedId == R.id.fragment_service_required_rb_newyork_certification) {
            itemName = getString(R.string.NewYorkCertificate);
            ammount = Constants.NEW_YORK_CERTIFICATION;


        } else if (checkedId == R.id.fragment_service_required_rb_newjersey_certification) {
            itemName = getString(R.string.NewJerseyCertificate);
            ammount = Constants.NEW_JERSY_CERTIFICATION;


        }
    }

    private void uploadDocumentAsyncTask() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (uploadDocumentAsyncTask != null && uploadDocumentAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                uploadDocumentAsyncTask.execute();
            } else if (uploadDocumentAsyncTask == null || uploadDocumentAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                uploadDocumentAsyncTask = new UploadDocumentAsyncTask();
                uploadDocumentAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    public class UploadDocumentAsyncTask extends AsyncTask<String, Void, Void> {
        private WsServiceUploadDocument wsServiceUploadDocument;
        private ProgressDialog progressDialog;

        public UploadDocumentAsyncTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(String... params) {
            wsServiceUploadDocument = new WsServiceUploadDocument(getActivity());
            wsServiceUploadDocument.executeService(documentModelArrayList);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsServiceUploadDocument.getCode() == WSConstants.STATUS_SUCCESS) {
//                    displayDialog(getActivity(), getString(R.string.app_name),
//                            wsServiceUploadDocument.getMessage());
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsServiceUploadDocument.getMessage());
                    documentModelArrayList.clear();
                    addCaseAdapter.notifyDataSetChanged();


                } else if (wsServiceUploadDocument.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsServiceUploadDocument.getMessage());

                } else if (wsServiceUploadDocument.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsServiceUploadDocument
                            .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

                }


            }
        }
    }

    public void displayDialog(final Context context, final String title, final String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                setUpMainFragment(new DashBoardFragment());
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }

    private void setUpMainFragment(Fragment nextFragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_home_container_main, nextFragment, nextFragment.getClass().getSimpleName()
        ).commit();
    }

}
