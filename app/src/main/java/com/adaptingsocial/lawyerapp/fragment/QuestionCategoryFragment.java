package com.adaptingsocial.lawyerapp.fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.QuesionCatagoryModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsQuestionCatagory;

import java.util.ArrayList;



public class QuestionCategoryFragment extends BaseFragment {
    private ArrayList<QuesionCatagoryModel> quesionCatagoryModelArrayList;
    private QuestionCatagoryListAsyncTask questionCatagoryListAsyncTask;
    private ListView lvQuestionCatagory;
    private LinearLayout llParent;
    private float weightSum = 10;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_question_catagory, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        QuestionCatagoryListUsAsyncTask();
        /*lvQuestionCatagory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openQuestionFragment(quesionCatagoryModelArrayList.get(position));
            }
        });*/
    }

    private void openQuestionFragment(QuesionCatagoryModel quesionCatagoryModel) {
        final QuestionFragment questionFragment = new QuestionFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL, quesionCatagoryModel);
        questionFragment.setArguments(mBundle);

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, questionFragment,
                QuestionFragment.class
                        .getSimpleName());
        fragmentTransaction.addToBackStack(QuestionFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    private void openServiceRequiredFragment() {
        final ServiceRequiredFragment serviceRequiredFragment = new ServiceRequiredFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        final Bundle mBundle = new Bundle();
        mBundle.putString(Constants.INTENT_KEY_QUESTION_FROM_CATAGORY, Constants.INTENT_KEY_QUESTION_FROM_CATAGORY);
        serviceRequiredFragment.setArguments(mBundle);
        fragmentTransaction.add(R.id.activity_home_container_main, serviceRequiredFragment,
                ServiceRequiredFragment.class
                        .getSimpleName());

        fragmentTransaction.addToBackStack(ServiceRequiredFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }


    private void openQuestionDescriptionFragment(QuesionCatagoryModel quesionCatagoryModel) {
        final QuestionCatDetailFragment questionFragment = new QuestionCatDetailFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL, quesionCatagoryModel);
        questionFragment.setArguments(mBundle);

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, questionFragment,
                QuestionCatDetailFragment.class
                        .getSimpleName());
        fragmentTransaction.addToBackStack(QuestionCatDetailFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    @Override
    protected void initializeComponent(View view) {
        quesionCatagoryModelArrayList = new ArrayList<>();
        //lvQuestionCatagory = (ListView) view.findViewById(R.id.fragment_question_catagory_lv_case);

        llParent = (LinearLayout) view.findViewById(R.id.fragment_question_catagory_ll_parent);
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    private void QuestionCatagoryListUsAsyncTask() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (questionCatagoryListAsyncTask != null && questionCatagoryListAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                questionCatagoryListAsyncTask.execute();
            } else if (questionCatagoryListAsyncTask == null || questionCatagoryListAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                questionCatagoryListAsyncTask = new QuestionCatagoryListAsyncTask();
                questionCatagoryListAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (questionCatagoryListAsyncTask != null && questionCatagoryListAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            questionCatagoryListAsyncTask.cancel(true);
        }


    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.start_my_buisness), 0);
            ((HomeActivity) getActivity()).showShadow();
            Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
            toolBar.getMenu().clear();

        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }

    /**
     * Set category view to parent layout
     */
    private void setCategoryView() {
        final float weight = weightSum / quesionCatagoryModelArrayList.size();

        for (final QuesionCatagoryModel model : quesionCatagoryModelArrayList) {
            final View view = LayoutInflater.from(getActivity()).inflate(R.layout.row_question_catagory, null);
            final RelativeLayout rlCategory = (RelativeLayout) view.findViewById(R.id.row_question_category_rl_catagory);
            final TextView tvName = (TextView) view.findViewById(R.id.row_question_catagory_tv_name);
            final ImageView ivCatagory = (ImageView) view.findViewById(R.id.row_question_catagory_iv_name);

            final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, weight);
            rlCategory.setLayoutParams(params);
            if (model.getId().equalsIgnoreCase("other service")) {
                tvName.setText(model.getName());
                ivCatagory.setImageResource(R.drawable.otherservices);
                tvName.setVisibility(View.GONE);

            } else {
                tvName.setText(model.getName());
                Utils.setImageView(getActivity(), model.getImage(), ivCatagory);
                tvName.setVisibility(View.GONE);


            }

            rlCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (model.getId().equalsIgnoreCase("other service")) {
                        openServiceRequiredFragment();
                    } else {
                        openQuestionDescriptionFragment(model);
                    }

                }
            });
            llParent.addView(view);
        }
    }

    public class QuestionCatagoryListAsyncTask extends AsyncTask<String, Void, Void> {
        private WsQuestionCatagory wsQuestionCatagory;
        private ProgressDialog progressDialog;

        public QuestionCatagoryListAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(String... params) {
            wsQuestionCatagory = new WsQuestionCatagory(getActivity());
            wsQuestionCatagory.executeService();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsQuestionCatagory.getCode() == WSConstants.STATUS_SUCCESS) {
                    if (wsQuestionCatagory.getQuesionCatagoryModels() != null &&
                            wsQuestionCatagory.getQuesionCatagoryModels()
                                    .size() > 0) {
                        quesionCatagoryModelArrayList = wsQuestionCatagory.getQuesionCatagoryModels();
                        AppLog.showLogD("Result", "size is" + quesionCatagoryModelArrayList.size());
                        /*QuestionCatagoryAdapter questionCatagoryAdapter = new
                                QuestionCatagoryAdapter(getActivity(),
                                quesionCatagoryModelArrayList);
                        lvQuestionCatagory.setAdapter(questionCatagoryAdapter);*/
                        QuesionCatagoryModel quesionCatagoryModel = new QuesionCatagoryModel();
                        quesionCatagoryModel.setId("other service");
                        quesionCatagoryModel.setName("other service");
                        quesionCatagoryModel.setImage("");
                        quesionCatagoryModelArrayList.add(quesionCatagoryModel);
                        setCategoryView();
                    } else {
                        Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsQuestionCatagory.getMessage());

                    }


                } else if (wsQuestionCatagory.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsQuestionCatagory.getMessage());

                } else if (wsQuestionCatagory.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsQuestionCatagory
                            .getMessage());

                } else if (wsQuestionCatagory.getCode() == WSConstants.STATUS_NO_DATA_FAIL) {


                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

                }


            }

        }

    }

}
