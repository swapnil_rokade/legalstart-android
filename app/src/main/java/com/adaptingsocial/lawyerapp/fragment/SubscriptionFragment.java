package com.adaptingsocial.lawyerapp.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.SubscriptionAdapter;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsServiceCancel;
import com.adaptingsocial.lawyerapp.webservice.WsSubscription;


public class SubscriptionFragment extends BaseFragment {
    private ListView lvSubscriptionList;
    private SubscriptionListAsyncTask subscriptionListAsyncTask;
    private SubscriptionAdapter subscriptionAdapter;
    private TextView tvCancelService;
    private long mLastClickTime = 0;
    private String key = "";
    private PreferenceUtils preferenceUtils;

    private CancelServiceAsyncTask cancelServiceAsyncTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_subscription, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        subscriptionListUsAsyncTask();

    }

    @Override
    protected void initializeComponent(View view) {
        lvSubscriptionList = (ListView) view.findViewById(R.id.fragment_subscription_list_lv_subscription);
        tvCancelService = (TextView) view.findViewById(R.id.fragment_subscription_list_bv_cancel);
        tvCancelService.setOnClickListener(this);
        preferenceUtils = new PreferenceUtils(getActivity());
        Boolean freeUser = preferenceUtils.getBoolean(preferenceUtils.KEY_APP_USER_TYPE);
        if (freeUser) {
            tvCancelService.setClickable(false);
            tvCancelService.setBackground(getResources().getDrawable(R.drawable.selector_bg_blue_disable));
            tvCancelService.setVisibility(View.GONE);
        } else {
            tvCancelService.setClickable(true);
            tvCancelService.setBackground(getResources().getDrawable(R.drawable.selector_bg_regitration_create_button));
            tvCancelService.setVisibility(View.VISIBLE);
        }

    }

    private void subscriptionListUsAsyncTask() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (subscriptionListAsyncTask != null && subscriptionListAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                subscriptionListAsyncTask.execute();
            } else if (subscriptionListAsyncTask == null || subscriptionListAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                subscriptionListAsyncTask = new SubscriptionListAsyncTask();
                subscriptionListAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    public class SubscriptionListAsyncTask extends AsyncTask<Void, Void, Void> {
        private WsSubscription wsSubscription;
        private ProgressDialog progressDialog;

        public SubscriptionListAsyncTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            wsSubscription = new WsSubscription(getActivity());
            wsSubscription.executeService();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsSubscription.getCode() == WSConstants.STATUS_SUCCESS) {
                    if (wsSubscription.getSubscriptionModelArrayList() != null && wsSubscription
                            .getSubscriptionModelArrayList().size() > 0) {
                        subscriptionAdapter = new SubscriptionAdapter(getActivity(),
                                wsSubscription.getSubscriptionModelArrayList(),
                                SubscriptionFragment.this);
                        lvSubscriptionList.setAdapter(subscriptionAdapter);

                    }

                } else if (wsSubscription.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsSubscription.getMessage());

                } else if (wsSubscription.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsSubscription
                            .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
                }
            }
        }
    }


    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.subscription_tag),
                    0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (subscriptionListAsyncTask != null &&
                subscriptionListAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            subscriptionListAsyncTask.cancel(true);
        } else if (cancelServiceAsyncTask != null &&
                cancelServiceAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            cancelServiceAsyncTask.cancel(true);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            Boolean freeUser = preferenceUtils.getBoolean(preferenceUtils.KEY_APP_USER_TYPE);
            if (freeUser) {
                tvCancelService.setClickable(false);
                tvCancelService.setBackground(getResources().getDrawable(R.drawable.selector_bg_blue_disable));
                tvCancelService.setVisibility(View.GONE);
            } else {
                tvCancelService.setClickable(true);
                tvCancelService.setBackground(getResources().getDrawable(R.drawable.selector_bg_regitration_create_button));
                tvCancelService.setVisibility(View.VISIBLE);
            }
            subscriptionAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (subscriptionAdapter != null) {
            subscriptionAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(getActivity(), view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        final int id = view.getId();
        switch (id) {
            case R.id.fragment_subscription_list_bv_cancel:
                displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.serveice_cancel_alert));

                break;
        }
    }

    public void displayDialog(final Context context, final String title, final String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(true);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelServiceAsyncTask();
                dialog.dismiss();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }

    private void cancelServiceAsyncTask() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (cancelServiceAsyncTask != null && cancelServiceAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                cancelServiceAsyncTask.execute();
            } else if (cancelServiceAsyncTask == null || cancelServiceAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                cancelServiceAsyncTask = new CancelServiceAsyncTask();
                cancelServiceAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }


    public class CancelServiceAsyncTask extends AsyncTask<String, Void, Void> {
        private WsServiceCancel wsServiceCancel;
        private ProgressDialog progressDialog;

        public CancelServiceAsyncTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(String... params) {
            wsServiceCancel = new WsServiceCancel(getActivity());
            wsServiceCancel.executeService();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsServiceCancel.getCode() == WSConstants.STATUS_SUCCESS) {
                    Boolean freeUser = preferenceUtils.getBoolean(preferenceUtils.KEY_APP_USER_TYPE);
                    if (freeUser) {
                        tvCancelService.setClickable(false);
                        tvCancelService.setBackground(getResources().getDrawable(R.drawable.selector_bg_blue_disable));
                        tvCancelService.setVisibility(View.GONE);
                    } else {
                        tvCancelService.setClickable(true);
                        tvCancelService.setVisibility(View.VISIBLE);
                        tvCancelService.setBackground(getResources().getDrawable(R.drawable.selector_bg_regitration_create_button));
                    }
                    subscriptionAdapter.notifyDataSetChanged();
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsServiceCancel.getMessage());

                } else if (wsServiceCancel.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsServiceCancel.getMessage());

                } else if (wsServiceCancel.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsServiceCancel
                            .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

                }


            }
        }
    }
}
