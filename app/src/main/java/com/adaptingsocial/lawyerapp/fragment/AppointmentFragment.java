package com.adaptingsocial.lawyerapp.fragment;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.DataAdapter;
import com.adaptingsocial.lawyerapp.adapter.SpacesItemDecoration;
import com.adaptingsocial.lawyerapp.model.TimeSlotModel;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSAppointmentList;
import com.adaptingsocial.lawyerapp.webservice.WSBookAppointment;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;


public class AppointmentFragment extends BaseFragment {
    private HorizontalCalendar horizontalCalendar;
    private TextView tvMonth;
    private TextView tvBookAppointment;
    private ImageView ivLeft;
    private ImageView ivRight;
    private String zero = "00";
    private String fiftin = "15";
    private String thirt = "30";
    private String fortyFive = "45";
    private LinearLayout llTimeLable;
    private LinearLayout llRecycleview;
    private ArrayList<TimeSlotModel> timesSlotArray;
    private DataAdapter adapter;
    private AsyncAppointmentList asyncAppointmentList;
    private AsyncBookAppointment asyncBookAppointment;
    private long mLastClickTime = 0;
    private String startTime = "";
    private String endTime = "";
    private String selecatedDate;
    private RecyclerView rvTimeSlot;
    private EditText etAppointmentName;
    private TextView tvNoData;
    private Bundle bundle;
    private int tempPos;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_appointment, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        setUpCalender();
    }

    @Override
    protected void initializeComponent(View view) {
        llTimeLable = (LinearLayout) view.findViewById(R.id.fragment_appointment_ll_time_lable);
        llRecycleview = (LinearLayout) view.findViewById(R.id.fragment_appointment_ll_rv);
        tvMonth = (TextView) view.findViewById(R.id.calender_header_tv_month_year);
        tvNoData = (TextView) view.findViewById(R.id.fragment_appointment_tv_no_data);
        tvBookAppointment = (TextView) view.findViewById(R.id.fragment_appointment_tv_book);
        etAppointmentName = (EditText) view.findViewById(R.id.fragment_appointment_et_name);
        ivLeft = (ImageView) view.findViewById(R.id.calender_header_iv_left);
        ivRight = (ImageView) view.findViewById(R.id.calender_header_iv_right);
        rvTimeSlot = (RecyclerView) view.findViewById(R.id.fragment_appointment_rv_list);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        rvTimeSlot.setLayoutManager(mLayoutManager);
        rvTimeSlot.addItemDecoration(new SpacesItemDecoration((int) getResources().getDimension(R.dimen._5sdp)));
//        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen._10sdp);
//        SpacesItemDecoration itemDecoration = new SpacesItemDecoration(getActivity(), R.dimen._10sdp);
//        rvTimeSlot.addItemDecoration(itemDecoration);
//        rvTimeSlot.setHasFixedSize(true);
//        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
//        rvTimeSlot.setLayoutManager(layoutManager);

        timesSlotArray = new ArrayList<>();
        adapter = new DataAdapter(getActivity(), timesSlotArray);
        rvTimeSlot.setAdapter(adapter);

        ivLeft.setOnClickListener(this);
        ivRight.setOnClickListener(this);
        tvBookAppointment.setOnClickListener(this);
//        generateTimeSlot();
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(getActivity(), view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        final int id = view.getId();
        switch (id) {
            case R.id.fragment_appointment_tv_book:
                startTime = "";
                endTime = "";
                for (int i = 0; i < timesSlotArray.size(); i++) {
                    if (timesSlotArray.get(i).isSelected()) {
                        SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");
                        SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm:ss");
                        try {
                            startTime = date24Format.format(date12Format.parse(timesSlotArray.get(i).getStartTime()));
                            endTime = date24Format.format(date12Format.parse(timesSlotArray.get(i).getEndTime()));
                            Log.e(startTime, endTime);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                final Bundle bundle = new Bundle();
                final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
                bundle.putString(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
                bundle.putString(WSConstants.WS_KEY_AVAILABLE_DATE, selecatedDate);
                bundle.putString(WSConstants.WS_KEY_APPOINTMENT_TITLE, etAppointmentName.getText().toString().trim());
                bundle.putString(WSConstants.WS_KEY_AVAILABLE_START_TIME, startTime);
                bundle.putString(WSConstants.WS_KEY_AVAILABLE_END_TIME, endTime);
                if (TextUtils.isEmpty(etAppointmentName.getText().toString().trim())) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please enter appointment name.");
                } else if (TextUtils.isEmpty(startTime)) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please select time slot to book an appointment.");
                } else {
                    etAppointmentName.setText("");
                    callAsyncBookAppointment(bundle);
                }
                break;
            case R.id.calender_header_iv_left:
                horizontalCalendar.setOnLeftScroll();
                break;
            case R.id.calender_header_iv_right:
                horizontalCalendar.setOnRightScroll();
                break;
        }
    }

    private void setUpCalender() {
        /** end after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 10);

        /** start before 1 month from now */
        Calendar startDate = Calendar.getInstance();
//        startDate.add(Calendar.MONTH, -2);
        horizontalCalendar = new HorizontalCalendar.Builder(getActivity(), R.id.calendarView)
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .datesNumberOnScreen(7)
                .dayNameFormat(getString(R.string.str_eee))
                .dayNumberFormat(getString(R.string.str_dd))
                .monthFormat(getString(R.string.str_mmm))
                .showDayName(true)
                .textSize(18f, 18f, 18f)
                .showMonthName(false)
                .textColor(Color.WHITE, Color.WHITE, ContextCompat.getColor(getActivity(), R.color.colorCyanBlue))
                .selectedDateBackground(ContextCompat.getColor(getActivity(), R.color.colorOrange))
                .build();
        final Date date = new Date();
        final SimpleDateFormat formatter = new SimpleDateFormat(getString(R.string.str_month_year));
        final SimpleDateFormat fullDateFormater = new SimpleDateFormat("yyyy-MM-dd");
        selecatedDate = fullDateFormater.format(date);
        tvMonth.setText(formatter.format(date));
        bundle = new Bundle();
        bundle.putString(WSConstants.WS_KEY_DATE, fullDateFormater.format(date));
        callAsyncGetAppointmentList(bundle);
        final boolean[] firstCall = {true};
        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {
                tvMonth.setText(formatter.format(date));
                selecatedDate = fullDateFormater.format(date);
                bundle.putString(WSConstants.WS_KEY_DATE, selecatedDate);
                if (tempPos != position) {
                    tempPos = position;
                    callAsyncGetAppointmentList(bundle);
                }
            }
        });
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, getString(com.adaptingsocial.lawyerapp.R.string.str_appointment), 0);
            ((HomeActivity) getActivity()).hideShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }


    /**
     * Called for perform user Login
     *
     * @param bundle
     */
    private void callAsyncBookAppointment(Bundle bundle) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncBookAppointment != null && asyncBookAppointment.getStatus() == AsyncTask.Status.PENDING) {
                asyncBookAppointment.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (asyncBookAppointment == null || asyncBookAppointment.getStatus() == AsyncTask.Status.FINISHED) {
                asyncBookAppointment = new AsyncBookAppointment(bundle);
                asyncBookAppointment.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.alert_no_internet));
        }
    }

    /**
     * call login web service
     */
    private class AsyncBookAppointment extends AsyncTask<Void, Void, Void> {
        private Bundle bundle;
        private WSBookAppointment wsBookAppointment;
        private ProgressDialog progressDialog;

        public AsyncBookAppointment(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wsBookAppointment = new WSBookAppointment(getActivity());
                wsBookAppointment.executeService(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (!isCancelled()) {
                if (wsBookAppointment.getCode() == WSConstants.STATUS_SUCCESS) {
//                    final Bundle bundle = new Bundle();
//                    bundle.putString(WSConstants.WS_KEY_DATE, selecatedDate);
//                    callAsyncGetAppointmentList(bundle);
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsBookAppointment.getMessage());
                } else if (wsBookAppointment.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsBookAppointment.getMessage());

                } else if (wsBookAppointment.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsBookAppointment
                            .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
                }
            }
        }
    }

    private void setHandlerForServiceReCall() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                callAsyncGetAppointmentList(bundle);
            }
        }, 10000);
    }

    /**
     * Called for perform user Login
     *
     * @param bundle
     */
    private void callAsyncGetAppointmentList(Bundle bundle) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncAppointmentList != null && asyncAppointmentList.getStatus() == AsyncTask.Status.PENDING) {
                asyncAppointmentList.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (asyncAppointmentList == null || asyncAppointmentList.getStatus() == AsyncTask.Status.FINISHED) {
                asyncAppointmentList = new AsyncAppointmentList(bundle);
                asyncAppointmentList.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.alert_no_internet));
        }
    }

    /**
     * call login web service
     */
    private class AsyncAppointmentList extends AsyncTask<Void, Void, Void> {
        private Bundle bundle;
        private WSAppointmentList wsAppointmentList;
        private ProgressDialog progressDialog;

        public AsyncAppointmentList(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wsAppointmentList = new WSAppointmentList(getActivity());
                wsAppointmentList.executeService(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }


            if (wsAppointmentList.getCode() == WSConstants.STATUS_SUCCESS) {
                adapter.setCheckBoxSelection();
                if (timesSlotArray.size() > 0) {
                    timesSlotArray.clear();
                }
                if (wsAppointmentList.getTimesSlotArray() != null && wsAppointmentList.getTimesSlotArray().size() > 0) {
                    timesSlotArray.addAll(wsAppointmentList.getTimesSlotArray());
                    adapter.notifyDataSetChanged();
//                        setHandlerForServiceReCall();
                    llTimeLable.setVisibility(View.VISIBLE);
                    llRecycleview.setVisibility(View.VISIBLE);
                    tvBookAppointment.setVisibility(View.VISIBLE);
                    tvNoData.setVisibility(View.GONE);
                } else {
                    llTimeLable.setVisibility(View.GONE);
                    llRecycleview.setVisibility(View.GONE);
                    tvBookAppointment.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                }

            } else if (wsAppointmentList.getCode() == WSConstants.STATUS_FAIL) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), wsAppointmentList.getMessage());

            } else if (wsAppointmentList.getCode() == WSConstants.STATUS_LOGOUT) {
                Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsAppointmentList
                        .getMessage());

            } else {
                Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
            }
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (asyncAppointmentList != null && asyncAppointmentList.getStatus() == AsyncTask.Status.RUNNING) {
            asyncAppointmentList.cancel(true);
        }
        if (asyncBookAppointment != null && asyncBookAppointment.getStatus() == AsyncTask.Status.RUNNING) {
            asyncBookAppointment.cancel(true);
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }


}