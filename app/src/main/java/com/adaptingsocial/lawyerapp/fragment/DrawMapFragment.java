package com.adaptingsocial.lawyerapp.fragment;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CompanyListModel;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.DirectionsJSONParser;
import com.adaptingsocial.lawyerapp.utill.GPSTracker;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Date : -
 * <p/>
 * Purpose:- use to draw map between user location and restaurant
 */

public class DrawMapFragment extends BaseFragment implements View.OnClickListener, OnMapReadyCallback {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 10060;
    private GoogleMap map;
    private Double latitude = 0.0;
    private Double longitude = 0.0;
    private Double desLatitude = 0.0;
    private Double desLongitude = 0.0;
    private LatLng origin;
    private LatLng destination;
    private boolean isGpsEnable = false;
    private MapFragment mapFragment;
    private SendUrlToGetDataOfMap sendUrlToGetDataOfMap;
    private GetAllRoutesFromGoogleMap getAllRoutesFromGoogleMap;
    private String data = "";
    private String url = "";
    private String restaurantName = "";
    private long mLastClickTime = 0;
    private CompanyListModel companyListModel;
    private String companyName = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_draw_map, container, false);
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, companyName,0);
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        if (Build.VERSION.SDK_INT < 21) {
            mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.fragment_draw_fragment_map);
        } else {
            mapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_draw_fragment_map);
        }
        mapFragment.getMapAsync(this);

    }

    @Override
    protected void initializeComponent(View view) {
        final GPSTracker gpsTracker = new GPSTracker(getActivity());
        final Bundle bundle = getArguments();
        if (bundle != null) {
            companyListModel = bundle.getParcelable(getString(R.string.str_company_detail));
        }
        companyName = companyListModel.getCompanyName();
        desLatitude = Double.valueOf(companyListModel.getLatitude());
        desLongitude = Double.valueOf(companyListModel.getLongitude());
//        mapView = (MapView) view.findViewById(R.id.fragment_draw_map_mapview);


        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        origin = new LatLng(latitude, longitude);
        destination = new LatLng(desLatitude, desLongitude);

        if (!gpsTracker.canGetLocation()) {
            isGpsEnable = false;
            gpsTracker.showSettingsAlert();
        } else {
            if (checkPlayServices()) {
                isGpsEnable = true;
            }
        }


    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Utils.hideSoftKeyBoard(getActivity(), v);

        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */

        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

//        switch (v.getId()) {
//            case R.id.actionbar_tv_skip:
//
//                final String url = "http://maps.google.com/maps?f=d&daddr=" + desLatitude + "," + desLongitude + "&dirflg=d&layer=t";
//                final Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
//                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//                startActivity(intent);
//            break;
//        }
    }

    /**
     * use to generat direction url
     *
     * @return map url
     */
    private String getDirectionsUrl() {

        // Origin of route
        final String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        final String str_dest = "destination=" + destination.latitude + "," + destination.longitude;

        // Sensor enabled
        final String sensor = "sensor=false";


        // Building the parameters to the web service
        final String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        final String output = "json";

        // Building the url to the web service
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If it doesn't, display a dialog that allows users to download the APK from the Google Play Store or enable it in the device's
     * system settings.
     */
    private boolean checkPlayServices() {

        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(getActivity(), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            }
            return false;
        }
        return true;
    }


    @Override
    public void onResume() {

        final GPSTracker gpsTracker = new GPSTracker(getActivity());

        if (map != null) {
            if (mapFragment != null) {
                mapFragment.onResume();
                if (!gpsTracker.canGetLocation()) {
//                    gpsTracker.showSettingsAlert();
                } else {
                    latitude = gpsTracker.getLatitude();
                    longitude = gpsTracker.getLongitude();
                    if (checkPlayServices()) {
                        origin = new LatLng(latitude, longitude);
                        // Getting URL to the Google Directions API
                        url = getDirectionsUrl();
                        callSendUrlToGetDataOfMap();
                    }
                }
            }
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroy();
        if (map != null) {
            if (mapFragment != null) {
                mapFragment.onDestroy();
            }
        }

        if (sendUrlToGetDataOfMap != null && sendUrlToGetDataOfMap.getStatus() == AsyncTask.Status.RUNNING) {
            sendUrlToGetDataOfMap.cancel(true);
        }
        if (getAllRoutesFromGoogleMap != null && getAllRoutesFromGoogleMap.getStatus() == AsyncTask.Status.RUNNING) {
            getAllRoutesFromGoogleMap.cancel(true);
        }

    }


    /**
     * A method to download json data from url
     */
    private String CreateMapUrl(String strUrl) throws IOException {

        try {
            data = new WSUtil().callServiceHttpPost(strUrl);
        } catch (Exception e) {

        }

        return data;
    }

    /**
     * Call method for SendUrlToGetDataOfMap
     */
    private void callSendUrlToGetDataOfMap() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (sendUrlToGetDataOfMap != null && sendUrlToGetDataOfMap.getStatus() == AsyncTask.Status.PENDING) {
                sendUrlToGetDataOfMap.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
            } else if (sendUrlToGetDataOfMap == null || sendUrlToGetDataOfMap.getStatus() == AsyncTask.Status.FINISHED) {
                sendUrlToGetDataOfMap = new SendUrlToGetDataOfMap();
                sendUrlToGetDataOfMap.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.alert_no_internet));
        }
    }

    /**
     * Call method for Get All Routes from google map
     */
    private void callGetAllRoutesFromGoogleMap(String result) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (getAllRoutesFromGoogleMap != null && getAllRoutesFromGoogleMap.getStatus() == AsyncTask.Status.PENDING) {
                getAllRoutesFromGoogleMap.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, result);
            } else if (getAllRoutesFromGoogleMap == null || getAllRoutesFromGoogleMap.getStatus() == AsyncTask.Status.FINISHED) {
                getAllRoutesFromGoogleMap = new GetAllRoutesFromGoogleMap();
                getAllRoutesFromGoogleMap.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, result);
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.alert_no_internet));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;

        setUpMap();

    }

    public void setUpMap() {

        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.setMyLocationEnabled(true);
        map.setTrafficEnabled(true);
        map.setIndoorEnabled(true);
        map.setBuildingsEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.addMarker(new MarkerOptions()
                .title(restaurantName)
                .position(destination));
        CameraPosition cameraPosition = CameraPosition.builder()
                .target(destination)
                .zoom(12)
                .bearing(0)
                .build();

//        // Animate the change in camera view over 2 seconds
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),
                2000, null);
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(final Marker marker) {
                marker.hideInfoWindow();
            }
        });
        if (isGpsEnable) {
            // Getting URL to the Google Directions API
            url = getDirectionsUrl();
            callSendUrlToGetDataOfMap();
        }

    }

    private class SendUrlToGetDataOfMap extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";
            try {
                data = CreateMapUrl(url[0]);
            } catch (Exception e) {
//                Log.d("Background Task", e.toString());
            }
            return data;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            callGetAllRoutesFromGoogleMap(result);
        }
    }

    /**
     * A class to get Google Places in JSON format
     */
    private class GetAllRoutesFromGoogleMap extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {


            final JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                final DirectionsJSONParser parser = new DirectionsJSONParser();
                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            if (result.size() == 0) {
                Utils.displayDialog(getActivity(), "Directions Not Available", "A route to the nearest road cannot be determined.");
            } else {
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    final List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        final HashMap<String, String> point = path.get(j);

                        final double lat = Double.parseDouble(point.get("lat"));
                        final double lng = Double.parseDouble(point.get("lng"));
                        final LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(5);
                    lineOptions.color(Color.RED);
                }
            }
            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                map.addPolyline(lineOptions);
            }
//            else{
//                Utils.displayDialog(getActivity(),"Directions Not Available","A route to the nearest road cannot be determined.");
//            }
        }
    }


}



