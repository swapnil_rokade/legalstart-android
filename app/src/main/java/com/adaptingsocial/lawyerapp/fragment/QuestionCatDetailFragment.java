package com.adaptingsocial.lawyerapp.fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.QuesionCatagoryModel;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.view.HomeActivity;

/**
 * Created by indianic on 10/08/17.
 */

public class QuestionCatDetailFragment extends BaseFragment {
    private QuesionCatagoryModel questionCatagoryModel;
    private TextView tvNext;
    private WebView wv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_question_cat_detail, container, false);


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Bundle mBundle = getArguments();
        if (mBundle != null) {
            questionCatagoryModel = mBundle.getParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL);
            wv.getSettings().setJavaScriptEnabled(true);
            wv.loadDataWithBaseURL(null,questionCatagoryModel.getDescription() , "text/html", "utf-8", null);
            initToolBar();


        }
    }

    @Override
    protected void initializeComponent(View view) {
        tvNext= (TextView) view.findViewById(R.id.fragment_question_cat_detail_tv_next);
        wv = (WebView) view.findViewById(R.id.fragment_question_cat_detail_wv_description);
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openQuestionCatListFragment(questionCatagoryModel);
            }
        });

    }
    private void openQuestionFragment(QuesionCatagoryModel quesionCatagoryModel) {
        final QuestionFragment questionFragment = new QuestionFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL, quesionCatagoryModel);
        questionFragment.setArguments(mBundle);

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, questionFragment,
                QuestionFragment.class
                        .getSimpleName());
        fragmentTransaction.addToBackStack(QuestionFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    private void openQuestionCatListFragment(QuesionCatagoryModel quesionCatagoryModel) {
        final QuestionCatListFragment questionFragment = new QuestionCatListFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL, quesionCatagoryModel);
        questionFragment.setArguments(mBundle);

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, questionFragment,
                QuestionCatListFragment.class
                        .getSimpleName());
        fragmentTransaction.addToBackStack(QuestionCatListFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }
    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, questionCatagoryModel.getName(),
                    0);
            ((HomeActivity) getActivity()).showShadow();
            Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
            toolBar.getMenu().clear();

        }
    }

}
