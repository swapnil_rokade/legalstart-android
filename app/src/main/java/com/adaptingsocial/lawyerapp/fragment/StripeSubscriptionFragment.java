package com.adaptingsocial.lawyerapp.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.SubscriptionModel;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsStripeSubscriptionPayment;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;


public class StripeSubscriptionFragment extends BaseFragment {
    private SubscriptionModel subscriptionModel;
//    private static final String PUBLISHABLE_KEY = "pk_test_tg9SgFWR8SqAQbksgLQtkiTq";

//    //mayur account
//    private static final String PUBLISHABLE_KEY = "pk_test_U3uyRF4NzmiNMbdRu7hDC2CF";

    private Button btnpay;
    private CardInputWidget mCardInputWidget;
    private long mLastClickTime = 0;
    private ProgressDialog pd;
    private AsyncPayment asyncPayment;
    private Bundle mBundle;
    private TextView tvAmmount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initializeComponent(View view) {
        btnpay = (Button) view.findViewById(R.id.fragment_stripe_btn_pay);
        mCardInputWidget = (CardInputWidget) view.findViewById(R.id
                .fragment_stripe_card_input_widget);
        tvAmmount = (TextView) view.findViewById(R.id.fragment_stripe_tv_amount);
        btnpay.setOnClickListener(this);

    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_stripe, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        mBundle = getArguments();
        subscriptionModel = mBundle.getParcelable(Constants.INTENT_KEY_Document_MODEL);
        tvAmmount.setText("$" + "" + subscriptionModel.getAmmount());
        final Card cardToSave = mCardInputWidget.getCard();

    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, getString(R.string.payment_process), 0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {

            initToolBar();
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(getActivity(), view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        final int id = view.getId();
        switch (id) {
            case R.id.fragment_stripe_btn_pay:
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.loading));
                pd.show();
                final Card cardToSave = mCardInputWidget.getCard();
                if (cardToSave == null) {
                    pd.dismiss();
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.card_invalid));


                } else {
                    boolean validation = cardToSave.validateCard();
                    Log.d("Result", "validation is" + validation);
                    Stripe stripe = new Stripe(getActivity(), Constants.PUBLISHABLE_KEY);
                    stripe.createToken(
                            cardToSave,
                            new TokenCallback() {
                                public void onSuccess(Token token) {
                                    // Send token to your server
                                    Log.d("Result", "validation true token is" + token.getId());
                                    callAsyncPayment(token.getId());
                                }

                                public void onError(Exception error) {
                                    Log.d("Result", "token null");
                                    pd.dismiss();
                                    Utils.displayDialog(getActivity(), getString(R.string
                                            .app_name), getString(R.string.token_error));
                                }
                            });


                }

                break;

        }
    }

    private void callAsyncPayment(String token) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncPayment != null && asyncPayment.getStatus() == AsyncTask.Status.PENDING) {
                asyncPayment.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (asyncPayment == null || asyncPayment.getStatus() == AsyncTask.Status.FINISHED) {
                asyncPayment = new AsyncPayment(token);
                asyncPayment.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string
                    .alert_no_internet));
        }
    }

    private class AsyncPayment extends AsyncTask<Void, Void, Void> {
        private String token;
        private WsStripeSubscriptionPayment wsStripeSubscriptionPayment;

        public AsyncPayment(String token) {
            this.token = token;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Void doInBackground(Void... params) {
            wsStripeSubscriptionPayment = new WsStripeSubscriptionPayment(getActivity());
            wsStripeSubscriptionPayment.executeService(token, subscriptionModel.getAmmount());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            if (wsStripeSubscriptionPayment.getCode() == WSConstants.STATUS_SUCCESS) {
                final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
                preferenceUtils.putBoolean(preferenceUtils.KEY_APP_USER_TYPE, false);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name),
                        wsStripeSubscriptionPayment.getMessage());
            } else if (wsStripeSubscriptionPayment.getCode() == WSConstants.STATUS_FAIL) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), wsStripeSubscriptionPayment.getMessage());

            } else {
                Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string
                                .app_name),
                        getString(R.string.something_went_wrong_msg));
            }

        }
    }


}
