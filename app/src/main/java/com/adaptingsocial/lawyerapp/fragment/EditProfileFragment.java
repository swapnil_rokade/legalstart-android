package com.adaptingsocial.lawyerapp.fragment;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.BuildConfig;
import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.dialog.SelectMediaDialog;
import com.adaptingsocial.lawyerapp.model.UserModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.DateSelector;
import com.adaptingsocial.lawyerapp.utill.GetFilePath;
import com.adaptingsocial.lawyerapp.utill.MediaSelectedListener;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.RoundedImageView;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WSViewProfile;
import com.adaptingsocial.lawyerapp.webservice.WsEditProfile;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

public class EditProfileFragment extends BaseFragment implements DateSelector, MediaSelectedListener {
    private TextView tvSaveProfile;
    private TextView tvDateOfBirth;
    private TextView tvEmail;
    private EditText etFirstName;
    private EditText etLastName;
    private EditText etMobileNumber;
    private long mLastClickTime = 0;
    private RoundedImageView ivProfilePic;
    private String cameraUri;
    private ImageView ivProfileBackgroundPic;
    private Dialog dialog;
    private int PIC_FROM_CAMERA = 0;
    private int PIC_FROM_GALLARY = 1;
    private int PIC_CROP = 2;
    private Uri mTempCameraPhotoFile;
    private String selectedProfileImagePath = "";
    private String selectedBackgroundImagePath = "";
    private String imagePath = "";
    private boolean isProfileBackground = false;
    private String strDate = "";
    private Calendar myCalendar = Calendar.getInstance();
    private String cropImagePath;
    private AsyncViewProfile asyncViewProfile;
    private AsyncEditProfile asyncEditProfile;
    private PreferenceUtils preferenceUtils;
    private String profileImagePath = "";
    private String cameraFilePath;
    private int year = 0;
    private int month = 0;
    private int day = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        preferenceUtils = new PreferenceUtils(getActivity());
        final Bundle bundle = new Bundle();
        bundle.putString(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        callAsyncViewProfile(bundle);
    }

    @Override
    protected void initializeComponent(View view) {
        tvEmail = (TextView) view.findViewById(R.id.activity_edit_profile_et_email);
        tvDateOfBirth = (TextView) view.findViewById(R.id.activity_edit_profile_et_date_of_birth);
        etFirstName = (EditText) view.findViewById(R.id.activity_edit_profile_et_first_name);
        etLastName = (EditText) view.findViewById(R.id.activity_edit_profile_et_last_name);
        etMobileNumber = (EditText) view.findViewById(R.id.activity_edit_profile_et_mobile_number);
        ivProfileBackgroundPic = (ImageView) view.findViewById(R.id.fragment_edit_profile_iv_background);
        ivProfilePic = (RoundedImageView) view.findViewById(R.id.fragment_edit_profile_iv_profilepic);
        tvSaveProfile = (TextView) view.findViewById(R.id.activity_edit_profile_tv_save_profile);
        tvSaveProfile.setOnClickListener(this);
        tvDateOfBirth.setOnClickListener(this);
        ivProfilePic.setOnClickListener(this);
        ivProfileBackgroundPic.setOnClickListener(this);
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }


    private void validation() {
        final String dob = tvDateOfBirth.getText().toString().trim();
        final String firstName = etFirstName.getText().toString().trim();
        final String lastName = etLastName.getText().toString().trim();
        final String mobile = etMobileNumber.getText().toString().trim();
        if (TextUtils.isEmpty(firstName)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_enter_first_name));
            etFirstName.requestFocus();
        } else if (TextUtils.isEmpty(lastName)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_enter_last_name));
            etLastName.requestFocus();
        } else if (TextUtils.isEmpty(mobile)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_enter_mobile));
            etMobileNumber.requestFocus();
        } else if (mobile.length() < 10) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_mobile_not_less_10));
            etMobileNumber.requestFocus();
        } else if (TextUtils.isEmpty(dob)) {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_select_date_of_birth));
            tvDateOfBirth.requestFocus();
        } else {
            final Bundle bundle = new Bundle();
            bundle.putString(WSConstants.WS_KEY_USER_ID, preferenceUtils.KEY_USER_ID);
            bundle.putString(WSConstants.WS_KEY_FIRSTNAME, firstName);
            bundle.putString(WSConstants.WS_KEY_LASTNAME, lastName);
            bundle.putString(WSConstants.WS_KEY_MOBILE_NO, mobile);
            bundle.putString(WSConstants.WS_KEY_DATE_OF_BIRTH, dob);
            bundle.putString(WSConstants.WS_KEY_BACKGROUND_IMAGE, selectedBackgroundImagePath);
            bundle.putString(WSConstants.WS_KEY_PROFILE_IMAGE, selectedProfileImagePath);
            callAsyncEditProfile(bundle);
        }
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.str_edit_profile), 0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(getActivity(), view);
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        final int id = view.getId();
        switch (id) {
            case R.id.activity_edit_profile_tv_save_profile:
                validation();
                break;
            case R.id.fragment_edit_profile_iv_background:
                isProfileBackground = true;
                if (Utils.checkForPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    openDialog();
                } else {
                    requestForPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.PERMISSION_REQUEST_CODE);
                }
                break;
            case R.id.fragment_edit_profile_iv_profilepic:
                isProfileBackground = false;
                if (Utils.checkForPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    openDialog();
                } else {
                    requestForPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.PERMISSION_REQUEST_CODE);
                }
                break;
            case R.id.activity_edit_profile_et_date_of_birth:
                openDatePicker();
                break;

        }
    }

    /**
     * used to open media
     */
    private void openMediaDialog() {

        final SelectMediaDialog dialog = new SelectMediaDialog();
        dialog.setMediaSelectedListener(EditProfileFragment.this);
        dialog.show(getFragmentManager(), SelectMediaDialog.class.getSimpleName());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PIC_FROM_GALLARY) {
                try {
                    final Uri uri = data.getData();
                    final String filePath = GetFilePath.getPath(getActivity(), uri);
                    File file = new File(filePath);
                    if (filePath != null) {
                        AppLog.showLogD("Result", "is" + filePath);
                        String strd = "file://" + filePath;
                        if (isProfileBackground) {
                            selectedBackgroundImagePath = GetFilePath.getPath(getActivity(), uri);
                            Utils.setImageView(getActivity(), strd, ivProfileBackgroundPic);
                        } else {
                            selectedProfileImagePath = GetFilePath.getPath(getActivity(), uri);
                            Utils.getCircleImageView(getActivity(), strd, ivProfilePic);
                        }
                    }
                } catch (Exception e) {
                }
            }
            // Callback from Camera
            else if (requestCode == PIC_FROM_CAMERA) {
                try {
                    if (!TextUtils.isEmpty(cameraFilePath)) {
                        final Uri uri = Uri.fromFile(new File(cameraFilePath));
                        final String filePath = GetFilePath.getPath(getActivity(), uri);
                        File file = new File(filePath);
                        String strd = "file://" + filePath;
                        AppLog.showLogD("Result camera", "is==>" + strd);
                        if (isProfileBackground) {
                            selectedBackgroundImagePath = GetFilePath.getPath(getActivity(), uri);
                            Utils.setImageView(getActivity(), strd, ivProfileBackgroundPic);
                        } else {
                            selectedProfileImagePath = GetFilePath.getPath(getActivity(), uri);
                            Utils.getCircleImageView(getActivity(), strd, ivProfilePic);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


//            if (!TextUtils.isEmpty(cropImagePath)) {
//                this.imagePath = cropImagePath;
//                final Uri selectedImage = data.getData();
//                if (isProfileBackground) {
//                    selectedBackgroundImagePath = GetFilePath.getPath(getActivity(), selectedImage);
//                    Utils.setImageView(getActivity(), cropImagePath, ivProfileBackgroundPic);
//                } else {
//                    selectedProfileImagePath = GetFilePath.getPath(getActivity(), selectedImage);
//                    Utils.getCircleImageView(getActivity(), cropImagePath, ivProfilePic);
//                }
//            }
        }
    }


    /**
     * Method to perform the cropping on the Image picked up from the gallery or the one clicked from the camera
     */
    public void performCrop(final String imagePath) {

        try {

            final Intent cropIntent = new Intent("com.android.camera.action.CROP");
            final File cropedFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + System.currentTimeMillis() + ".jpg");
            cropedFile.createNewFile();
            final Uri mCropImagedUri = Uri.fromFile(cropedFile);
            cropImagePath = cropedFile.getAbsolutePath();
            cropIntent.setDataAndType(Uri.fromFile(new File(imagePath)), "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("return-data", false);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri);
            startActivityForResult(cropIntent, Constants.CAMERA_CROP);
        } catch (Exception e) {
            e.printStackTrace();
            Utils.getCircleImageView(getActivity(), imagePath, ivProfilePic);
        }
    }

    /**
     * Request for runtime permission of READ_PHONE_STATE
     *
     * @param permssion      permission name
     * @param permissionCode request code for permission
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void requestForPermissions(String permssion, int permissionCode) {
        if (shouldShowRequestPermissionRationale(permssion)) {
            requestPermissions(new String[]{permssion}, permissionCode);
        } else {
            requestPermissions(new String[]{permssion}, permissionCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openDialog();
            } else {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.err_msg_permission_write_phone));
            }
        }
    }

    /**
     * open calender dailog
     */
    private void openCalender() {
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                monthOfYear = monthOfYear + 1;
                strDate = year + "-" + String.format("%02d", monthOfYear) + "-" + String.format("%02d", dayOfMonth);
                tvDateOfBirth.setText(strDate);
            }
        }, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        if (year != 0) {
            datePickerDialog.updateDate(year, month - 1, day);
        }
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }


    @Override
    public void onImageSelected(String imagePath) {
        this.imagePath = imagePath;
        if (!TextUtils.isEmpty(imagePath)) {
            performCrop(imagePath);
        }
    }

    private void openDialog() {
        final Dialog dialog = new Dialog(getActivity(), R.style.picture_dialog_style);
        dialog.setContentView(R.layout.dialog_profile);
        final WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(wlmp);
        dialog.show();
        final TextView tvGallery = (TextView) dialog.findViewById(R.id.dialog_profile_tv_gallery);
        final TextView tvCamera = (TextView) dialog.findViewById(R.id.dialog_profile_tv_camera);
        final TextView tvCancel = (TextView) dialog.findViewById(R.id.dialog_profile_tv_cancel);
        tvCamera.setText("Take a picture");
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                dialog.dismiss();

            }
        });
        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openCamera();
                dialog.dismiss();

            }
        });


    }

    /**
     * code of take a picture from the Camera app
     */
    private void openCameraForImage() {

        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {

            final File mFile = Utils.createPhotoFile(getActivity());
            cameraUri = mFile.getAbsolutePath();

            final Uri outputFileUri = Uri.fromFile(mFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, Constants.CHOOSE_IMAGE_CAMERA);
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    /**
     * Method to Select an Image from the Phone's Gallery
     */
    private void selectImageFromGallery() {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (isKitKat) {
            final Intent openGalleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            openGalleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
            openGalleryIntent.setType("image/*");
            openGalleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(Intent.createChooser(openGalleryIntent, "Select Picture"), Constants.CHOOSE_IMAGE_GALLERY);
        } else {
            final Intent openGalleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            openGalleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(Intent.createChooser(openGalleryIntent, "Select Picture"), Constants.CHOOSE_IMAGE_GALLERY);
        }
    }


    private void openGallery() {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (isKitKat) {

            final Intent openGalleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            openGalleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
            openGalleryIntent.setType("image/*");
            openGalleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(Intent.createChooser(openGalleryIntent, "Select Picture"),
                    PIC_FROM_GALLARY);
        } else {

            final Intent openGalleryIntent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            openGalleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(Intent.createChooser(openGalleryIntent, "Select Picture"), PIC_FROM_GALLARY);
        }

    }

    private void openCamera() {
        final Intent intentCamera = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            final Uri outputFileUri = getPostImageUri(true);


            intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            intentCamera.putExtra("return-data", true);
            startActivityForResult(intentCamera, PIC_FROM_CAMERA);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        final Intent intentCamera = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//        try {
//            final Uri outputFileUri = getPostImageUri(true);
//            intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
//            intentCamera.putExtra("return-data", true);
//            startActivityForResult(intentCamera, PIC_FROM_CAMERA);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }


    private Uri getPostImageUri(boolean canCleanup) {
        final File file = new File(getActivity().getExternalCacheDir() + File.separator + System
                .currentTimeMillis() + Constants.FILE_EXTENSION);
        if (canCleanup) {
            if (file.exists()) {
                file.delete();
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        cameraFilePath = file.getAbsolutePath();

        Uri photoURI = FileProvider.getUriForFile(getActivity(),
                BuildConfig.APPLICATION_ID + ".provider",
                file);
        return photoURI;
    }

    /**
     * Called for perform user view profile
     *
     * @param bundle
     */
    private void callAsyncViewProfile(Bundle bundle) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncViewProfile != null && asyncViewProfile.getStatus() == AsyncTask.Status.PENDING) {
                asyncViewProfile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (asyncViewProfile == null || asyncViewProfile.getStatus() == AsyncTask.Status.FINISHED) {
                asyncViewProfile = new AsyncViewProfile(bundle);
                asyncViewProfile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.alert_no_internet));
        }
    }

    @Override
    public void onFromDateSelect(DatePicker picker, int requestDate) {
        if (picker != null) {
            final String strDate = (picker.getMonth() + 1) + "/" + picker.getDayOfMonth() + "/" + picker.getYear();
            final String serverFormat = picker.getYear() + "-" + (picker.getMonth() + 1) + "-" + picker
                    .getDayOfMonth();

            tvDateOfBirth.setText(serverFormat);


        }
    }

    @Override
    public void onMonthSelect(String month, String year) {

    }

    @Override
    public void onToDateSelect(DatePicker picker) {

    }

    /**
     * call view profile web service
     */
    private class AsyncViewProfile extends AsyncTask<Void, Void, Void> {

        private Bundle bundle;
        private WSViewProfile wsViewProfile;
        private ProgressDialog progressDialog;

        public AsyncViewProfile(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wsViewProfile = new WSViewProfile(getActivity());
                wsViewProfile.executeService(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (getActivity().isDestroyed()) {
                return;
            }
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (!isCancelled()) {
                if (wsViewProfile.getCode() == WSConstants.STATUS_SUCCESS) {
                    setAllUserData(wsViewProfile.getUserModel());
                } else if (wsViewProfile.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsViewProfile.getMessage());
                } else if (wsViewProfile.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsViewProfile
                            .getMessage());

                } else {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsViewProfile.getMessage());
                }
            }
        }
    }

    /**
     * Called for perform user view profile
     *
     * @param bundle
     */
    private void callAsyncEditProfile(Bundle bundle) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncEditProfile != null && asyncEditProfile.getStatus() == AsyncTask.Status.PENDING) {
                asyncEditProfile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (asyncEditProfile == null || asyncEditProfile.getStatus() == AsyncTask.Status.FINISHED) {
                asyncEditProfile = new AsyncEditProfile(bundle);
                asyncEditProfile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.alert_no_internet));
        }
    }

    /**
     * call view profile web service
     */
    private class AsyncEditProfile extends AsyncTask<Void, Void, Void> {

        private Bundle bundle;
        private WsEditProfile wsEditProfile;
        private ProgressDialog progressDialog;

        public AsyncEditProfile(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wsEditProfile = new WsEditProfile(getActivity());
                wsEditProfile.executeService(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (getActivity().isDestroyed()) {
                return;
            }
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (!isCancelled()) {
                if (wsEditProfile.getCode() == WSConstants.STATUS_SUCCESS) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsEditProfile.getMessage());
                } else if (wsEditProfile.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsEditProfile.getMessage());
                } else if (wsEditProfile.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsEditProfile
                            .getMessage());

                } else {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsEditProfile.getMessage());
                }
            }
        }
    }

    private void setAllUserData(UserModel userModel) {
        etFirstName.setText("" + userModel.getFirstName());
        etLastName.setText("" + userModel.getLastName());
        tvEmail.setText("" + userModel.getEmail());
        etMobileNumber.setText("" + userModel.getMobileNo());
        if (!userModel.getDateOfBirth().equals("0000-00-00")) {
            final String strDOB = userModel.getDateOfBirth();
            tvDateOfBirth.setText(strDOB);
            String[] separated = strDOB.split("-");
            year = Integer.parseInt(separated[0]);
            month = Integer.parseInt(separated[1]);
            day = Integer.parseInt(separated[2]);
        }
        if(!userModel.getProfileImage().equalsIgnoreCase("")){
            Utils.setImageView(getActivity(), userModel.getProfileImage(), ivProfilePic);
        }

        Utils.setImageView(getActivity(), userModel.getBgImage(), ivProfileBackgroundPic);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (asyncViewProfile != null && asyncViewProfile.getStatus() == AsyncTask.Status.RUNNING) {
            asyncViewProfile.cancel(true);
        }
        if (asyncEditProfile != null && asyncEditProfile.getStatus() == AsyncTask.Status.RUNNING) {
            asyncEditProfile.cancel(true);
        }
    }

    private void openDatePicker() {
        final DatePickerFragment newFragment = new DatePickerFragment();
        Bundle bundle = new Bundle();
        String strDob = "";
        strDob = tvDateOfBirth.getText().toString().trim();
        bundle.putBoolean(Constants.INTENT_KEY_IS_FUTURE_DATE_ALLOWED, true);
        bundle.putBoolean(Constants.INTENT_KEY_IS_PAST_DATE_ALLOWED, false);
        String newDate = "";
        if (strDob.length() > 0) {
            newDate = Utils.changeDateTimeFormat(strDob, Constants.DATE_FORMATE_MM_DD_YYYY, Constants.DATE_FORMATE_DD_MMM_YY);
        }


        newFragment.setArguments(bundle);
        newFragment.setFilterData(true, this, newDate);
        newFragment.show(getFragmentManager(), "Date picker");

    }
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }
}
