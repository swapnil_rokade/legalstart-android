package com.adaptingsocial.lawyerapp.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.sax.StartElementListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.RecyclerPagerAdapter;
import com.adaptingsocial.lawyerapp.customview.EndlessRecyclerOnScrollListener;
import com.adaptingsocial.lawyerapp.model.BlogModel;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSBlogList;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;

import java.util.ArrayList;

/**
 * Created by indianic on 03/05/17.
 */

public class BlogFragmentNew extends BaseFragment {
    private ArrayList<BlogModel> blogModelArrayList;
    private ArrayList<BlogModel> blogModelFinalArrayList;
    private BlogListAsyncTask blogListAsyncTask;
    private RecyclerView rv;
    private RecyclerPagerAdapter myAdapter;
    private String nextPage;
    private boolean canLoadMore = true;
    int i = 0;
    private EndlessRecyclerOnScrollListener scrollListener;
    private int page = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_blogs, container, false);

    }

    @Override
    protected void initializeComponent(View view) {
        rv = (RecyclerView) view.findViewById(R.id.recycler_view);
        blogModelFinalArrayList = new ArrayList<>();
        myAdapter = new RecyclerPagerAdapter(getActivity(), blogModelFinalArrayList, rv);
        LinearLayoutManager ll = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,
                false);
        ll.getStackFromEnd();
        rv.setLayoutManager(ll);
        rv.setAdapter(myAdapter);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(rv);
        rv.invalidate();


        scrollListener = new EndlessRecyclerOnScrollListener(ll, page) {
            @Override
            public void onLoadMore(int current_page) {
                if (nextPage.equalsIgnoreCase("1")) {
                    page = current_page;
                    CaseListUsAsyncTask("" + page, 0, false);
                } else {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), "No " +
                            "more blog.");
                }
            }

            @Override
            public void onNotify() {
                myAdapter.notifyDataSetChanged();
            }

        };
        rv.addOnScrollListener(scrollListener);


    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        CaseListUsAsyncTask("", 0, false);


    }


    public void CaseListUsAsyncTask(String pageIndex, int position, boolean flag) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (blogListAsyncTask != null && blogListAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                blogListAsyncTask.execute();
            } else if (blogListAsyncTask == null || blogListAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                blogListAsyncTask = new BlogListAsyncTask(pageIndex, position, flag);
                blogListAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false, getString(R.string.blogs), 0);
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();
    }


    public class BlogListAsyncTask extends AsyncTask<String, Void, Void> {
        private WSBlogList wsBlogList;
        private ProgressDialog progressDialog;
        private String pageIndex;
        private int position;
        boolean flag;

        public BlogListAsyncTask(String pageIndex, int position, boolean flag) {
            this.pageIndex = pageIndex;
            this.position = position;
            this.flag = flag;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(String... params) {
            wsBlogList = new WSBlogList(getActivity());
            wsBlogList.executeService(pageIndex);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsBlogList.getCode() == WSConstants.STATUS_SUCCESS) {
                    nextPage = wsBlogList.getNextPage();
                    if (wsBlogList.getBlogListModelArrayList() != null && wsBlogList.getBlogListModelArrayList().size() > 0) {
                        blogModelFinalArrayList.addAll(wsBlogList.getBlogListModelArrayList());
                        myAdapter.notifyDataSetChanged();
                        myAdapter.setLoaded();
                        Log.e("Paging called", "page index==>" + pageIndex);

                    } else {
                        Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsBlogList.getMessage());

                    }


                } else if (wsBlogList.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsBlogList.getMessage());

                } else if (wsBlogList.getCode() == WSConstants.STATUS_NO_DATA_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsBlogList.getMessage());


                } else if (wsBlogList.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsBlogList
                            .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

                }


            }

        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (blogListAsyncTask != null && blogListAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            blogListAsyncTask.cancel(true);
        }
        if (blogListAsyncTask != null && blogListAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            blogListAsyncTask.cancel(true);
        }

    }


}
