package com.adaptingsocial.lawyerapp.fragment;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.DocumentListModel;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;

/**
 * Created by indianic on 06/06/17.
 */

public class PaypalPaymentFragment extends BaseFragment {
    private WebView webView;
    private DocumentListModel documentListModel;
    private String paymentUrl;

    @Override
    protected void initializeComponent(View view) {
        webView = (WebView) view.findViewById(R.id.fragment_paypal_wv_payment);

    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        http://localhost/lawyer/WS/paypal_payment?&
//        // document_name=a&amount=22&users_id=1&document_id=p7DN17fEPE&access_token=DYtcGpL26O


    }

    private void startWebView(String url) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i("result", "Processing webview url click..." + view.getUrl());
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                Log.i("result", "Finished loading URL: " + url);
                boolean status = url.contains("fsucces" +
                        "");
                Log.i("result", "payment status" + status);
                if (status) {
                    Log.i("result", "payment value" + url.substring(url.length() - 1));
                    //payment sucess
                    if (url.substring(url.length() - 1).equalsIgnoreCase("1")) {

                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                        getFragmentManager().popBackStack();

                    }
                    //payment failed
                    else {


                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, null);
                        getFragmentManager().popBackStack();
                    }

                }


            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e("result", "Error: " + description);
                Toast.makeText(getActivity(), "Oh no! " + description, Toast.LENGTH_SHORT).show();

            }
        });
        webView.loadUrl(paymentUrl);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_paypal, container, false);


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        final Bundle mBundle = getArguments();
        webView = (WebView) getView().findViewById(R.id.fragment_paypal_wv_payment);
        if (mBundle != null) {
            final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
            final String userId = preferenceUtils.getString(preferenceUtils.KEY_USER_ID);
            String acessToken = preferenceUtils
                    .getString(preferenceUtils.KEY_ACESS_TOKEN);
            documentListModel = mBundle.getParcelable(Constants.INTENT_KEY_Document_MODEL);


            paymentUrl = "http://php54.indianic.com/lawyerapp/WS/paypal_payment?&" + "document_name="
                    + documentListModel.getDocumentName() + "&amount=" + documentListModel
                    .getDocumentAmt() + "& users_id=" + userId + "&document_id=" + documentListModel
                    .getDocumentId() + "&access_token=" + acessToken;


//            http://php54.indianic.com/lawyerapp/WS/paypal_payment?&document_name=a&amount=22&users_id=1&document_id=p7DN17fEPE&access_token=DYtcGpL26O

//            startWebView(paymentUrl);
            webView.getSettings().setJavaScriptEnabled(true);
            if (Build.VERSION.SDK_INT >= 21) {
                webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            }
            startWebView(paymentUrl);
//            webView.setWebViewClient(new WebViewClient());
//            webView.setWebChromeClient(new WebChromeClient() {
//                @Override
//                public void onProgressChanged(WebView view, int newProgress) {
//                    super.onProgressChanged(view, newProgress);
//
//                    // Your custom code.
//                    AppLog.showLogD("result","progress");
//                }
//            });
//            webView.loadUrl(paymentUrl);
        }

    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, getString(R.string.payment_process), 0);
            ((HomeActivity) getActivity()).showShadow();
        }
        Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
        toolBar.getMenu().clear();

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }

}
