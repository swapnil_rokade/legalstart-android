package com.adaptingsocial.lawyerapp.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.PastAppointmentAdapter;
import com.adaptingsocial.lawyerapp.model.TimeSlotModel;
import com.adaptingsocial.lawyerapp.utill.Constants;

import java.util.ArrayList;

/**
 * Created by indianic on 09/05/17.
 */

public class PastAppointmentsFragment extends  BaseFragment {
    private PastAppointmentAdapter appointmentListAdapter;
    private ArrayList<TimeSlotModel> appointmentModelArrayList;
    private ListView lvPastAppointments;
    private TextView tvNodata;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_past_appointments, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    @Override
    protected void initializeComponent(View view) {
        lvPastAppointments= (ListView) view.findViewById(R.id.fragment_past_appointment_lv);
        tvNodata = (TextView) view.findViewById(R.id.fragment_past_appointment_tv_no_data);

//        appointmentListAdapter = new PastAppointmentAdapter(getActivity(), appointmentModelArrayList);
        if(getArguments()!=null){
            appointmentModelArrayList=getArguments().getParcelableArrayList(Constants.PAST_PARCELABLE_ARRAY);
        }
        appointmentListAdapter = new PastAppointmentAdapter(getActivity(), appointmentModelArrayList);
        lvPastAppointments.setAdapter(appointmentListAdapter);

        if (appointmentModelArrayList.size() == 0) {
            tvNodata.setText(R.string.str_no_appointment_found);
            tvNodata.setVisibility(View.VISIBLE);
            lvPastAppointments.setVisibility(View.GONE);
        } else {
            tvNodata.setVisibility(View.GONE);
            lvPastAppointments.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }
}
