package com.adaptingsocial.lawyerapp.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.adapter.CaseListAdapter;
import com.adaptingsocial.lawyerapp.model.CaseListModel;
import com.adaptingsocial.lawyerapp.swipemenuview.SwipeMenu;
import com.adaptingsocial.lawyerapp.swipemenuview.SwipeMenuCreator;
import com.adaptingsocial.lawyerapp.swipemenuview.SwipeMenuItem;
import com.adaptingsocial.lawyerapp.swipemenuview.SwipeMenuListView;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSCaseList;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WSDeleteCase;

import java.util.ArrayList;


public class CaseListFragment extends BaseFragment implements AdapterView.OnItemClickListener {
    private SwipeMenuListView swipeMenuListView;
    private CaseListAdapter caseListAdapter;
    private ArrayList<CaseListModel> caseListModelArrayList;
    private CaseListAsyncTask caseListAsyncTask;
    private TextView tvNodata;
    private DeleteCaseAsyncTask deleteCaseAsyncTask;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_case_list, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();
        CaseListUsAsyncTask();
    }


    @Override
    protected void initializeComponent(View view) {
        swipeMenuListView = (SwipeMenuListView) view.findViewById(R.id.fragment_case_list_lv_case);
        tvNodata = (TextView) view.findViewById(R.id.fragment_case_list_tv_no_data);
        caseListModelArrayList = new ArrayList<>();
        setSwipeMenu();
        swipeMenuListView.setOnItemClickListener(this);
//        caseListAdapter = new CaseListAdapter(getActivity(), caseListModelArrayList);
//        lvCaseList.setAdapter(caseListAdapter);
//        lvCaseList.setOnItemClickListener(this);
//        lvCaseList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ((SwipeLayout) (lvCaseList.getChildAt(position - lvCaseList.getFirstVisiblePosition()))).open(true);
//            }
//        });
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    private void CaseListUsAsyncTask() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (caseListAsyncTask != null && caseListAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                caseListAsyncTask.execute();
            } else if (caseListAsyncTask == null || caseListAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                caseListAsyncTask = new CaseListAsyncTask();
                caseListAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    private void deleteCaseUsAsyncTask(final Bundle bundle) {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (deleteCaseAsyncTask != null && deleteCaseAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                deleteCaseAsyncTask.execute();
            } else if (deleteCaseAsyncTask == null || deleteCaseAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                deleteCaseAsyncTask = new DeleteCaseAsyncTask(bundle);
                deleteCaseAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }


    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(false,getString(R.string.upload_document), 0);
            ((HomeActivity) getActivity()).showShadow();
            Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
            toolBar.getMenu().clear();
            toolBar.inflateMenu(R.menu.menu_case);
            toolBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_case_add:
                            openAddCaseScreen();
                            return true;
                    }
                    return false;
                }


            });
        }
    }

    private void openAddCaseScreen() {
        final AddCaseFragment addCaseFragment = new AddCaseFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, addCaseFragment,
                AddCaseFragment.class
                        .getSimpleName());
        addCaseFragment.setTargetFragment(this, 1);
        fragmentTransaction.addToBackStack(AddCaseFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }



    private void openCaseDetailsFragment(CaseListModel caseListModel) {
        final CaseDetailFragment caseDetailFragment = new CaseDetailFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_CASE_MODEL, caseListModel);
        caseDetailFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, caseDetailFragment,
                CaseDetailFragment.class
                        .getSimpleName());
        fragmentTransaction.addToBackStack(CaseDetailFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        openCaseDetailsFragment(caseListModelArrayList.get(position));
    }

    private void openUpdateCaseFragment(CaseListModel caseListModel) {
        final UpdateCaseFragment updateCaseFragment = new UpdateCaseFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_CASE_MODEL, caseListModel);
        updateCaseFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, updateCaseFragment,
                UpdateCaseFragment.class
                        .getSimpleName());
        updateCaseFragment.setTargetFragment(this, 1);
        fragmentTransaction.addToBackStack(UpdateCaseFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    public class CaseListAsyncTask extends AsyncTask<String, Void, Void> {
        private WSCaseList wsCaseList;
        private ProgressDialog progressDialog;

        public CaseListAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(String... params) {
            wsCaseList = new WSCaseList(getActivity());
            wsCaseList.executeService();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsCaseList.getCode() == WSConstants.STATUS_SUCCESS) {
                    if (wsCaseList.getCaseListModelArrayList() != null && wsCaseList.getCaseListModelArrayList().size() > 0) {
                        caseListModelArrayList = wsCaseList.getCaseListModelArrayList();
                        caseListAdapter = new CaseListAdapter(getActivity(), caseListModelArrayList);
                        swipeMenuListView.setAdapter(caseListAdapter);
                        tvNodata.setVisibility(View.GONE);
                        swipeMenuListView.setVisibility(View.VISIBLE);


                    } else {
                        Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsCaseList.getMessage());

                    }


                } else if (wsCaseList.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsCaseList.getMessage());

                }
                else if (wsCaseList.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsCaseList
                            .getMessage());

                }

                else if (wsCaseList.getCode() == WSConstants.STATUS_NO_DATA_FAIL) {
                    tvNodata.setText("" + wsCaseList.getMessage());
                    tvNodata.setVisibility(View.VISIBLE);
                    swipeMenuListView.setVisibility(View.GONE);

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

                }


            }

        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (caseListAsyncTask != null && caseListAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            caseListAsyncTask.cancel(true);
        }
        if (caseListAsyncTask != null && caseListAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            caseListAsyncTask.cancel(true);
        }
        if (deleteCaseAsyncTask != null && deleteCaseAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            deleteCaseAsyncTask.cancel(true);
        }

    }

    public class DeleteCaseAsyncTask extends AsyncTask<String, Void, Void> {
        private final Bundle bundle;
        private WSDeleteCase wsDeleteCase;
        private ProgressDialog progressDialog;

        public DeleteCaseAsyncTask(final Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(String... params) {
            wsDeleteCase = new WSDeleteCase(getActivity());
            wsDeleteCase.executeService(bundle);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsDeleteCase.getCode() == WSConstants.STATUS_SUCCESS) {
                    caseListAdapter.notifyDataSetChanged();
                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsDeleteCase.getMessage());

                }


            } else if (wsDeleteCase.getCode() == WSConstants.STATUS_FAIL) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), wsDeleteCase.getMessage());

            } else if (wsDeleteCase.getCode() == WSConstants.STATUS_NO_DATA_FAIL) {
                tvNodata.setText("" + wsDeleteCase.getMessage());
                tvNodata.setVisibility(View.VISIBLE);
                swipeMenuListView.setVisibility(View.GONE);

            } else if (wsDeleteCase.getCode() == WSConstants.STATUS_LOGOUT) {
                Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsDeleteCase
                        .getMessage());

            } else {
                Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            AppLog.showLogD("Result", "call on activity fragment");
            CaseListUsAsyncTask();
        }
    }


    private void setSwipeMenu() {
        final SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                switch (menu.getViewType()) {
                    case 0:
                        final SwipeMenuItem swipeMenuItemEdit = new SwipeMenuItem(getActivity());
                        swipeMenuItemEdit.setBackground(new ColorDrawable(ContextCompat.getColor
                                (getActivity(), R.color.colorBlue)));
                        swipeMenuItemEdit.setWidth(Utils.dpToPx(getActivity(), 60));
                        swipeMenuItemEdit.setTitle(R.string.basic_action_2);
//                        swipeMenuItemEdit.setIcon(R.drawable.ic_edit);
                        swipeMenuItemEdit.setTitleColor(Color.WHITE);
                        swipeMenuItemEdit.setTitleSize((int) 15);
                        menu.addMenuItem(swipeMenuItemEdit);
                        break;

                    case 1:
                        break;
                }

                final SwipeMenuItem swipeMenuItemDelete = new SwipeMenuItem(getActivity());
                swipeMenuItemDelete.setBackground(new ColorDrawable(ContextCompat
                        .getColor(getActivity(), R.color.colorOrange)));
                swipeMenuItemDelete.setWidth(Utils.dpToPx(getActivity(), 70));
                swipeMenuItemDelete.setTitle(R.string.basic_action_1);
                swipeMenuItemDelete.setTitleColor(Color.WHITE);
//                swipeMenuItemDelete.setIcon(R.drawable.ic_remove);
                swipeMenuItemDelete.setTitleSize((int) 15);
                menu.addMenuItem(swipeMenuItemDelete);

            }
        };

        swipeMenuListView.setMenuCreator(creator);
        swipeMenuListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        openUpdateCaseFragment(caseListModelArrayList.get(position));
                        break;

                    case 1:
                        displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name),
                                getString(R.string.alert_delete_case), position);
                        break;
                }
                return false;
            }
        });


        swipeMenuListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override
            public void onSwipeStart(int position) {
            }

            @Override
            public void onSwipeEnd(int position) {
            }
        });
    }

    public void displayDialogWithPopBackStack(final Context context, final String title, final String message, final int position) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                final PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
                final Bundle bundle = new Bundle();
                bundle.putString(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
                bundle.putString(WSConstants.WS_KEY_CASE_ID, caseListModelArrayList.get(position).getCaseId());
                caseListModelArrayList.remove(position);
                deleteCaseUsAsyncTask(bundle);
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }
}
