package com.adaptingsocial.lawyerapp.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.QuesionCatagoryModel;
import com.adaptingsocial.lawyerapp.model.QuestionDescriptionDetailModal;
import com.adaptingsocial.lawyerapp.model.QuestionModel;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.view.HomeActivity;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsQuestionList;
import com.adaptingsocial.lawyerapp.webservice.WsSubmitAnswer;

import java.util.ArrayList;


public class QuestionFragment extends BaseFragment implements SingleChoiceFragment.OnFlowItemClickListener {
    private QuesionCatagoryModel questionCatagoryModel;
    private QuestionListAsyncTask questionListAsyncTask;
    private TextView tvSubmit;
    private TextView tvNext;
    private TextView tvQuestion;
    private TextView tvPrivious;
    private TextView tvQuestionNo;
    private ArrayList<QuestionModel> questionModelArrayList;
    private int nextValue = 0;
    private RelativeLayout rlmain;
    private ProgressDialog progressDialog;
    private AsyncSubmitAnswer asyncSubmitAnswer;
    private String paymentId = "";
    private TextView tvComplete;
    private TextView tvUnComplete;
    private int qno = 1;
    private double ammount;
    private QuestionDescriptionDetailModal questionDescriptionDetailModal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_question, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Bundle mBundle = getArguments();
        if (mBundle != null) {
            ShortTypeQuestionFragment multipleAnswerFragment = new ShortTypeQuestionFragment();
            setUpMainFragment(multipleAnswerFragment);
            questionCatagoryModel = mBundle.getParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL);
            questionDescriptionDetailModal = mBundle.getParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_LIST);
            initToolBar();
            QuestionListUsAsyncTask();

        }

    }

    private void setUpMainFragment(Fragment nextFragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_question_container_answers, nextFragment, nextFragment.getClass().getSimpleName()
        ).commit();
    }

    private void QuestionListUsAsyncTask() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (questionListAsyncTask != null && questionListAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                questionListAsyncTask.execute();
            } else if (questionListAsyncTask == null || questionListAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                questionListAsyncTask = new QuestionListAsyncTask();
                questionListAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    public class QuestionListAsyncTask extends AsyncTask<String, Void, Void> {
        private WsQuestionList wsQuestionList;
        private ProgressDialog progressDialog;

        public QuestionListAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(String... params) {
            wsQuestionList = new WsQuestionList(getActivity());
            wsQuestionList.executeService(questionCatagoryModel.getId());
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsQuestionList.getCode() == WSConstants.STATUS_SUCCESS) {
                    if (wsQuestionList.getQuestionModelArrayList() != null &&
                            wsQuestionList.getQuestionModelArrayList()
                                    .size() > 0) {
                        tvNext.setVisibility(View.VISIBLE);
                        tvPrivious.setVisibility(View.VISIBLE);
                        if (wsQuestionList.getQuestionModelArrayList()
                                .size() == 1) {
                            tvSubmit.setVisibility(View.VISIBLE);
                            tvNext.setVisibility(View.GONE);
                        } else {
                            tvSubmit.setVisibility(View.GONE);
                            tvNext.setVisibility(View.VISIBLE);

                        }
                        tvQuestionNo.setText("" + qno);
                        tvComplete.setText("" + qno);
                        tvPrivious.setVisibility(View.GONE);
                        rlmain.setVisibility(View.VISIBLE);
                        questionModelArrayList = new ArrayList<>();
                        questionModelArrayList = wsQuestionList.getQuestionModelArrayList();
//                        if (questionModelArrayList.get(0).getFlow_finish().equalsIgnoreCase("yes")) {
//                            tvSubmit.setVisibility(View.VISIBLE);
//                            tvNext.setVisibility(View.GONE);
//                        } else {
//                            tvSubmit.setVisibility(View.GONE);
//                            tvNext.setVisibility(View.VISIBLE);
//                        }


                        tvUnComplete.setText("" + questionModelArrayList.size());
                        tvQuestion.setText("" + questionModelArrayList.get(0).getQuestion());
                        if (questionModelArrayList.get(0).getQtypecode().equalsIgnoreCase("AddNew")) {

                            AddNewQuestionFragment addNewQuestionFragment = new
                                    AddNewQuestionFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("addmore", questionModelArrayList.get(nextValue).getAdd_another());
                            bundle.putString("answers", questionModelArrayList.get(0).getQoption());
                            addNewQuestionFragment.setArguments(bundle);
                            setUpMainFragment(addNewQuestionFragment);

                        } else if (questionModelArrayList.get(0).getQtypecode().equalsIgnoreCase
                                ("ShortAnswerQuestion")) {

                            ShortTypeQuestionFragment shortTypeQuestionFragment = new
                                    ShortTypeQuestionFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("answers", questionModelArrayList.get(0).getQoption());
                            shortTypeQuestionFragment.setArguments(bundle);
                            setUpMainFragment(shortTypeQuestionFragment);

                        } else if (questionModelArrayList.get(0).getQtypecode().equalsIgnoreCase
                                ("LongAnswerQuestion")) {

                            LongTypeQuestionFragment longTypeQuestionFragment = new
                                    LongTypeQuestionFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("answers", questionModelArrayList.get(0).getQoption());
                            longTypeQuestionFragment.setArguments(bundle);
                            setUpMainFragment(longTypeQuestionFragment);

                        } else if (questionModelArrayList.get(0).getQtypecode().equalsIgnoreCase
                                ("SingleChoice")) {

                            SingleChoiceFragment singleChoiceFragment = new
                                    SingleChoiceFragment();
                            singleChoiceFragment.setOnItemClickListener(QuestionFragment.this);
                            Bundle bundle = new Bundle();
                            bundle.putString("answers", questionModelArrayList.get(0).getQoption());
                            singleChoiceFragment.setArguments(bundle);
                            setUpMainFragment(singleChoiceFragment);
                        } else if (questionModelArrayList.get(0).getQtypecode().equalsIgnoreCase
                                ("Multiselect")) {

                            MultipleAnswerFragment multipleAnswerFragment = new
                                    MultipleAnswerFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("answers", questionModelArrayList.get(0).getQoption());
                            multipleAnswerFragment.setArguments(bundle);
                            setUpMainFragment(multipleAnswerFragment);
                        }

                    } else {
                        rlmain.setVisibility(View.GONE);
                        Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), wsQuestionList.getMessage());

                    }


                } else if (wsQuestionList.getCode() == WSConstants.STATUS_FAIL) {
                    rlmain.setVisibility(View.GONE);
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsQuestionList.getMessage());

                } else if (wsQuestionList.getCode() == WSConstants.STATUS_LOGOUT) {
                    rlmain.setVisibility(View.GONE);
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsQuestionList
                            .getMessage());

                } else if (wsQuestionList.getCode() == WSConstants.STATUS_NO_DATA_FAIL) {


                } else {

                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

                }


            }

        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /**
         * Cancel the AsyncTask instance if the
         * Fragment gets destroyed in any case.
         */
        if (questionListAsyncTask != null && questionListAsyncTask.getStatus() ==
                AsyncTask.Status.RUNNING) {
            questionListAsyncTask.cancel(true);
        }


    }

    private void openDialog() {
        final Dialog dialog = new Dialog(getActivity(), R.style.picture_dialog_style);
        dialog.setContentView(R.layout.dialog_payment);
        final WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(wlmp);
        dialog.show();
        final TextView tvPaypal = (TextView) dialog.findViewById(R.id
                .dialog_payment_paypal);
        final TextView tvStripe = (TextView) dialog.findViewById(R.id.dialog_payment_stripe);
        final TextView tvCancel = (TextView) dialog.findViewById(R.id.dialog_payment_cancel);
        tvStripe.setText(R.string.authorize);
        tvPaypal.setVisibility(View.GONE);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvPaypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPaypalScreen();
                dialog.dismiss();

            }
        });
        tvStripe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                openStripeScreen();
                openAuthorizePayment();
                dialog.dismiss();
            }
        });

    }


    private void openAuthorizePayment() {
        final AuthorieQuestionPaymentFragment authorizeServicePaymentFragment = new AuthorieQuestionPaymentFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL, questionCatagoryModel);
        mBundle.putParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_LIST, questionDescriptionDetailModal);
        authorizeServicePaymentFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, authorizeServicePaymentFragment,
                AuthorieQuestionPaymentFragment.class
                        .getSimpleName());
        authorizeServicePaymentFragment.setTargetFragment(QuestionFragment.this, 1);
        fragmentTransaction.addToBackStack(AuthorieQuestionPaymentFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }

    private void openStripeScreen() {
        final StripeQuestionFragment stripePaymentFragment = new StripeQuestionFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL, questionCatagoryModel);
        mBundle.putParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_LIST, questionDescriptionDetailModal);
        stripePaymentFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, stripePaymentFragment,
                StripeQuestionFragment.class
                        .getSimpleName());
        stripePaymentFragment.setTargetFragment(QuestionFragment.this, 1);
        fragmentTransaction.addToBackStack(StripeQuestionFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();

    }


    private void openPaypalScreen() {
        final PaypalQuestionFragment paypalQuestionFragment = new PaypalQuestionFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL, questionCatagoryModel);

        mBundle.putParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_LIST, questionDescriptionDetailModal);
        paypalQuestionFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, paypalQuestionFragment,
                PaypalQuestionFragment.class
                        .getSimpleName());
        paypalQuestionFragment.setTargetFragment(QuestionFragment.this, 1);
        fragmentTransaction.addToBackStack(PaypalQuestionFragment.class.getSimpleName());
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }


    @Override
    protected void initializeComponent(View view) {
        tvComplete = (TextView) view.findViewById(R.id.fragment_question_tv_question_complete);
        tvUnComplete = (TextView) view.findViewById(R.id.fragment_question_tv_question_uncomplete);
        tvSubmit = (TextView) view.findViewById(R.id.fragment_question_tv_submit);
        tvNext = (TextView) view.findViewById(R.id.fragment_question_tv_next);
        tvPrivious = (TextView) view.findViewById(R.id.fragment_question_tv_privious);
        tvQuestion = (TextView) view.findViewById(R.id.fragment_question_tv_question);
        tvQuestionNo = (TextView) view.findViewById(R.id.fragment_question_tv_question_no);
        rlmain = (RelativeLayout) view.findViewById(R.id.rlmain);
        tvNext.setOnClickListener(this);
        tvPrivious.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);


    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    public void initToolBar() {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showToolBar();
            ((HomeActivity) getActivity()).setupToolbar(true, questionCatagoryModel.getName(),
                    0);
            ((HomeActivity) getActivity()).showShadow();
            Toolbar toolBar = ((HomeActivity) getActivity()).getToolbar();
            toolBar.getMenu().clear();

        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolBar();
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Utils.hideSoftKeyBoard(getActivity(), view);

        final int id = view.getId();
        switch (id) {
            case R.id.fragment_question_tv_next:

                if (saveAnswers()) {
                    nextValue++;
                    qno++;
                    tvPrivious.setVisibility(View.VISIBLE);
                    if (nextValue < questionModelArrayList.size()) {
                        if (nextValue == questionModelArrayList.size() - 1) {
                            tvSubmit.setVisibility(View.VISIBLE);
                            tvNext.setVisibility(View.GONE);
                        } else {
                            tvSubmit.setVisibility(View.GONE);
                            tvNext.setVisibility(View.VISIBLE);
                        }

                        tvQuestion.setText(questionModelArrayList.get(nextValue).getQuestion());
                        tvQuestionNo.setText(String.valueOf(qno));
//                        if (questionModelArrayList.get(nextValue).getFlow_finish().equalsIgnoreCase
//                                ("yes")) {
//                            tvSubmit.setVisibility(View.VISIBLE);
//                            tvNext.setVisibility(View.GONE);
//                        }


                        tvComplete.setText("" + qno);

                        if (questionModelArrayList.get(nextValue).getQtypecode().equalsIgnoreCase("AddNew")) {
                            AddNewQuestionFragment addNewQuestionFragment = new
                                    AddNewQuestionFragment();
                            Bundle bundle = new Bundle();

                            bundle.putString("addmore", questionModelArrayList.get(nextValue).getAdd_another());
                            bundle.putString("answers", questionModelArrayList.get(nextValue).getQoption());
                            bundle.putStringArray("answersselect", questionModelArrayList.get(nextValue)
                                    .getAnswers());
                            addNewQuestionFragment.setArguments(bundle);
                            setUpMainFragment(addNewQuestionFragment);

                        } else if (questionModelArrayList.get(nextValue).getQtypecode().equalsIgnoreCase
                                ("ShortAnswerQuestion")) {

                            ShortTypeQuestionFragment shortTypeQuestionFragment = new
                                    ShortTypeQuestionFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("answersselect", questionModelArrayList.get(nextValue)
                                    .getAnswer());
                            bundle.putString("answers", questionModelArrayList.get(nextValue).getQoption());
                            shortTypeQuestionFragment.setArguments(bundle);
                            setUpMainFragment(shortTypeQuestionFragment);

                        } else if (questionModelArrayList.get(nextValue).getQtypecode().equalsIgnoreCase
                                ("LongAnswerQuestion")) {
                            LongTypeQuestionFragment longTypeQuestionFragment = new
                                    LongTypeQuestionFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("answers", questionModelArrayList.get(nextValue).getQoption());
                            bundle.putString("answersselect", questionModelArrayList.get(nextValue)
                                    .getAnswer());
                            longTypeQuestionFragment.setArguments(bundle);
                            setUpMainFragment(longTypeQuestionFragment);

                        } else if (questionModelArrayList.get(nextValue).getQtypecode().equalsIgnoreCase
                                ("SingleChoice")) {
                            SingleChoiceFragment singleChoiceFragment = new
                                    SingleChoiceFragment();
                            singleChoiceFragment.setOnItemClickListener(QuestionFragment.this);
                            Bundle bundle = new Bundle();
                            bundle.putString("answers", questionModelArrayList.get(nextValue).getQoption());
                            bundle.putString("answersselect", questionModelArrayList.get(nextValue)
                                    .getAnswer());
                            bundle.putString("textfield_open", questionModelArrayList.get(nextValue)
                                    .getTextfield_open());
                            singleChoiceFragment.setArguments(bundle);
                            setUpMainFragment(singleChoiceFragment);
                        } else if (questionModelArrayList.get(nextValue).getQtypecode().equalsIgnoreCase
                                ("Multiselect")) {
                            MultipleAnswerFragment multipleAnswerFragment = new
                                    MultipleAnswerFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("answers", questionModelArrayList.get(nextValue).getQoption());
                            bundle.putString("answersselect", questionModelArrayList.get(nextValue)
                                    .getAnswer());
                            bundle.putString("otheranswer", questionModelArrayList.get(nextValue)
                                    .getMultipleotherField());
                            bundle.putString("otheranswerlbl", questionModelArrayList.get(nextValue)
                                    .getMultipleotherFieldanswer());
                            multipleAnswerFragment.setArguments(bundle);
                            setUpMainFragment(multipleAnswerFragment);
                        }
                    }


                }


                break;

            case R.id.fragment_question_tv_privious:
//                if (nextValue == questionModelArrayList.size() - 1) {
//                    if (questionModelArrayList.get(nextValue).getAnswer() != null) {
////                        saveAnswers();
//
//
//                    }
//
//                }

                nextValue = nextValue - 1;
                qno--;
                if (nextValue != -1) {
                    if (nextValue == 0) {
                        tvPrivious.setVisibility(View.GONE);
                    }
                    tvQuestionNo.setText("" + qno);
                    tvComplete.setText("" + qno);
                    tvQuestion.setText(questionModelArrayList.get(nextValue).getQuestion());


                    if (nextValue == questionModelArrayList.size()) {
                        tvSubmit.setVisibility(View.VISIBLE);
                        tvNext.setVisibility(View.GONE);
                    } else {
                        tvSubmit.setVisibility(View.GONE);
                        tvNext.setVisibility(View.VISIBLE);
                    }


                    if (questionModelArrayList.get(nextValue).getQtypecode().equalsIgnoreCase("AddNew")) {
                        AddNewQuestionFragment addNewQuestionFragment = new
                                AddNewQuestionFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("addmore", questionModelArrayList.get(nextValue).getAdd_another());
                        bundle.putString("answers", questionModelArrayList.get(nextValue).getQoption());
                        bundle.putStringArray("answersselect", questionModelArrayList.get(nextValue)
                                .getAnswers());
                        addNewQuestionFragment.setArguments(bundle);
                        setUpMainFragment(addNewQuestionFragment);

                    } else if (questionModelArrayList.get(nextValue).getQtypecode().equalsIgnoreCase
                            ("ShortAnswerQuestion")) {
                        ShortTypeQuestionFragment shortTypeQuestionFragment = new
                                ShortTypeQuestionFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("answers", questionModelArrayList.get(nextValue).getQoption());
                        bundle.putString("answersselect", questionModelArrayList.get(nextValue)
                                .getAnswer());
                        shortTypeQuestionFragment.setArguments(bundle);
                        setUpMainFragment(shortTypeQuestionFragment);

                    } else if (questionModelArrayList.get(nextValue).getQtypecode().equalsIgnoreCase
                            ("LongAnswerQuestion")) {
                        LongTypeQuestionFragment longTypeQuestionFragment = new
                                LongTypeQuestionFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("answers", questionModelArrayList.get(nextValue).getQoption());
                        bundle.putString("answersselect", questionModelArrayList.get(nextValue)
                                .getAnswer());
                        longTypeQuestionFragment.setArguments(bundle);
                        setUpMainFragment(longTypeQuestionFragment);

                    } else if (questionModelArrayList.get(nextValue).getQtypecode().equalsIgnoreCase
                            ("SingleChoice")) {
                        SingleChoiceFragment singleChoiceFragment = new
                                SingleChoiceFragment();
                        singleChoiceFragment.setOnItemClickListener(QuestionFragment.this);
                        Bundle bundle = new Bundle();
                        bundle.putString("answers", questionModelArrayList.get(nextValue).getQoption());
                        bundle.putString("answersselect", questionModelArrayList.get(nextValue)
                                .getAnswer());
                        bundle.putString("textfield_open", questionModelArrayList.get(nextValue)
                                .getTextfield_open());
                        singleChoiceFragment.setArguments(bundle);
                        setUpMainFragment(singleChoiceFragment);
                    } else if (questionModelArrayList.get(nextValue).getQtypecode().equalsIgnoreCase
                            ("Multiselect")) {
                        MultipleAnswerFragment multipleAnswerFragment = new
                                MultipleAnswerFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("answers", questionModelArrayList.get(nextValue).getQoption());
                        bundle.putString("answersselect", questionModelArrayList.get(nextValue)
                                .getAnswer());
                        bundle.putString("otheranswer", questionModelArrayList.get(nextValue)
                                .getMultipleotherField());
                        bundle.putString("otheranswerlbl", questionModelArrayList.get(nextValue)
                                .getMultipleotherFieldanswer());
                        multipleAnswerFragment.setArguments(bundle);
                        setUpMainFragment(multipleAnswerFragment);
                    }


                } else {
                    nextValue = 0;
                }
                break;
            case R.id.fragment_question_tv_submit:
                if (questionModelArrayList.get(nextValue).getAnswer() != null) {
                    if (saveAnswers()) {
//                        openDialog();

                        openStripeScreen();
//                        asyncSubmitAnswer();


                    }

                }


                break;

        }
    }

    private void asyncSubmitAnswer() {
        if (Utils.isNetworkAvailable(getActivity())) {
            if (asyncSubmitAnswer != null && asyncSubmitAnswer.getStatus() == AsyncTask.Status.PENDING) {
                asyncSubmitAnswer.execute();
            } else if (asyncSubmitAnswer == null || asyncSubmitAnswer.getStatus() == AsyncTask.Status.FINISHED) {
                asyncSubmitAnswer = new AsyncSubmitAnswer();
                asyncSubmitAnswer.execute();
            }
        } else {
            Utils.displayDialog(getActivity(), getString(R.string.app_name), getString(R.string.check_internet_msg));
        }
    }

    private class AsyncSubmitAnswer extends AsyncTask<Void, Void, Void> {

        private WsSubmitAnswer wsSubmitAnswer;


        public AsyncSubmitAnswer() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wsSubmitAnswer = new WsSubmitAnswer(getActivity(), questionModelArrayList);
                wsSubmitAnswer.executeService(paymentId, questionCatagoryModel.getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsSubmitAnswer.getCode() == WSConstants.STATUS_SUCCESS) {
                    displayDialogWithPopBackStack(getActivity(), "CONGRATULATIONS",
                            "Your answers have been successfully submitted.");
                } else if (wsSubmitAnswer.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(getActivity(), getString(R.string.app_name), wsSubmitAnswer.getMessage());

                } else if (wsSubmitAnswer.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(getActivity(), getString(R.string.app_name), wsSubmitAnswer
                            .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(getActivity(), getString(R.string.app_name), getString(R.string.something_went_wrong_msg));

                }

            }
        }

    }


    public void displayDialogWithPopBackStack(final Context context, final String title, final String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
//                ((Activity) context).getFragmentManager().popBackStack();
                openDialogInfo();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }


    private boolean saveAnswers() {
        boolean flag = false;
        Fragment f = getActivity().getFragmentManager().
                findFragmentById(R.id.fragment_question_container_answers);
        if (f instanceof ShortTypeQuestionFragment) {
            String answer = ((ShortTypeQuestionFragment) f).getAnswer();
            if (answer.equalsIgnoreCase("")) {

                Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please write " +
                        "answer.");
                flag = false;
            } else {
                questionModelArrayList.get(nextValue).setAnswer(answer);
                flag = true;

            }


        } else if (f instanceof LongTypeQuestionFragment) {
            String answer = ((LongTypeQuestionFragment) f).getAnswer();
            if (answer.equalsIgnoreCase("")) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please write " +
                        "answer.");
                flag = false;
            } else {
                questionModelArrayList.get(nextValue).setAnswer(answer);
                flag = true;
            }
        } else if (f instanceof MultipleAnswerFragment) {
            String answer = ((MultipleAnswerFragment) f).getAnswer();
            if (answer.equalsIgnoreCase("")) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please select " +
                        "answer.");
                flag = false;
            }
            else if(((MultipleAnswerFragment) f).getotherAnswerlbl()!=null &&(
                    (MultipleAnswerFragment) f)
                    .getotherAnswerlbl().equalsIgnoreCase("")){
                Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please write " +
                        "answer.");
                flag = false;
            }

            else {
                questionModelArrayList.get(nextValue).setAnswer(answer);
                questionModelArrayList.get(nextValue).setMultipleotherField(((MultipleAnswerFragment) f).getotherAnswer());
                questionModelArrayList.get(nextValue).setMultipleotherFieldanswer(((MultipleAnswerFragment) f).getotherAnswerlbl());
                flag = true;
            }
        } else if (f instanceof SingleChoiceFragment) {
            String answer = ((SingleChoiceFragment) f).getAnswer();
            if (answer.equalsIgnoreCase("")) {
                Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please select " +
                        "answer.");
                flag = false;
            } else {

                questionModelArrayList.get(nextValue).setAnswer(answer);
                flag = true;
            }
        } else if (f instanceof AddNewQuestionFragment) {
            String[] answers = ((AddNewQuestionFragment) f).getAnswer();
            flag = true;
            for (String answer : answers) {

                String[] options = answer.split(",");
                for (String option : options) {
                    String[] ops1 = option.split(":");

                    if (ops1 == null || ops1.length <= 1) {
                        Utils.displayDialog(getActivity(), getString(R.string.app_name), "Please write " +
                                "answer.");
                        flag = false;
                        break;
                    }
                }
                if (!flag) {
                    break;
                }
            }
            if (flag) {
                questionModelArrayList.get(nextValue).setAnswers(answers);
            }

        }
        return flag;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            paymentId = data.getExtras().getString("payment_id");
            asyncSubmitAnswer();
        }
    }

    private void openDialogInfo() {
        final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (questionCatagoryModel.getName().equalsIgnoreCase("llc")) {
            dialog.setContentView(R.layout.dialog_llc_information);
        } else {
            dialog.setContentView(R.layout.dialog_corporation_information);
        }

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(Toolbar.LayoutParams.FILL_PARENT, Toolbar.LayoutParams.MATCH_PARENT);
        dialog.show();
        ImageView ivClose = (ImageView) dialog.findViewById(R.id.dialog_llc_information_iv_close);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getFragmentManager().popBackStack();
            }
        });


    }

    @Override
    public void onItemClick(String answer) {
        flowFinish(answer);
    }

    public void flowFinish(String answer) {
        if (nextValue != questionModelArrayList.size() - 1) {
            if (answer.equalsIgnoreCase(questionModelArrayList.get(nextValue).getFlow_finish())) {
                tvSubmit.setVisibility(View.VISIBLE);
                tvNext.setVisibility(View.GONE);
            } else {
                tvSubmit.setVisibility(View.GONE);
                tvNext.setVisibility(View.VISIBLE);
            }


        }
    }


}
