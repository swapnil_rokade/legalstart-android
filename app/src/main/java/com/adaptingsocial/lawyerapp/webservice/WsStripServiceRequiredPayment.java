package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class WsStripServiceRequiredPayment {
    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private int success;
    private String message = "";
    private String paymentId = "";

    public WsStripServiceRequiredPayment(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }



    public String getMessage() {
        return message;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void executeService(String token, String amount) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_STRIPE_SERVICE_REQUIRED_PAYMENT;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url, generateRequest(amount, token));
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private RequestBody generateRequest(String amount, String token) throws
            JSONException {
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add("token", token);
        Float amountValue = Float.parseFloat(amount);
        Float pay = amountValue * 100;
        int ammount = Math.round(pay);
        String finalAmount = String.valueOf(ammount);
        formEncodingBuilder.add("amount", finalAmount);

        return formEncodingBuilder.build();
    }

    public int getCode() {
        return success;
    }

    private void parseResponse(final String response) {
        if (response != null && response.trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                final JSONObject mainSettings = mainObject.optJSONObject(WSConstants.SETTINGS);
                success = Integer.parseInt(mainSettings.optString(WSConstants.SUCCESS));
                message = mainSettings.optString(WSConstants.MESSAGE);

                final JSONArray dataArray = mainObject.optJSONArray(WSConstants.DATA);
                if (dataArray != null && dataArray.length() > 0) {
                    final JSONObject jsonObject = dataArray.optJSONObject(0);
                    paymentId = jsonObject.optString("document_payment_id");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
