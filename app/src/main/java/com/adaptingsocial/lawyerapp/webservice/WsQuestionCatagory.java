package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.QuesionCatagoryModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by indianic on 03/07/17.
 */

public class WsQuestionCatagory {
    private Context mContext;
    private QuesionCatagoryModel quesionCatagoryModel;
    private int code;
    private String message;
    private ArrayList<QuesionCatagoryModel> quesionCatagoryModelArrayList;

    public ArrayList<QuesionCatagoryModel> getQuesionCatagoryModels() {
        return quesionCatagoryModelArrayList;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public WsQuestionCatagory(final Context mContext) {
        this.mContext = mContext;
        quesionCatagoryModel = new QuesionCatagoryModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }


    public void executeService() {
        final String url = WSConstants.BASE_URL + WSConstants.WS_QUESTION_CATAGORY_LIST;
        final String response = new WSUtil().callServiceHttpPost(url, generateRequest());
        parseResponse(response);
        AppLog.showLogD("Result", "" + response);


    }

    private RequestBody generateRequest() {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));

        return formEncodingBuilder.build();
    }


    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");


                final JSONArray dataArray = mainObject.optJSONArray("data");
                if (dataArray != null && dataArray.length() > 0) {
                    quesionCatagoryModelArrayList = new ArrayList<>();
                    final int length = dataArray.length();
                    for (int i = 0; i < length; i++) {
                        final JSONObject dataObject = dataArray.getJSONObject(i);
                        QuesionCatagoryModel model = new QuesionCatagoryModel();
                        model.setId(dataObject.optString("question_category_id"));
                        model.setAmmount(dataObject.optString("amount"));
                        model.setName(dataObject.optString("category_name"));
                        model.setImage(dataObject.optString("image"));
                        model.setDescription(dataObject.optString("description"));
                        quesionCatagoryModelArrayList.add(model);

                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

}
