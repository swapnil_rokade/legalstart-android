package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;
import android.util.Log;

import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Web Service utility class to call web urls. And returns response.
 */
public class WSUtil {

    public static String callHttpGet(final HttpUrl url) {
        String responseString = "";
        try {
            final OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setConnectTimeout(WSConstants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
            okHttpClient.setReadTimeout(WSConstants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
            final Request.Builder builder = new Request.Builder();
            builder.url(url);
            // Add authentication header if required
            //builder.header(AUTHORIZATION, CREDENTIAL);
            final Response response;
            response = okHttpClient.newCall(builder.build()).execute();
            responseString = response.body().string();


        } catch (Exception e) {
            e.printStackTrace();

        }
        return responseString;
    }



    public String callServiceHttpPost(final String url, final RequestBody requestBody) {

        Log.e(WSUtil.class.getSimpleName(), String.format("Request String : %s", requestBody.toString()));

        String responseString = "";

        try {
            final OkHttpClient okHttpClient = new OkHttpClient();

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClient.setConnectTimeout(WSConstants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
            okHttpClient.setReadTimeout(WSConstants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
            final Request request = new Request.Builder()
                    .url(url).addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .post(requestBody)
                    .build();
            AppLog.showLogD("Result","request is" + request);

            final Response response = okHttpClient.newCall(request).execute();
            responseString = response.body().string();
            Log.e(WSUtil.class.getSimpleName(), String.format("Response String : %s", responseString));
        } catch (IOException e) {
            e.printStackTrace();
            responseString = WSConstants.getNetworkError();

        }
        return responseString;

    }

    public String callServiceHttpPost(final String url) {
        String responseString = "";

        try {
            final OkHttpClient okHttpClient = new OkHttpClient();

            okHttpClient.setConnectTimeout(WSConstants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
            okHttpClient.setReadTimeout(WSConstants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
            final Request request = new Request.Builder()
                    .url(url).addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .build();
            AppLog.showLogD("Result","request is" + request);
            final Response response = okHttpClient.newCall(request).execute();
            responseString = response.body().string();
            Log.e(WSUtil.class.getSimpleName(), String.format("Response String : %s", responseString));
        } catch (IOException e) {
            e.printStackTrace();
            responseString = WSConstants.getNetworkError();

        }
        return responseString;

    }


}
