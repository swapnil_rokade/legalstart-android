package com.adaptingsocial.lawyerapp.webservice;


import android.content.Context;
import android.os.Bundle;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WSDeleteCase {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private int success;
    private String message = "";


    public WSDeleteCase(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public String getMessage() {
        return message;
    }


    public void executeService(Bundle bundle) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_DELETE_CASE;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url, generateRequest(bundle));
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private RequestBody generateRequest(final Bundle bundle) throws JSONException {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, bundle.getString(WSConstants.WS_KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_CASE_ID, bundle.getString(WSConstants.WS_KEY_CASE_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));

        return formEncodingBuilder.build();
    }

    public int getCode() {
        return success;
    }


    private void parseResponse(final String response) {
        if (response != null && response.trim().length() > 0) {
            try {
                final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
                final JSONObject mainObject = new JSONObject(response);
                final JSONObject mainSettings = mainObject.optJSONObject(WSConstants.SETTINGS);
                success = Integer.parseInt(mainSettings.optString(WSConstants.SUCCESS));
                message = mainSettings.optString(WSConstants.MESSAGE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
