package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by indianic on 01/05/17.
 */

public class WsLikeComment {
    private Context mContext;
    private int code;
    private String message;
    private String blogLike;
    private String count_like;

    public String getCount_like() {
        return count_like;
    }


    public String getBlogLike() {
        return blogLike;
    }


    public WsLikeComment(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService(String blogId) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_LIKE_COMMENT;
        final String response;
        response = new WSUtil().callServiceHttpPost(url, generateRequest(blogId));
        AppLog.showLogD("Result", "" + response);
        parseResponse(response);


    }

    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            final JSONObject mainObject;
            try {
                mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");
                final JSONArray dataArray = mainObject.optJSONArray("data");
                if (dataArray != null && dataArray.length() > 0) {
                    for (int i = 0; i < dataArray.length(); i++) {
                        final JSONObject dataObject = dataArray.getJSONObject(i);
                        blogLike = dataObject.optString("blog_like");
                        count_like = dataObject.optString("count_like");
                    }
                } else {
                    //dislike
                    blogLike = "0";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    private RequestBody generateRequest(String blogId) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_BLOG_COMMENT_ID, blogId);
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        return formEncodingBuilder.build();
    }


}
