package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CaseListModel;
import com.adaptingsocial.lawyerapp.model.DocumentModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by indianic on 02/06/17.
 */

public class WsCaseDetail {
    private Context mContext;
    private CaseListModel caseListModel;
    private int code;
    private String message;
    private ArrayList<CaseListModel> caseListModelArrayList;
    private ArrayList<DocumentModel> documentListModelArrayList;

    public ArrayList<CaseListModel> getCaseListModelArrayList() {
        return caseListModelArrayList;
    }


    public int getCode() {

        return code;
    }

    public String getMessage() {

        return message;
    }

    public WsCaseDetail(final Context mContext) {
        this.mContext = mContext;
        caseListModel = new CaseListModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }


    public void executeService(String caseId) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_CASE_DETAIL;
        final String response = new WSUtil().callServiceHttpPost(url, generateRequest(caseId));
        parseResponse(response);
        AppLog.showLogD("Result", "" + response);


    }

    private RequestBody generateRequest(String caseId) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        formEncodingBuilder.add(WSConstants.WS_KEY_CASE_ID, caseId);
        return formEncodingBuilder.build();
    }

    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                JSONObject settingObject = null;
                settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");
                final JSONObject dataObj = mainObject.getJSONObject("data");
                final JSONArray caseDetailArray = dataObj.getJSONArray("case_details");
                caseListModelArrayList = new ArrayList<>();
                final JSONObject jsonCaseDetailObj = caseDetailArray.getJSONObject(0);
                final CaseListModel model = new CaseListModel();
                model.setCaseId(jsonCaseDetailObj.optString("case_id"));
                model.setCaseTitle(jsonCaseDetailObj.optString("case_title"));
                model.setCourtName(jsonCaseDetailObj.optString("document_type"));
                model.setCaseNo(jsonCaseDetailObj.optString("agreement"));
                model.setCaseBehalfOf(jsonCaseDetailObj.optString("concerns"));
                model.setUserName(jsonCaseDetailObj.optString("users_name"));

// document array
                final JSONArray documentListArray = dataObj.getJSONArray("document_listing");
                if (documentListArray != null && documentListArray.length() > 0) {
                    final int lengthdocument = documentListArray.length();
                    documentListModelArrayList = new ArrayList<>();
                    for (int j = 0; j < lengthdocument; j++) {
                        final JSONObject docObject = documentListArray.getJSONObject(j);
                        final DocumentModel documentModel = new DocumentModel();
                        documentModel.setDocumentId(docObject.optString("document_id"));
                        documentModel.setDocumentName(docObject.optString("document_name"));
                        documentModel.setFilePath(docObject.optString("document"));
                        documentModel.setImageExt(docObject.optString("document_type"));
                        documentModel.setDate(docObject.optString("date"));
                        documentModel.setUserid(docObject.optString("users_id"));
                        documentListModelArrayList.add(documentModel);
                    }
                }

                model.setDocumentModelArrayList(documentListModelArrayList);
                caseListModelArrayList.add(model);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
}
