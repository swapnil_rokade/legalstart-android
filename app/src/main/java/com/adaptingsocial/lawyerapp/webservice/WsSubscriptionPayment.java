package com.adaptingsocial.lawyerapp.webservice;


import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CardDetailModel;
import com.adaptingsocial.lawyerapp.model.CaseListModel;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WsSubscriptionPayment {
    private Context context;
    private CaseListModel caseListModel;
    private int code;
    private String message;

    public WsSubscriptionPayment(final Context mContext) {
        this.context = mContext;
        caseListModel = new CaseListModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService(CardDetailModel cardDetailModel) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_SUBSCRIPTION_ARB_PAYMENT;
        final String response;
        response = new WSUtil().callServiceHttpPost(url, generateRequest(cardDetailModel));
        parseResponse(response);


    }

    private RequestBody generateRequest(CardDetailModel cardDetailModel) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(context);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        formEncodingBuilder.add(WSConstants.WS_KEY_AMMOUNT, cardDetailModel.getAmount());
        formEncodingBuilder.add(WSConstants.WS_KEY_CARD_NUMBER, cardDetailModel.getCardNumber());
        formEncodingBuilder.add(WSConstants.WS_ORDER_UNIQUE_NUMBER, cardDetailModel.getOrderNumber());
        formEncodingBuilder.add(WSConstants.WS_FIRST_NAME, cardDetailModel.getFirstName());
        formEncodingBuilder.add(WSConstants.WS_LAST_NAME, cardDetailModel.getLastName());
        formEncodingBuilder.add(WSConstants.WS_EXP_DATE, cardDetailModel.getExpDate());
        return formEncodingBuilder.build();
    }

    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {

            final JSONObject mainObject;
            try {
                mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject(WSConstants.SETTINGS);
                code = settingObject.optInt(WSConstants.SUCCESS);
                message = settingObject.optString(WSConstants.MESSAGE);

                final JSONArray dataArray = mainObject.optJSONArray("data");
                if (dataArray != null && dataArray.length() > 0) {
                    JSONObject jsonObject = dataArray.optJSONObject(0);
                    String chargeId = jsonObject.optString("u_charge_id");
                    final PreferenceUtils preferenceUtils = new PreferenceUtils(context);
                    preferenceUtils.putString(preferenceUtils.KEY_CHARGE_ID, chargeId);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }
}
