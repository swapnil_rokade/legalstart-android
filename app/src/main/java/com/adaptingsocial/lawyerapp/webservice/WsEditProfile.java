package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;


public class WsEditProfile {
    private Context mContext;
    private int code;
    private String message;
    private Bundle bundle;

    public WsEditProfile(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService(Bundle bundle) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_UPDATE_PROFILE;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url, generateRequest(bundle));
            parseResponse(response);
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private RequestBody generateRequest(final Bundle bundle) throws JSONException {
        this.bundle = bundle;
        final MultipartBuilder multipartBuilder = new MultipartBuilder();
        multipartBuilder.type(MultipartBuilder.FORM);
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_FIRSTNAME, bundle.getString(WSConstants.WS_KEY_FIRSTNAME));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_LASTNAME, bundle.getString(WSConstants.WS_KEY_LASTNAME));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_MOBILE_NO, bundle.getString(WSConstants.WS_KEY_MOBILE_NO));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_DATE_OF_BIRTH, bundle.getString(WSConstants.WS_KEY_DATE_OF_BIRTH));
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));


        if (!TextUtils.isEmpty(bundle.getString(WSConstants.WS_KEY_BACKGROUND_IMAGE))) {
            final File fileBackgroundImage = new File((bundle.getString(WSConstants.WS_KEY_BACKGROUND_IMAGE)));
            multipartBuilder.addFormDataPart(WSConstants.WS_KEY_BACKGROUND_IMAGE, fileBackgroundImage.getName(),
                    RequestBody.create(MediaType.parse("image/png"), fileBackgroundImage));

        }
        if (!TextUtils.isEmpty(bundle.getString(WSConstants.WS_KEY_PROFILE_IMAGE))) {
            final File fileProfileImage = new File(bundle.getString(WSConstants.WS_KEY_PROFILE_IMAGE));
            multipartBuilder.addFormDataPart(WSConstants.WS_KEY_PROFILE_IMAGE, fileProfileImage.getName(),
                    RequestBody.create(MediaType.parse("image/png"), fileProfileImage));


        }
        return multipartBuilder.build();

    }

    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            final JSONObject mainObject;
            final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
            try {
                mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");
                if (code == WSConstants.STATUS_SUCCESS) {
                    preferenceUtils.putString(preferenceUtils.KEY_FIRST_NAME, bundle.getString(WSConstants.WS_KEY_FIRSTNAME));
                    preferenceUtils.putString(preferenceUtils.KEY_LAST_NAME, bundle.getString(WSConstants.WS_KEY_LASTNAME));
                }
                JSONArray jData = mainObject.getJSONArray("data");
                if (jData != null && jData.length() > 0) {
                    final int length = jData.length();
                    for (int i = 0; i < length; i++) {
                        final JSONObject dataObject = jData.getJSONObject(i);
                        String name = dataObject.optString("name");
                        String profielImage = dataObject.optString("profile_image");
                        preferenceUtils.putString(preferenceUtils.KEY_PROFILE_IMAGE, profielImage);

                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }
}
