package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CaseListModel;
import com.adaptingsocial.lawyerapp.model.CommentModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by indianic on 01/05/17.
 */

public class WsAddComment {
    private Context mContext;
    private CaseListModel caseListModel;
    private int code;
    private String message;
    private ArrayList<CommentModel> commentListModelArrayList;


    public ArrayList<CommentModel> getCommentListModelArrayList() {
        return commentListModelArrayList;
    }


    public WsAddComment(final Context mContext) {
        this.mContext = mContext;
        caseListModel = new CaseListModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService(String blogId, String comment) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_ADD_COMMENT;
        final String response;
        response = new WSUtil().callServiceHttpPost(url, generateRequest(blogId, comment));
        AppLog.showLogD("Result", "" + response);
        parseResponse(response);


    }

    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            final JSONObject mainObject;
            try {
                mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");
                JSONObject dataObject = mainObject.getJSONObject("data");
                JSONArray commentArray = dataObject.getJSONArray("comment_listing");
                if (commentArray != null && commentArray.length() > 0) {
                    final int lengthComment = commentArray.length();
                    commentListModelArrayList = new ArrayList<>();
                    for (int j = 0; j < lengthComment; j++) {
                        final JSONObject commentObject = commentArray.getJSONObject(j);
                        final CommentModel commentModel = new CommentModel();
                        commentModel.setUserId(commentObject.optString("u_users_id"));
                        commentModel.setName(commentObject.optString("name"));
                        commentModel.setLikeCounts(commentObject.optString("count_like"));
                        commentModel.setProfileImage(commentObject.optString("profile_picture"));
                        commentModel.setComment(commentObject.optString("comment"));
                        commentModel.setDate(commentObject.optString("date"));
                        commentModel.setBlogLike(commentObject.optString("blog_like"));
                        commentModel.setBlog_comment_id(commentObject.optString("blog_comment_id"));
                        commentListModelArrayList.add(commentModel);

                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    private RequestBody generateRequest(String blogId, String comment) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_BLOG_ID, blogId);
        formEncodingBuilder.add(WSConstants.WS_KEY_COMMENT, comment);
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        return formEncodingBuilder.build();
    }


}
