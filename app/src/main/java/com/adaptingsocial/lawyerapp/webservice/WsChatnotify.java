package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;
import android.os.Bundle;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CaseListModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by indianic on 11/05/17.
 */

public class WsChatnotify {
    private Context mContext;
    private CaseListModel caseListModel;
    private int code;
    private String message;

    public WsChatnotify(final Context mContext) {
        this.mContext = mContext;
        caseListModel = new CaseListModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService(Bundle bundle) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_EMAIL_NOTIFY;
        final String response;
        response = new WSUtil().callServiceHttpPost(url, generateRequest(bundle));
        parseResponse(response);
        AppLog.showLogD("Result", "" + response);

    }

    private void parseResponse(String response) {
        if (response != null && response.toString().trim().length() > 0) {
            final JSONObject mainObject;
            try {
                mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    private RequestBody generateRequest(Bundle bundle) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_NAME, bundle.getString(WSConstants.WS_KEY_NAME));
        formEncodingBuilder.add(WSConstants.WS_KEY_EMAIL, bundle.getString(WSConstants.WS_KEY_EMAIL));
        formEncodingBuilder.add(WSConstants.WS_KEY_PURPOSE, bundle.getString(WSConstants.WS_KEY_PURPOSE));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        return formEncodingBuilder.build();


    }

}