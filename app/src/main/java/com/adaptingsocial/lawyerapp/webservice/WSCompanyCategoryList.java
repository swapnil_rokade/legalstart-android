package com.adaptingsocial.lawyerapp.webservice;


import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CompanyCategoryListModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WSCompanyCategoryList {
    private Context mContext;
    private CompanyCategoryListModel companyCategoryListModel;
    private int code;
    private String message;
    private ArrayList<CompanyCategoryListModel> companyCategoryListModelArrayList;

    public ArrayList<CompanyCategoryListModel> getCompanyCategoryListModelArrayList() {
        return companyCategoryListModelArrayList;
    }


    public int getCode() {

        return code;
    }

    public String getMessage() {

        return message;
    }

    public WSCompanyCategoryList(final Context mContext) {
        this.mContext = mContext;
        companyCategoryListModel = new CompanyCategoryListModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }


    public void executeService() {
        final String url = WSConstants.BASE_URL + WSConstants.WS_COMPANY_CATEGORY_LIST;
        final String response;
        response = new WSUtil().callServiceHttpPost(url,generateRequest());
        parseResponse(response);
        AppLog.showLogD("Result", "" + response);


    }


    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject(WSConstants.SETTINGS);
                code = settingObject.optInt(WSConstants.SUCCESS);
                message = settingObject.optString(WSConstants.MESSAGE);
                final JSONArray dataArray = mainObject.optJSONArray(WSConstants.DATA);
                if (dataArray != null && dataArray.length() > 0) {
                    companyCategoryListModelArrayList = new ArrayList<>();
                    final int length = dataArray.length();
                    for (int i = 0; i < length; i++) {
                        final JSONObject dataObject = dataArray.getJSONObject(i);
                        final CompanyCategoryListModel model = new CompanyCategoryListModel();
                        model.setCompanyCategoryId(dataObject.optString("company_category_id"));
                        model.setCompanyCategoryName(dataObject.optString("company_category_name"));
                        companyCategoryListModelArrayList.add(model);
                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
    private RequestBody generateRequest() {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));

        return formEncodingBuilder.build();
    }



}
