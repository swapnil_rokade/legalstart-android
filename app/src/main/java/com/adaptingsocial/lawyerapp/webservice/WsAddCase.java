package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CaseListModel;
import com.adaptingsocial.lawyerapp.model.DocumentModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

;

/**
 * Created by indianic on 27/04/17.
 */

public class WsAddCase {
    private Context mContext;
    private CaseListModel caseListModel;
    private int code;
    private String message;

    public WsAddCase(final Context mContext) {
        this.mContext = mContext;
        caseListModel = new CaseListModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService(Bundle bundle, ArrayList<DocumentModel> documentModelArrayList) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_ADD_CASE;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url, generateRequest(bundle, documentModelArrayList));
            parseResponse(response);
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private RequestBody generateRequest(final Bundle bundle, ArrayList<DocumentModel>
            documentModelArrayList) throws JSONException {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final MultipartBuilder multipartBuilder = new MultipartBuilder();
        multipartBuilder.type(MultipartBuilder.FORM);
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_CASE_TITLE, bundle.getString(WSConstants.WS_KEY_CASE_TITLE));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_DOCUMENT_TEXT_TYPE, bundle.getString(WSConstants.WS_KEY_DOCUMENT_TEXT_TYPE));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_AGREEMENT, bundle.getString(WSConstants.WS_KEY_AGREEMENT));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_CONCERN, bundle.getString(WSConstants.WS_KEY_CONCERN));

        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));

        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));


        for (int i = 0; i < documentModelArrayList.size(); i++) {
            final DocumentModel photoModal = documentModelArrayList.get(i);
            final File fileAvatarImage = new File(photoModal.getFilePath());
            if (!TextUtils.isEmpty(photoModal.getFilePath()) && fileAvatarImage.exists()) {
                multipartBuilder.addFormDataPart("document_name[" + i + "]", fileAvatarImage.getName());
                multipartBuilder.addFormDataPart("document_type[" + i + "]", photoModal.getImageExt());

                if (photoModal.getImageExt().equalsIgnoreCase(".png") || photoModal.getImageExt()
                        .equalsIgnoreCase(".jpg") || photoModal.getImageExt().equalsIgnoreCase(".jpeg")) {
                    multipartBuilder.addFormDataPart("document[" + i + "]", fileAvatarImage.getName(),
                            RequestBody.create(MediaType.parse("image/png"), fileAvatarImage));
                } else {
                    multipartBuilder.addFormDataPart("document[" + i + "]", fileAvatarImage.getName(),
                            RequestBody.create(MediaType.parse("application/" + photoModal.getImageExt()),
                                    fileAvatarImage));

                }

            }


        }
        return multipartBuilder.build();

    }

    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            final JSONObject mainObject;
            try {
                mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }
}
