package com.adaptingsocial.lawyerapp.webservice;


import android.content.Context;
import android.os.Bundle;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.TimeSlotModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WSBookAppointment {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private int success;
    private String message = "";
    private ArrayList<TimeSlotModel> timesSlotArray;


    public ArrayList<TimeSlotModel> getTimesSlotArray() {
        return timesSlotArray;
    }

    public WSBookAppointment(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
        timesSlotArray = new ArrayList<>();
    }

    public String getMessage() {
        return message;
    }


    public void executeService(Bundle bundle) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_BOOK_APPOINTMENT;
        final String response;
        response = new WSUtil().callServiceHttpPost(url, generateRequest(bundle));
        AppLog.showLogD("Result", "" + response);
        parseResponse(response);


    }

    private RequestBody generateRequest(Bundle bundle) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, bundle.getString(WSConstants.WS_KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_AVAILABLE_DATE, bundle.getString(WSConstants.WS_KEY_AVAILABLE_DATE));
        formEncodingBuilder.add(WSConstants.WS_KEY_APPOINTMENT_TITLE, bundle.getString(WSConstants.WS_KEY_APPOINTMENT_TITLE));
        formEncodingBuilder.add(WSConstants.WS_KEY_AVAILABLE_START_TIME, bundle.getString(WSConstants.WS_KEY_AVAILABLE_START_TIME));
        formEncodingBuilder.add(WSConstants.WS_KEY_AVAILABLE_END_TIME, bundle.getString(WSConstants.WS_KEY_AVAILABLE_END_TIME));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        return formEncodingBuilder.build();
    }

    public int getCode() {
        return success;
    }


    private void parseResponse(final String response) {
        if (response != null && response.trim().length() > 0) {
            try {
                final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
                final JSONObject mainObject = new JSONObject(response);
                final JSONObject mainSettings = mainObject.optJSONObject(WSConstants.SETTINGS);
                success = Integer.parseInt(mainSettings.optString(WSConstants.SUCCESS));
                message = mainSettings.optString(WSConstants.MESSAGE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
