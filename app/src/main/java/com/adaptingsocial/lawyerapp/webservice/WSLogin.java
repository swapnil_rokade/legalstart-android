package com.adaptingsocial.lawyerapp.webservice;


import android.content.Context;
import android.os.Bundle;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WSLogin {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private int success;
    private String message = "";


    public WSLogin(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public String getMessage() {
        return message;
    }


    public void executeService(Bundle bundle) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_USER_LOGIN;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url, generateRequest(bundle));
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private RequestBody generateRequest(final Bundle bundle) throws JSONException {

        final MultipartBuilder multipartBuilder = new MultipartBuilder();
        multipartBuilder.type(MultipartBuilder.FORM);
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_EMAIL, bundle.getString(WSConstants.WS_KEY_EMAIL));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_PASSWORD, bundle.getString(WSConstants.WS_KEY_PASSWORD));
        PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);


        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_DEVICE_TOKEN, FirebaseInstanceId.getInstance().getToken());
        preferenceUtils.putString(preferenceUtils.KEY_TOKEN, FirebaseInstanceId.getInstance().getToken());
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_DEVICE_ID, Utils.getDeviceId(mContext));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_DEVICE_TYPE, Constants.KEY_ANDROID);

        preferenceUtils.putString(preferenceUtils.KEY_EMAIL, bundle.getString(WSConstants.WS_KEY_EMAIL));
        return multipartBuilder.build();
    }

    public int getCode() {
        return success;
    }


    private void parseResponse(final String response) {
        if (response != null && response.trim().length() > 0) {
            try {
                final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
                final JSONObject mainObject = new JSONObject(response);
                final JSONObject mainSettings = mainObject.optJSONObject(WSConstants.SETTINGS);
                success = Integer.parseInt(mainSettings.optString(WSConstants.SUCCESS));
                message = mainSettings.optString(WSConstants.MESSAGE);
                if (success == WSConstants.STATUS_SUCCESS) {


                    final JSONObject jsonObjData = mainObject.optJSONObject(WSConstants.DATA);
                    final JSONArray jAcessToken = jsonObjData.getJSONArray("random_function");
                    final JSONArray jsonaRRAY = jsonObjData.getJSONArray
                            ("check_user_email_and_password");
                    final JSONObject jsonObjectData = jsonaRRAY.optJSONObject(0);
                    preferenceUtils.putBoolean(preferenceUtils.KEY_USER_LOGIN, true);
                    preferenceUtils.putString(preferenceUtils.KEY_USER_ID, jsonObjectData.getString(WSConstants.WS_KEY_USER_ID));
                    preferenceUtils.putString(preferenceUtils.KEY_FIRST_NAME, jsonObjectData.getString(WSConstants.WS_KEY_FIRSTNAME));
                    preferenceUtils.putString(preferenceUtils.KEY_LAST_NAME, jsonObjectData.getString(WSConstants.WS_KEY_LASTNAME));
                    preferenceUtils.putString(preferenceUtils.KEY_PROFILE_IMAGE, jsonObjectData.getString(WSConstants.WS_KEY_PROFILE_PICTURE));
                    preferenceUtils.putString(preferenceUtils.KEY_LAST_NAME, jsonObjectData.getString(WSConstants.WS_KEY_LASTNAME));
                    preferenceUtils.putString("moblie_number",
                            jsonObjectData.getString("moblie_number"));

                    preferenceUtils.putString(preferenceUtils.KEY_CHARGE_ID, jsonObjectData
                            .optString(WSConstants.WS_KEY_SUBSCRIPTION_ID));


                    preferenceUtils.putString(preferenceUtils.KEY_QUICK_BLOX_EMAIL, jsonObjectData.getString
                            (WSConstants.WS_KEY_QUICK_BLOX_EMAIL));

                    preferenceUtils.putString(preferenceUtils.KEY_QUICK_BLOX_PASSWORD, jsonObjectData.getString
                            (WSConstants.WS_KEY_QUICK_BLOX_PASSWORD));
                    preferenceUtils.putString(preferenceUtils.KEY_QUICK_BLOX_ID, jsonObjectData.getString
                            (WSConstants.WS_KEY_QUICK_BLOX_ID));

                    //put condition for free and paid user
                    String paymentStatus = jsonObjectData.optString("u_payment_status");
                    if (paymentStatus.equalsIgnoreCase("pending")) {
                        preferenceUtils.putBoolean(preferenceUtils.KEY_APP_USER_TYPE, true);
                    } else {
                        preferenceUtils.putBoolean(preferenceUtils.KEY_APP_USER_TYPE, false);
                    }


                    final JSONObject jsonObjAcessToken = jAcessToken.getJSONObject(0);
                    preferenceUtils.putString(preferenceUtils.KEY_ACESS_TOKEN, jsonObjAcessToken
                            .getString("access_token"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
