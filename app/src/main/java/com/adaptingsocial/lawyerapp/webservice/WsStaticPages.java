package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;
import android.os.Bundle;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by indianic on 24/07/17.
 */

public class WsStaticPages {
    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private int success;
    private String message = "";

    public String getPageContent() {
        return pageContent;
    }

    private String pageContent="";
    public WsStaticPages(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }
    public String getMessage() {
        return message;
    }

    public void executeService(final String pageCode) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_STATIC_PAGE;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url, generateRequest(pageCode));
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
    private RequestBody generateRequest(final String pageCode) throws JSONException {
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_PAGE_CODE, pageCode);
        return formEncodingBuilder.build();
    }
    public int getCode() {
        return success;
    }
    private void parseResponse(final String response) {
        if (response != null && response.trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                final JSONObject mainSettings = mainObject.optJSONObject(WSConstants.SETTINGS);
                success = Integer.parseInt(mainSettings.optString(WSConstants.SUCCESS));
                message = mainSettings.optString(WSConstants.MESSAGE);
               JSONArray jsonArray= mainObject.getJSONArray("data");
                if(jsonArray!=null &&jsonArray.length()>0){
                     pageContent=jsonArray.getJSONObject(0).optString("page_content");

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
