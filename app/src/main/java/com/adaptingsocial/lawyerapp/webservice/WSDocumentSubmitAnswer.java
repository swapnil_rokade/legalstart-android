package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.QuestionModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WSDocumentSubmitAnswer {
    private Context mContext;
    private QuestionModel questionModel;
    private int code;
    private String message;
    private ArrayList<QuestionModel> questionModelArrayList;
    private String documentId;
    private String paymentId;

    public WSDocumentSubmitAnswer(final Context mContext, ArrayList<QuestionModel> questionModelArrayList, String documentId, String paymentId) {
        this.mContext = mContext;
        this.documentId = documentId;
        this.paymentId = paymentId;
        this.questionModelArrayList = questionModelArrayList;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService() {
        final String url = WSConstants.BASE_URL + WSConstants.WS_DOCUMENT_SUBMIT_ANSWER;
        //final String response = new WSUtil().callServiceHttpPost(url, generateRequest(questionModelArrayList));

//        final String response = new WSUtil().callServiceHttpPost(url
//                + "?document_id=" + documentId + "&document_payment_id=" + paymentId + "&question_answer=" +
//                generateRequest(questionModelArrayList));


        final String response = new WSUtil().callServiceHttpPost(url,
                generateRequest(questionModelArrayList));
        parseResponse(response);
        /*try {
            String encodedUrl = URLEncoder.encode(url
                    + "?question_category_id=1&startup_payment_id=1&question_answer=" +
                    generateRequest(questionModelArrayList), "UTF-8");

            HttpUrl url1 = HttpUrl.parse(encodedUrl);
            final String response = new WSUtil().callServiceHttpPost(url
                    + "?question_category_id=1&startup_payment_id=1&question_answer=" +
                    generateRequest(questionModelArrayList));


            AppLog.showLogD("Result", "" + response);
            parseResponse(response);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/


    }

    private RequestBody generateRequest(ArrayList<QuestionModel> questionModelArrayList) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);

        final FormEncodingBuilder multipartBuilder = new FormEncodingBuilder();
//        final MultipartBuilder multipartBuilder = new MultipartBuilder();
        JSONArray jsonAnswerArray = new JSONArray();
        for (int j = 0; j < questionModelArrayList.size(); j++) {

            try {
                JSONObject jAnswerObject = new JSONObject();

                jAnswerObject.put("type", questionModelArrayList.get(j).getQtypecode());
                jAnswerObject.put("question_id", questionModelArrayList.get(j).getId());
                jAnswerObject.put("users_id", preferenceUtils.getString(preferenceUtils.KEY_USER_ID));

                if (questionModelArrayList.get(j).getQtypecode().equalsIgnoreCase("AddNew")) {
                    final JSONArray jAnswerAray = new JSONArray();
                    final String[] answers = questionModelArrayList.get(j).getAnswers();
                    for (String answer : answers) {
                        JSONObject obj = new JSONObject();
                        String[] options = answer.split(",");
                        for (String option : options) {
                            String[] ops1 = option.split(":");
                            obj.put(ops1[0], ops1[1]);
                        }
                        jAnswerAray.put(obj);
                    }
                    jAnswerObject.put("answer", jAnswerAray);
                } else if (questionModelArrayList.get(j).getQtypecode().equalsIgnoreCase("Multiselect")) {
                    final JSONArray jsonArray = new JSONArray();


                    final QuestionModel questionModel = questionModelArrayList.get(j);
                    final String[] answers = questionModel.getAnswer().split(",");

                    for (String answer : answers) {
                        final JSONObject jsonObject = new JSONObject();
                        jsonObject.put("question_option", answer);
                        jsonArray.put(jsonObject);
                    }

                    jAnswerObject.put("answer", jsonArray.toString());
                }

                else {
                    jAnswerObject.put("answer", questionModelArrayList.get(j).getAnswer());
                }
                jsonAnswerArray.put(jAnswerObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        AppLog.showLogD("request", "" + jsonAnswerArray.toString());
        multipartBuilder.add("question_answer", jsonAnswerArray.toString());
        multipartBuilder.add("document_id", documentId);
        multipartBuilder.add("document_payment_id", paymentId);
        multipartBuilder.build().toString();
        AppLog.showLogD("Result is", "request is" + multipartBuilder.build().toString());
        return multipartBuilder.build();
    }



    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            final JSONObject mainObject;
            try {
                mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
