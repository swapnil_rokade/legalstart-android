package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.SubscriptionModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by indianic on 23/06/17.
 */

public class WsCheckPaymentStatus {
    private Context mContext;
    private SubscriptionModel subscriptionModel;
    private int code;
    private String message;

    public String getPaymentStatus() {
        return paymentStatus;
    }

    private String paymentStatus;

    public int getCode() {
        return code;
    }

    public String getMessage() {

        return message;
    }

    public WsCheckPaymentStatus(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public void executeService() {
        final String url = WSConstants.BASE_URL + WSConstants.WS_SUBSCRIPTION_STATUS;
        final String response = new WSUtil().callServiceHttpPost(url, generateRequest());
        parseResponse(response);
        AppLog.showLogD("Result", "" + response);

    }

    private RequestBody generateRequest() {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();

        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));

        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));

        formEncodingBuilder.add("ws_debug", "1");


        return formEncodingBuilder.build();
    }

    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject(WSConstants.SETTINGS);
                code = settingObject.optInt(WSConstants.SUCCESS);
                message = settingObject.optString(WSConstants.MESSAGE);

                final JSONArray dataArray = mainObject.optJSONArray(WSConstants.DATA);
                if (dataArray != null && dataArray.length() > 0) {
                    paymentStatus=dataArray.getJSONObject(0).optString("u_payment_status");
                    if(paymentStatus.equalsIgnoreCase("succeeded")){
                        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
                        preferenceUtils.putBoolean(preferenceUtils.KEY_APP_USER_TYPE, false);

                    }
                    else{
                        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
                        preferenceUtils.putBoolean(preferenceUtils.KEY_APP_USER_TYPE, true);
                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


}
