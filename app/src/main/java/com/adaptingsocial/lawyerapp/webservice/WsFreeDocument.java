package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by indianic on 25/07/17.
 */

public class WsFreeDocument {
    private Context mContext;
    private int code;
    private String message;
    private String insertId = "";

    public WsFreeDocument(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService(String documentId) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_FREE_DOCUMENT;
        final String response;
        response = new WSUtil().callServiceHttpPost(url, generateRequest(documentId));
        parseResponse(response);
        AppLog.showLogD("Result", "" + response);


    }

    private RequestBody generateRequest(String documentId) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        formEncodingBuilder.add("document_id", documentId);

        return formEncodingBuilder.build();
    }

    public String getInsertId() {
        return insertId;
    }

    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            final JSONObject mainObject;
            try {
                mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");
                JSONArray jDataArray = mainObject.getJSONArray("data");
                if (jDataArray != null && jDataArray.length() > 0) {
                    JSONObject jsonObject = jDataArray.getJSONObject(0);
                    insertId = jsonObject.optString("insert_id");


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

}
