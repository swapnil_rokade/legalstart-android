package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class WsStripeDocumentPayment {
    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private int success;
    private String message = "";
    private String paymentId = "";

    public WsStripeDocumentPayment(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }


    //    http://php54.indianic.com/lawyerapp/WS/stripe_charge_document?&
// token=&users_id=&access_token=&document_id=&ws_debug=1
    public String getMessage() {
        return message;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void executeService(String token, String ammount, String documentId) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_STRIPE_CHARGE_DOCUMENT_PAYMENT;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url, generateRequest(ammount, token, documentId));
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private RequestBody generateRequest(String amount, String token, String documentId) throws
            JSONException {
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));


        formEncodingBuilder.add("document_id", documentId);
        formEncodingBuilder.add("ws_debug", "1");
        formEncodingBuilder.add("token", token);
        Float amountvalue = Float.parseFloat(amount);
        Float pay = amountvalue * 100;
        int amnt = Math.round(pay);
        String finalAmount = String.valueOf(amnt);
        formEncodingBuilder.add("amount", finalAmount);

        return formEncodingBuilder.build();
    }

    public int getCode() {
        return success;
    }

    private void parseResponse(final String response) {
        if (response != null && response.trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                final JSONObject mainSettings = mainObject.optJSONObject(WSConstants.SETTINGS);
                success = Integer.parseInt(mainSettings.optString(WSConstants.SUCCESS));
                message = mainSettings.optString(WSConstants.MESSAGE);

                final JSONArray dataArray = mainObject.optJSONArray(WSConstants.DATA);
                if (dataArray != null && dataArray.length() > 0) {
                    final JSONObject jsonObject = dataArray.optJSONObject(0);
                    paymentId = jsonObject.optString("document_payment_id");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
