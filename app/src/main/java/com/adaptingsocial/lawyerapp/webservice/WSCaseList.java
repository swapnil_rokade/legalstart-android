package com.adaptingsocial.lawyerapp.webservice;


import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CaseListModel;
import com.adaptingsocial.lawyerapp.model.DocumentModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WSCaseList {
    private Context mContext;
    private CaseListModel caseListModel;
    private int code;
    private String message;
    private ArrayList<CaseListModel> caseListModelArrayList;
    private ArrayList<DocumentModel> documentListModelArrayList;

    public ArrayList<CaseListModel> getCaseListModelArrayList() {
        return caseListModelArrayList;
    }


    public int getCode() {

        return code;
    }

    public String getMessage() {

        return message;
    }

    public WSCaseList(final Context mContext) {
        this.mContext = mContext;
        caseListModel = new CaseListModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }


    public void executeService() {
        final String url = WSConstants.BASE_URL + WSConstants.WS_CASE_LIST;
        final String response = new WSUtil().callServiceHttpPost(url, generateRequest());
        parseResponse(response);
        AppLog.showLogD("Result", "" + response);


    }

    private RequestBody generateRequest() {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));

        return formEncodingBuilder.build();
    }


    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");
                final JSONArray dataArray = mainObject.optJSONArray("data");
                if (dataArray != null && dataArray.length() > 0) {
                    caseListModelArrayList = new ArrayList<>();
                    final int length = dataArray.length();
                    for (int i = 0; i < length; i++) {
                        final JSONObject dataObject = dataArray.getJSONObject(i);
                        final CaseListModel model = new CaseListModel();
                        model.setCaseId(dataObject.optString("case_id"));
                        model.setCaseTitle(dataObject.optString("case_title"));
                        model.setDocumentType(dataObject.optString("document_type"));
                        model.setAgreement(dataObject.optString("agreement"));
                        model.setCaseBehalfOf(dataObject.optString("concerns"));
                        model.setUserName(dataObject.optString("users_name"));
                        model.setConcerns(dataObject.optString("concerns"));


                        final JSONArray documentArray = dataObject.getJSONArray("document_listings");
                        if (documentArray != null && documentArray.length() > 0) {
                            final int lengthdocument = documentArray.length();
                            documentListModelArrayList = new ArrayList<>();
                            for (int j = 0; j < lengthdocument; j++) {
                                final JSONObject docObject = documentArray.getJSONObject(j);
                                final DocumentModel documentModel = new DocumentModel();
                                documentModel.setDocumentId(docObject.optString("document_id"));
                                documentModel.setDocumentName(docObject.optString("document_name"));
                                documentModel.setFilePath(docObject.optString("document"));
                                documentModel.setImageExt(docObject.optString("document_type"));
                                documentModel.setDate(docObject.optString("date"));
                                documentModel.setUserid(docObject.optString("users_id"));
                                documentListModelArrayList.add(documentModel);
                            }
                        }

                        model.setDocumentModelArrayList(documentListModelArrayList);
                        caseListModelArrayList.add(model);

                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


}
