package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.QuesionCatagoryModel;
import com.adaptingsocial.lawyerapp.model.QuestionDescriptionDetailModal;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by indianic on 10/08/17.
 */

public class WsQuestionDescriptionDetail {
    private Context mContext;
    private int code;
    private String message;
    private ArrayList<QuestionDescriptionDetailModal> questionDescriptionList;


    public ArrayList<QuestionDescriptionDetailModal> getQuestionDescriptionList() {
        return questionDescriptionList;
    }

    public WsQuestionDescriptionDetail(final Context mContext,QuesionCatagoryModel questionCatagoryModel) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);

    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService(QuesionCatagoryModel questionCatagoryModel) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_QUESTION_CAT;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url,generateRequest(questionCatagoryModel));
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }
    private RequestBody generateRequest(QuesionCatagoryModel questionCatagoryModel) throws
            JSONException {
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));

        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add("category_name",questionCatagoryModel.getName());

        return formEncodingBuilder.build();
    }
    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject(WSConstants.SETTINGS);
                code = settingObject.optInt(WSConstants.SUCCESS);
                message = settingObject.optString(WSConstants.MESSAGE);
                final JSONArray dataArray = mainObject.optJSONArray(WSConstants.DATA);

                if (dataArray != null && dataArray.length() > 0) {
                    questionDescriptionList = new ArrayList<>();
                    final int length = dataArray.length();
                    for (int i = 0; i < length; i++) {
                        final JSONObject dataObject = dataArray.getJSONObject(i);
                        final QuestionDescriptionDetailModal model = new QuestionDescriptionDetailModal();
                        model.setQuestion_state_category_id(dataObject.optString("question_state_category_id"));
                        model.setState_name(dataObject.optString("state_name"));
                        model.setAmmount(dataObject.optString("amount"));
                        model.setOther_information(dataObject.optString("other_information"));
                        questionDescriptionList.add(model);

                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }
}
