package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by indianic on 20/08/17.
 */

public class WsAuthorizeQuestionPayment {
    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private int success;
    private String message = "";
    private String paymentId = "";
    private int code;

    public int getCode() {
        return code;
    }
    public String getPaymentId() {
        return paymentId;
    }
    public WsAuthorizeQuestionPayment(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public String getMessage() {
        return message;
    }

    public void executeService(String amount, String payment_id,String question_category_id) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_QUESTION_AUTHORIZE_PAYMENT;
        final String response;
        response = new WSUtil().callServiceHttpPost(url, generateRequest(amount, payment_id,question_category_id));
        AppLog.showLogD("Result", "" + response);
        parseResponse(response);


    }


    private RequestBody generateRequest(String amount, String payment_id,String question_category_id) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_AMMOUNT, amount);
        formEncodingBuilder.add(WSConstants.WS_KEY_PAYMENT_ID, payment_id);
        formEncodingBuilder.add(WSConstants.WS_KEY_QUESTION_CATAGORY_ID, question_category_id);
        return formEncodingBuilder.build();
    }

    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            final JSONObject mainObject;
            try {
                mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");
                final JSONArray dataArray = mainObject.optJSONArray(WSConstants.DATA);
                if (dataArray != null && dataArray.length() > 0) {
                    final JSONObject jsonObject = dataArray.optJSONObject(0);
                    paymentId = jsonObject.optString("startup_payment_id");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }
}
