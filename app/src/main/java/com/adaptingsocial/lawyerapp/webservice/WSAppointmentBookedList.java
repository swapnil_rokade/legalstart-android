package com.adaptingsocial.lawyerapp.webservice;


import android.content.Context;
import android.os.Bundle;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.TimeSlotModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class WSAppointmentBookedList {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private int success;
    private String message = "";
    private ArrayList<TimeSlotModel> pastAppointmentArray;
    private ArrayList<TimeSlotModel> upAppointmentArray;

    public ArrayList<TimeSlotModel> getPastAppointmentArray() {
        return pastAppointmentArray;
    }

    public ArrayList<TimeSlotModel> getUpAppointmentArray() {
        return upAppointmentArray;
    }

    public WSAppointmentBookedList(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
        pastAppointmentArray = new ArrayList<>();
        upAppointmentArray = new ArrayList<>();
    }

    public String getMessage() {
        return message;
    }


    public void executeService(Bundle bundle) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_APPOINTMENT_BOOKED_LIST;
        final String response;
        response = new WSUtil().callServiceHttpPost(url, generateRequest(bundle));
        AppLog.showLogD("Result", "" + response);
        parseResponse(response);

    }

    private RequestBody generateRequest(final Bundle bundle) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, bundle.getString(WSConstants.WS_KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        return formEncodingBuilder.build();
    }

    public int getCode() {
        return success;
    }


    private void parseResponse(final String response) {
        if (response != null && response.trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                final JSONObject mainSettings = mainObject.optJSONObject(WSConstants.SETTINGS);
                success = Integer.parseInt(mainSettings.optString(WSConstants.SUCCESS));
                message = mainSettings.optString(WSConstants.MESSAGE);
                if (success == WSConstants.STATUS_SUCCESS) {
                    final JSONObject jsonObjectData = mainObject.optJSONObject(WSConstants.DATA);
                    final JSONArray appointmentUpcomingJsonArray = jsonObjectData.optJSONArray("appointment_upcoming_book_list");
                    final JSONArray appointmentPastCommingJsonArray = jsonObjectData.optJSONArray("appointment_pastcoming_book_list");
                    for (int i = 0; i < appointmentUpcomingJsonArray.length(); i++) {
                        final TimeSlotModel timeSlotModel = new TimeSlotModel();
                        final JSONObject appointmentUpObj = appointmentUpcomingJsonArray.optJSONObject(i);
                        timeSlotModel.setDate(appointmentUpObj.optString("availability_date"));
                        timeSlotModel.setId(appointmentUpObj.optString("appointment_id"));
                        timeSlotModel.setStartTime(appointmentUpObj.optString("availability_start_time"));
                        timeSlotModel.setEndTime(appointmentUpObj.optString("availability_end_time"));
                        timeSlotModel.setName(appointmentUpObj.optString("appointment_title"));
                        upAppointmentArray.add(timeSlotModel);
                    }
                    for (int k = 0; k < appointmentPastCommingJsonArray.length(); k++) {
                        final TimeSlotModel timeSlotModel = new TimeSlotModel();
                        final JSONObject appointmentPastObj = appointmentPastCommingJsonArray.optJSONObject(k);
                        timeSlotModel.setId(appointmentPastObj.optString("appointment_id"));
                        timeSlotModel.setDate(appointmentPastObj.optString("availability_date"));
                        timeSlotModel.setStartTime(appointmentPastObj.optString("availability_start_time"));
                        timeSlotModel.setEndTime(appointmentPastObj.optString("availability_end_time"));
                        timeSlotModel.setName(appointmentPastObj.optString("appointment_title"));
                        pastAppointmentArray.add(timeSlotModel);
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void generateTimeSlot(String start_time, String end_time) {
        final String[] start_Time = start_time.split(":");
        final String[] end_Time = end_time.split(":");
        long dif = TimeUnit.HOURS.toMillis(Long.parseLong(start_Time[0]));
        while (dif < TimeUnit.HOURS.toMillis(Long.parseLong(end_Time[0]))) {
            final TimeSlotModel timeSlotModel = new TimeSlotModel();
            Calendar cal1 = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
            cal1.setTimeInMillis(dif);
            final String startTime = dateFormat.format(cal1.getTime());
            dif += 900000;
            cal1.setTimeInMillis(dif);
            final String endTime = dateFormat.format(cal1.getTime());
            timeSlotModel.setStartTime(startTime);
            timeSlotModel.setEndTime(endTime);
            timeSlotModel.setBooked(false);
            timeSlotModel.setSelected(false);
            pastAppointmentArray.add(timeSlotModel);
        }
    }


}
