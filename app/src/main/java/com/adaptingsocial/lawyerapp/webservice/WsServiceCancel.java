package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;
import android.os.Bundle;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CaseListModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by indianic on 05/09/17.
 */

public class WsServiceCancel {
    private Context mContext;
    private CaseListModel caseListModel;
    private int code;
    private String message;
    private String paymentStatus;
    public WsServiceCancel(final Context mContext) {
        this.mContext = mContext;
        caseListModel = new CaseListModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService() {
        final String url = WSConstants.BASE_URL + WSConstants.WS_CANCEL_SERVICE;
        final String response;
        response = new WSUtil().callServiceHttpPost(url, generateRequest());
        parseResponse(response);
        AppLog.showLogD("Result", "" + response);


    }



    private RequestBody generateRequest() {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        formEncodingBuilder.add(WSConstants.WS_KEY_SUBSCRIPTION_ID, preferenceUtils
                .getString(preferenceUtils.KEY_CHARGE_ID));
        return formEncodingBuilder.build();
    }


    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            final JSONObject mainObject;
            try {
                mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");

                final JSONArray dataArray = mainObject.optJSONArray(WSConstants.DATA);
                if (dataArray != null && dataArray.length() > 0) {
                    paymentStatus = dataArray.getJSONObject(0).optString("u_payment_status");
                    if (paymentStatus.equalsIgnoreCase("succeeded")) {
                        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
                        preferenceUtils.putBoolean(preferenceUtils.KEY_APP_USER_TYPE, false);

                    } else {
                        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
                        preferenceUtils.putBoolean(preferenceUtils.KEY_APP_USER_TYPE, true);
                    }

                }

                } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

}
