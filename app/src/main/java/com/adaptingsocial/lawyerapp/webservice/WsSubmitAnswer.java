package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.QuestionModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by indianic on 06/07/17.
 */

public class WsSubmitAnswer {
    private Context mContext;
    private QuestionModel questionModel;
    private int code;
    private String message;
    private ArrayList<QuestionModel> questionModelArrayList;

    public WsSubmitAnswer(final Context mContext, ArrayList<QuestionModel> questionModelArrayList) {
        this.mContext = mContext;
        this.questionModelArrayList = questionModelArrayList;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService(String paymentId,String catId) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_SUBMIT_ANSWER;
//        final String response = new WSUtil().callServiceHttpPost(url, generateRequest(questionModelArrayList));
//        parseResponse(response);
//        try {
//            String encodedUrl = URLEncoder.encode(url
//                    +"?question_category_id="+catId+"&startup_payment_id="+paymentId +"&question_answer="+
//                    generateRequest(questionModelArrayList) , "UTF-8");

//            HttpUrl url1 = HttpUrl.parse(encodedUrl);
//            final String response = new WSUtil().callServiceHttpPost(url
//                    +"?question_category_id=1&startup_payment_id=1&question_answer="+
//                    generateRequest(questionModelArrayList));


//            final String response = new WSUtil().callServiceHttpPost(url
//                    +"?question_category_id="+catId+"&startup_payment_id="+paymentId
//                            +"&question_answer="+
//                    generateRequest(questionModelArrayList));




            final String response = new WSUtil().callServiceHttpPost(url,
                    generateRequest(questionModelArrayList,catId,paymentId));
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);

//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }



    }

    private RequestBody generateRequest(ArrayList<QuestionModel> questionModelArrayList,String
            catId,String paymentId) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder multipartBuilder = new FormEncodingBuilder();
        JSONArray jsonAnswerArray = new JSONArray();
        for (int j = 0; j < questionModelArrayList.size(); j++) {

            try {
                JSONObject jAnswerObject = new JSONObject();
                jAnswerObject.put("type", questionModelArrayList.get(j).getQtypecode());
                jAnswerObject.put("question_id", questionModelArrayList.get(j).getId());
                jAnswerObject.put("users_id", preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
                if (questionModelArrayList.get(j).getQtypecode().equalsIgnoreCase("AddNew")) {
                    JSONArray jAnswerAray = new JSONArray();
                    String[] answers = questionModelArrayList.get(j).getAnswers();
                    for (String answer : answers) {
                        JSONObject obj = new JSONObject();
                        String[] options = answer.split(",");
                        for (String option : options) {
                            String[] ops1 = option.split(":");
                            obj.put(ops1[0], ops1[1]);
                        }
                        jAnswerAray.put(obj);
                    }

                    jAnswerObject.put("answer", jAnswerAray);

                } else {

                    jAnswerObject.put("answer", questionModelArrayList.get(j).getAnswer());
                }
                jsonAnswerArray.put(jAnswerObject);
                if(questionModelArrayList.get(j).getFlow_finish().equalsIgnoreCase("yes")){
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        AppLog.showLogD("request", "" + jsonAnswerArray.toString());
        multipartBuilder.add("question_answer", jsonAnswerArray.toString());
        multipartBuilder.add("question_category_id", catId);
        multipartBuilder.add("startup_payment_id", paymentId);
        multipartBuilder.build().toString();
        AppLog.showLogD("Result is", "request is" + multipartBuilder.build().toString());
        return  multipartBuilder.build();
    }

    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            final JSONObject mainObject;
            try {
                mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }
}
