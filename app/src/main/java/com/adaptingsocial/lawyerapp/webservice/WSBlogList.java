package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.BlogModel;
import com.adaptingsocial.lawyerapp.model.CommentModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by indianic on 01/05/17.
 */

public class WSBlogList {
    private Context mContext;
    private BlogModel blogModel;
    private int code;
    private String message;
    private ArrayList<BlogModel> blogModelArrayList;
    private ArrayList<CommentModel> commentListModelArrayList;
    private String nextPage;
    private String count;

    public String getNextPage() {
        return nextPage;
    }



    public ArrayList<BlogModel> getBlogListModelArrayList() {
        return blogModelArrayList;
    }

    public int getCode() {

        return code;
    }

    public String getMessage() {

        return message;
    }

    public WSBlogList(final Context mContext) {
        this.mContext = mContext;
        blogModel = new BlogModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public void executeService(String pageindex) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_BLOG_LIST;
        final String response = new WSUtil().callServiceHttpPost(url, generateRequest(pageindex));
        parseResponse(response);
        AppLog.showLogD("Result", "" + response);


    }
    public String getCount() {
        return count;
    }

    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            final JSONObject mainObject;
            try {
                mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");
                nextPage = settingObject.optString("next_page");
                count = settingObject.optString(WSConstants.COUNT);

                final JSONArray dataArray = mainObject.optJSONArray("data");
                if (dataArray != null && dataArray.length() > 0) {
                    blogModelArrayList = new ArrayList<>();
                    final int length = dataArray.length();
                    for (int i = 0; i < length; i++) {
                        final JSONObject dataObject = dataArray.getJSONObject(i);
                        final BlogModel model = new BlogModel();
                        String id = dataObject.optString("blog_id");
                        String blogName = dataObject.optString("blog_name");
                        String blogDescription = dataObject.optString("blog_descripation");
                        String blogImage = dataObject.optString("blog_image");
                        String blogDate = dataObject.optString("blog_date");
                        model.setBlogId(id);
                        model.setBlogName(blogName);
                        model.setBlogDescription(blogDescription);
                        model.setBlogImage(blogImage);
                        model.setBolgDate(blogDate);
                        final JSONArray commentArray = dataObject.getJSONArray("comment_listing");

                        if (commentArray != null && commentArray.length() > 0) {
                            final int lengthComment = commentArray.length();
                            commentListModelArrayList = new ArrayList<>();
                            for (int j = 0; j < lengthComment; j++) {
                                final JSONObject commentObject = commentArray.getJSONObject(j);
                                final CommentModel commentModel = new CommentModel();
                                commentModel.setUserId(commentObject.optString("u_users_id"));
                                commentModel.setName(commentObject.optString("name"));
                                commentModel.setLikeCounts(commentObject.optString("count_like"));
                                commentModel.setProfileImage(commentObject.optString("profile_picture"));
                                commentModel.setComment(commentObject.optString("comment"));
                                commentModel.setDate(commentObject.optString("date"));
                                commentModel.setBlogLike(commentObject.optString("blog_like"));
                                commentModel.setBlog_comment_id(commentObject.optString("blog_comment_id"));
                                commentListModelArrayList.add(commentModel);

                            }
                            model.setCommentModelArrayList(commentListModelArrayList);
                        }


                        blogModelArrayList.add(model);
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    private RequestBody generateRequest(String pageindex) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_PAGE_INDEX, pageindex);
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));

        return formEncodingBuilder.build();
    }

}
