package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.MyDocumentModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WSMyDocument {
    private Context mContext;
    private int code;
    private String message;
    private ArrayList<MyDocumentModel> documentList;

    public WSMyDocument(final Context mContext) {
        this.mContext = mContext;

        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public ArrayList<MyDocumentModel> getDocumentList() {
        return documentList;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService() {
        final String url = WSConstants.BASE_URL + WSConstants.WS_USER_DOCUMENT_LISTING;
        final String response = new WSUtil().callServiceHttpPost(url, generateRequest());
        parseResponse(response);
        AppLog.showLogD("Result", "" + response);
    }

    private RequestBody generateRequest() {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));

        return formEncodingBuilder.build();
    }


    private void parseResponse(final String response) {
        documentList = new ArrayList<>();
        if (response != null && response.toString().trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");


                final JSONArray dataArray = mainObject.optJSONArray("data");
                if (dataArray != null && dataArray.length() > 0) {
                    final int length = dataArray.length();
                    for (int i = 0; i < length; i++) {
                        final JSONObject dataObject = dataArray.getJSONObject(i);

                        final MyDocumentModel myDocumentModel = new MyDocumentModel();

                        myDocumentModel.setDocumentId(dataObject.optString("user_documents_id"));
                        myDocumentModel.setDocumentName(dataObject.optString("documemt_name"));
                        myDocumentModel.setDocumentFile(dataObject.optString("doc_file"));


                        documentList.add(myDocumentModel);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
