package com.adaptingsocial.lawyerapp.webservice;


import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CompanyListModel;
import com.adaptingsocial.lawyerapp.model.DocumentListModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WSDocumentList {
    private Context mContext;
    private CompanyListModel companyListModel;
    private int code;
    private String message;
    private String nextPage;
    private String count;
    private ArrayList<DocumentListModel> documentListModelArrayList;

    public ArrayList<DocumentListModel> getDocumentListModelArrayList() {
        return documentListModelArrayList;
    }


    public int getCode() {
        return code;
    }

    public String getMessage() {

        return message;
    }

    public WSDocumentList(final Context mContext) {
        this.mContext = mContext;
        companyListModel = new CompanyListModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }


    public void executeService() {
        final String url = WSConstants.BASE_URL + WSConstants.WS_DOCUMENT_LIST;
        final String response = new WSUtil().callServiceHttpPost(url, generateRequest());
        parseResponse(response);
        AppLog.showLogD("Result", "" + response);

    }

    private RequestBody generateRequest() {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));

        return formEncodingBuilder.build();
    }


    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject(WSConstants.SETTINGS);
                code = settingObject.optInt(WSConstants.SUCCESS);
                message = settingObject.optString(WSConstants.MESSAGE);
                count = settingObject.optString(WSConstants.COUNT);
                nextPage = settingObject.optString(WSConstants.NEXT_PAGE);
                final JSONArray dataArray = mainObject.optJSONArray(WSConstants.DATA);
                if (dataArray != null && dataArray.length() > 0) {
                    documentListModelArrayList = new ArrayList<>();
                    final int length = dataArray.length();
                    for (int i = 0; i < length; i++) {
                        final JSONObject dataObject = dataArray.getJSONObject(i);
                        final DocumentListModel model = new DocumentListModel();
                        model.setDocumentId(dataObject.optString("document_id"));
                        model.setDocumentName(dataObject.optString("document_name"));
                        model.setDocumentUrl(dataObject.optString("dcoument"));
                        model.setDocumentType(dataObject.optString("document_type"));
                        model.setDocumentAmt(dataObject.optString("amount"));
                        model.setUserPaymentStaus(dataObject.optString("users_payment_status"));
                        model.setUsers_document_status(dataObject.getString("users_document_status"));
                        documentListModelArrayList.add(model);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


}
