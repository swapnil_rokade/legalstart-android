package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;
import android.text.TextUtils;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CaseListModel;
import com.adaptingsocial.lawyerapp.model.DocumentModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;


public class WsServiceUploadDocument {
    private Context mContext;
    private CaseListModel caseListModel;
    private int code;
    private String message;

    public WsServiceUploadDocument(final Context mContext) {
        this.mContext = mContext;
        caseListModel = new CaseListModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService(ArrayList<DocumentModel> documentModelArrayList) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_UPLOAD_SERVICE_DOCUMENT;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url, generateRequest(documentModelArrayList));
            parseResponse(response);
            AppLog.showLogD("Result", "" + response);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private RequestBody generateRequest(ArrayList<DocumentModel>
                                                documentModelArrayList) throws JSONException {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final MultipartBuilder multipartBuilder = new MultipartBuilder();
        multipartBuilder.type(MultipartBuilder.FORM);
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_OPTION, "No");

        for (int k = 0; k < documentModelArrayList.size(); k++) {
            final DocumentModel photoModal = documentModelArrayList.get(k);
            final File fileAvatarImage = new File(photoModal.getFilePath());

            if (!TextUtils.isEmpty(photoModal.getFilePath()) && fileAvatarImage.exists()) {
                multipartBuilder.addFormDataPart("document_name[" + k + "]", fileAvatarImage
                        .getName());
                multipartBuilder.addFormDataPart("document_type[" + k + "]", photoModal
                        .getImageExt());

                if (photoModal.getImageExt().equalsIgnoreCase(".png") || photoModal.getImageExt()
                        .equalsIgnoreCase(".jpg")
                        || photoModal.getImageExt().equalsIgnoreCase(".jpeg")) {
                    multipartBuilder.addFormDataPart("document[" + k + "]", fileAvatarImage
                                    .getName(),
                            RequestBody.create(MediaType.parse("image/png"), fileAvatarImage));
                }

                else {
                    multipartBuilder.addFormDataPart("document[" + k + "]", fileAvatarImage
                                    .getName(),
                            RequestBody.create(MediaType.parse("application/" + photoModal
                                            .getImageExt()),
                                    fileAvatarImage));



                }

            }


        }
        return multipartBuilder.build();

    }


    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            final JSONObject mainObject;
            try {
                mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }
}
