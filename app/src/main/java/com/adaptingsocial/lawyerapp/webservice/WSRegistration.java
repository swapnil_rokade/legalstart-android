package com.adaptingsocial.lawyerapp.webservice;


import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

public class WSRegistration {

    private final String TAG = this.getClass().getSimpleName();

    private Context mContext;

    private int success;
    private String message = "";


    public WSRegistration(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public String getMessage() {
        return message;
    }

    public void executeService(Bundle bundle) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_CREATE_ACCOUNT;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url, generateRequest(bundle));
//            parseResponse(response);
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private RequestBody generateRequest(final Bundle bundle) throws JSONException {
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_FIRSTNAME, bundle.getString(WSConstants.WS_KEY_FIRSTNAME));
        formEncodingBuilder.add(WSConstants.WS_KEY_LASTNAME, bundle.getString(WSConstants.WS_KEY_LASTNAME));
        formEncodingBuilder.add(WSConstants.WS_KEY_EMAIL, bundle.getString(WSConstants.WS_KEY_EMAIL));
        formEncodingBuilder.add(WSConstants.WS_KEY_PASSWORD, bundle.getString(WSConstants.WS_KEY_PASSWORD));
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        formEncodingBuilder.add(WSConstants.WS_KEY_TOKEN, FirebaseInstanceId.getInstance().getToken());
        formEncodingBuilder.add(WSConstants.WS_KEY_DEVICE_ID, Utils.getDeviceId(mContext));
        formEncodingBuilder.add(WSConstants.WS_KEY_DEVICE_TYPE, Constants.KEY_ANDROID);
        return formEncodingBuilder.build();
    }

    public int getCode() {
        return success;
    }

    private void parseResponse(final String response) {
        if (response != null && response.trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                final JSONObject mainSettings = mainObject.optJSONObject(WSConstants.SETTINGS);
                success = Integer.parseInt(mainSettings.optString(WSConstants.SUCCESS));
                message = mainSettings.optString(WSConstants.MESSAGE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
