package com.adaptingsocial.lawyerapp.webservice;


import android.content.Context;
import android.os.Bundle;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

public class WSChangePassword {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private int success;
    private String message = "";


    public WSChangePassword(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public String getMessage() {
        return message;
    }


    public void executeService(Bundle bundle) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_CHANGE_PASSWORD;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url, generateRequest(bundle));
//            parseResponse(response);
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private RequestBody generateRequest(final Bundle bundle) throws JSONException {

        final MultipartBuilder multipartBuilder = new MultipartBuilder();
        multipartBuilder.type(MultipartBuilder.FORM);
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_USER_ID, bundle.getString(WSConstants.WS_KEY_USER_ID));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_OLD_PASSWORD, bundle.getString(WSConstants.WS_KEY_OLD_PASSWORD));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_NEW_PASSWORD, bundle.getString(WSConstants.WS_KEY_NEW_PASSWORD));
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils.getString(preferenceUtils.KEY_ACESS_TOKEN));
        return multipartBuilder.build();
    }

    public int getCode() {
        return success;
    }


    private void parseResponse(final String response) {
        if (response != null && response.trim().length() > 0) {
            try {
                final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
                final JSONObject mainObject = new JSONObject(response);
                final JSONObject mainSettings = mainObject.optJSONObject(WSConstants.SETTINGS);
                success = Integer.parseInt(mainSettings.optString(WSConstants.SUCCESS));
                message = mainSettings.optString(WSConstants.MESSAGE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
