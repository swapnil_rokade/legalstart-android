package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.QuestionModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by indianic on 03/07/17.
 */

public class WsQuestionList {
    private Context mContext;
    private int code;
    private String message;
    private ArrayList<QuestionModel> questionModelArrayList;

    public WsQuestionList(final Context mContext) {
        this.mContext = mContext;

        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public ArrayList<QuestionModel> getQuestionModelArrayList() {
        return questionModelArrayList;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void executeService(String id) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_QUESTION_LIST;
        final String response = new WSUtil().callServiceHttpPost(url, generateRequest(id));
        parseResponse(response);
        AppLog.showLogD("Result", "" + response);


    }

    private RequestBody generateRequest(String id) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        formEncodingBuilder.add(WSConstants.WS_KEY_CATAGORY_ID, id);

        return formEncodingBuilder.build();
    }


    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject("settings");
                code = settingObject.optInt("success");
                message = settingObject.optString("message");


                final JSONArray dataArray = mainObject.optJSONArray("data");
                if (dataArray != null && dataArray.length() > 0) {
                    questionModelArrayList = new ArrayList<>();
                    final int length = dataArray.length();
                    for (int i = 0; i < length; i++) {
                        final JSONObject dataObject = dataArray.getJSONObject(i);
                        QuestionModel model = new QuestionModel();
                        model.setId(dataObject.optString("question_id"));
                        model.setQtype(dataObject.optString("question_type"));
                        model.setQuestion(dataObject.optString("question"));
                        //model.setQoption(dataObject.optString("question_options"));
                        model.setStatus(dataObject.optString("status"));
                        model.setQtypename(dataObject.optString("question_type_name"));
                        model.setQtypecode(dataObject.optString("question_type_code"));
                        model.setFlow_finish(dataObject.optString("flow_finish"));
                        model.setTextfield_open(dataObject.optString("textfield_open"));

                        final JSONArray optionsArray = dataObject.optJSONArray("option_listing");

                        String option = "";

                        if (optionsArray != null) {
                            for (int j = 0; j < optionsArray.length(); j++) {
                                final JSONObject optionObject = optionsArray.optJSONObject(j);

                                option += optionObject.optString("question_option") + ",";
                            }
                        }

                        option = option.length() > 0 ? option.substring(0, option.length() - 1) : option;

                        model.setQoption(option);

                        questionModelArrayList.add(model);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

