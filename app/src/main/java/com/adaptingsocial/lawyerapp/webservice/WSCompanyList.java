package com.adaptingsocial.lawyerapp.webservice;


import android.content.Context;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CompanyListModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WSCompanyList {
    private Context mContext;
    private CompanyListModel companyListModel;
    private int code;
    private String message;
    private String nextPage;
    private String count;
    private ArrayList<CompanyListModel> companyListModelArrayList;

    public ArrayList<CompanyListModel> getCompanyListModelArrayList() {
        return companyListModelArrayList;
    }


    public int getCode() {

        return code;
    }


    public String getNextPage() {
        return nextPage;
    }

    public String getMessage() {

        return message;
    }

    public String getCount() {
        return count;
    }

    public WSCompanyList(final Context mContext) {
        this.mContext = mContext;
        companyListModel = new CompanyListModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }


    public void executeService(final int pageIndex, final String companyId) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_COMPANY_LIST;
        final String response = new WSUtil().callServiceHttpPost(url, generateRequest(pageIndex, companyId));
        parseResponse(response);
        AppLog.showLogD("Result", "" + response);

    }

    private RequestBody generateRequest(final int pageIndex,final String companyId) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        AppLog.showLogD("userid", "" +preferenceUtils.getString(preferenceUtils.KEY_USER_ID) );
        formEncodingBuilder.add(WSConstants.WS_KEY_PAGE_INDEX, String.valueOf(pageIndex));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));


        if(!companyId.equals("0")) {
            formEncodingBuilder.add(WSConstants.WS_KEY_COMPANY_CATEGORY_ID, String.valueOf(companyId));
        }
        return formEncodingBuilder.build();
    }


    private void parseResponse(final String response) {
        if (response != null && response.toString().trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                JSONObject settingObject = mainObject.getJSONObject(WSConstants.SETTINGS);
                code = settingObject.optInt(WSConstants.SUCCESS);
                message = settingObject.optString(WSConstants.MESSAGE);
                count = settingObject.optString(WSConstants.COUNT);
                nextPage = settingObject.optString(WSConstants.NEXT_PAGE);
                final JSONArray dataArray = mainObject.optJSONArray(WSConstants.DATA);
                if (dataArray != null && dataArray.length() > 0) {
                    companyListModelArrayList = new ArrayList<>();
                    final int length = dataArray.length();
                    for (int i = 0; i < length; i++) {
                        final JSONObject dataObject = dataArray.getJSONObject(i);
                        final CompanyListModel model = new CompanyListModel();
                        model.setCompanyId(dataObject.optString("company_id"));
                        model.setCompanyTitle(dataObject.optString("company_name"));
                        model.setCompanyAddress(dataObject.optString("city_state"));
                        model.setCompanyContact(dataObject.optString("mobile_number"));
                        model.setCompanyEmail(dataObject.optString("company_email"));
                        model.setLatitude(dataObject.optString("latitude"));
                        model.setLongitude(dataObject.optString("longitude"));
                        model.setCompanyImage(dataObject.optString("company_image"));
                        model.setCompanyDescreption(dataObject.optString("company_description"));
                        model.setCompanyBackgroundImage(dataObject.optString("company_back_ground_image"));
                        model.setCompanyField(dataObject.optString("company_category_name"));
                        companyListModelArrayList.add(model);

                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


}
