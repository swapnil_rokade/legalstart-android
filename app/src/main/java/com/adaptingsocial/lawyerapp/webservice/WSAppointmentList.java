package com.adaptingsocial.lawyerapp.webservice;


import android.content.Context;
import android.os.Bundle;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.TimeSlotModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class WSAppointmentList {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private int success;
    private String message = "";
    private ArrayList<TimeSlotModel> timesSlotArray;
    private String serverTime = "";
    private String currentDate;

    public ArrayList<TimeSlotModel> getTimesSlotArray() {
        return timesSlotArray;
    }

    public WSAppointmentList(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
        timesSlotArray = new ArrayList<>();
    }

    public String getMessage() {
        return message;
    }


    public void executeService(Bundle bundle) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_APPOINTMENT_LIST;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url, generateRequest(bundle));
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private RequestBody generateRequest(final Bundle bundle) throws JSONException {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        currentDate = bundle.getString(WSConstants.WS_KEY_DATE);
        formEncodingBuilder.add(WSConstants.WS_KEY_DATE, bundle.getString(WSConstants.WS_KEY_DATE));
        formEncodingBuilder.add(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        return formEncodingBuilder.build();
    }

    public int getCode() {
        return success;
    }


    private void parseResponse(final String response) {
        if (response != null && response.trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                final JSONObject mainSettings = mainObject.optJSONObject(WSConstants.SETTINGS);
                success = Integer.parseInt(mainSettings.optString(WSConstants.SUCCESS));
                message = mainSettings.optString(WSConstants.MESSAGE);
                if (success == WSConstants.STATUS_SUCCESS) {
                    final JSONObject jsonObjectData = mainObject.optJSONObject(WSConstants.DATA);
                    final JSONArray appointmentHoursJsonArray = jsonObjectData.optJSONArray("appointment_hours");
                    final JSONArray appointmentBookedHoursJsonArray = jsonObjectData.optJSONArray("appointment_book_listing");
                    for (int j = 0; j < appointmentHoursJsonArray.length(); j++) {
                        final JSONObject appointmentObj = appointmentHoursJsonArray.optJSONObject
                                (j);
                        if (appointmentObj.has("current_server_time")) {
                            serverTime = appointmentObj.optString("current_server_time");
                        }
                        generateTimeSlot(appointmentObj.optString("start_time"), appointmentObj.optString("end_time"));
                    }
                    for (int k = 0; k < appointmentBookedHoursJsonArray.length(); k++) {
                        final JSONObject appointmentBookedObj = appointmentBookedHoursJsonArray.optJSONObject(k);
                        for (int j = 0; j < timesSlotArray.size(); j++) {
                            if (timesSlotArray.get(j).getStartTime().equalsIgnoreCase(appointmentBookedObj.optString("availability_start_time"))) {
                                timesSlotArray.get(j).setBooked(true);
                            }
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void generateTimeSlot(String start_time, String end_time) {

        try {
            Date dtStartTime = new SimpleDateFormat("HH:mm:ss").parse(start_time);
            Calendar calStartTime = Calendar.getInstance();
            calStartTime.setTime(dtStartTime);

            Date dtEndTime = new SimpleDateFormat("HH:mm:ss").parse(end_time);
            Calendar calEndTime = Calendar.getInstance();
            calEndTime.setTime(dtEndTime);
//            calendar2.add(Calendar.DATE, 1);

            final String[] server_Time = serverTime.split(" ");
            Date dtServerTime = new SimpleDateFormat("HH:mm:ss").parse(server_Time[1]);
            Calendar calServer = Calendar.getInstance();
            calServer.setTime(dtServerTime);
//            calendar3.add(Calendar.DATE, 1);

            Date dtServerDateTime = calServer.getTime();
            if (currentDate.equalsIgnoreCase(server_Time[0])) {
                if (dtServerDateTime.after(calStartTime.getTime()) && dtServerDateTime.before(calEndTime.getTime())) {
                    start_time = server_Time[1];
                } else {
                    if (dtServerDateTime.after(calEndTime.getTime())) {
                        return;
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        final String[] start_Time = start_time.split(":");
        final String[] end_Time = end_time.split(":");
//        long strartTimeMilli = TimeUnit.HOURS.toMillis(Long.parseLong(start_Time[0])) + TimeUnit.MINUTES.toMillis(Long.parseLong(getMinSlot(start_Time[1])));
        long strartTimeMilli = 0;
        if (start_Time[1].equalsIgnoreCase("00")) {
            strartTimeMilli = TimeUnit.HOURS.toMillis(Long.parseLong(start_Time[0])) + TimeUnit.MINUTES.toMillis(Long.parseLong("00"));
        } else {
            if (getMinSlot(start_Time[1]).equalsIgnoreCase("00")) {
                strartTimeMilli = TimeUnit.HOURS.toMillis(Long.parseLong(start_Time[0] + 1)) + TimeUnit.MINUTES.toMillis(Long.parseLong(getMinSlot(start_Time[1])));
            } else {
                strartTimeMilli = TimeUnit.HOURS.toMillis(Long.parseLong(start_Time[0])) + TimeUnit.MINUTES.toMillis(Long.parseLong(getMinSlot(start_Time[1])));
            }
        }
        long endTimeMilli = TimeUnit.HOURS.toMillis(Long.parseLong(end_Time[0])) + TimeUnit.MINUTES.toMillis(Long.parseLong(getMinSlot(end_Time[1])));

        while (strartTimeMilli+900000 < endTimeMilli) {
            final TimeSlotModel timeSlotModel = new TimeSlotModel();
            Calendar cal1 = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            cal1.setTimeInMillis(strartTimeMilli);
            final String startTime = dateFormat.format(cal1.getTime());
            strartTimeMilli += 900000;
            cal1.setTimeInMillis(strartTimeMilli);
            final String endTime = dateFormat.format(cal1.getTime());
            timeSlotModel.setStartTime(startTime);
            timeSlotModel.setEndTime(endTime);
            timeSlotModel.setBooked(false);
            timeSlotModel.setSelected(false);
            timesSlotArray.add(timeSlotModel);
        }
    }

    private String getMinSlot(final String m) {
        int min = Integer.parseInt(m);
        if (min <= 15) {
            return "15";
        } else if (min <= 30) {
            return "30";
        } else if (min <= 45) {
            return "45";
        } else if (min > 45) {
            return "00";
        }
        return "00";
    }

}
