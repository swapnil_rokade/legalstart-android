package com.adaptingsocial.lawyerapp.webservice;


import android.content.Context;
import android.os.Bundle;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.UserModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WSViewProfile {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private int success;
    private String message = "";
    final UserModel userModel;


    public WSViewProfile(final Context mContext) {
        this.mContext = mContext;
        userModel = new UserModel();
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public String getMessage() {
        return message;
    }


    public void executeService(Bundle bundle) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_VIEW_PROFILE;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url, generateRequest(bundle));
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public UserModel getUserModel() {
        return userModel;
    }

    private RequestBody generateRequest(final Bundle bundle) throws JSONException {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        final MultipartBuilder multipartBuilder = new MultipartBuilder();
        multipartBuilder.type(MultipartBuilder.FORM);
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_USER_ID, bundle.getString(WSConstants.WS_KEY_USER_ID));
        multipartBuilder.addFormDataPart(WSConstants.WS_KEY_ACESS_TOKEN, preferenceUtils
                .getString(preferenceUtils.KEY_ACESS_TOKEN));


        return multipartBuilder.build();
    }

    public int getCode() {
        return success;
    }


    private void parseResponse(final String response) {
        if (response != null && response.trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                final JSONObject mainSettings = mainObject.optJSONObject(WSConstants.SETTINGS);
                success = Integer.parseInt(mainSettings.optString(WSConstants.SUCCESS));
                message = mainSettings.optString(WSConstants.MESSAGE);
                if (success == WSConstants.STATUS_SUCCESS) {
                    final JSONArray jsonArrayData = mainObject.optJSONArray(WSConstants.DATA);
                    final JSONObject jsonObjectData = jsonArrayData.getJSONObject(0);
                    userModel.setFirstName(jsonObjectData.getString("first_name"));
                    userModel.setLastName(jsonObjectData.getString("last_name"));
                    userModel.setEmail(jsonObjectData.getString("email"));
                    userModel.setMobileNo(jsonObjectData.getString("moblie_number"));
                    userModel.setDateOfBirth(jsonObjectData.getString("date_of_birth"));
                    userModel.setProfileImage(jsonObjectData.getString("profile_image"));
                    userModel.setBgImage(jsonObjectData.getString("back_ground_image"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
