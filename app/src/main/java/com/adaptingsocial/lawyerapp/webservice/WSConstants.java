package com.adaptingsocial.lawyerapp.webservice;

import org.json.JSONException;
import org.json.JSONObject;

public class WSConstants {
    public static final long CONNECTION_TIMEOUT = 60;  // In Second format
//   public static final String BASE_URL = "http://p.configure.it/lawyerapp32412/WS/";

//    public static final String BASE_URL = "http://php54.indianic.com/lawyerapp/WS/";

    public static final String BASE_URL ="http://34.195.154.175/WS/";



    public static final String WS_CREATE_ACCOUNT = "users_registration";
    public static final String WS_FORGOT_PASSWORD = "forgot_password";
    public static final String WS_CHANGE_PASSWORD = "change_password";
    public static final String WS_USER_LOGIN = "users_login";
    public static final String WS_APPOINTMENT_LIST = "appointment_book_listing";
    public static final String WS_APPOINTMENT_BOOKED_LIST = "appointment_listing";
    public static final String WS_BOOK_APPOINTMENT = "book_appointment";
    public static final String WS_VIEW_PROFILE = "view_users_profile";
    public static final String WS_ADD_CASE = "add_case";
    public static final String WS_UPDATE_CASE = "update_case";
    public static final String WS_DELETED_DOC_ARRAY_ID = "delete_attchement_id";
    public static final String WS_UPDATE_PROFILE = "update_users_profile";
    public static final String WS_CASE_LIST = "case_listing";
    public static final String WS_COMPANY_LIST = "company_listing";
    public static final String WS_DOCUMENT_LIST = "document_listing";
    public static final String WS_COMPANY_CATEGORY_LIST = "company_category_listing";
    public static final String WS_LOG_OUT = "logout";
    public static final String WS_BLOG_LIST = "blog_listing";
    public static final String WS_ADD_COMMENT = "add_comment";
    public static final String WS_LIKE_COMMENT = "blog_comment_like_or_dislike";
    public static final String WS_DELETE_CASE = "delete_case";
    public static final String WS_DELETE_APPOINTMENT = "delete_appointment";
    public static final String WS_EMAIL_NOTIFY = "email_notify_by_admin";
    public static final String WS_CASE_DETAIL = "case_details";
    public static final String WS_UPDATE_STRIPE_PAYMENT = "update_stripe_payment_status";
    public static final String WS_EMAIL_NOTIFY_LAWER = "eamil_notify_lawyer";
    public static final String WS_SUBSCRIPTION_LIST = "subscription_listing";
    public static final String WS_SUBSCRIPTION_STATUS = "user_subscription_status";
    public static final String WS_QUESTION_CATAGORY_LIST = "question_category_list";
    public static final String WS_QUESTION_CAT = "start_my_business_state_listing";
    public static final String WS_DOCUMENT_QUESTION_LIST = "document_question_listing";
    public static final String WS_SUBMIT_ANSWER = "question_answer_by_users";
    public static final String WS_DOCUMENT_SUBMIT_ANSWER = "document_question_answer_by_users";
    public static final String WS_USER_DOCUMENT_LISTING = "users_document_listing";
    public static final String WS_STATIC_PAGE = "get_cms_pages_listing";
    public static final String WS_FREE_DOCUMENT = "free_document";
    public static final String WS_QUESTION_LIST = "question_list";
    public static final String WS_UPLOAD_SERVICE_DOCUMENT = "service_document_uploaded";
    public static final String WS_CANCEL_SERVICE = "cancel_subscription_ios";

    public static final String WS_SUBSCRIPTION_ARB_PAYMENT = "subscription_authorise_net_payment";



//    http://php54.indianic.com/lawyerapp/WS/service_required_authorise_net_payment?users_id=1&transaction_id=12&amount=123

    public static final String WS_AUTHORIZE_PAYMENT = "service_required_authorise_net_payment";

    public static final String
            WS_QUESTION_AUTHORIZE_PAYMENT = "questionnaire_authorisece_net_payment";

    public static final String
            WS_SUBSCRIPTION_AUTHORIZE_PAYMENT = "subscription_authorise_net_payment";

    public static final String
            WS_DOCUMENT_AUTHORIZE_PAYMENT = "document_authorise_net_payment";


//    http://php54.indianic.com/lawyerapp/WS/document_authorise_net_payment
// ?users_id=1&document_id=1&amount=12&payment_id=123


//    http://php54.indianic.com/lawyerapp/WS/question_answer_by_users?question_answer= [{"type":"SingleChoice","question_id":"1","answer":"yes","users_id":"1"},{"type":"AddNew","users_id":"1","question_id":"1","answer":[{"answer":"MemberName:shuhbh,Manager:yes,Adderss"},
    // {"answer":"MemberName:mayur,Manager:yes,Adderss"}]}]&question_category_id=2&startup_payment_id=1

//    http://php54.indianic.com/lawyerapp/WS/eamil_notify_lawyer?name=shubh&message=shubh&email=shubh

//    http://p.configure.it/lawyerapp32412/WS/forgot_password?&email=shubh.jain%40indianic.com
//    http://p.configure.it/lawyerapp32412/WS/add_case?
    // &case_title=test&court_name=test%20one&case_number=797344164&
    // on_behalf_of=test&users_id=1&document=C%3Afakepath3_0_HFRA_Profile_Public_1-201612122117586850.jpg

    //ALL WS KEYS
//add case
    public static final String WS_KEY_CASE_TITLE = "case_title";
    public static final String WS_KEY_COURT_NAME = "court_name";
    public static final String WS_KEY_CASE_NO = "case_number";
    public static final String WS_KEY_CASE_ID = "case_id";
    public static final String WS_KEY_APPOINTMENT_ID = "appointment_id";
    public static final String WS_KEY_ON_BEHALF_OF = "on_behalf_of";
    public static final String WS_KEY_USER_ID = "users_id";

    public static final String WS_KEY_OPTION = "option";
    public static final String WS_KEY_CATAGORY_ID = "category_id";
    public static final String WS_KEY_DATE = "date";
    public static final String WS_KEY_AVAILABLE_START_TIME = "available_start_time";
    public static final String WS_KEY_AVAILABLE_END_TIME = "available_end_time";
    public static final String WS_KEY_AVAILABLE_DATE = "available_date";
    public static final String WS_KEY_DOCUMENT = "document";
    //    public static final String WS_KEY_EMAIL = "email";
    public static final String WS_KEY_PASSWORD = "password";
    public static final String WS_KEY_TOKEN = "device_token";
    public static final String WS_PAYMENT_STATUS = "payment_status";


    public static final String WS_KEY_DOCUMENT_TEXT_TYPE = "document_text_type";
    public static final String WS_KEY_DOCUMENT_TYPE = "document_type";
    public static final String WS_KEY_AGREEMENT = "agreement";
    public static final String WS_KEY_CONCERN = "concerns";

    public static final String WS_DOCUMENT_ID = "document_id";
    public static final String WS_TRANSACTION_ID = "trancation_id";

    public static final String WS_KEY_ACESS_TOKEN = "access_token";
    public static final String WS_KEY_NEW_PASSWORD = "new_password";
    public static final String WS_KEY_OLD_PASSWORD = "old_password";
    public static final String WS_KEY_FIRSTNAME = "first_name";
    public static final String WS_KEY_LASTNAME = "last_name";
    public static final String WS_KEY_PROFILE_IMAGE = "profile_image";
    public static final String WS_KEY_PROFILE_PICTURE = "profile_picture";
    public static final String WS_KEY_BACKGROUND_IMAGE = "users_background_image";
    public static final String WS_KEY_MOBILE_NO = "mobile_number";
    public static final String WS_KEY_DATE_OF_BIRTH = "date_of_birth";
    public static final String WS_KEY_DEVICE_TOKEN = "device_token";
    public static final String WS_KEY_DEVICE_ID = "device_id";
    public static final String WS_KEY_DEVICE_TYPE = "device_type";
    public static final String WS_KEY_PAGE_INDEX = "page_index";
    public static final String WS_KEY_COMPANY_CATEGORY_ID = "company_category_id";
    public static final String WS_KEY_BLOG_ID = "blog_id";
    public static final String WS_KEY_BLOG_COMMENT_ID = "blog_comment_id";
    public static final String WS_KEY_COMMENT = "comment";

    public static final String WS_KEY_SUBSCRIPTION_ID = "subcription_id";


    public static final String WS_KEY_QUICK_BLOX_EMAIL = "blog_email";
    public static final String WS_KEY_QUICK_BLOX_PASSWORD = "blog_passwsord";
    public static final String WS_KEY_QUICK_BLOX_ID = "blog_id";

    public static final String WS_KEY_NAME = "name";
    public static final String WS_KEY_APPOINTMENT_TITLE = "appointment_title";
    public static final String WS_KEY_PURPOSE = "purpose";


    public static final String WS_STRIPE_CHARGE_SUBSCRIPTION_PAYMENT = "stripe_charge";

    public static final String WS_STRIPE_CHARGE_DOCUMENT_PAYMENT = "stripe_charge_document";

    public static final String WS_STRIPE_SERVICE_REQUIRED_PAYMENT =
            "service_required_stripe_payment";



    public static final String WS_PAYPAL_SUBSCRIPTION_PAYMENT = "subscription_paypal_payment";


    public static final String WS_STRIPE_CHARGE_QUESTION_PAYMENT = "questionnaire_stripe_payment";


    public static final String WS_KEY_FROM = "name";
    public static final String WS_KEY_MESSAGE = "message";
    public static final String WS_KEY_EMAIL = "email";
    public static final String WS_KEY_PAGE_CODE = "page_code";

    public static final String WS_KEY_AMMOUNT = "amount";
    public static final String WS_KEY_TRANSACTION_ID = "transaction_id";
    public static final String WS_KEY_PAYMENT_ID="payment_id";
    public static final String WS_KEY_QUESTION_CATAGORY_ID="question_category_id";

    public static final String WS_KEY_DOCUEMNT_ID="document_id";


    public static final String WS_KEY_CARD_NUMBER = "card_number";
    public static  final String WS_ORDER_UNIQUE_NUMBER="order_unique_number";
    public static  final String WS_FIRST_NAME="first_name";
    public static final String WS_LAST_NAME="last_name";
    public static final String WS_EXP_DATE="exp_date";

;    /**
     * Web service request params
     */



    public static final String SETTINGS = "settings";
    public static final String SUCCESS = "success";
    public static final String MESSAGE = "message";
    public static final String
            COUNT = "count";
    public static final String NEXT_PAGE = "next_page";
    public static final String FIELDS = "fields";
    public static final String DATA = "data";
    public static final int STATUS_SUCCESS = 1;
    public static final int STATUS_FAIL = 0;
    public static final int STATUS_LOGOUT = 100;
    public static final int STATUS_NO_DATA_FAIL = 99;

    public static final String PASSWORD = "password";

    public static final int STATUS_SUSPENDED = 102;
    public static final int STATUS_NETWORK_ERROR = 201;

    public static String getNetworkError() {

        try {
            final JSONObject object = new JSONObject();
            object.put("status", STATUS_FAIL);
            object.put("code", STATUS_NETWORK_ERROR);
            object.put("message", "Network error, Please try after some time.");
            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
