package com.adaptingsocial.lawyerapp.webservice;

import android.content.Context;
import android.os.Bundle;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.DocumentListModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by indianic on 12/06/17.
 */

public class WsStripeUpdateStatus {
    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private int success;
    private String message = "";

    public WsStripeUpdateStatus(final Context mContext) {
        this.mContext = mContext;
        message = mContext.getString(R.string.something_went_wrong_msg);
    }

    public String getMessage() {
        return message;
    }

    public void executeService(DocumentListModel documentListModel,String chargeId) {
        final String url = WSConstants.BASE_URL + WSConstants.WS_UPDATE_STRIPE_PAYMENT;
        final String response;
        try {
            response = new WSUtil().callServiceHttpPost(url, generateRequest(documentListModel,chargeId));
            AppLog.showLogD("Result", "" + response);
            parseResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private RequestBody generateRequest(DocumentListModel documentListModel,String chargeId) throws
            JSONException {
        final FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
        final PreferenceUtils preferenceUtils = new PreferenceUtils(mContext);
        formEncodingBuilder.add(WSConstants.WS_KEY_TOKEN, preferenceUtils.getString(preferenceUtils.KEY_TOKEN));
        formEncodingBuilder.add(WSConstants.WS_KEY_USER_ID, preferenceUtils.getString(preferenceUtils.KEY_USER_ID));
        formEncodingBuilder.add(WSConstants.WS_PAYMENT_STATUS, "Paid");
        formEncodingBuilder.add(WSConstants.WS_DOCUMENT_ID, documentListModel.getDocumentId());
        formEncodingBuilder.add(WSConstants.WS_TRANSACTION_ID, chargeId);
        return formEncodingBuilder.build();
    }

    public int getCode() {
        return success;
    }

    private void parseResponse(final String response) {
        if (response != null && response.trim().length() > 0) {
            try {
                final JSONObject mainObject = new JSONObject(response);
                final JSONObject mainSettings = mainObject.optJSONObject(WSConstants.SETTINGS);
                success = Integer.parseInt(mainSettings.optString(WSConstants.SUCCESS));
                message = mainSettings.optString(WSConstants.MESSAGE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
