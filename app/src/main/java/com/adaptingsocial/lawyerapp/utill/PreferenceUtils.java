package com.adaptingsocial.lawyerapp.utill;

import android.content.Context;
import android.content.SharedPreferences;

import com.adaptingsocial.lawyerapp.R;


public class PreferenceUtils {
    public final String KEY_IS_LOGIN = "KEY_IS_LOGIN";
    public final String KEY_IS_STUDENT_LOGIN = "KEY_IS_STUDENT_LOGIN";
    public final String KEY_IS_TUTOR_LOGIN = "KEY_IS_TUTOR_LOGIN";
    public final String KEY_USER_ID = "KEY_USER_ID";
    public final String KEY_FIRST_NAME = "KEY_FIRST_NAME";
    public final String KEY_LAST_NAME = "KEY_LAST_NAME";
    public final String KEY_USER_NAME = "KEY_USER_NAME";
    public final String KEY_PROFILE_IMAGE = "KEY_PROFILE_IMAGE";
    public final String KEY_USER_TYPE = "KEY_USER_TYPE";
    public final String KEY_USER_EMAIL = "KEY_USER_EMAIL";
    public final String KEY_FCM_TOKEN = "KEY_FCM_TOKEN";
    public final String KEY_QUICK_BLOX_EMAIL = "KEY_QUICK_BLOX_EMAIL";
    public final String KEY_QUICK_BLOX_PASSWORD = "KEY_QUICK_BLOX_PASSWORD";
    public final String KEY_QUICK_BLOX_ID = "KEY_QUICK_BLOX_ID";

    public final String KEY_EMAIL = "KEY_EMAIL";
    public final String KEY_MOBILE = "KEY_MOBILE";
    public final String KEY_USER_LOGIN="";
    public final String KEY_TOKEN = "KEY_TOKEN";
    public final String KEY_ACESS_TOKEN = "acesstoken";
    public final String KEY_APP_USER_TYPE = "KEY_APP_USER_TYPE";

    public final String KEY_CHARGE_ID = "chargeId";
    public final String KEY_DIALOG_ID = "KEY_DIALOG_ID";
    private SharedPreferences sharedPreferences;

    public PreferenceUtils(Context mContext) {
        sharedPreferences = (SharedPreferences) mContext.getSharedPreferences(mContext.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    /**
     * Get String value from shared preference
     *
     * @param key
     * @return
     */
    public String getString(final String key) {
        return sharedPreferences.getString(key, "");
    }

    /**
     * Put String value to shared preference
     *
     * @param key
     * @return
     */
    public void putString(final String key, final String value) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Get Integer value from shared preference
     *
     * @param key
     * @return
     */
    public int getInt(final String key) {
        return sharedPreferences.getInt(key, -1);
    }

    /**
     * Put Integer value to shared preference
     *
     * @param key
     * @return
     */
    public void putInt(final String key, final int value) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * Get Boolean value from shared preference
     *
     * @param key
     * @return
     */
    public boolean getBoolean(final String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    /**
     * Put Boolean value to shared preference
     *
     * @param key
     * @return
     */
    public void putBoolean(final String key, final boolean value) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }
    public boolean getLoadStoryFirstTime() {
        return sharedPreferences.getBoolean("firsttimefinger", true);
    }

    /**
     * @param isLoaded true/false
     */
    public void setLoadStoryFirstTime(final boolean isLoaded) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("firsttimefinger", isLoaded);
        editor.apply();
    }
}
