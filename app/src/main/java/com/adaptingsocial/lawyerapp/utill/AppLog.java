package com.adaptingsocial.lawyerapp.utill;

import android.util.Log;

/**
 * *******************************************************************************
 * <p/>
 * Class Name: AppLog
 * Created By: lbamarnani
 * Created Date:20-APRIL-17
 * Modified By:
 * Modified Date:
 * Purpose: this class is used to show logs.
 * <p/>
 * **********************************************************************************
 */
public class AppLog {

    private static boolean sFlagDebug = true;
    private static boolean sFlagInfo = true;
    private static boolean sFlagErr = true;

    /**
     * THis will print in Blue
     *
     * @param tag
     */
    public static void showLogD(String tag, String message) {
        if (sFlagDebug) {
            Log.d(tag, message);
        }
    }

    /**
     * This will print in Green
     *
     * @param tag
     * @param message
     */
    public static void showLogI(String tag, String message) {
        if (sFlagInfo) {
            Log.i(tag, message);
        }
    }

    /**
     * This will print in Error
     *
     * @param tag
     * @param message
     */
    public static void showLogE(String tag, String message) {
        if (sFlagErr) {
            Log.e(tag, message);
        }
    }
}
