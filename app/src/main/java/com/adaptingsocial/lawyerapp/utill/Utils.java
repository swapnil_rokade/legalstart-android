package com.adaptingsocial.lawyerapp.utill;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.encryption.AES256JNCryptor;
import com.adaptingsocial.lawyerapp.encryption.CryptorException;
import com.adaptingsocial.lawyerapp.encryption.JNCryptor;
import com.adaptingsocial.lawyerapp.view.LoginActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {

    private static final  String phaseKeyForEncryption = "vjkd#g&m36@k8?j3m2dvm0ke5f";
    private final static String[] charsToReplace = {"!", "\\*", "'", "\\(", "\\)", ";", ":", "@", "&", "=", "\\+", "$", ",", "/", "\\?", "%", "#", "\\[", "]"};
    private final static String[] charsToBeReplace = {"e_O21", "e_O2A", "e_O27", "e_O28", "e_O29", "e_O3B", "e_O3A", "e_O40", "e_O26", "e_O3D", "e_O2B", "e_O24", "e_O2C", "e_O2F", "e_O3F", "e_O25", "e_O23", "e_O5B", "e_O5D"};

    private static final String TAG = Utils.class.getSimpleName();

    /**
     * Method is used for checking network availability.
     *
     * @param context
     * @return isNetAvailable: boolean true for Internet availability, false otherwise
     */

    public static boolean isNetworkAvailable(Context context) {

        boolean isNetAvailable = false;
        if (context != null) {
            final ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (mConnectivityManager != null) {

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    final Network[] allNetworks = mConnectivityManager.getAllNetworks();

                    for (Network network : allNetworks) {
                        final NetworkInfo networkInfo = mConnectivityManager.getNetworkInfo(network);
                        if (networkInfo != null && networkInfo.isConnected()) {
                            isNetAvailable = true;
                            break;
                        }
                    }

                } else {
                    boolean wifiNetworkConnected = false;
                    boolean mobileNetworkConnected = false;

                    final NetworkInfo mobileInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                    final NetworkInfo wifiInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                    if (mobileInfo != null) {
                        mobileNetworkConnected = mobileInfo.isConnected();
                    }
                    if (wifiInfo != null) {
                        wifiNetworkConnected = wifiInfo.isConnected();
                    }
                    isNetAvailable = (mobileNetworkConnected || wifiNetworkConnected);
                }
            }
        }
        return isNetAvailable;
    }

    /**
     * Alert dialog to show common messages.
     *
     * @param title
     * @param message
     * @param context
     */
    public static void displayDialog(final Context context, final String title, final String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }

    /**
     * Alert dialog to show common messages.
     *
     * @param title
     * @param message
     * @param activity
     */
    public static void displayDialogFinishActivity(final Activity activity, final String title, final String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(activity.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                activity.finish();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(activity, R.color.colorDarkGray));
    }


    /**
     * Method used for creating new Photo path
     *
     * @param mActivity
     * @return
     */
    public static File createPhotoFile(Activity mActivity) {

        final File file = new File(mActivity.getApplicationContext().getExternalCacheDir() + File.separator + System.currentTimeMillis() + ".jpg");
        if (file.exists()) {
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }


    /**
     * Alert dialog to be displayed conditionally and getting the Current Fragment Popped back on it's "Ok" Button Click
     *
     * @param title   Title of the Dialog : Application's Name
     * @param message Message to be shown in the Dialog displayed
     * @param context Context of the Application, where the Dialog needs to be displayed
     */
    public static void displayDialogWithPopBackStack(final Context context, final String title, final String message) {

//        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//        alertDialog.setTitle(title);
//        alertDialog.setCancelable(false);
//
//        alertDialog.setMessage(message);
//
//        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int which) {
//
//                dialog.dismiss();
//                ((Activity) context).getFragmentManager().popBackStack();
//            }
//        });
//        if (!((Activity) context).isFinishing()) {
//
//            alertDialog.show();
//        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);

        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ((Activity) context).getFragmentManager().popBackStack();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }


    /**
     * Hide the soft keyboard from screen for edit text only
     *
     * @param mContext
     * @param view
     */
    public static void hideSoftKeyBoard(Context mContext, View view) {

        try {

            final InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /**
     * Check if email is in valid format
     *
     * @param inputEmail
     * @return true, if email is in proper format
     */
    public final static boolean isValidEmail(CharSequence inputEmail) {
        if (inputEmail == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(inputEmail).matches();
        }
    }

    /**
     * Validate password with regular expression
     *
     * @param password
     * @return true valid password and matches it's pattern, false invalid password
     */
    public static boolean isValidPassword(String password) {
        final String PASSWORD_PATTERN = "^(?=.*[A-Z]).{6,10}$";
//                "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$";
        final Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        final Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    /**
     * Called to check permission(In Android M and above versions only)
     *
     * @param permission, which we need to pass
     * @return true, if permission is granted else false
     */
    public static boolean checkForPermission(final Context context, final String permission) {
        int result = ContextCompat.checkSelfPermission(context, permission);
        //If permission is granted then it returns 0 as result
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * show image in circle view using glide
     *
     * @param context
     * @param imgUrl
     * @param imageView
     */
    public static void getCircleImageView(final Context context, String imgUrl, final ImageView imageView) {
        Glide.with(context).load(imgUrl).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create((context).getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imageView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    // This method is for show image in circleview using Glide
    public static void setImageView(final Context context, String imgUrl, final ImageView imageView) {
        Glide.with(context).load(imgUrl).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                imageView.setImageBitmap(resource);
            }
        });
    }

    public static ProgressDialog wsProgressDialog(final Context context) {
        ProgressDialog dialog = new ProgressDialog(context);
        try {
            dialog.show();
        } catch (WindowManager.BadTokenException e) {

        }
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.progressdialog);
        // dialog.setMessage(Message);
        return dialog;
    }

    public final static String getCurrentDate() {
        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        String dateString = sdf.format(date);
        return dateString;
    }

    public static String getDeviceId(Context context) {
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_id;
    }

    public static String changeDateTimeFormat(String strDate, String srcFormat, String destFormat) {

        Date date = new Date();
        String strFormatedDate = "";

        try {

            DateFormat formatter = new SimpleDateFormat(srcFormat);
            date = formatter.parse(strDate);
            strFormatedDate = new SimpleDateFormat(destFormat).format(date);
        } catch (ParseException e) {

            e.printStackTrace();
        }

        return strFormatedDate;
    }

    public static int dpToPx(final Context context, final int dp) {
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static Snackbar showSnackbar(View view, String message,
                                        String actionLabel,
                                        View.OnClickListener clickListener, Context context) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(actionLabel, clickListener);
//        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
//        TextView textView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(Color.WHITE);
        snackbar.show();
        return snackbar;
    }

    public static void openMailClent(String mail, Context context) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{mail});
        intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
        context.startActivity(Intent.createChooser(intent, ""));

    }

    public static void displayLogoutDialog(final Context context, final String title, final String message) {

        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                final PreferenceUtils preferenceUtils = new PreferenceUtils(context);
                preferenceUtils.putBoolean(preferenceUtils.KEY_USER_LOGIN, false);
                preferenceUtils.putString(preferenceUtils.KEY_USER_ID, "");
                preferenceUtils.putString(preferenceUtils.KEY_FIRST_NAME, "");
                preferenceUtils.putString(preferenceUtils.KEY_LAST_NAME, "");
                preferenceUtils.putString(preferenceUtils.KEY_TOKEN, "");

                final Intent loginIntent = new Intent(context, LoginActivity.class);
                context.startActivity(loginIntent);
                ((Activity) context).finish();

            }
        });
        if (!((Activity) context).isFinishing()) {
            alertDialog.show();
        }
    }


    public static ProgressDialog showProgressDialog(final Context mActivity, final String message, boolean isCancelable) {
        final ProgressDialog mDialog = new ProgressDialog(mActivity);
        mDialog.show();
        mDialog.setCancelable(isCancelable);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setMessage(message);
        return mDialog;
    }

    public static final void dismissProgressDialog(ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
    public static boolean isValidExpDate(String expMonth, String expYear) {
        final int currentYear = Integer.parseInt(String.valueOf(
                Calendar.getInstance().get(Calendar.YEAR)).substring(2));
        final int currentMonth = Integer.parseInt(String.valueOf(Calendar
                .getInstance().get(Calendar.MONTH))) + 1;
        try {
            if (Integer.parseInt(expYear) < currentYear) {
                return false;
            } else if (Integer.parseInt(expYear) == currentYear) {
                if (Integer.parseInt(expMonth) < currentMonth)
                    return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    public static byte[] encryption(final String data) {
        final JNCryptor cryptor = new AES256JNCryptor();
        final byte[] plaintext = data.getBytes();

        byte[] ciphertext = new byte[0];
//        String encodedText = "";
        try {
            ciphertext = cryptor.encryptData(plaintext, phaseKeyForEncryption.toCharArray());
//            encodedText = Base64.encodeToString(ciphertext, Base64.DEFAULT);
        } catch (CryptorException e) {
            // Something went wrong
            e.printStackTrace();
        }
        return ciphertext;
    }

    public static String convertEncryptedByteToString(final byte[] encryptedBytes) {
        String encryptedText = "";
        encryptedText = Base64.encodeToString(encryptedBytes, Base64.DEFAULT);

        return replaceAllSpecialChars(encryptedText);
    }
    private static String replaceAllSpecialChars(String encryptedText) {

        for (int i = 0; i < charsToReplace.length; i++) {
            encryptedText = encryptedText.replaceAll(charsToReplace[i], charsToBeReplace[i]);
        }

        return encryptedText;
    }


}
