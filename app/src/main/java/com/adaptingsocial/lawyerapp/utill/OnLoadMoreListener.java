package com.adaptingsocial.lawyerapp.utill;

/**
 * Created by indianic on 3/5/17.
 */

public interface OnLoadMoreListener {
    public void onLoadMore();
}
