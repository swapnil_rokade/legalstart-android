package com.adaptingsocial.lawyerapp.utill;

/**
 * MediaSelectedListener class created.
 */

public interface MediaSelectedListener {
    void onImageSelected(final String imagePath);

}
