package com.adaptingsocial.lawyerapp.utill;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.adaptingsocial.lawyerapp.chat.model.utill.QbDialogHolder;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;

/**
 * Created by indianic on 27/06/17.
 */

public class ChatDeleteService extends Service {
    /**
     * indicates how to behave if the service is killed
     */
    int mStartMode;
    PreferenceUtils preferenceUtils;

    /**
     * interface for clients that bind
     */
    IBinder mBinder;

    /**
     * indicates whether onRebind should be used
     */
    boolean mAllowRebind;

    /**
     * Called when the service is being created.
     */
    @Override
    public void onCreate() {
        //Do some work
        preferenceUtils = new PreferenceUtils(ChatDeleteService.this);
        deleteDialog(preferenceUtils.getString(preferenceUtils.KEY_DIALOG_ID));
    }

    /**
     * The service is starting, due to a call to startService()
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return mStartMode;
    }

    /**
     * A client is binding to the service with bindService()
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * Called when all clients have unbound with unbindService()
     */
    @Override
    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }

    /**
     * Called when a client is binding to the service with bindService()
     */
    @Override
    public void onRebind(Intent intent) {

    }

    /**
     * Called when The service is no longer used and is being destroyed
     */
    @Override
    public void onDestroy() {

    }

    private void deleteDialog(final String qbChatDialogId) {
        QBRestChatService.deleteDialog(qbChatDialogId, false).performAsync(new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                QbDialogHolder.getInstance().deleteDialog(qbChatDialogId);
                preferenceUtils.putString(preferenceUtils.KEY_DIALOG_ID, "");
                stopSelf();


            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }


}