package com.adaptingsocial.lawyerapp.utill;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.adaptingsocial.lawyerapp.fragment.AddNewQuestionFragment;

import java.util.HashMap;

/**
 * Created by indianic on 05/07/17.
 */

public class MyTextWatcher implements TextWatcher {

    private EditText mEditText;
    private AddNewQuestionFragment addNewQuestionFragment;


    private HashMap<String, String> stringHashMap;


    public MyTextWatcher(EditText editText, HashMap<String, String> stringHashMap,
                         AddNewQuestionFragment addNewQuestionFragment) {
        mEditText = editText;
        this.stringHashMap = stringHashMap;
        this.addNewQuestionFragment = addNewQuestionFragment;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        AppLog.showLogD("Result", "before text");
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        AppLog.showLogD("Result", "on text");
    }

    @Override
    public void afterTextChanged(Editable s) {
        AppLog.showLogD("Result", "after text");
        String tag = (String) mEditText.getTag();
        stringHashMap.put(tag, s.toString().trim());
//        for (String key : stringHashMap.keySet()) {
//            if(key.equalsIgnoreCase(tag)){
//                stringHashMap.put(key,mEditText.getText().toString().trim());
//            }
//
//        }
        //addNewQuestionFragment.setStringHashMap(stringHashMap);

    }


}
