package com.adaptingsocial.lawyerapp.utill;


import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.chat.model.utill.ResourceUtils;

public class Constants {


    //mayur account
//    public static final String PUBLISHABLE_KEY = "pk_test_U3uyRF4NzmiNMbdRu7hDC2CF";


    //Client account
    public static final String PUBLISHABLE_KEY = "pk_live_HI6lKeYyFKcSKzuYRDpHh5sR";

    public static final long MAX_CLICK_INTERVAL = 1000;
    public static final int SPLASH_TIME_OUT = 1000;
    public static final int ATTACH_DOCUMENT_LIMIT = 9;


//    public static final String LOGIN_ID = "rahul456";
//    public static final String LOGIN_PASSWORD = "Indianic123&";


    public static final String LOGIN_ID = "Start2851";
    public static final String LOGIN_PASSWORD = "summer_51";

    // for Camera and Gallery
    public static final int CHOOSE_IMAGE_CAMERA = 1001;
    public static final int CHOOSE_IMAGE_GALLERY = 1002;
    public static final int CAMERA_CROP = 1010;
    //MARSHMELLOW PERMISSION
    public static final int WRITE_EXTERNAL_STORAGE_PERMISSION = 1;
    public static final int PERMISSION_REQUEST_CAMERA = 100;
    public static final int PERMISSION_REQUEST_CODE = 101;
    //temp
    public static final String INTENT_KEY_BLOG_MODEL = "blog";
    public static final String INTENT_KEY_Document_MODEL = "document";
    public static final String INTENT_KEY_USER_TYPE = "userType";
    public static final String INTENT_KEY_CASE_MODEL = "case";
    public static final String INTENT_KEY_QUESTION_CATAGORY_MODEL = "questioncatagory";
    public static final String INTENT_KEY_QUESTION_CATAGORY_LIST = "questioncatagorylist";
    public static final String KEY_ANDROID = "android";
    public final static String FILE_EXTENSION = ".jpg";
    public static final String REQUEST_FOR_DATE = "";
    public static final String INTENT_KEY_IS_FUTURE_DATE_ALLOWED = "isFutureDateAllowed";
    public static final String INTENT_KEY_IS_PAST_DATE_ALLOWED = "isPastDateAllowed";
    public static final String DATE_FORMATE_DD_MMM_YY = "dd MMM, yy";
    public static final String DATE_FORMATE_MM_DD_YYYY = "yyyy-MM-dd";
    public static final String SAMPLE_CONFIG_FILE_NAME = "qb_config.json";
    public static final int PREFERRED_IMAGE_SIZE_PREVIEW = ResourceUtils.getDimen(R.dimen._320sdp);
    public static final int PREFERRED_IMAGE_SIZE_FULL = ResourceUtils.dpToPx(320);

    public static final String INTENT_KEY_QUESTION_FROM_CATAGORY = "fromquestioncatagory";
    public static final String PAST_PARCELABLE_ARRAY = "past_parcelable_array";
    public static final String UP_PARCELABLE_ARRAY = "up_parcelable_array";
    public static final String PUBLICATION_PRICE = "699";
    public static final String NEW_YORK_CERTIFICATION = "399";
    public static final String NEW_JERSY_CERTIFICATION = "399";

    public static final String ZIPCODE_TAG = "ZIPCODE";
    public static final String AMOUNT_TAG = "TOTAL_AMOUNT";
    public static final String CREDIT_CARD_TAG = "CREDIT_CARD_TAG";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_ID = "USER_ID";
    public static final String ITEM_NAME="itemName";

    public static final int FROM_APPOINTMENT=0;
    public static final int FROM_CREATE_DOCUMENT=1;
}
