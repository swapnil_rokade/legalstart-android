package com.adaptingsocial.lawyerapp.utill;

import android.widget.DatePicker;

public interface DateSelector {

    public abstract void onFromDateSelect(DatePicker picker, int requestDate);

    public abstract void onMonthSelect(final String month, final String year);

    public abstract void onToDateSelect(DatePicker picker);

}
