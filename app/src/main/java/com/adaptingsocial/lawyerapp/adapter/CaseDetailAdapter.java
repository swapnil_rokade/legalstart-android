package com.adaptingsocial.lawyerapp.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.DocumentModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Utils;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by indianic on 25/04/17.
 */

public class CaseDetailAdapter extends BaseAdapter {
    private ArrayList<DocumentModel> documentModelArrayList;
    private LayoutInflater inflater;
    private Context context;

    public CaseDetailAdapter(final Context context, final ArrayList<DocumentModel>
            documentModelArrayList) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.documentModelArrayList = documentModelArrayList;
    }

    @Override
    public int getCount() {
        return documentModelArrayList.size();

    }

    @Override
    public DocumentModel getItem(int position) {
        return documentModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_document_uploaded, null);
            holder = new Holder();
            holder.tvDocumentName = (TextView) convertView.findViewById(R.id
                    .row_document_uploaded_tv_doc_name);
            holder.tvUploadedBy = (TextView) convertView.findViewById(R.id
                    .row_document_uploaded_tv_doc_uploaded_by_user_name);
            holder.tvDate = (TextView) convertView.findViewById(R.id
                    .row_document_uploaded_tv_date);
            holder.ivDocImage = (ImageView) convertView.findViewById(R.id
                    .row_document_uploaded_iv_pdf);
            holder.ivDownLoad = (ImageView) convertView.findViewById(R.id
                    .row_document_uploaded_iv_downloaded);
            holder.ivShare = (ImageView) convertView.findViewById(R.id
                    .row_document_uploaded_iv_share);
            convertView.setTag(holder);


        } else {
            holder = (CaseDetailAdapter.Holder) convertView.getTag();
        }


        holder.tvDocumentName.setText("" + documentModelArrayList.get(position).getDocumentName());
        if (documentModelArrayList.get(position).getUserid().equalsIgnoreCase("0")) {
            holder.tvUploadedBy.setText(R.string.uploaded_by_admin);
        } else {
            holder.tvUploadedBy.setText(R.string.uploaded_by_me);
        }


        holder.tvDate.setText("" + documentModelArrayList.get(position).getDate());
        if (documentModelArrayList.get(position).getFilePath().equalsIgnoreCase("")
                || documentModelArrayList.get(position).getFilePath().equalsIgnoreCase(null)) {
            holder.ivDownLoad.setVisibility(View.GONE);
        } else {
            holder.ivDownLoad.setVisibility(View.VISIBLE);
        }
        holder.ivDownLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(context)) {
                    new DownloadFileFromURL().execute(documentModelArrayList.get(position)
                            .getFilePath(), documentModelArrayList.get(position).getDocumentName());

                } else {
                    Utils.displayDialog(context, context.getString(R.string.app_name), context
                            .getString
                                    (R.string
                                            .check_internet_msg));
                }


            }
        });
        if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase("png")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_png);
        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase("jpeg")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_jpg);

        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase("xls")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_xls);

        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase("doc")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_doc);

        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase("txt")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_txt);

        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase("pdf")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_pdf);

        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase("docx")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_doc);

        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase("jpg")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_jpg);

        } else {
            holder.ivDocImage.setImageResource(R.drawable.ic_pdf);
        }

        return convertView;
    }


    class Holder {
        private TextView tvDocumentName;
        private TextView tvDate;
        private TextView tvUploadedBy;
        private ImageView ivDownLoad;
        private ImageView ivShare;
        private ImageView ivDocImage;

    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        private ProgressDialog progressDialog;

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getString(R.string.down_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();
                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                // Output stream
                OutputStream output = new FileOutputStream("/sdcard/" + f_url[1]);
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                AppLog.showLogE("Error: ", e.getMessage());
            }

            return null;
        }


        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            progressDialog.dismiss();
            Toast.makeText(context, R.string.file_downloaded, Toast.LENGTH_SHORT).show();

        }

    }
}
