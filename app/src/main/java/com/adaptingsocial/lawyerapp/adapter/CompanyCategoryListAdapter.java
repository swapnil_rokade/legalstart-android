package com.adaptingsocial.lawyerapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CompanyCategoryListModel;

import java.util.ArrayList;


public class CompanyCategoryListAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<CompanyCategoryListModel> companyCategoryListModelArrayList;
    private LayoutInflater inflater;

    public CompanyCategoryListAdapter(final Context context, final ArrayList<CompanyCategoryListModel> companyCategoryListModelArrayList) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.companyCategoryListModelArrayList = companyCategoryListModelArrayList;
    }

    @Override
    public int getCount() {
        return companyCategoryListModelArrayList.size();

    }

    @Override
    public CompanyCategoryListModel getItem(int position) {
        return companyCategoryListModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_company_category_list, null);
            holder = new Holder();
            holder.tvCompanyCategoryName = (TextView) convertView.findViewById(R.id.row_company_list_filter_tv_pr);
            convertView.setTag(holder);

        } else {
            holder = (CompanyCategoryListAdapter.Holder) convertView.getTag();
        }
        if(companyCategoryListModelArrayList.get(position).isSelected()){
            holder.tvCompanyCategoryName.setTextColor(ContextCompat.getColor(context,R.color.colorDarkGray));
        }else {
            holder.tvCompanyCategoryName.setTextColor(ContextCompat.getColor(context,R.color.colorLightGray));
        }
        holder.tvCompanyCategoryName.setText("" + companyCategoryListModelArrayList.get(position).getCompanyCategoryName());
        return convertView;
    }

    private class Holder {
        private TextView tvCompanyCategoryName;
    }
}
