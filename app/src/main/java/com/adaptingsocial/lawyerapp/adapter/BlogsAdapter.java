//package com.adaptingsocial.lawyerapp.adapter;
//
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.app.FragmentManager;
//import android.app.FragmentTransaction;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.drawable.ColorDrawable;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.support.v4.content.ContextCompat;
//import android.support.v4.view.PagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import com.adaptingsocial.lawyerapp.R;
//import com.adaptingsocial.lawyerapp.fragment.AddCommentFragment;
//import com.adaptingsocial.lawyerapp.fragment.BlogFragment;
//import com.adaptingsocial.lawyerapp.model.BlogModel;
//import com.adaptingsocial.lawyerapp.model.CommentModel;
//import com.adaptingsocial.lawyerapp.utill.Utils;
//import com.adaptingsocial.lawyerapp.webservice.WSConstants;
//import com.adaptingsocial.lawyerapp.webservice.WsAddComment;
//
//import java.util.ArrayList;
//
///**
// * Created by indianic on 01/05/17.
// */
//
//public class BlogsAdapter extends PagerAdapter {
//    private Context mContext;
//    private LayoutInflater mLayoutInflater;
//    private ArrayList<BlogModel> blogModelArrayList;
//    private AddComentAsyncTask addComentAsyncTask;
//    private BlogFragment fragment;
//    private WsAddComment wsAddComment;
//    private addCommentListner maddCommentListner;
//    private ViewPager viewPager;
//
//    public void setMaddCommentListner(addCommentListner maddCommentListner) {
//        this.maddCommentListner = maddCommentListner;
//    }
//
//    public BlogsAdapter(Context context, BlogFragment fragment, ArrayList<BlogModel>
//            blogModelArrayList, ViewPager viewPager) {
//        this.mContext = context;
//        this.blogModelArrayList = blogModelArrayList;
//        this.fragment = fragment;
//        this.viewPager = viewPager;
//        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    }
//
//    @Override
//    public int getCount() {
//        return blogModelArrayList.size();
//
//    }
//
////    @Override
////    public int getItemPosition(Object object) {
////        return POSITION_NONE;
////    }
//
//    @Override
//    public Object instantiateItem(ViewGroup container, final int position) {
//        LayoutInflater inflater = LayoutInflater.from(mContext);
//        View itemView = inflater.inflate(R.layout.row_blogs, container, false);
//        ImageView ivBg = (ImageView) itemView.findViewById(R.id.row_blog_iv_background);
//        TextView tvBlogTitle = (TextView) itemView.findViewById(R.id.row_blog_tv_title);
//        TextView tvBlogDate = (TextView) itemView.findViewById(R.id.row_blog_tv_date);
//        TextView tvBlogDescription = (TextView) itemView.findViewById(R.id.row_blog_tv_description);
//        tvBlogTitle.setText("" + blogModelArrayList.get(position).getBlogName());
//        tvBlogDate.setText("" + blogModelArrayList.get(position).getBolgDate());
//        tvBlogDescription.setText("" + blogModelArrayList.get(position).getBlogDescription());
//        Utils.setImageView(mContext, blogModelArrayList.get(position).getBlogImage(),
//                ivBg);
//        ListView lvComment = (ListView) itemView.findViewById(R.id.row_blog_lv_comments);
//        TextView tvAddComment = (TextView) itemView.findViewById(R.id.row_blog_tv_add_comment);
//        TextView tvShare= (TextView) itemView.findViewById(R.id.row_blog_tv_comment);
//
//        if (blogModelArrayList.get
//                (position).getCommentModelArrayList() != null && blogModelArrayList.get
//                (position).getCommentModelArrayList().size() > 0) {
//            CommentAdapter commentAdapter = new CommentAdapter(mContext, blogModelArrayList.get
//                    (position).getCommentModelArrayList(),
//                    blogModelArrayList.get(position).getBlogId());
//            lvComment.setAdapter(commentAdapter);
//        }
//
//
//        tvAddComment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                if (maddCommentListner != null) {
////                    maddCommentListner.onCommentAdd();
////                }
////                 openDialog(blogModelArrayList.get(position).getBlogId(), position);
//                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://www.google.com");
//                mContext.startActivity(Intent.createChooser(sharingIntent, "Share via"));
//
//
//            }
//        });
//
//
//        tvShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://www.google.com");
//                mContext.startActivity(Intent.createChooser(sharingIntent, "Share via"));
//
//            }
//        });
//        container.addView(itemView);
//        itemView.setTag("myview" + position);
//        return itemView;
//    }
//
//
//    private void openAddCaseScreen(String id) {
//        final AddCommentFragment addCaseFragment = new AddCommentFragment();
//        final FragmentManager fragmentManager = fragment.getFragmentManager();
//        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        Bundle bundle = new Bundle();
//        bundle.putString("id", id);
//        addCaseFragment.setArguments(bundle);
//        fragmentTransaction.add(R.id.activity_home_container_main, addCaseFragment,
//                AddCommentFragment.class
//                        .getSimpleName());
//
//        addCaseFragment.setTargetFragment(fragment, 1);
//        fragmentTransaction.addToBackStack(AddCommentFragment.class.getSimpleName());
//        fragmentTransaction.hide(fragment);
//        fragmentTransaction.commit();
//
//
//    }
//
//
//    public void openDialog(final String id, final int position) {
//        final Dialog dialog = new Dialog(mContext, R.style.picture_dialog_style);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.setContentView(R.layout.dialog_post_comment);
//        final WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
//        wlmp.gravity = Gravity.BOTTOM;
//        wlmp.width = WindowManager.LayoutParams.FILL_PARENT;
//        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        dialog.getWindow().setAttributes(wlmp);
//        dialog.show();
//        final EditText etComment = (EditText) dialog.findViewById(R.id.dialog_post_comment_et_add_comment);
//        TextView tvPostComment = (TextView) dialog.findViewById(R.id
//                .dialog_post_comment_tv_post_comment);
//        tvPostComment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (etComment.getText().toString().trim().equalsIgnoreCase("")) {
//                    Utils.displayDialog(mContext, mContext.getString(R.string.app_name), mContext.getString(R.string.add_comment_msg));
//                } else {
//                    callPostCommentApi(id, etComment.getText().toString().trim(), position);
//                    dialog.dismiss();
//                }
//
//
//            }
//        });
//
//
//    }
//
//    private void callPostCommentApi(String id, String comment, int position) {
//        if (Utils.isNetworkAvailable(mContext)) {
//            if (addComentAsyncTask != null && addComentAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
//                addComentAsyncTask.execute();
//            } else if (addComentAsyncTask == null || addComentAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
//                addComentAsyncTask = new AddComentAsyncTask(id, comment, position);
//                addComentAsyncTask.execute();
//            }
//        } else {
//            Utils.displayDialog(mContext, mContext.getString(R.string.app_name), mContext.
//                    getString(R.string
//                            .check_internet_msg));
//        }
//
//    }
//
//    public class AddComentAsyncTask extends AsyncTask<String, Void, Void> {
//
//        private ProgressDialog progressDialog;
//        private String id;
//        private String comment;
//        private int position;
//
//        public AddComentAsyncTask(String id, String comment, int position) {
//            this.id = id;
//            this.comment = comment;
//            this.position = position;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(mContext);
//            progressDialog.setMessage(mContext.getString(R.string.msg_loading));
//            progressDialog.show();
//            progressDialog.setCancelable(false);
//        }
//
//        @Override
//        protected Void doInBackground(String... params) {
//            wsAddComment = new WsAddComment(mContext);
//            wsAddComment.executeService(id, comment);
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            if (progressDialog != null && progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
//            if (!isCancelled()) {
//                if (wsAddComment.getCode() == WSConstants.STATUS_SUCCESS) {
//                    displayDialog(mContext, mContext.getString(R.string.app_name), wsAddComment
//                            .getMessage(), position);
//
//
//                } else if (wsAddComment.getCode() == WSConstants.STATUS_FAIL) {
//                    Utils.displayDialog(mContext, mContext.getString(R.string.app_name), wsAddComment
//                            .getMessage());
//
//                } else if (wsAddComment.getCode() == WSConstants.STATUS_NO_DATA_FAIL) {
//                    Utils.displayDialog(mContext, mContext.getString(R.string.app_name), wsAddComment
//                            .getMessage());
//
//
//                } else {
//                    Utils.displayDialogWithPopBackStack(mContext, mContext.getString(R.string
//                                    .app_name),
//                            mContext.getString(R.string.something_went_wrong_msg));
//
//                }
//
//
//            }
//
//        }
//
//    }
//
//    public void displayDialog(final Context context, final String title, final String message,
//                              final int position) {
//
//        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
//        alertDialog.setTitle(title);
//        alertDialog.setCancelable(false);
//
//        alertDialog.setMessage(message);
//        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//
////                commentAdapter = new CommentAdapter(mContext, commentModelArrayList, blogModelArrayList.get(position).getBlogId());
////                lvComment.setAdapter(commentAdapter);
////                blogModelArrayList.get(position).setCommentModelArrayList(wsAddComment.getCommentListModelArrayList());
////
////                notifyDataSetChanged();
////                View view = (View) viewPager.findViewWithTag("myview" + position);
////                ListView listView = (ListView) view.findViewById(R.id
////                        .row_blog_lv_comments);
////                if (listView != null) {
////                    CommentAdapter commentAdapter = new CommentAdapter(mContext, wsAddComment.getCommentListModelArrayList(),
////                            blogModelArrayList.get(position).getBlogId());
////                    listView.setAdapter(commentAdapter);
////                }
//
//
////
////
////
////
//                fragment.CaseListUsAsyncTask("", position,true);
//
//
////                CommentModel model = new CommentModel();
////                model.setComment("djrhsdfrhdejsftnedsf;ghtjdfgthdrfgjtfrh");
////                AppLog.showLogD("comment list", "before" + blogModelArrayList.get(position)
////                        .getCommentModelArrayList().size());
////                commentModelArrayList.add(model);
////                AppLog.showLogD("comment list", "after" + blogModelArrayList.get(position)
////                        .getCommentModelArrayList().size());
////
////
////
////                commentAdapter.notifyDataSetChanged();
//
//
//            }
//        });
//        AlertDialog alert = alertDialog.create();
//        alert.show();
//        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
//        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
//    }
//
//
//    @Override
//    public void destroyItem(ViewGroup container, int position, Object object) {
//        container.removeView((LinearLayout) object);
//    }
//
//    @Override
//    public boolean isViewFromObject(View view, Object object) {
//        return view == ((LinearLayout) object);
//    }
//
//
//
//
//    public interface addCommentListner {
//        public void onCommentAdd();
//    }
//
//
//}
