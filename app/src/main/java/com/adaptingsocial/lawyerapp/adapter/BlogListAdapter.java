package com.adaptingsocial.lawyerapp.adapter;

import android.app.Fragment;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.fragment.BlogListFragment;
import com.adaptingsocial.lawyerapp.model.BlogModel;
import com.adaptingsocial.lawyerapp.utill.Utils;

import java.util.ArrayList;

/**
 * Created by indianic on 15/06/17.
 */

public class BlogListAdapter extends BaseAdapter {
    private final Context context;
    private final Fragment blogListFragment;
    private ArrayList<BlogModel> blogModelArrayList;
    private LayoutInflater inflater;
    final public int HEADER_VIEW = 0;
    final public int NORMAL_VIEW = 1;

    public BlogListAdapter(final Context context, final ArrayList<BlogModel> blogModelArrayList,
                           final BlogListFragment blogListFragment) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.blogListFragment = blogListFragment;
        this.blogModelArrayList = blogModelArrayList;
    }

    @Override
    public int getCount() {
        return blogModelArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? NORMAL_VIEW : HEADER_VIEW;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public BlogModel getItem(int position) {
        return blogModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        HeaderHolder headerHolder = null;
        switch (getItemViewType(position)) {
            case 0:
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.row_blog_list, null);
                    headerHolder = new HeaderHolder();
                    headerHolder.tvBlogTitle = (TextView) convertView.findViewById(R.id.row_blog_list_tv_title);
                    headerHolder.tvDate = (TextView) convertView.findViewById(R.id.row_blog_list_tv_date);
                    headerHolder.tvDescription = (TextView) convertView.findViewById(R.id.row_blog_list_tv_decription);
                    headerHolder.ivBlogImage = (ImageView) convertView.findViewById(R.id.row_blog_list_iv_blog);
                    convertView.setTag(headerHolder);
                } else {
                    headerHolder = (HeaderHolder) convertView.getTag();
                }
                headerHolder.tvBlogTitle.setText("" + blogModelArrayList.get(position).getBlogName());
                headerHolder.tvDate.setText("" + blogModelArrayList.get(position).getBolgDate());
                headerHolder.tvDescription.setText(" " + blogModelArrayList.get(position).getBlogDescription());
                Utils.setImageView(context, blogModelArrayList.get(position).getBlogImage(), headerHolder.ivBlogImage);
                return convertView;
            case 1:
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.row_blog_list_header, null);
                    holder = new Holder();
                    holder.tvBlogTitle = (TextView) convertView.findViewById(R.id.row_blog_list_tv_title);
                    holder.tvDate = (TextView) convertView.findViewById(R.id.row_blog_list_tv_date);
                    holder.tvDescription = (TextView) convertView.findViewById(R.id.row_blog_list_tv_decription);
                    holder.ivBlogImage = (ImageView) convertView.findViewById(R.id.row_blog_list_iv_blog);
                    convertView.setTag(holder);
                } else {
                    holder = (Holder) convertView.getTag();
                }
                holder.tvBlogTitle.setText("" + blogModelArrayList.get(position).getBlogName());
                holder.tvDate.setText("" + blogModelArrayList.get(position).getBolgDate());
                holder.tvDescription.setText(" " + blogModelArrayList.get(position).getBlogDescription());
                Utils.setImageView(context, blogModelArrayList.get(position).getBlogImage(), holder.ivBlogImage);
                return convertView;

        }
        return null;
    }

    private class Holder {
        private TextView tvBlogTitle;
        private TextView tvDate;
        private TextView tvDescription;
        private ImageView ivBlogImage;
    }

    private class HeaderHolder {
        private TextView tvBlogTitle;
        private TextView tvDate;
        private TextView tvDescription;
        private ImageView ivBlogImage;
    }
}
