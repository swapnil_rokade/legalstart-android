package com.adaptingsocial.lawyerapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.QuesionCatagoryModel;
import com.adaptingsocial.lawyerapp.utill.Utils;

import java.util.ArrayList;

/**
 * Created by indianic on 03/07/17.
 */

public class QuestionCatagoryAdapter extends BaseAdapter {
    private ArrayList<QuesionCatagoryModel> quesionCatagoryModelArrayList;
    private LayoutInflater inflater;
    private Context context;

    public QuestionCatagoryAdapter(final Context context, final ArrayList<QuesionCatagoryModel> quesionCatagoryModelArrayList) {
        inflater = LayoutInflater.from(context);
        this.quesionCatagoryModelArrayList = quesionCatagoryModelArrayList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return quesionCatagoryModelArrayList.size();


    }

    @Override
    public QuesionCatagoryModel getItem(int position) {
        return quesionCatagoryModelArrayList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_question_catagory, null);
            holder = new Holder();
            holder.tvName = (TextView) convertView.findViewById(R.id
                    .row_question_catagory_tv_name);
            holder.ivCatagory = (ImageView) convertView.findViewById(R.id
                    .row_question_catagory_iv_name);

            convertView.setTag(holder);

        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.tvName.setText("" + quesionCatagoryModelArrayList.get(position).getName());
        Utils.setImageView(context, quesionCatagoryModelArrayList.get(position).getImage(), holder
                .ivCatagory);
        holder.tvName.setVisibility(View.GONE);
        return convertView;
    }

    class Holder {
        private TextView tvName;
        private ImageView ivCatagory;


    }
}
