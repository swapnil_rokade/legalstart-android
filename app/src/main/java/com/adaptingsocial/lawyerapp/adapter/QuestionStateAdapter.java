package com.adaptingsocial.lawyerapp.adapter;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.fragment.QuestionCatListFragment;
import com.adaptingsocial.lawyerapp.fragment.QuestionFragment;
import com.adaptingsocial.lawyerapp.model.QuesionCatagoryModel;
import com.adaptingsocial.lawyerapp.model.QuestionDescriptionDetailModal;
import com.adaptingsocial.lawyerapp.utill.Constants;

import java.util.ArrayList;

/**
 * Created by indianic on 11/08/17.
 */

public class QuestionStateAdapter extends BaseAdapter {
    private final Context context;
    private final QuestionCatListFragment QuestionCatListFragment;
    private ArrayList<QuestionDescriptionDetailModal> questionDescriptionList;
    private LayoutInflater inflater;
    private QuesionCatagoryModel questionDescriptionDetailModal;
    private QuesionCatagoryModel questionCatagoryModel;

    public QuestionStateAdapter(final Context context, final ArrayList<QuestionDescriptionDetailModal> questionDescriptionList,
                                final QuestionCatListFragment QuestionCatListFragment,
                                final QuesionCatagoryModel questionDescriptionDetailModal, QuesionCatagoryModel questionCatagoryModel) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.QuestionCatListFragment = QuestionCatListFragment;
        this.questionDescriptionList = questionDescriptionList;
        this.questionDescriptionDetailModal = questionDescriptionDetailModal;
        this.questionCatagoryModel = questionCatagoryModel;
    }

    @Override
    public int getCount() {
        return questionDescriptionList.size();

    }

    @Override
    public QuestionDescriptionDetailModal getItem(int position) {
        return questionDescriptionList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_state_cat_list, null);
            holder = new Holder();
            holder.tvLLcLabel = (TextView) convertView.findViewById(R.id.row_state_cat_list_tv_llc);

            holder.tvStateName = (TextView) convertView.findViewById(R.id.row_state_cat_list_tv_title);
            holder.tvLlc = (TextView) convertView.findViewById(R.id.row_state_cat_list_tv_llc_value);

            holder.rgDalawara = (RadioGroup) convertView.findViewById(R.id
                    .row_state_cat_list_rg_dalware);
            holder.cardView = (CardView) convertView.findViewById(R.id.row_state_cat_list_row);
            holder.rbAdditional = (RadioButton) convertView.findViewById(R.id.row_state_cat_list_rb_additional);
            holder.rbHours = (RadioButton) convertView.findViewById(R.id
                    .row_state_cat_list_rb_24_hours);
            convertView.setTag(holder);

        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.tvStateName.setText("" + questionDescriptionList.get(position).getState_name());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openQuestionFragment(questionDescriptionDetailModal, questionDescriptionList.get(position));
            }
        });


        if (questionDescriptionList.get(position).getState_name().equalsIgnoreCase(context.getString(R.string.delaware))) {
            holder.rgDalawara.setVisibility(View.VISIBLE);
            questionDescriptionList.get(position).
                    setAdditionaCharge(questionDescriptionList.get(position).getOther_information());
        } else {
            holder.rgDalawara.setVisibility(View.GONE);
            questionDescriptionList.get(position).setAdditionaCharge("");
        }
        holder.tvLLcLabel.setText("" + questionCatagoryModel.getName());
        holder.tvLlc.setText("$" + " " + questionDescriptionList.get(position).getAmmount());
        holder.rbHours.setText("24 hours processing -$" + questionDescriptionList.get(position).getOther_information()
                + "additional");
        holder.rbHours.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    questionDescriptionList.get(position).
                            setAdditionaCharge(questionDescriptionList.get(position).getOther_information());
                } else {
                    questionDescriptionList.get(position).setAdditionaCharge("");
                }
            }
        });

//        if (questionCatagoryModel.getName().equalsIgnoreCase("llc")) {
//            holder.tvCorporation.setVisibility(View.GONE);
//            holder.tvCorporationLable.setVisibility(View.GONE);
//            holder.tvLlc.setVisibility(View.VISIBLE);
//            holder.tvLLcLabel.setVisibility(View.VISIBLE);
//        } else {
//            holder.tvCorporation.setVisibility(View.VISIBLE);
//            holder.tvCorporationLable.setVisibility(View.VISIBLE);
//            holder.tvLlc.setVisibility(View.GONE);
//            holder.tvLLcLabel.setVisibility(View.GONE);
//        }
        return convertView;
    }

    private void openQuestionFragment(QuesionCatagoryModel quesionCatagoryModel,
                                      QuestionDescriptionDetailModal questionDescriptionDetailModal) {
        final QuestionFragment questionFragment = new QuestionFragment();
        final FragmentManager fragmentManager = QuestionCatListFragment.getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_MODEL, quesionCatagoryModel);
        mBundle.putParcelable(Constants.INTENT_KEY_QUESTION_CATAGORY_LIST, questionDescriptionDetailModal);
        questionFragment.setArguments(mBundle);

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, questionFragment,
                QuestionCatListFragment.class
                        .getSimpleName());
        fragmentTransaction.addToBackStack(QuestionCatListFragment.class.getSimpleName());
        fragmentTransaction.hide(QuestionCatListFragment);
        fragmentTransaction.commit();
    }


    private class Holder {
        private TextView tvStateName;
        private TextView tvLlc;
        private TextView tvLLcLabel;

        private RadioGroup rgDalawara;
        private CardView cardView;
        private RadioButton rbAdditional;
        private RadioButton rbHours;
    }
}
