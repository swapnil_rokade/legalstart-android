package com.adaptingsocial.lawyerapp.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CommentModel;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsLikeComment;

import java.util.ArrayList;

/**
 * Created by indianic on 01/05/17.
 */

public class CommentAdapter extends BaseAdapter {
    private ArrayList<CommentModel> commentModelArrayList;
    private LayoutInflater inflater;
    private Context context;
    private String blogId;
    private LikeComentAsyncTask likeComentAsyncTask;
    private boolean iconClick;


    public CommentAdapter(final Context context, ArrayList<CommentModel> commentModelArrayList,
                          String blogId) {
        inflater = LayoutInflater.from(context);
        this.commentModelArrayList = commentModelArrayList;
        this.context = context;
        this.blogId = blogId;
    }

    @Override
    public int getCount() {
        if (commentModelArrayList != null && commentModelArrayList.size() > 0) {
            return commentModelArrayList.size();
        } else {
            return 0;
        }


    }

//    public void setData(ArrayList<CommentModel> commentModelArrayList) {
//        this.commentModelArrayList.clear();
//        this.commentModelArrayList.addAll(commentModelArrayList);
//        this.notifyDataSetChanged();
//    }

    @Override
    public CommentModel getItem(int position) {
        return commentModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_comment, null);
            holder = new Holder();
            holder.tvCommentTitle = (TextView) convertView.findViewById(R.id
                    .row_case_list_tv_commenter_name);
            holder.tvCommentDescription = (TextView) convertView.findViewById(R.id
                    .row_case_list_tv_comment);
            holder.tvLikersCount = (TextView) convertView.findViewById(R.id
                    .row_case_list_tv_comment_likes);
            holder.tvDate = (TextView) convertView.findViewById(R.id
                    .row_case_list_tv_comment_date_time);
            holder.ivProfilePic = (ImageView) convertView.findViewById(R.id
                    .row_comment_iv_photo);
            holder.tvLike = (TextView) convertView.findViewById(R.id
                    .row_case_list_tv_like);
            convertView.setTag(holder);

        } else {
            holder = (Holder) convertView.getTag();
        }
        Utils.setImageView(context, commentModelArrayList.get(position).getProfileImage(), holder
                .ivProfilePic);
        holder.tvCommentTitle.setText("" + commentModelArrayList.get(position).getName());
        holder.tvCommentDescription.setText("" + commentModelArrayList.get(position).getComment());
        holder.tvLikersCount.setText("" + commentModelArrayList.get(position).getLikeCounts() + "" +
                " " + context.getString(R.string.likes));
        holder.tvDate.setText("" + commentModelArrayList.get(position).getDate());
        final TextView tv = holder.tvLike;
        final TextView tvCount = holder.tvLikersCount;
        if (commentModelArrayList.get(position).getBlogLike().equalsIgnoreCase("1")) {
            tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like_selected, 0, 0, 0);
        } else {
            tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like, 0, 0, 0);
        }


        holder.tvLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callLikeCommentApi(commentModelArrayList.get(position).getBlog_comment_id(), tv,
                        tvCount, commentModelArrayList.get(position).getLikeCounts(), position);
            }
        });

        return convertView;
    }

    class Holder {
        private TextView tvCommentTitle;
        private TextView tvCommentDescription;
        private TextView tvLikersCount;
        private ImageView ivProfilePic;
        private TextView tvDate;
        private TextView tvLike;


    }


    private void callLikeCommentApi(String id, TextView tvLike, TextView tvLikeCount, String
            totalCount, int position) {
        if (Utils.isNetworkAvailable(context)) {
            if (likeComentAsyncTask != null && likeComentAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                likeComentAsyncTask.execute();
            } else if (likeComentAsyncTask == null || likeComentAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                likeComentAsyncTask = new LikeComentAsyncTask(id, tvLike, tvLikeCount,
                        totalCount, position);
                likeComentAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(context, context.getString(R.string.app_name), context.
                    getString(R.string
                            .check_internet_msg));
        }

    }

    public class LikeComentAsyncTask extends AsyncTask<String, Void, Void> {
        private WsLikeComment wsLikeComment;
        private ProgressDialog progressDialog;
        private String id;
        TextView tvLike;
        private TextView tvLikeCount;
        String totalCount;
        int position;

        public LikeComentAsyncTask(String id, TextView tvLike, TextView tvLikeCount, String
                totalCount, int position) {
            this.id = id;
            this.tvLike = tvLike;
            this.tvLikeCount = tvLikeCount;
            this.totalCount = totalCount;
            this.position = position;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(String... params) {
            wsLikeComment = new WsLikeComment(context);
            wsLikeComment.executeService(id);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsLikeComment.getCode() == WSConstants.STATUS_SUCCESS) {
                    displayDialog(context, context.getString(R.string.app_name), wsLikeComment
                                    .getMessage(), wsLikeComment.getBlogLike(), tvLike, totalCount,
                            tvLikeCount, wsLikeComment.getCount_like(), position);

                } else if (wsLikeComment.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(context, context.getString(R.string.app_name), wsLikeComment
                            .getMessage());
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like, 0, 0, 0);

                } else if (wsLikeComment.getCode() == WSConstants.STATUS_NO_DATA_FAIL) {
                    Utils.displayDialog(context, context.getString(R.string.app_name), wsLikeComment
                            .getMessage());
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like, 0, 0, 0);

                } else if (wsLikeComment.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(context, context.getString(R.string.app_name),
                            wsLikeComment
                                    .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(context, context.getString(R.string
                                    .app_name),
                            context.getString(R.string.something_went_wrong_msg));
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like, 0, 0, 0);
                }


            }

        }

    }

    public void displayDialog(final Context context, final String title, final String message,
                              final String blogLike, final TextView tvLike, final String countLike,
                              final TextView tvLikeCount, final String currentLike, final int
                                      position) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (currentLike != null) {
                    int totalLike = Integer.parseInt(countLike);
                    int result = totalLike + 1;
                    tvLikeCount.setText("" + result + " " + "Likes");
                    commentModelArrayList.get(position).setLikeCounts(String.valueOf(result));
                } else {
                    int totalLike = Integer.parseInt(countLike);
                    int result = totalLike - 1;
                    tvLikeCount.setText("" + result + " " + "Likes");
                    commentModelArrayList.get(position).setLikeCounts(String.valueOf(result));
                }

                if (blogLike.equalsIgnoreCase("1")) {
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like_selected, 0, 0, 0);
                    commentModelArrayList.get(position).setBlogLike("1");


                } else {
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like, 0, 0, 0);
                    commentModelArrayList.get(position).setBlogLike("0");
                }


            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }

}
