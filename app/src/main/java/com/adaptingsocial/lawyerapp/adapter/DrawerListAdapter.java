package com.adaptingsocial.lawyerapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.DrawerModel;
import com.adaptingsocial.lawyerapp.view.HomeActivity;

import java.util.ArrayList;


public class DrawerListAdapter extends BaseAdapter {
    private ArrayList<DrawerModel> drawerMenuArray;
    private Context context;
    private LayoutInflater inflater;
    private int mSelectedItem;
    ;

    public DrawerListAdapter(HomeActivity baseActivityCustom, ArrayList<DrawerModel> drawerModelList) {
        this.context = baseActivityCustom;
        this.drawerMenuArray = drawerModelList;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return drawerMenuArray.size();
    }

    @Override
    public Object getItem(int position) {
        return drawerMenuArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.drawer_list_item, parent, false);
            viewHolder.tvTextView = (TextView) convertView.findViewById(R.id.drawer_list_item_tv_name);
            viewHolder.ivImage = (ImageView) convertView.findViewById(R.id.drawer_list_item_iv_image);
            viewHolder.llMain= (LinearLayout) convertView.findViewById(R.id.main);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvTextView.setText(drawerMenuArray.get(position).getTitle());
        if (position == mSelectedItem) {
            viewHolder.llMain.setBackgroundColor(context.getResources().getColor(R.color.color_selecter));
        } else {
            viewHolder.llMain.setBackgroundColor(context.getResources().getColor(R.color.colorBlue));
        }

        viewHolder.ivImage.setImageResource(drawerMenuArray.get(position).getImage());

        return convertView;
    }


    private class ViewHolder {
        private TextView tvTextView;
        private ImageView ivImage;
        private LinearLayout llMain;
    }

    public void setSelectedItem(int selectedItem) {
        this.mSelectedItem = selectedItem;
    }
}
