package com.adaptingsocial.lawyerapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.TimeSlotModel;

import java.util.ArrayList;

/**
 * Created by indianic on 09/05/17.
 */

public class PastAppointmentAdapter extends BaseAdapter {
    private ArrayList<TimeSlotModel> appointmentModelArrayList;
    private LayoutInflater inflater;

    public PastAppointmentAdapter(final Context context, final ArrayList<TimeSlotModel> appointmentModelArrayList) {
        inflater = LayoutInflater.from(context);
        this.appointmentModelArrayList = appointmentModelArrayList;
    }

    @Override
    public int getCount() {
        return appointmentModelArrayList.size();
    }

    @Override
    public TimeSlotModel getItem(int position) {
        return appointmentModelArrayList.get(position);
//        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_past_appointment, null);
            holder = new Holder();
            holder.tvAppointmnetTitle= (TextView) convertView.findViewById(R.id.row_past_appointment_tv_title);
            holder.tvDate= (TextView) convertView.findViewById(R.id.row_past_appointment_tv_date);
            holder.tvTime= (TextView) convertView.findViewById(R.id.row_past_appointment_tv_time);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.tvAppointmnetTitle.setText(getItem(position).getName()+"");
        holder.tvDate.setText(getItem(position).getDate()+"");
        holder.tvTime.setText(getItem(position).getStartTime()+" - "+getItem(position).getEndTime());
        return convertView;
    }


    class Holder {
        private TextView tvAppointmnetTitle;
        private TextView tvDate;
        private TextView tvTime;




    }

}
