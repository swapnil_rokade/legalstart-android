package com.adaptingsocial.lawyerapp.adapter;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.fragment.AuthorizeSubscriptionFragment;
import com.adaptingsocial.lawyerapp.fragment.PaypalSubscriptionFragment;
import com.adaptingsocial.lawyerapp.fragment.StripeSubscriptionFragment;
import com.adaptingsocial.lawyerapp.fragment.SubscriptionFragment;
import com.adaptingsocial.lawyerapp.model.SubscriptionModel;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;

import java.util.ArrayList;

/**
 * Created by indianic on 23/06/17.
 */

public class SubscriptionAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<SubscriptionModel> subscriptionModelArrayList;
    private LayoutInflater inflater;
    private SubscriptionFragment subscriptionFragment;
    private Dialog dialog;

    public SubscriptionAdapter(final Context context, final ArrayList<SubscriptionModel> subscriptionModelArrayList,
                               final SubscriptionFragment subscriptionFragment) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.subscriptionFragment = subscriptionFragment;
        this.subscriptionModelArrayList = subscriptionModelArrayList;
    }

    @Override
    public int getCount() {
        return subscriptionModelArrayList.size();

    }

    @Override
    public SubscriptionModel getItem(int position) {
        return subscriptionModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_subscription_list, null);
            holder = new Holder();
            holder.tvTitle = (TextView) convertView.findViewById(R.id.row_subscription_list_tv_title);
            holder.tvPay = (TextView) convertView.findViewById(R.id
                    .row_subscription_tv_pay);
            holder.tvAmmount = (TextView) convertView.findViewById(R.id.row_subscription_tv_ammount);
            holder.tvDescription = (TextView) convertView.findViewById(R.id.row_subscription_list_tv_description);
            convertView.setTag(holder);

        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.tvTitle.setText("" + subscriptionModelArrayList.get(position).getTitle());
        holder.tvDescription.setText("" + subscriptionModelArrayList.get(position).getDescription());
        holder.tvAmmount.setText("$" + subscriptionModelArrayList.get(position).getAmmount());

        PreferenceUtils preferenceUtils = new PreferenceUtils(context);
        Boolean freeUser = preferenceUtils.getBoolean(preferenceUtils.KEY_APP_USER_TYPE);
        if (freeUser) {
            holder.tvPay.setText(R.string.pay);
            holder.tvPay.setVisibility(View.VISIBLE);
            holder.tvPay.setClickable(true);
            holder.tvPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openStripeScreen(subscriptionModelArrayList.get(position));
//                    openDialog(subscriptionModelArrayList.get(position));
                }
            });
        } else {
            holder.tvPay.setClickable(false);
            holder.tvPay.setText(R.string.paid);
            holder.tvPay.setVisibility(View.INVISIBLE);

        }


        return convertView;
    }

    private void openDialog(final SubscriptionModel subscriptionModel) {
        dialog = new Dialog(context, R.style.picture_dialog_style);
        dialog.setContentView(R.layout.dialog_payment);
        final WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(wlmp);
        dialog.show();
        final TextView tvPaypal = (TextView) dialog.findViewById(R.id
                .dialog_payment_paypal);
        final TextView tvStripe = (TextView) dialog.findViewById(R.id.dialog_payment_stripe);
        final TextView tvCancel = (TextView) dialog.findViewById(R.id.dialog_payment_cancel);
//        tvStripe.setText(R.string.authorize);
        tvPaypal.setVisibility(View.GONE);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvPaypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPaypalScreen(subscriptionModel);
                dialog.dismiss();

            }
        });
        tvStripe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openStripeScreen(subscriptionModel);
//                openAuthorizeScreen(subscriptionModel);
                dialog.dismiss();
            }
        });

    }


    private class Holder {
        private TextView tvTitle;
        private TextView tvPay;
        private TextView tvAmmount;
        private TextView tvDescription;


    }

    private void openStripeScreen(SubscriptionModel subscriptionModel) {
        final StripeSubscriptionFragment stripePaymentFragment = new StripeSubscriptionFragment();
        final FragmentManager fragmentManager = subscriptionFragment.getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_Document_MODEL, subscriptionModel);
        stripePaymentFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, stripePaymentFragment,
                StripeSubscriptionFragment.class
                        .getSimpleName());
        stripePaymentFragment.setTargetFragment(subscriptionFragment, 1);
        fragmentTransaction.addToBackStack(context.getClass().getSimpleName());
        fragmentTransaction.hide(subscriptionFragment);
        fragmentTransaction.commit();
    }


    private void openAuthorizeScreen(SubscriptionModel subscriptionModel) {
        final AuthorizeSubscriptionFragment authorizeSubscriptionFragment = new AuthorizeSubscriptionFragment();
        final FragmentManager fragmentManager = subscriptionFragment.getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_Document_MODEL, subscriptionModel);
        authorizeSubscriptionFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, authorizeSubscriptionFragment,
                StripeSubscriptionFragment.class
                        .getSimpleName());
        authorizeSubscriptionFragment.setTargetFragment(subscriptionFragment, 1);
        fragmentTransaction.addToBackStack(context.getClass().getSimpleName());
        fragmentTransaction.hide(subscriptionFragment);
        fragmentTransaction.commit();
    }


    private void openPaypalScreen(SubscriptionModel subscriptionModel) {
        final PaypalSubscriptionFragment paypalPaymentFragment = new PaypalSubscriptionFragment();
        final FragmentManager fragmentManager = subscriptionFragment.getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_Document_MODEL, subscriptionModel);
        paypalPaymentFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, paypalPaymentFragment,
                StripeSubscriptionFragment.class
                        .getSimpleName());
        paypalPaymentFragment.setTargetFragment(subscriptionFragment, 1);
        fragmentTransaction.addToBackStack(context.getClass().getSimpleName());
        fragmentTransaction.hide(subscriptionFragment);
        fragmentTransaction.commit();
    }
}
