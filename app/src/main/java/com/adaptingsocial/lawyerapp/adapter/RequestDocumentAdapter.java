package com.adaptingsocial.lawyerapp.adapter;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.adaptingsocial.lawyerapp.BuildConfig;
import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.fragment.CaseDetailFragment;
import com.adaptingsocial.lawyerapp.fragment.DocumentQuestionsFragment;
import com.adaptingsocial.lawyerapp.fragment.PaypalPaymentFragment;
import com.adaptingsocial.lawyerapp.fragment.RequestDocumentFragment;
import com.adaptingsocial.lawyerapp.fragment.StripeFragment;
import com.adaptingsocial.lawyerapp.model.DocumentListModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.itextpdf.text.Document;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by indianic on 20/05/17.
 */

public class RequestDocumentAdapter extends BaseAdapter {
    public static final String storagePath =
            "/sdcard/LegalStart/";
    private ArrayList<DocumentListModel> requestModelArrayList;
    private Context context;
    private long enqueue;
    private DownloadManager dm;
    private Dialog dialog;
    private Boolean view = false;
    private RequestDocumentFragment requestDocumentFragment;


    public RequestDocumentAdapter(final Context context, ArrayList<DocumentListModel>
            requestModelArrayList, RequestDocumentFragment requestDocumentFragment) {
        this.requestModelArrayList = requestModelArrayList;
        this.context = context;
        this.requestDocumentFragment = requestDocumentFragment;

    }

    @Override
    public int getCount() {
        return requestModelArrayList.size();
    }

    @Override
    public DocumentListModel getItem(int position) {
        return requestModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.row_request_documents, null);
            holder = new Holder();
            holder.tvDocName = (TextView) convertView.findViewById(R.id.row_request_document_tv_document_name);
            holder.tvPrise = (TextView) convertView.findViewById(R.id.row_request_document_tv_prise);
            holder.tvFree = (TextView) convertView.findViewById(R.id.row_request_document_tv_free);
            holder.tvRequest = (TextView) convertView.findViewById(R.id.row_request_document_tv_request);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        PreferenceUtils preferenceUtils = new PreferenceUtils(context);
        final Boolean freeUser = preferenceUtils.getBoolean(preferenceUtils.KEY_APP_USER_TYPE);
        File file = new File(storagePath + requestModelArrayList.get(position).getDocumentName() + "." + getMimeType(String.valueOf
                (requestModelArrayList.get(position).getDocumentUrl())));

//        if (file != null)
//            if (file.exists()) {
//                holder.tvRequest.setText("Start");
//                requestModelArrayList.get(position).setView(true);
//                requestModelArrayList.get(position).setLocalFilePath(storagePath + requestModelArrayList.get(position).getDocumentName() + "." + getMimeType(String.valueOf
//                        (requestModelArrayList.get(position).getDocumentUrl())));
//
//
//            } else {
//                holder.tvRequest.setText("Download");
//                requestModelArrayList.get(position).setView(false);
//            }
        holder.tvRequest.setText("Free");
        holder.tvDocName.setText(requestModelArrayList.get(position).getDocumentName());
        holder.tvPrise.setText("$ " + requestModelArrayList.get(position).getDocumentAmt());
        if (requestModelArrayList.get(position).getDocumentType().equalsIgnoreCase("free")) {
            holder.tvFree.setVisibility(View.VISIBLE);



            if (requestModelArrayList.get(position).getUsers_document_status().equalsIgnoreCase("0")) {
                holder.tvPrise.setVisibility(View.INVISIBLE);
                holder.tvFree.setText(R.string.free);
                holder.tvFree.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_request_document_free));
            } else {
                holder.tvFree.setText(R.string.paid);
                holder.tvPrise.setVisibility(View.VISIBLE);
                holder.tvFree.setBackground(ContextCompat.getDrawable(context, R.drawable
                        .bg_request_document_paid));
            }


            holder.tvRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (freeUser) {
                        Utils.displayDialog(context, context.getString(R.string.app_name),
                                context.getString(R.string.app_paid_msg));
                    } else {
                        openDocumentQuestionFragment(requestModelArrayList.get(position));
//                        if (requestModelArrayList.get(position).getView()) {
//                            File myFile = new File(requestModelArrayList.get(position).getLocalFilePath());
//                            try {
//                                openFile(context, myFile);
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                        } else {
//                            if (Utils.isNetworkAvailable(context)) {
//                                DownloadFileFromURL downloadFileFromURL = new DownloadFileFromURL
//                                        (position);
//                                downloadFileFromURL.execute
//                                        (requestModelArrayList.get(position)
//                                                .getDocumentUrl(), requestModelArrayList.get(position)
//                                                .getDocumentName());
//
//                            } else {
//                                Utils.displayDialog(context, context.getString(R.string.app_name), context
//                                        .getString
//                                                (R.string
//                                                        .check_internet_msg));
//                            }
//                        }
                    }

                }
            });

        } else {
            if (requestModelArrayList.get(position).getUserPaymentStaus().equalsIgnoreCase("Not " +
                    "Paid")) {
                holder.tvFree.setVisibility(View.VISIBLE);
                holder.tvFree.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_request_document_paid));
                holder.tvRequest.setText(R.string.paid);
                holder.tvFree.setText(R.string.paid_new);
                holder.tvRequest.setOnClickListener(

                        new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (freeUser) {
                            Utils.displayDialog(context, context.getString(R.string.app_name), context.getString(R.string.app_paid_msg));
                            //Utils.displayDialog(context, context.getString(R.string.app_name), context.getString(R.string.app_paid_msg));
                        } else {
                            openDocumentQuestionFragment(requestModelArrayList.get(position));
                            //openDialog(requestModelArrayList.get(position));
                        }





                    }
                });


            } else {


                // change after done
                holder.tvFree.setVisibility(View.VISIBLE);
                if (requestModelArrayList.get(position).getUsers_document_status().equalsIgnoreCase("0")) {
                    holder.tvPrise.setVisibility(View.VISIBLE);
                    holder.tvFree.setText(R.string.free);
                    holder.tvFree.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_request_document_free));
                } else {
                    holder.tvFree.setText(R.string.paid);
                    holder.tvPrise.setVisibility(View.VISIBLE);
                    holder.tvFree.setBackground(ContextCompat.getDrawable(context, R.drawable
                            .bg_request_document_paid));

                    holder.tvPrise.setVisibility(View.INVISIBLE);
                }

                holder.tvRequest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (freeUser) {
                            Utils.displayDialog(context, context.getString(R.string.app_name), context.getString(R.string.app_paid_msg));
                        } else {
                            openDocumentQuestionFragment(requestModelArrayList.get(position));
//                            if (requestModelArrayList.get(position).getView()) {
//                                File myFile = new File(requestModelArrayList.get(position).getLocalFilePath());
//                                try {
//                                    openFile(context, myFile);
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                            } else {
//                                if (Utils.isNetworkAvailable(context)) {
//
//                                    DownloadFileFromURL downloadFileFromURL = new DownloadFileFromURL
//                                            (position);
//                                    downloadFileFromURL.execute(requestModelArrayList.get(position)
//                                            .getDocumentUrl(), requestModelArrayList.get(position).getDocumentName());
//
//
//                                } else {
//                                    Utils.displayDialog(context, context.getString(R.string.app_name), context
//                                            .getString
//                                                    (R.string
//                                                            .check_internet_msg));
//                                }
//                            }
                        }

                    }


                });

            }
        }


        return convertView;
    }

    /**
     * Open {@link com.adaptingsocial.lawyerapp.fragment.DocumentQuestionsFragment} for paid document
     *
     * @param documentModle id of selected document
     */
    private void openDocumentQuestionFragment(final DocumentListModel documentModle) {
        final DocumentQuestionsFragment documentQuestionsFragment = new DocumentQuestionsFragment();

        final Bundle bundle = new Bundle();
        bundle.putParcelable("document_model", documentModle);
        documentQuestionsFragment.setArguments(bundle);

        final FragmentManager fragmentManager = requestDocumentFragment.getFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, documentQuestionsFragment,
                DocumentQuestionsFragment.class
                        .getSimpleName());
        documentQuestionsFragment.setTargetFragment(requestDocumentFragment, 1);
        fragmentTransaction.addToBackStack(DocumentQuestionsFragment.class.getSimpleName());
        fragmentTransaction.hide(requestDocumentFragment);
        fragmentTransaction.commit();
    }

    private void openDialog(final DocumentListModel documentModel) {
        dialog = new Dialog(context, R.style.picture_dialog_style);
        dialog.setContentView(R.layout.dialog_payment);
        final WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(wlmp);
        dialog.show();
        final TextView tvPaypal = (TextView) dialog.findViewById(R.id
                .dialog_payment_paypal);
        final TextView tvStripe = (TextView) dialog.findViewById(R.id.dialog_payment_stripe);
        final TextView tvCancel = (TextView) dialog.findViewById(R.id.dialog_payment_cancel);
        tvPaypal.setVisibility(View.GONE);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvPaypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPaypalScreen(documentModel);
                dialog.dismiss();

            }
        });
        tvStripe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openStripeScreen(documentModel);
                dialog.dismiss();
            }
        });

    }

    private void openStripeScreen(DocumentListModel documentModel) {
        final StripeFragment stripePaymentFragment = new StripeFragment();
        final FragmentManager fragmentManager = requestDocumentFragment.getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_Document_MODEL, documentModel);
        stripePaymentFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, stripePaymentFragment,
                CaseDetailFragment.class
                        .getSimpleName());
        stripePaymentFragment.setTargetFragment(requestDocumentFragment, 1);
        fragmentTransaction.addToBackStack(context.getClass().getSimpleName());
        fragmentTransaction.hide(requestDocumentFragment);
        fragmentTransaction.commit();
    }

    private void openPaypalScreen(DocumentListModel documentModel) {
        final PaypalPaymentFragment paypalPaymentFragment = new PaypalPaymentFragment();
        final FragmentManager fragmentManager = requestDocumentFragment.getFragmentManager();
        final Bundle mBundle = new Bundle();
        mBundle.putParcelable(Constants.INTENT_KEY_Document_MODEL, documentModel);
        paypalPaymentFragment.setArguments(mBundle);
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_home_container_main, paypalPaymentFragment,
                CaseDetailFragment.class
                        .getSimpleName());
        paypalPaymentFragment.setTargetFragment(requestDocumentFragment, 1);
        fragmentTransaction.addToBackStack(context.getClass().getSimpleName());
        fragmentTransaction.hide(requestDocumentFragment);
        fragmentTransaction.commit();
    }


    public void showDownload(final Uri path) {
        Log.i("Fragment2", String.valueOf(path));
        Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
        pdfOpenintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pdfOpenintent.setDataAndType(path, "*/*");
        try {
            context.startActivity(pdfOpenintent);
        } catch (ActivityNotFoundException e) {

        }


    }

    private class Holder {
        private TextView tvDocName;
        private TextView tvRequest;
        private TextView tvPrise;
        private TextView tvFree;
    }

    //download file
    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        private ProgressDialog progressDialog;
        private int position;

        public DownloadFileFromURL(int position) {
            this.position = position;

        }

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getString(R.string.down_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();
                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                Document document = new Document();
                File docDirectory = new File(storagePath);
                // have the object build the directory structure, if needed.
                if (!docDirectory.exists()) {
                    docDirectory.mkdirs();
                }
                File outputFile = new File(storagePath, f_url[1] + "." + getMimeType(String.valueOf
                        (url)));
                // Output stream
                requestModelArrayList.get(position).setLocalFilePath(storagePath + f_url[1] + "." + getMimeType(String.valueOf
                        (url)));
                OutputStream output = new FileOutputStream(outputFile);
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                AppLog.showLogE("Error: ", e.getMessage());
            }

            return null;
        }


        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            progressDialog.dismiss();
            Toast.makeText(context, R.string.file_downloaded, Toast.LENGTH_SHORT).show();
            notifyDataSetChanged();

        }

    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return extension;
    }

    public void openFile(Context context, File url) throws IOException {
        // Create URI
        Uri uri;
        File file = url;

//        Uri uri = Uri.fromFile(file);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;

        if (currentapiVersion >= 24) {
            // Do something for 14 and above versions
            uri = FileProvider.getUriForFile(context,
                    BuildConfig.APPLICATION_ID + ".provider",
                    file);


        } else {
            uri = Uri.fromFile(file);
            // do something for phones running an SDK before 14

        }

        try {


            Intent intent = new Intent(Intent.ACTION_VIEW);
            // Check what kind of file you are trying to open, by comparing the url with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) type,
            // so Android knew what application to use to open the file
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
                // WAV audio file
                intent.setDataAndType(uri, "application/x-wav");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                //if you want you can also define the intent type for any other file

                //additionally use else clause below, to manage other unknown extensions
                //in this case, Android will show all applications installed on the device
                //so you can choose which application to use
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            Utils.displayDialog(context, context.getString(R.string.app_name), "Supported app not" +
                    " found.");
        }
    }
}
