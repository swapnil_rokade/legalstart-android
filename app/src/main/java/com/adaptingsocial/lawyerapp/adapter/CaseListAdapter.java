package com.adaptingsocial.lawyerapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.CaseListModel;

import java.util.ArrayList;

/**
 * Created by indianic on 25/04/17.
 */

public class CaseListAdapter extends BaseAdapter {
    private ArrayList<CaseListModel> caseListModelArrayList;
    private LayoutInflater inflater;

    public CaseListAdapter(final Context context, final ArrayList<CaseListModel> caseListModelArrayList) {
        inflater = LayoutInflater.from(context);
        this.caseListModelArrayList = caseListModelArrayList;
    }

    @Override
    public int getCount() {
        return caseListModelArrayList.size();

    }

    @Override
    public CaseListModel getItem(int position) {
        return caseListModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_case_list, null);
            holder = new Holder();
            holder.tvCaseTitle = (TextView) convertView.findViewById(R.id
                    .row_case_list_tv_case_title);
            holder.tvCourtName = (TextView) convertView.findViewById(R.id
                    .row_case_list_tv_court_name);

            holder.tvUserName = (TextView) convertView.findViewById(R.id
                    .row_case_list_tv_client_name);

            holder.tvCaseNo = (TextView) convertView.findViewById(R.id
                    .row_case_list_tv_case_no);

            convertView.setTag(holder);

        } else {
            holder = (CaseListAdapter.Holder) convertView.getTag();
        }
        holder.tvCaseTitle.setText("" + caseListModelArrayList.get(position).getCaseTitle());
        holder.tvCourtName.setText("" + caseListModelArrayList.get(position).getCourtName());
        holder.tvUserName.setText("By"+ " " + caseListModelArrayList.get(position).getUserName());
        holder.tvCaseNo.setText("" + caseListModelArrayList.get(position).getCaseNo());
        return convertView;
    }

    class Holder {
        private TextView tvCaseTitle;
        private TextView tvCourtName;
        private TextView tvUserName;
        private TextView tvCaseNo;

    }
}
