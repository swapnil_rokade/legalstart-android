package com.adaptingsocial.lawyerapp.adapter;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.BlogModel;
import com.adaptingsocial.lawyerapp.utill.OnLoadMoreListener;
import com.adaptingsocial.lawyerapp.utill.PreferenceUtils;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.adaptingsocial.lawyerapp.webservice.WSConstants;
import com.adaptingsocial.lawyerapp.webservice.WsAddComment;

import java.util.ArrayList;

/**
 * Created  on 05/08/16.
 */
public class RecyclerPagerAdapter extends RecyclerView.Adapter {
    private ArrayList<BlogModel> modelObjectArrayList;
    private Context context;
    //    private ArrayList<CommentModel> arrayList = new ArrayList<>();
    private OnLoadMoreListener onLoadMoreListener;
    private RecyclerView recyclerView;
    private boolean isLoading;
    private int visibleThreshold = 5;
    private WsAddComment wsAddComment;
    private AddComentAsyncTask addComentAsyncTask;
    private CommentAdapter commentAdapter;
    boolean flag;


    public RecyclerPagerAdapter(Context Context, ArrayList<BlogModel>
            modelObjectArrayList, RecyclerView recyclerView) {
        this.context = Context;
        this.modelObjectArrayList = modelObjectArrayList;
        this.recyclerView = recyclerView;

//        arrayList = new ArrayList<>();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        final RecyclerView.ViewHolder vh;
        final View v;
        v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_blogs, parent, false);
        vh = new ActionStatusViewHolder(v);
        return vh;
    }


    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder.getPosition() != 0)
            ((ActionStatusViewHolder) holder).ivFinger.setVisibility(View.GONE);
//        if(holder.getPosition()<2){
//            notifyDataSetChanged();
//        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final BlogModel notificationModel = modelObjectArrayList.get(position);

//        if (modelObjectArrayList.get(position).getCommentModelArrayList() != null) {
//            arrayList.addAll(modelObjectArrayList.get(position).getCommentModelArrayList());
//        }

        final PreferenceUtils preferenceUtils = new PreferenceUtils(context);


//            rlMain.setVisibility(View.VISIBLE);
        if (position == 0) {
            if (preferenceUtils.getLoadStoryFirstTime()) {
                ((ActionStatusViewHolder) holder).ivFinger.setVisibility(View.VISIBLE);
                animImage(context, ((ActionStatusViewHolder) holder).ivFinger);
            } else {
                ((ActionStatusViewHolder) holder).ivFinger.setVisibility(View.GONE);
                preferenceUtils.setLoadStoryFirstTime(false);
            }
        } else {
            ((ActionStatusViewHolder) holder).ivFinger.setVisibility(View.GONE);
            preferenceUtils.setLoadStoryFirstTime(false);

        }

        ((ActionStatusViewHolder) holder).tvBlogTitle.setText("" + modelObjectArrayList.get(position).getBlogName());
        ((ActionStatusViewHolder) holder).tvBlogDate.setText("" + modelObjectArrayList.get(position).getBolgDate());
        ((ActionStatusViewHolder) holder).tvBlogDescription.setText("" + modelObjectArrayList.get(position).getBlogDescription());
        Utils.setImageView(context, modelObjectArrayList.get(position).getBlogImage(),
                ((ActionStatusViewHolder) holder).ivBg);

//        if (modelObjectArrayList.get(position).getCommentModelArrayList() != null &&
//                modelObjectArrayList.get(position).getCommentModelArrayList().size() > 0)
        commentAdapter = new CommentAdapter(context, modelObjectArrayList.get(position).getCommentModelArrayList(), "");

        ((ActionStatusViewHolder) holder).lvComment.setAdapter(commentAdapter);
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final int totalItemCount = linearLayoutManager.getItemCount();
                final int lastVisiblePosition = linearLayoutManager.findLastVisibleItemPosition();

                if (totalItemCount <= (lastVisiblePosition + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });


        ((ActionStatusViewHolder) holder).tvAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(modelObjectArrayList.get(position).getBlogId(), position);


            }
        });

    }

    public void openDialog(final String id, final int position) {
        final Dialog dialog = new Dialog(context, R.style.picture_dialog_style);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_post_comment);
        final WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.BOTTOM;
        wlmp.width = WindowManager.LayoutParams.FILL_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(wlmp);
        dialog.show();
        final EditText etComment = (EditText) dialog.findViewById(R.id.dialog_post_comment_et_add_comment);
        TextView tvPostComment = (TextView) dialog.findViewById(R.id
                .dialog_post_comment_tv_post_comment);
        tvPostComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etComment.getText().toString().trim().equalsIgnoreCase("")) {
                    Utils.displayDialog(context, context.getString(R.string.app_name), context.getString(R.string.add_comment_msg));
                } else {
                    callPostCommentApi(id, etComment.getText().toString().trim(), position);
                    dialog.dismiss();
                }


            }
        });

    }

    @Override
    public int getItemViewType(int position) {
        final PreferenceUtils preferenceUtils = new PreferenceUtils(context);

        if (position == 0 && preferenceUtils.getLoadStoryFirstTime()) {
            return 0;
        } else {
            return 1;
        }

    }

    @Override
    public int getItemCount() {
        return modelObjectArrayList.size();
    }

    private class ActionStatusViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivBg;
        private TextView tvBlogTitle;
        private TextView tvBlogDate;
        private TextView tvBlogDescription;
        private ListView lvComment;
        private TextView tvAddComment;
        private ImageView ivFinger;


        private ActionStatusViewHolder(View itemView) {
            super(itemView);
            ivBg = (ImageView) itemView.findViewById(R.id.row_blog_iv_background);
            tvBlogTitle = (TextView) itemView.findViewById(R.id.row_blog_tv_title);
            tvBlogDate = (TextView) itemView.findViewById(R.id.row_blog_tv_date);
            tvBlogDescription = (TextView) itemView.findViewById(R.id.row_blog_tv_description);
            lvComment = (ListView) itemView.findViewById(R.id.row_blog_lv_comments);
            tvAddComment = (TextView) itemView.findViewById(R.id.row_blog_tv_add_comment);
            ivFinger = (ImageView) itemView.findViewById(R.id.row_blog_iv_finger);


        }
    }

    public void setLoaded() {
        isLoading = false;
    }

    private void callPostCommentApi(String id, String comment, int position) {
        if (Utils.isNetworkAvailable(context)) {
            if (addComentAsyncTask != null && addComentAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
                addComentAsyncTask.execute();
            } else if (addComentAsyncTask == null || addComentAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
                addComentAsyncTask = new AddComentAsyncTask(id, comment, position);
                addComentAsyncTask.execute();
            }
        } else {
            Utils.displayDialog(context, context.getString(R.string.app_name), context.
                    getString(R.string
                            .check_internet_msg));
        }

    }


    public class AddComentAsyncTask extends AsyncTask<String, Void, Void> {

        private ProgressDialog progressDialog;
        private String id;
        private String comment;
        private int position;
        private CommentAdapter commentAdapter;

        public AddComentAsyncTask(String id, String comment, int position) {
            this.id = id;
            this.comment = comment;
            this.position = position;
            this.commentAdapter = commentAdapter;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getString(R.string.msg_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(String... params) {
            wsAddComment = new WsAddComment(context);
            wsAddComment.executeService(id, comment);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (!isCancelled()) {
                if (wsAddComment.getCode() == WSConstants.STATUS_SUCCESS) {
                    displayDialog(context, context.getString(R.string.app_name), wsAddComment
                            .getMessage(), position);


                } else if (wsAddComment.getCode() == WSConstants.STATUS_FAIL) {
                    Utils.displayDialog(context, context.getString(R.string.app_name), wsAddComment
                            .getMessage());

                } else if (wsAddComment.getCode() == WSConstants.STATUS_NO_DATA_FAIL) {
                    Utils.displayDialog(context, context.getString(R.string.app_name), wsAddComment
                            .getMessage());


                } else if (wsAddComment.getCode() == WSConstants.STATUS_LOGOUT) {
                    Utils.displayLogoutDialog(context, context.getString(R.string.app_name),
                            wsAddComment
                                    .getMessage());

                } else {
                    Utils.displayDialogWithPopBackStack(context, context.getString(R.string
                                    .app_name),
                            context.getString(R.string.something_went_wrong_msg));

                }


            }

        }

    }

    public void setBlogList(ArrayList<BlogModel> arrayList) {
        this.modelObjectArrayList = arrayList;
    }

    public void displayDialog(final Context context, final String title, final String message,
                              final int position) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                modelObjectArrayList.get(position).setCommentModelArrayList(wsAddComment.getCommentListModelArrayList());
                setBlogList(modelObjectArrayList);
                notifyDataSetChanged();


            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        final Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        posButton.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
    }

    private void animImage(final Context context, final ImageView ivFinger) {
        // Load the animation like this
        final Animation animLeftToRight = AnimationUtils.loadAnimation(context, R.anim.anim_move_right_to_left);
        ivFinger.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        // Start the animation like this
        ivFinger.startAnimation(animLeftToRight);
    }
}