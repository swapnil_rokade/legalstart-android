package com.adaptingsocial.lawyerapp.adapter;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import android.widget.Toast;

import com.adaptingsocial.lawyerapp.BuildConfig;
import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.MyDocumentModel;
import com.adaptingsocial.lawyerapp.utill.AppLog;
import com.adaptingsocial.lawyerapp.utill.Utils;
import com.itextpdf.text.Document;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * MyDocumentAdapter class created.
 */

public class MyDocumentAdapter extends RecyclerView.Adapter<MyDocumentAdapter.ViewHolder> {
    private ArrayList<MyDocumentModel> myDocumentModelArrayList;
    private Context context;
    public static final String storagePath =
            "/sdcard/LegalStart/";

    public MyDocumentAdapter(Context context, ArrayList<MyDocumentModel> myDocumentModelArrayList) {
        this.myDocumentModelArrayList = myDocumentModelArrayList;
        this.context = context;
    }

    @Override
    public MyDocumentAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_document_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyDocumentAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.tvDocumentName.setText(myDocumentModelArrayList.get(i).getDocumentName());
        File file = new File(storagePath + myDocumentModelArrayList.get(i).getDocumentName() + "" +
                "." + getMimeType(String.valueOf
                (myDocumentModelArrayList.get(i).getDocumentFile())));
        if (file != null)
            if (file.exists()) {
                viewHolder.tvDownload.setText(R.string.start);
                myDocumentModelArrayList.get(i).setView(true);
                myDocumentModelArrayList.get(i).setLocalFilePath(storagePath +
                        myDocumentModelArrayList.get(i).getDocumentName() + "." + getMimeType
                        (String.valueOf(myDocumentModelArrayList.get(i).getDocumentFile())));


            } else {
                viewHolder.tvDownload.setText("Download");
                myDocumentModelArrayList.get(i).setView(false);
            }
        viewHolder.tvDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myDocumentModelArrayList.get(i).getView()) {
                    File myFile = new File(myDocumentModelArrayList.get(i).getLocalFilePath());
                    try {
                        openFile(context, myFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (Utils.isNetworkAvailable(context)) {
                        DownloadFileFromURL downloadFileFromURL = new DownloadFileFromURL(i);
                        downloadFileFromURL.execute
                                (myDocumentModelArrayList.get(i)
                                        .getDocumentFile(), myDocumentModelArrayList.get(i)
                                        .getDocumentName());

                    } else {
                        Utils.displayDialog(context, context.getString(R.string.app_name), context
                                .getString
                                        (R.string
                                                .check_internet_msg));
                    }
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return myDocumentModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDownload;
        private TextView tvDocumentName;

        public ViewHolder(View view) {
            super(view);
            tvDownload = (TextView) view.findViewById(R.id.row_document_list_tv_download);
            tvDocumentName = (TextView) view.findViewById(R.id.row_document_list_tv_document_title);
        }
    }

    //download file
    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        private ProgressDialog progressDialog;
        private int position;

        public DownloadFileFromURL(int position) {
            this.position = position;

        }

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getString(R.string.down_loading));
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();
                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                Document document = new Document();
                File docDirectory = new File(storagePath);
// have the object build the directory structure, if needed.
                if (!docDirectory.exists()) {
                    docDirectory.mkdirs();
                }
                File outputFile = new File(storagePath, f_url[1] + "." + getMimeType(String.valueOf
                        (url)));
                // Output stream
                myDocumentModelArrayList.get(position).setLocalFilePath(storagePath + f_url[1] + "." + getMimeType(String.valueOf
                        (url)));
                OutputStream output = new FileOutputStream(outputFile);
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                AppLog.showLogE("Error: ", e.getMessage());
            }

            return null;
        }


        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            progressDialog.dismiss();
            Toast.makeText(context, R.string.file_downloaded, Toast.LENGTH_SHORT).show();
            notifyDataSetChanged();

        }

    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return extension;
    }

    public void openFile(Context context, File url) throws IOException {
        // Create URI
        Uri uri;
        File file = url;

//        Uri uri = Uri.fromFile(file);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;

        if (currentapiVersion >= 24) {
            // Do something for 14 and above versions
            uri = FileProvider.getUriForFile(context,
                    BuildConfig.APPLICATION_ID + ".provider",
                    file);


        } else {
            uri = Uri.fromFile(file);
            // do something for phones running an SDK before 14

        }

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            // Check what kind of file you are trying to open, by comparing the url with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) type,
            // so Android knew what application to use to open the file
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
                // WAV audio file
                intent.setDataAndType(uri, "application/x-wav");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                //if you want you can also define the intent type for any other file

                //additionally use else clause below, to manage other unknown extensions
                //in this case, Android will show all applications installed on the device
                //so you can choose which application to use
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            Utils.displayDialog(context, context.getString(R.string.app_name), "Supported app not" +
                    " found.");
        }
    }
}