package com.adaptingsocial.lawyerapp.adapter;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.fragment.CompanyListFragment;
import com.adaptingsocial.lawyerapp.model.CompanyListModel;
import com.adaptingsocial.lawyerapp.utill.Utils;

import java.util.ArrayList;


public class CompanyListAdapter extends BaseAdapter {
    private final Context context;
    private final Fragment companyListFragment;
    private ArrayList<CompanyListModel> companyListModelArrayList;
    private LayoutInflater inflater;

    public CompanyListAdapter(final Context context, final ArrayList<CompanyListModel> caseListModelArrayList, final CompanyListFragment companyListFragment) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.companyListFragment = companyListFragment;
        this.companyListModelArrayList = caseListModelArrayList;
    }

    @Override
    public int getCount() {
        return companyListModelArrayList.size();

    }

    @Override
    public CompanyListModel getItem(int position) {
        return companyListModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_company_list, null);
            holder = new Holder();
            holder.ivMap = (ImageView) convertView.findViewById(R.id.row_company_list_iv_map);
            holder.ivCompanyPic = (ImageView) convertView.findViewById(R.id.row_company_list_iv_profile);
            holder.tvCompanyTitle = (TextView) convertView.findViewById(R.id.row_company_list_tv_title);
            holder.tvCompanyAddress = (TextView) convertView.findViewById(R.id.row_company_list_tv_address);
            holder.tvCompanyContactNo = (TextView) convertView.findViewById(R.id.row_company_list_tv_contact_no);
            holder.tvCompanyEmail = (TextView) convertView.findViewById(R.id.row_company_list_tv_email);
            holder.tvField= (TextView) convertView.findViewById(R.id.row_company_list_tv_field);
            convertView.setTag(holder);

        } else {
            holder = (CompanyListAdapter.Holder) convertView.getTag();
        }

        holder.tvCompanyTitle.setText("" + companyListModelArrayList.get(position).getCompanyName());
        holder.tvCompanyAddress.setText("" + companyListModelArrayList.get(position).getCompanyAddress());
        holder.tvCompanyContactNo.setText(" " + companyListModelArrayList.get(position).getCompanyContact());
        holder.tvCompanyEmail.setText("" + companyListModelArrayList.get(position).getCompanyEmail());
        holder.tvField.setText("" + companyListModelArrayList.get(position).getCompanyField());

        Utils.setImageView(context, companyListModelArrayList.get(position).getCompanyImage(), holder.ivCompanyPic);
        holder.tvCompanyEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.openMailClent(companyListModelArrayList.get(position).getCompanyEmail(), context);
            }
        });
//        holder.ivMap.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final Bundle bundle = new Bundle();
//                bundle.putParcelable(context.getString(R.string.str_company_detail), companyListModelArrayList.get(position));
//                ((CompanyListFragment) companyListFragment).openDrawMapFragment(bundle);
//            }
//        });
        holder.tvCompanyContactNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CompanyListFragment) companyListFragment).openDialUp(companyListModelArrayList.get(position).getCompanyContact());
            }
        });
        return convertView;
    }

    private class Holder {
        private TextView tvCompanyTitle;
        private TextView tvCompanyAddress;
        private TextView tvCompanyContactNo;
        private TextView tvCompanyEmail;
        private ImageView ivCompanyPic;
        private ImageView ivMap;
        private  TextView tvField;
    }
}
