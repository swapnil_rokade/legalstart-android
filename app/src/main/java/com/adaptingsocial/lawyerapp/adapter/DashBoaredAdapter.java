package com.adaptingsocial.lawyerapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;

/**
 * Created by indianic on 30/05/17.
 */

public class DashBoaredAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater inflater;



    public DashBoaredAdapter(Context c) {
        inflater = LayoutInflater.from(c);
        mContext = c;
    }

    public int getCount() {
        return 10;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_dashboared, null);
            holder = new Holder();
            holder.tvName = (TextView) convertView.findViewById(R.id.row_dashboared_tv_name);
            holder.ivModule = (ImageView) convertView.findViewById(R.id.row_dashboared_iv_module);

            convertView.setTag(holder);

        } else {
            holder = (Holder) convertView.getTag();
        }
        if (position == 0) {
            holder.tvName.setText(R.string.appointments);
            holder.ivModule.setImageResource(R.drawable.ic_dash_appointment);
        } else if (position == 1) {
            holder.tvName.setText(R.string.upload_document);
            holder.ivModule.setImageResource(R.drawable.ic_dash_cases);
        } else if (position == 2) {
            holder.tvName.setText(R.string.str_start_my_business);
            holder.ivModule.setImageResource(R.drawable.ic_questionaries);
        } else if (position == 3) {
            holder.tvName.setText(R.string.startup_network);
            holder.ivModule.setImageResource(R.drawable.ic_dash_company_info);
        } else if (position == 4) {
            holder.tvName.setText(R.string.request_document);
            holder.ivModule.setImageResource(R.drawable.ic_req_doc);
        } else if (position == 5) {
            holder.tvName.setText(R.string.chat);
            holder.ivModule.setImageResource(R.drawable.ic_dash_chat);
        } else if (position == 6) {
            holder.tvName.setText(R.string.blog);
            holder.ivModule.setImageResource(R.drawable.ic_dash_blog);
        } else if (position == 7) {
            holder.tvName.setText(R.string.email_to_lawyer);
            holder.ivModule.setImageResource(R.drawable.ic_dash_mail);
        } else if (position == 8) {
            holder.tvName.setText(R.string.subscription);
            holder.ivModule.setImageResource(R.drawable.ic_subcribe);
        }else if (position == 9) {
            holder.tvName.setText(R.string.str_my_document);
            holder.ivModule.setImageResource(R.drawable.ic_my_docs_big);
        }
//        else if (position == 10) {
//            holder.tvName.setText(R.string.service_required);
//            holder.ivModule.setImageResource(R.drawable.ic_my_docs_big);
//        }


        return convertView;


    }

    class Holder {
        private TextView tvName;
        private ImageView ivModule;


    }

}
