package com.adaptingsocial.lawyerapp.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.adaptingsocial.lawyerapp.R;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by indianic on 31/05/17.
 */

public class CameraAdapter extends BaseAdapter {
    private ArrayList<File> imagepath;
    private LayoutInflater inflater;
    private ArrayList<String> imageString;
    public CameraAdapter(final Context context, ArrayList<File> imagepath, ArrayList<String> imageString) {
        inflater = LayoutInflater.from(context);
        this.imagepath = imagepath;
        this.imageString=imageString;
    }

    @Override
    public int getCount() {
        return imagepath.size();


    }

    @Override
    public File getItem(int position) {
        return imagepath.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_images, null);
            holder = new Holder();
            holder.image = (ImageView) convertView.findViewById(R.id
                    .img_gallery);
            holder.ivDelete=(ImageView) convertView.findViewById(R.id
                    .ivdelete);


            convertView.setTag(holder);

        } else {
            holder = (Holder) convertView.getTag();
        }
        if(imagepath.get(position).exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imagepath.get(position).getAbsolutePath());
            holder.image.setImageBitmap(myBitmap);

        }
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagepath.remove(position);
                imageString.remove(position);
                notifyDataSetChanged();

            }
        });

        return convertView;
    }

    class Holder {
        private ImageView image;
        private ImageView ivDelete;


    }
}



