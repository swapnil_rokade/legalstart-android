package com.adaptingsocial.lawyerapp.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.DocumentModel;

import java.util.ArrayList;


public class UpdateCaseAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<DocumentModel> documentModelArrayList;
    private LayoutInflater inflater;
    private ArrayList<String> deletedDocumentIdArray;

    public UpdateCaseAdapter(final Context context, final ArrayList<DocumentModel>
            documentModelArrayList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.documentModelArrayList = documentModelArrayList;
        deletedDocumentIdArray = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return documentModelArrayList.size();

    }

    @Override
    public DocumentModel getItem(int position) {
        return documentModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_document_uploaded, null);
            holder = new Holder();
            holder.tvDocumentName = (TextView) convertView.findViewById(R.id
                    .row_document_uploaded_tv_doc_name);
            holder.tvUploadedBy = (TextView) convertView.findViewById(R.id
                    .row_document_uploaded_tv_doc_uploaded_by_user_name);
            holder.tvDate = (TextView) convertView.findViewById(R.id
                    .row_document_uploaded_tv_date);
            holder.ivDocImage = (ImageView) convertView.findViewById(R.id
                    .row_document_uploaded_iv_pdf);
            holder.ivClose = (ImageView) convertView.findViewById(R.id
                    .row_document_uploaded_iv_downloaded);
            holder.ivShare = (ImageView) convertView.findViewById(R.id
                    .row_document_uploaded_iv_share);
            convertView.setTag(holder);

        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.tvDocumentName.setText("" + documentModelArrayList.get(position).getDocumentName());
        holder.tvDate.setText("" + documentModelArrayList.get(position).getDate());
        holder.ivClose.setImageResource(R.drawable.ic_delete);
        holder.ivShare.setVisibility(View.GONE);
        if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase(".png")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_png);
        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase(".jpeg")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_jpg);

        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase(".xls")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_xls);

        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase(".doc")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_doc);

        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase(".txt")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_txt);

        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase(".pdf")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_pdf);

        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase(".docx")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_doc);

        } else if (documentModelArrayList.get(position).getImageExt().equalsIgnoreCase(".jpg")) {
            holder.ivDocImage.setImageResource(R.drawable.ic_jpg);

        } else {
            holder.ivDocImage.setImageResource(R.drawable.ic_pdf);
        }
        if (documentModelArrayList.get(position).getUserid() != null) {
            if (documentModelArrayList.get(position).getUserid().equalsIgnoreCase("0")) {
                holder.tvUploadedBy.setText(R.string.uploaded_by_admin);
                holder.ivClose.setVisibility(View.INVISIBLE);
            } else {
                holder.tvUploadedBy.setText(R.string.uploaded_by_me);
                holder.ivClose.setVisibility(View.VISIBLE);
            }
        }else {
            holder.tvUploadedBy.setText(R.string.uploaded_by_me);
            holder.ivClose.setVisibility(View.VISIBLE);
        }
        holder.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(documentModelArrayList.get(position).getDocumentId())) {
                    deletedDocumentIdArray.add(documentModelArrayList.get(position).getDocumentId());
                }
                documentModelArrayList.remove(position);
                notifyDataSetChanged();
            }
        });
        return convertView;
    }

    public ArrayList<String> getDeletedDocumentIdArray() {
        return deletedDocumentIdArray;
    }

    class Holder {
        private TextView tvDocumentName;
        private TextView tvDate;
        private TextView tvUploadedBy;
        private ImageView ivClose;
        private ImageView ivShare;
        private ImageView ivDocImage;


    }
}
