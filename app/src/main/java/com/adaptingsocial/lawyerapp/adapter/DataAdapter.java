package com.adaptingsocial.lawyerapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.model.TimeSlotModel;

import java.util.ArrayList;

/**
 * DataAdapter class created.
 */
public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private final Context context;
    private ArrayList<TimeSlotModel> timeSlotArray;
    private boolean cbSelected = false;
    private int pos = -1;
    private int prevPos = -1;

    public DataAdapter(Context context, ArrayList<TimeSlotModel> timeSlotArray) {
        this.timeSlotArray = timeSlotArray;
        this.context = context;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_time_slot, viewGroup, false);
        return new DataAdapter.ViewHolder(view);
    }

    public void setCheckBoxSelection() {
        cbSelected = false;
    }

    @Override
    public void onBindViewHolder(final DataAdapter.ViewHolder holder, final int position) {
        holder.cbTimeSlot.setText(timeSlotArray.get(position).getStartTime() + " - " + timeSlotArray.get(position).getEndTime());
        if (timeSlotArray.get(position).isBooked()) {
            holder.cbTimeSlot.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_time_cheep_green));
            holder.cbTimeSlot.setTextColor(ContextCompat.getColor(context, R.color.color_sea_green));
            holder.ivSelected.setVisibility(View.INVISIBLE);
        } else if (timeSlotArray.get(position).isSelected()) {
            holder.cbTimeSlot.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_time_cheep_gray));
            holder.cbTimeSlot.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
            holder.ivSelected.setVisibility(View.VISIBLE);
        } else {
            holder.cbTimeSlot.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_time_cheep_unselected));
            holder.cbTimeSlot.setTextColor(ContextCompat.getColor(context, R.color.colorLightGray));
            holder.ivSelected.setVisibility(View.INVISIBLE);
        }
        holder.cbTimeSlot.setOnCheckedChangeListener(null);
        holder.cbTimeSlot.setChecked(timeSlotArray.get(position).isSelected());
//        if (cbSelected) {
//            if (pos != position) {
//                holder.cbTimeSlot.setClickable(false);
//            } else {
//                if (!timeSlotArray.get(position).isBooked()) {
//                    holder.cbTimeSlot.setClickable(true);
//                } else {
//                    holder.cbTimeSlot.setClickable(false);
//                }
//            }
//        } else {
//            if (!timeSlotArray.get(position).isBooked()) {
//                holder.cbTimeSlot.setClickable(true);
//            } else {
//                holder.cbTimeSlot.setClickable(false);
//            }
//        }
        holder.cbTimeSlot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(pos!=-1){
                    timeSlotArray.get(pos).setSelected(false);
                }
                if (isChecked) {
                    holder.cbTimeSlot.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_time_cheep_gray));
                    holder.cbTimeSlot.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
                    holder.ivSelected.setVisibility(View.VISIBLE);
                    timeSlotArray.get(position).setSelected(true);
                    cbSelected = true;
                } else {
                    holder.cbTimeSlot.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_time_cheep_unselected));
                    holder.cbTimeSlot.setTextColor(ContextCompat.getColor(context, R.color.colorLightGray));
                    holder.ivSelected.setVisibility(View.INVISIBLE);
                    timeSlotArray.get(position).setSelected(false);
                    cbSelected = false;
                }
                pos=position;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return timeSlotArray.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CheckBox cbTimeSlot;
        private ImageView ivSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            ivSelected = (ImageView) itemView.findViewById(R.id.row_time_slot_iv_selected);
            cbTimeSlot = (CheckBox) itemView.findViewById(R.id.row_time_slot_check_box);
        }
    }
}
