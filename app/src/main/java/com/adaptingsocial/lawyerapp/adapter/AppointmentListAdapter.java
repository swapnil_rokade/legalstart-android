package com.adaptingsocial.lawyerapp.adapter;


import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.adaptingsocial.lawyerapp.fragment.PastAppointmentsFragment;
import com.adaptingsocial.lawyerapp.fragment.UpComingAppointmentsFragment;
import com.adaptingsocial.lawyerapp.model.TimeSlotModel;
import com.adaptingsocial.lawyerapp.utill.Constants;

import java.util.ArrayList;

/**
 * Created by indianic on 09/05/17.
 */

public class AppointmentListAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    private ArrayList<TimeSlotModel> pastAppointmentArray;
    private ArrayList<TimeSlotModel> upAppointmentArray;

    public AppointmentListAdapter(FragmentManager fm, ArrayList<TimeSlotModel> pastAppointmentArray, ArrayList<TimeSlotModel> upAppointmentArray, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.pastAppointmentArray = pastAppointmentArray;
        this.upAppointmentArray = upAppointmentArray;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                final Bundle upBundle = new Bundle();
                upBundle.putParcelableArrayList(Constants.UP_PARCELABLE_ARRAY, upAppointmentArray);
                UpComingAppointmentsFragment tab1 = new UpComingAppointmentsFragment();
                tab1.setArguments(upBundle);

                return tab1;
            case 1:
                final Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(Constants.PAST_PARCELABLE_ARRAY, pastAppointmentArray);
                PastAppointmentsFragment tab2 = new PastAppointmentsFragment();
                tab2.setArguments(bundle);

                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}


