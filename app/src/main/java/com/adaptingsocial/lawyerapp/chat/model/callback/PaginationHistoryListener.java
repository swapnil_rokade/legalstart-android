package com.adaptingsocial.lawyerapp.chat.model.callback;

public interface PaginationHistoryListener {
    void downloadMore();
}
