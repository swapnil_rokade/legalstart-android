package com.adaptingsocial.lawyerapp.chat.model.utill;

public class PREF {
    public static final String PREF_ISLOGIN = "PREF_ISLOGIN";
    public static final String PREF_USERNAME = "PREF_USERNAME";
    public static final String PREF_PASSWORD = "PREF_PASSWORD";
    public static final String PREF_QB_USER_ID = "qb_user_id";
    public static final String PREF_QB_USER_LOGIN = "qb_user_login";
    public static final String PREF_QB_USER_PASSWORD = "qb_user_password";
    public static final String PREF_QB_USER_OLDPASSWORD = "qb_user_oldpassword";
    public static final String PREF_QB_USER_FULL_NAME = "qb_user_full_name";
    public static final String PREF_QB_USER_TAGS = "qb_user_tags";
    public static final String PREF_ENABLE_REMEMBER_ME = "PREF_ENABLE_REMEMBER_ME";
    public static final String PREF_QBUSER_EMAIL = "PREF_QBUSER_EMAIL";
    public static final String PREF_QBUSER_CUSTOMDATA = "PREF_QBUSER_CUSTOMDATA";
    public static final String PREF_QBUSER_IMAGE = "PREF_QBUSER_IMAGE";

}
