package com.adaptingsocial.lawyerapp.dialog;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.adaptingsocial.lawyerapp.R;
import com.adaptingsocial.lawyerapp.utill.Constants;
import com.adaptingsocial.lawyerapp.utill.GetFilePath;
import com.adaptingsocial.lawyerapp.utill.MediaSelectedListener;
import com.adaptingsocial.lawyerapp.utill.Utils;

import java.io.File;

/**
 * Purpose:- This class is used to show dialog selecting media files (Image,Videos).
 */
public class SelectMediaDialog extends DialogFragment implements View.OnClickListener {


    private String TAG = this.getClass().getSimpleName();

    private View view;
    // Local variable declaration
    private long mLastClickTime = 0;

    private TextView tvImageTakeAPicture;
    private TextView tvImageGallery;

    private String cameraUri;

    private File tempFile;
    private MediaSelectedListener mediaSelectedListener;

    private String cropImagePath = "";
    private String imgPath = "";

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri)) {
                return uri.getLastPathSegment();
            }

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {

        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri
     *         The Uri to check.
     *
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {

        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri
     *         The Uri to check.
     *
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {

        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri
     *         The Uri to check.
     *
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {

        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }


    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // TODO Auto-generated method stub
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        WindowManager.LayoutParams windowParams = getDialog().getWindow().getAttributes();
        windowParams.gravity = Gravity.BOTTOM;
        windowParams.dimAmount = 0.60f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        windowParams.width = FrameLayout.LayoutParams.MATCH_PARENT;
        windowParams.height = FrameLayout.LayoutParams.WRAP_CONTENT;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        view = inflater.inflate(R.layout.dialog_upload_media, null);

        cropImagePath = "";
        imgPath = "";

        tvImageTakeAPicture = (TextView) view.findViewById(R.id.dialog_upload_media_tv_image_take_a_picture);
        tvImageGallery = (TextView) view.findViewById(R.id.dialog_upload_media_tv_image_gallery);

        tvImageTakeAPicture.setOnClickListener(this);
        tvImageGallery.setOnClickListener(this);

        return view;
    }

    public void setMediaSelectedListener(MediaSelectedListener mediaSelectedListener) {

        this.mediaSelectedListener = mediaSelectedListener;
    }

    @Override
    public void onClick(View v) {
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < Constants.MAX_CLICK_INTERVAL) {

            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        if (v == tvImageTakeAPicture) {

            openCameraForImage();

        } else if (v == tvImageGallery) {

            selectImageFromGallery();
        }
    }

    /**
     * code of take a picture from the Camera app
     */
    private void openCameraForImage() {

        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {

            final File mFile = Utils.createPhotoFile(getActivity());
            cameraUri = mFile.getAbsolutePath();

            final Uri outputFileUri = Uri.fromFile(mFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, Constants.CHOOSE_IMAGE_CAMERA);
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    /**
     * Method to Select an Image from the Phone's Gallery
     */
    private void selectImageFromGallery() {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (isKitKat) {
            final Intent openGalleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            openGalleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
            openGalleryIntent.setType("image/*");
            openGalleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(Intent.createChooser(openGalleryIntent, "Select Picture"), Constants.CHOOSE_IMAGE_GALLERY);
        } else {
            final Intent openGalleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            openGalleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(Intent.createChooser(openGalleryIntent, "Select Picture"), Constants.CHOOSE_IMAGE_GALLERY);
        }
    }

    //THIS METHOD IS TO PERFORM CROP
    private void performCrop(Uri caputredUri) {
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(caputredUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, Constants.CAMERA_CROP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == Constants.CHOOSE_IMAGE_CAMERA) {

            try {
                if (!TextUtils.isEmpty(cameraUri)) {
                    tempFile = new File(cameraUri);

                    mediaSelectedListener.onImageSelected(tempFile.getAbsolutePath());
                    dismiss();

                } else {
                    dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (resultCode == Activity.RESULT_OK && requestCode == Constants.CHOOSE_IMAGE_GALLERY) {

            try {
                final Uri uri = data.getData();
                final String imgPath = GetFilePath.getPath(getActivity(), uri);

                if (imgPath != null) {
                    if (!TextUtils.isEmpty(imgPath)) {
                        if (imgPath.startsWith("http")) {
                            Utils.displayDialog(getActivity(), getString(R.string.app_name), getActivity().getString(R.string.alert_msg_select_img_other));
                            dismiss();
                        } else {

                            this.imgPath = imgPath;
//                            performCrop(uri);
                            mediaSelectedListener.onImageSelected(imgPath);
                            dismiss();

                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.displayDialog(getActivity(), getString(R.string.app_name), getActivity().getString(R.string.alert_msg_invalid_file_selection));
            }

        }
    }

}
